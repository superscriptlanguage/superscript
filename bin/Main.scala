import org.superscript._
import org.superscript.eventloop._
import org.superscript.event._
import java.io.FileInputStream
object Main {
    def main(args: Array[String]) = {
        val loop = SuperScript.loop
        loop.addEvent(new Test)
        loop.addEvent(new Test)
        var counter = 0
        val uuid = loop.setInterval(() => {
            println("Interval")
            Thread.sleep(110)
            counter += 1
        }, 100)
        loop.addEventPoller(new GenericPoller)
        loop.setTimeout(() => {
            println("1 second")
            loop.setImmediate(() => println("Immediate"))   
            loop.clearInterval(uuid) 
            println("Counter: " + counter)
        }, 1000)
        val emitter = new StreamEventEmitter(new FileInputStream("/home/rouli-freeman/Documents/SuperScript/src/Main.scala"), chunkSize = 100)
        emitter.on(DataEventEmitter.data, (x) => {
            print(new String(x.data))
        })
        loop.setTimeout(() => emitter.close(), 4000)
        loop.start()
    }

    private class Test extends eventloop.Event {
        def apply(): Unit = {
            println("Evaluated")
        }
    }

    private class GenericPoller extends EventPoller {
        val middle = System.currentTimeMillis + 500
        var evented = false
        def events(): TraversableOnce[eventloop.Event] = {
            if(System.currentTimeMillis > middle && !evented) {
                evented = true
                Array(new Test)
            } else {
                Array[eventloop.Event]()
            }
        }
        def close(): Unit = {
            println("Closed")
        }
        def closing(): Boolean = {
            System.currentTimeMillis > middle + 1500
        }
    }
}