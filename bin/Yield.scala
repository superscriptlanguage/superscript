import scala.collection.Iterator;
import org.superscript.SuperScript

sealed class Yield(val yielder: Int => Iterator[Int])
object Yield extends SuperScript[Yield] {
    lazy val exports = {
        val yielder = (x: Int) => {
            val $x = x
            new Iterator[Int] {
                private var _x: Int = $x
                private var x = _x
                private lazy val max = x * 2
                private enum State {
                    case Yield1, Yield2, Yield3Before, Yield3After, Return, End
                }
                private var _state: State = State.Yield1
                private var _result: Int = _
                def hasNext: Boolean = _state != State.End
                def next(): Int = {
                    _state match {
                        case State.Yield1 => {
                            _result = x
                            _state = State.Yield2
                            _result
                        }
                        case State.Yield2 => {
                            print(_result)
                            x += 2
                            _result = x + 1
                            _state = State.Yield3Before
                            _result
                        }
                        case State.Yield3Before => {
                            max
                            if(x < max) {
                                print(x)
                                x += 1
                                _result = x
                                _state = State.Yield3After
                                _result
                            } else {
                                _state = State.Return
                                next()
                            }
                        }
                        case State.Yield3After => {
                            print(x)
                            if(x < max) {
                                print(x)
                                x += 1
                                _result = x
                                _result
                            } else {
                                _state = State.Return
                                next()
                            }
                        }
                        case State.Return => {
                            _result
                            _state = State.End
                            _result
                        }
                        case State.End => throw new Error("Invalid call to next")
                    }
                }
            }
        }
        new Yield(yielder)
    }
}