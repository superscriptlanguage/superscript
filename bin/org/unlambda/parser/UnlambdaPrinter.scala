package org.unlambda.parser


import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.tree.TerminalNode
import org.antlr.v4.runtime.tree.Trees

class UnlambdaPrinter(parser: UnlambdaParser) extends UnlambdaBaseListener {
    private var indent: List[String] = "" :: Nil
    var str = "Parse Tree:\n"
    private val vocab = parser.getVocabulary

    override def enterEveryRule(ctx: ParserRuleContext): Unit = {
        val first = indent(0)
        str += first + Trees.getNodeText(ctx, parser) + " (" + "\n"
        indent = (first + "    ") :: indent
    }

    override def exitEveryRule(ctx: ParserRuleContext): Unit = {
        indent = indent.drop(1)
        str += indent(0) + ")" + "\n"
    }

    override def visitTerminal(node: TerminalNode): Unit = {
        val tok = node.getSymbol
        str += indent(0) + '"' + tok.getText.replaceAll("\n", "\\\\n") + "\": " + vocab.getSymbolicName(tok.getType)  + '\n'
    }
}