package org.superscript

import scala.quoted._
import scala.tasty._

inline def mix[T, S](x: T): T & S = ${mixImpl('[T], '[S], '{x})}

def mixImpl[T, S](t: Type[T], s: Type[S], x: Expr[T])(implicit reflection: Reflection) = {
    import reflection._
    val tTree: TypeTree = t.unseal
    val sTree: TypeTree = s.unseal
    val xTree: Term = x.unseal.underlyingArgument
    println(s"T: $tTree, S: $sTree, x: $xTree")
    //'{${New(tTree).seal}(x, x).asInstanceOf[${t} & ${s}]}
    '{???}
}

inline def atCompileTime[T] = ${atCompileTimeImpl('[T])}

def atCompileTimeImpl[T](t: Type[T])(implicit reflection: Reflection, imptype: Type[T]) = {
    import reflection._
    println(s"First: ${t.unseal}, Second: ${typeOf[T]}, Third: ${typeTreeOf[T]}")
    '{???}
}

def typeTreeOf[T](implicit t: Type[T], reflection: Reflection): reflection.TypeTree = {
    import reflection._
    t.unseal
}

/*
class Map {
    def defaultSize: Int
}

def createMap(size: Int) = new Map {
    def defaultSize: Int = size
}

class HashMap inherits Map {
    val size: Int
}

def createHashMap(map: Map) = new HashMap inherits map {
    val size = defaultSize
}

--------------------------------------------------------

trait Map {
    def defaultSize: Int
}

class HashMap extends Map {
    val size: Int
}

def createHashMap(size: Size) = new HashMap with Map {
    val defaultSize: Int = size
    val size = defaultSize
}
*/