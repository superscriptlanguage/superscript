package org.superscript.promise

import org.superscript.SuperScript

trait Promise[T] {
    enum State {
        case Fail, Success, Wait
    }
    def state: State
    def value: Option[T]
    def error: Option[Throwable]
    def run: Unit
    def loop: Unit
    def compute[S](error: Throwable => S, value: T => S, wait: () => S): S = {
        state match {
            case State.Fail => this.error.map(error).get
            case State.Success => this.value.map(value).get
            case State.Wait => wait()
        }
    }
    def compute[S](error: Throwable => S, value: T => S): Option[S] = compute((x) => Some(error(x)), (x) => Some(value(x)), () => None)
}

object Promise {
    def apply[T](function: => Either[Throwable, T]): Promise[T] = new Promise[T] {
        private var _state = State.Wait
        def state: State = _state
        private var _value: Option[T] = None
        def value: Option[T] = _value
        private var _error: Option[Throwable] = None
        def error: Option[Throwable] = _error
        def run: Unit = {
            val either = function
            either match {
                case Right(value) => _value = Some(value)
                case Left(error) => _error = Some(error)
            }
        }
        def loop: Unit = {
            SuperScript.loop.addEvent(() => run)
        }
    }
}