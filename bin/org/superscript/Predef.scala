package org.superscript

type INumber = Long
type DNumber = Double
type Id[A] = A
type Optional[+T[_]] = [X] =>> Option[T[X]]

def (f: A => B) +> [A, B, C] (g: B => C): A => C = x => g(f(x))

def (f: B => C) <+ [A, B, C] (g: A => B): A => C = x => f(g(x))

def (x: A) |> [A, B] (f: A => B): B = f(x)

def declare[T](to: Boolean)(further: => T): Option[T] = if(to) Some(further) else None

def (option: Option[T]) collect[T] (cont: => T) = option.getOrElse(cont)

trait Returning[T] {
    def exp: T
}