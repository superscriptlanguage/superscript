package org.superscript.eventloop

import scala.collection.mutable.Set
import scala.collection.mutable.Queue

import scala.concurrent._

import java.util.UUID

abstract class ExecutionContextEventLoop(ctx: ExecutionContext) extends EventLoop[UUID, UUID, UUID] {
    private val timeouts: Set[UUID] = Set()
    private val intervals: Set[UUID] = Set()
    private val immediates: Set[UUID] = Set()
    private val poll: Queue[Event] = new Queue()
    private val pollers: Set[EventPoller] = Set()

    private def remaining(): Boolean = {
        timeouts.nonEmpty || intervals.nonEmpty || immediates.nonEmpty || poll.nonEmpty || pollers.nonEmpty
    }

    def start(): Unit = {
        while(remaining()) {
            var break = false
            while(!break) {
                while(poll.isEmpty && !break) {
                    if(pollers.isEmpty) break = true
                    if(!break) {
                        pollers.foreach((poller) => poll ++= EventPoller.events(poller))
                    }
                    if(pollers.exists((poller) => EventPoller.closing(poller))) break = true
                }
                while(poll.length > 0 && !break) {
                    val event = poll.dequeue()
                    ctx.execute(new Runnable {
                        def run(): Unit = event()
                    })
                }
            }
            pollers --= pollers.filter((poller) => if(EventPoller.closing(poller)) {
                    EventPoller.close(poller)
                    true
                } else false)
        }
    }

    def setTimeout(timeout: () => Unit, time: Long): UUID = {
        val id = UUID.randomUUID
        timeouts += id
        ctx.execute(new Runnable {
            def run(): Unit = {
                Thread.sleep(time)
                if(timeouts.contains(id)) {
                    timeouts.remove(id)
                    timeout()
                }
            }
        })
        id
    }
    def setInterval(interval: () => Unit, time: Long): UUID = {
        val id = UUID.randomUUID
        intervals += id
        ctx.execute(new Runnable {
            def run(): Unit = {
                Thread.sleep(time)
                if(timeouts.contains(id)) {
                    interval()
                    ctx.execute(this)
                }
            }
        })
        id
    }
    def setImmediate(immediate: () => Unit): UUID = {
        val id = UUID.randomUUID
        immediates += id
        ctx.execute(new Runnable {
            def run(): Unit = {
                if(immediates.contains(id)) {
                    immediates.remove(id)
                    immediate()
                }
            }
        })
        id
    }
    def clearTimeout(timeout: UUID): Boolean = {
        if(timeouts.contains(timeout)) {
            timeouts.remove(timeout)
            true
        } else {
            false
        }
    }
    def clearInterval(interval: UUID): Boolean = {
        if(intervals.contains(interval)) {
            intervals.remove(interval)
            true
        } else {
            false
        }
    }
    def clearImmediate(immediate: UUID): Boolean = {
        if(immediates.contains(immediate)) {
            immediates.remove(immediate)
            true
        } else {
            false
        }
    }

    def addEventPoller(poller: EventPoller): Unit = pollers += poller
    def addEvent(event: Event): Unit = poll += event
}