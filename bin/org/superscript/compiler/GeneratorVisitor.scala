package org.superscript.compiler

import ast._
import org.superscript.template._
import org.superscript.template.Template._

implicit val unit: Unit = ()

val indentation = "    "

val nl = >("\n")

case class Settings(indent: String)

def indent(settings: Settings) = settings.copy(indent = settings.indent + indentation)

class GeneratorVisitor extends DefaultAstVisitor[Template, Settings]($"") {
    override def visitLiteral(literal: Literal, settings: Settings): Template = >(literal.contains)
    override def visitBlock(block: Block, settings: Settings): Template = $"{@${block.exprs.map(a => $"@${>(settings.indent + indentation)}@${visit(a)(indent(settings))}").toPostfixTemplate($";")}@${>(settings.indent)}}"
    override def visitInfix(infix: Infix, settings: Settings): Template = $"@${visit(infix.before)(settings)} @${>(infix.infix)} @${visit(infix.after)(settings)}"
    override def visitRefinement(refinement: Refinement, settings: Settings): Template = $"@${visit(refinement.expr)(settings)}.@${>(refinement.refinement)}"
    override def visitApplication(application: Application, settings: Settings): Template = $"@${visit(application.expr)(settings)}(@${application.args.map(a => visit(a)(settings)).toTemplate($", ")})"
    override def visitWith(wth: With, settings: Settings): Template = $"{ val __0_with = @${visit(wth.expr)(settings)}; import __0_with._; @${visit(wth.cont)(settings)}; }"
    override def visitClass(cls: Class, settings: Settings): Template = $"class @${>(cls.name)} extends @${>(cls.sup._2)}@${cls.extensions.map(x => >(x)).toPrefixTemplate($" with ")} @${visit(Block(None, cls.block))(settings)}"//"
    //def visitTrait(trt: Trait, settings: Settings): Template
    //def visitFunction(function: Function, settings: Settings): Template
    //def visitMethod(method: Method, settings: Settings): Template
}

object GeneratorVisitor {
    val extensions = Seq[Extension]()
    def run(ast: Ast): String = new GeneratorVisitor().visit(ast)(Settings("\n"))(extensions).result
}

object Test {
    def main(args: Array[String]) = {
        val ast = Block(None, Seq(
            Block(None, Seq()),
            Block(None, Seq()),
            Class(None, "Foo", (false, "Bar"), Seq(), Seq(
                Block(None, Seq())
            ))
        ))
        println(GeneratorVisitor.run(ast))
    }
}