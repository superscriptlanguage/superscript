package org.superscript.compiler.ast

type Name = String
type Type = String // type Type = Type.typetype.type

abstract class Ast {
    def visit[R, S](implicit visitor: AstVisitor[R, S], settings: S): R
}

abstract class Expression(val t: Option[Type]) extends Ast

case class Literal(override val t: Option[Type], contains: String) extends Expression(t) {
    def visit[R, S](implicit visitor: AstVisitor[R, S], settings: S): R = visitor.visitLiteral(this, settings)
}

case class Block(override val t: Option[Type], exprs: Seq[Expression]) extends Expression(t) {
    def visit[R, S](implicit visitor: AstVisitor[R, S], settings: S): R = visitor.visitBlock(this, settings)
}

case class Infix(override val t: Option[Type], before: Expression, infix: Name, after: Expression) extends Expression(t) {
    def visit[R, S](implicit visitor: AstVisitor[R, S], settings: S): R = visitor.visitInfix(this, settings)
}

case class Refinement(override val t: Option[Type], expr: Expression, refinement: Name) extends Expression(t) {
    def visit[R, S](implicit visitor: AstVisitor[R, S], settings: S): R = visitor.visitRefinement(this, settings)
}

case class Application(override val t: Option[Type], expr: Expression, args: Seq[Expression]) extends Expression(t) {
    def visit[R, S](implicit visitor: AstVisitor[R, S], settings: S): R = visitor.visitApplication(this, settings)
}

case class With(override val t: Option[Type], expr: Expression, cont: Expression) extends Expression(t) {
    def visit[R, S](implicit visitor: AstVisitor[R, S], settings: S): R = visitor.visitWith(this, settings)
}

case class Class(override val t: Option[Type], name: Name, sup: (Boolean, Name), extensions: Seq[Name], block: Seq[Expression]) extends Expression(t) {
    def visit[R, S](implicit visitor: AstVisitor[R, S], settings: S): R = visitor.visitClass(this, settings)
}

case class Trait(override val t: Option[Type], name: Name, higher: Seq[Name], block: Seq[Expression]) extends Expression(t) {
    def visit[R, S](implicit visitor: AstVisitor[R, S], settings: S): R = visitor.visitTrait(this, settings)
}

case class Function(override val t: Option[Type], args: Seq[(Name, Option[Type])], ret: Option[Type], block: Expression) extends Expression(t) {
    def visit[R, S](implicit visitor: AstVisitor[R, S], settings: S): R = visitor.visitFunction(this, settings)
}

case class Method(override val t: Option[Type], name: Name, args: Seq[(Name, Type)], ret: Option[Type], block: Expression) extends Expression(t) {
    def visit[R, S](implicit visitor: AstVisitor[R, S], settings: S): R = visitor.visitMethod(this, settings)
}

trait AstVisitor[@specialized R, S] {
    implicit val visitor: AstVisitor[R, S] = this

    def visitLiteral(literal: Literal, settings: S): R
    def visitBlock(block: Block, settings: S): R
    def visitInfix(infix: Infix, settings: S): R
    def visitRefinement(refinement: Refinement, settings: S): R
    def visitApplication(application: Application, settings: S): R
    def visitWith(wth: With, settings: S): R
    def visitClass(cls: Class, settings: S): R
    def visitTrait(trt: Trait, settings: S): R
    def visitFunction(function: Function, settings: S): R
    def visitMethod(method: Method, settings: S): R

    def visit(ast: Ast)(implicit settings: S) = ast.visit(this, settings)
}

class DefaultAstVisitor[@specialized R, S](default: R) extends AstVisitor[R, S] {
    def visitLiteral(literal: Literal, settings: S): R = default
    def visitBlock(block: Block, settings: S): R = default
    def visitInfix(infix: Infix, settings: S): R = default
    def visitRefinement(refinement: Refinement, settings: S): R = default
    def visitApplication(application: Application, settings: S): R = default
    def visitWith(wth: With, settings: S): R = default
    def visitClass(cls: Class, settings: S): R = default
    def visitTrait(trt: Trait, settings: S): R = default
    def visitFunction(function: Function, settings: S): R = default
    def visitMethod(method: Method, settings: S): R = default
}