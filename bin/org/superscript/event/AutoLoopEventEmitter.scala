package org.superscript.event
import org.superscript.SuperScript
/**
 * Automatically adds itself to the event loop when created.
 *
 * @inheritdoc
 */
trait AutoLoopEventEmitter[S <: EventType, T <: AutoLoopEventEmitter[S, T, R], R[_ <: S]]
        extends EventEmitter[S, T, R] { this: T =>
    SuperScript.loop.addEventPoller(this)    
}