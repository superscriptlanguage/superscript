package org.superscript.deprecated.template

import Template._

type Template = (Iterable[Extension]) => String

@deprecated("See org.superscript.template.Template", "1.0.0")
object Template {
    type Extension = PartialFunction[(String, () => String, String), String]
    val bif = """(?s)^(.*)(?!>\\)>(-?\d*)$""".r
    val bmerge = """(?s)^(.*)(?!>\\)@$""".r
    def folder(before: String, arg: Template, after: String, extensions: Iterable[Extension]): String = {
        (before, after) match {
            case (bif(start, question), _) => if(question.toInt > 0) start + arg(extensions) + after
                else start + after
            case (bmerge(start), _) => start + arg(extensions) + after
            case _ => {
                for(e <- extensions) {
                    if(e.isDefinedAt((before, () => arg(extensions), after))) {
                        return e(before, () => arg(extensions), after)
                    }
                }
                throw new Error("Invalid template")
            }
        }
    }
    
    def (sc: StringContext) $(args: => Template*): Template = (extensions) => {
        sc.checkLengths(args)
        var result = sc.parts(0)
        args.zipWithIndex.foreach((arg, i) => {
            result = folder(result, arg, sc.parts(i + 1), extensions)
        })
        result
    }
    
    def empty(x: => Unit = ()): Template = {
        println("once")
        x
        (e) => ""
    }
    def >(x: => Any = ""): Template = (e) => x.toString
}

object Test {
    import Template._
    def main(args: Array[String]): Unit = {
        val rec = {
            var recursion: (Int) => Template = (i) => empty()
            recursion = (i) => $"Hi>@${>(1000 - i)}${recursion(i + 1)}"
            recursion(0)
        }
        println(rec(List[Extension]()))
    }
}