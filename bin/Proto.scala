import org.superscript._

sealed class Proto(val getInputStream: String => Proto.InputStream, val getNiceInputStream: Proto.InputStream => Proto.NiceInputStream)
object Proto extends SuperScript[Proto] {
    sealed class InputStream(delegator: InputStream) {
        def source: String = delegator.source
        def getNextChar: () => Char = delegator.getNextChar
        def avaliable: () => Boolean = delegator.avaliable
    }
    sealed class NiceInputStream(delegator: NiceInputStream, prototype: InputStream) extends InputStream(prototype) {
        def string: String = delegator.string
    }
    lazy val exports = {
        var getInputStream = (input: String) => {
            var currentPos = 0;
            new InputStream(null) {
                override val source = input
                override val getNextChar = () => {
                    val next = source(currentPos)
                    currentPos = currentPos + 1
                    next
                }
                override val avaliable = () => currentPos < source.length()
            }
        }
        var getNiceInputStream = (stream: InputStream) => {
            new NiceInputStream(null, stream) {
                override val string = source
            }
        }

        {
            val wit = getInputStream("foo")
            import wit._
            println(source)
        }

        new Proto(getInputStream, getNiceInputStream)
    }
}