
class AnyRefDelegator(__0_del: AnyRef);

abstract class Tree[T : Ordering](__0_proto: AnyRef) extends AnyRefDelegator(__0_proto) {
    def contains(x: BTree[T], t: T): Boolean;
    def insert(x: BTree[T], t: T): BTree[T];
}
class TreeDelegator[T : Ordering](__0_del: Tree[T]) extends Tree[T](__0_del) {
    def contains(x: BTree[T], t: T): Boolean = contains(x, t);
    def insert(x: BTree[T], t: T): BTree[T] = insert(x, t);
}
abstract class BTree[T](__0_proto: AnyRef) extends AnyRefDelegator(__0_proto) {
    val data: List[T];
    val nodes: List[BTree[T]];
    val max: Int;
}
class BTreeDelegator[T](__0_del: BTree[T]) extends BTree[T](__0_del) {
    val data: List[T] = __0_del.data;
    val nodes: List[BTree[T]] = __0_del.nodes;
    val max: Int = __0_del.max;
}
object Tree extends AnyRefDelegator(null) {
    def makeBTree[T](a: List[T], b: List[BTree[T]], c: Int): BTree[T] = new BTree[T](null) {
        val data: List[T] = a;
        val nodes: List[BTree[T]] = b;
        val max: Int = c;
    };
    def getTree[T](given x: Ordering[T]): Tree[T] = new Tree(null) {
        import x.{given};
        def contains(x: BTree[T], t: T): Boolean = (x.data.contains(t)) || (if(x.nodes.nonEmpty) (((x.data) zip (x.nodes)).foldLeft[Option[Boolean]](None)((opt, v) => opt.map((a) => Some(a)).getOrElse(if(v._1 > t) Some(contains(v._2, t)) else None)).getOrElse(contains(x.nodes.last, t))) else false);
        def insert(x: BTree[T], t: T): BTree[T] = {
            def insertRecursive(a: Option[BTree[T]], key: T): BTree[T] | Tuple2[Option[Tuple2[BTree[T], BTree[T]]], T] = a.map((tree) => {
                val index = tree.data.zipWithIndex.foldLeft[Option[Int]](None)((opt, v) => opt.map((a) => Some(a)).getOrElse(if(v._1 > t) Some(v._2) else None)).getOrElse(tree.data.length);
                val recInsert = insertRecursive(if(tree.nodes.isEmpty) None else Some(tree.nodes(index)), t);
                val newT: Tuple2[List[T], List[BTree[T]]] = recInsert match {
                    case a: BTree[T] => Tuple2(tree.data, tree.nodes.patch(index, a :: Nil, 1))
                    case x: Tuple2[Option[Tuple2[BTree[T], BTree[T]]], T] => x match {
                        case Tuple2(None, a) => Tuple2(tree.data.patch(index, a :: Nil, 1), tree.nodes)
                        case Tuple2(Some(Tuple2(b, c)), a) => Tuple2(tree.data.patch(index, a :: Nil, 1), tree.nodes.patch(index, b :: c :: Nil, 2))
                    }
                };
                val len = newT._1.length;
                (if(len <= 2 * tree.max) makeBTree(newT._1, newT._2, tree.max) else {
                    val splitData = newT._1.splitAt((len - 1) / 2);
                    val splitNodes = newT._2.splitAt((len + 1) / 2);
                    val firstPart = makeBTree(splitData._1, splitNodes._1, tree.max);
                    val secondPart = makeBTree(splitData._2.tail, splitNodes._2, tree.max);
                    Tuple2(Some(Tuple2(firstPart, secondPart)), newT._1((len - 1) / 2));
                });
            }).getOrElse(Tuple2(None, key)): BTree[T] | (Option[(BTree[T], BTree[T])], T);
            insertRecursive(Some(x), t) match {
                case a: BTree[T] => a
                case Tuple2(None, _) => null
                case Tuple2(Some(Tuple2(x, y)), z) => makeBTree(List(z), List(x, y), x.max)
            };
        };
    };
}
object Tests extends AnyRefDelegator(null) {
    def main(args: Array[String]): Unit = {
        import Tree.{_};
        val tc = getTree[Int];
        val btree1 = makeBTree(List(0), List(), 3);
        val btree2 = makeBTree(List(2, 3), List(), 3);
        val btree3 = makeBTree(List(6, 7), List(), 3);
        val btree4 = makeBTree(List(8, 8), List(), 3);
        val tree = makeBTree(List(1, 5, 7), List(btree1, btree2, btree3, btree4), 3);
        println(tc.contains(tree, 10));
        println(tc.contains(tree, 2));
        val btree = makeBTree(List(5), List(), 3);
        val bin1 = tc.insert(btree, 3);
        println(bin1);
        val bin2 = tc.insert(bin1, 5);
        println(bin2);
        val bin3 = tc.insert(bin2, 7);
        println(bin3);
    };
}