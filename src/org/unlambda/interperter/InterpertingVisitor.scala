/*package org.unlambda.interperter

import org.unlambda.parser._

import scala.collection.JavaConverters._

import scala.language.implicitConversions

import org.antlr.v4.runtime.CommonTokenFactory
import org.antlr.v4.runtime.CommonTokenStream
import org.antlr.v4.runtime.UnbufferedCharStream
import org.antlr.v4.runtime.tree.ParseTree
import org.antlr.v4.runtime.tree.ParseTreeWalker
import org.antlr.v4.runtime.CharStreams
import java.util.Scanner

type Name = String

trait Scope[T] {
    def apply(name: String): Option[T]
    def set(name: String, value: T): Option[Scope[T]]
    def create(name: String, value: T, const: Boolean): Scope[T]
}

object Lazy {
    implicit def now[T](t: T): () => T = () => t
    def lazily[T](f: () => T): () => T = {
        var t: Option[T] = None
        () => t.getOrElse {
            val v = f()
            t = Some(v)
            v
        }
    }
}

case class ReturnException(payload: Expr) extends RuntimeException
class IncorrectArgumentException extends RuntimeException

object Scope {
    def empty[T]: Scope[T] = new Scope[T] {
        def apply(name: String): Option[T] = None
        def set(name: String, value: T): Option[Scope[T]] = None
        def create(name: String, value: T, const: Boolean): Scope[T] = Scope.create(Scope.empty).create(name, value, const)
    }
    def create[T](outer: Scope[T], map: Map[String, (Boolean, T)] = Map()): Scope[T] = new Scope[T] {
        def apply(name: String): Option[T] = map.get(name).map((x) => x._2) match {
            case Some(x) => Some(x)
            case None => outer(name)
        }
        def set(name: String, value: T): Option[Scope[T]] = {
            val a = map.get(name)
            a.flatMap((x) => {
                val (const, result) = x
                if(const) {
                    None
                } else Some(Scope.create(outer, map + (name -> (false, value))))
            })
        }
        def create(name: String, value: T, const: Boolean): Scope[T] = Scope.create(outer, map + (name -> (const, value)))
    }
}

trait Expr {
    def apply(expressions: () => Expr*): Expr
    def laziers: Seq[Boolean]
}

case class Func(scope: Map[String, () => Expr],
                arguments: Seq[Name],
                evaluated: UnlambdaParser.ExpressionContext, 
                evaluator: (UnlambdaParser.ExpressionContext, Map[String, () => Expr]) => Expr, laziers: Seq[Boolean], len: Int) extends Expr {
    def apply(expressions: () => Expr*): Expr = {
        var newScope = scope
        if(expressions.length != len) {
            println("Incorrect length, expected: " + len + ", actual: " + expressions.length + "\n    At `" + this + "`.")
            throw new IndexOutOfBoundsException
        }
        for(x <- expressions.zipWithIndex) {
            val arg = arguments(x._2)
            newScope += (arg -> x._1)
        }
        try {
            evaluator(evaluated, newScope)
        } catch {
            case x: IncorrectArgumentException => {
                println("    At `" + this + "`.")
                throw x
            }
            case x: IndexOutOfBoundsException => {
                println("    At `" + this + "`.")
                throw x
            }
        }
    }

    override def toString: String = (if(arguments.nonEmpty) {
        "(" + arguments.zipWithIndex.map(x => if(laziers(x._2)) "=> " + x._1 else x._1).mkString(", ") + ")"
    } else "()") + " -> " + expand

    def expand: String = evaluated.getText
}

class InterpertingVisitor extends UnlambdaBaseVisitor[Expr] {
    var currentScope = Map[String, () => Expr]()
    var indent = ""
    override def visitFile(ctx: UnlambdaParser.FileContext): Expr = {
        var last: Option[Expr] = None
        for(x <- ctx.statement().asScala) {
            last = Some(visit(x))
        }
        last.get
    }
    override def visitExprstmt(ctx: UnlambdaParser.ExprstmtContext): Expr = visit(ctx.expression)
    override def visitNullstmt(ctx: UnlambdaParser.NullstmtContext): Expr = visit(ctx.nullexp)
    override def visitCall(ctx: UnlambdaParser.CallContext): Expr = {
        val first = visit(ctx.first)
        try {
            first(ctx.call.asScala.zipWithIndex.map((expr) => {
                laccess(expr._1, first.laziers(expr._2))
            }): _*)
        } catch {
            case x: IncorrectArgumentException => {
                println("    From `" + first + "`.")
                throw x
            }
            case x: IndexOutOfBoundsException => {
                println("    From `" + first + "`.")
                throw x
            }
        }
    }
    override def visitParens(ctx: UnlambdaParser.ParensContext): Expr = visit(ctx.expression)
    override def visitAccess(ctx: UnlambdaParser.AccessContext): Expr = currentScope.get(strip(ctx.NAME)) match {
        case Some(x) => x()
        case None => {
            println("Error at invalid name: " + strip(ctx.NAME))
            throw new IncorrectArgumentException
        }
    }
    override def visitDefine(ctx: UnlambdaParser.DefineContext): Expr = {
        val size = ctx.lazyname.size
        val la = new Array[Boolean](size)
        Func(currentScope, ctx.lazyname.asScala.zipWithIndex.map((x) => {
            val lz = lazyify(x._1)
            la(x._2) = lz._1
            lz._2
        }), ctx.expression, (e, scope) => {
            val old = currentScope
            currentScope = scope
            val ret = try {
                visit(e)
            } catch {
                case ex: ReturnException => ex.payload
            }
            currentScope = old
            ret
        }, la, size)
    }
    override def visitCreate(ctx: UnlambdaParser.CreateContext): Expr = {
        val la = lazyify(ctx.lazyname)
        currentScope = currentScope + (la._2 -> laccess(ctx.expression, la._1))
        null
    }
    override def visitBlock(ctx: UnlambdaParser.BlockContext): Expr = {
        val oldScope = currentScope
        var last: Option[Expr] = None
        for(x <- ctx.statement().asScala) {
            last = Some(visit(x))
        }
        currentScope = oldScope
        last.get
    }
    override def visitReturn(ctx: UnlambdaParser.ReturnContext): Expr = throw ReturnException(visit(ctx.expression))

    def strip(x: ParseTree): String = {
        val text = x.getText
        if(text.startsWith("`")) {
            val newText = text.stripPrefix("`").stripSuffix("`")
            newText.replaceAll("\\\\(.)", "$1")
        } else text
    }

    def lazyify(x: ParseTree): (Boolean, String) = x match {
        case x: UnlambdaParser.LazyyContext => (true, strip(x.NAME))
        case x: UnlambdaParser.LazynContext => (false, strip(x.NAME))
    }

    def laccess(x: ParseTree, b: Boolean) = if(b) {
        val scope = currentScope
        Lazy.lazily(() => {
            val old = currentScope
            currentScope = scope
            val v = visit(x)
            currentScope = old
            v
        })
    } else {
        val f = visit(x)
        () => f
    }

    def addFunction(x: (Seq[() => Expr]) => Expr, lz: () => Seq[Boolean], name: Name) = {
        val a = new Expr {
            def apply(expressions: () => Expr*): Expr = x(expressions)
            def laziers: Seq[Boolean] = lz()
            override def toString = name
        }
        currentScope = currentScope + (name -> (() => a))
    }
}

object InterpertingVisitor {
    def main(args: Array[String]) = {
        val str = 
"""
# S := => x -> => y -> => z -> x(z)(y(z))
# K := => x -> => y -> x
# I := => x -> x
Y := \f.
    W := \|x. f(x(x))
    W(W)
N := Y(\|x.\.x)

T := \|x,|y.x
F := \|x,|y.y
Or := \x,y.x(T, y)
And := \x,y.x(y, F)
Not := \x.x(F, T)

Pair := \a,b.\f.f(a,b)
PFst := T
PSec := F

Triple := \a,b,c.\f.f(a,b,c)
TFst := \a,b,c.a
TSec := \a,b,c.b
TTrd := \a,b,c.c

Z := \f.\x.x
S := \i.\f.\x.f(i(f)(x))
Sum := \a.\b.a(S)(b)
IsZero := \a.a(\x.F)(T)
P := \n.n(\a.Pair(a(PSec), S(a(PSec))))(Pair(Z, Z))(PFst)
G := Y(\|g.\a,b.Or(IsZero(a),IsZero(b))(IsZero(a)(F,T),g(P(a),P(b))))
L := \a,b.G(b,a)
Ge := \x.Not(L(x))
Le := \x.Not(G(x))
E := \a,b.And(Ge(a,b),Le(a,b))
R := \s.\n.n(s)(n)

Some := \x.Pair(x,T)
None := Pair(N,F)
IsNone := \x.Not(x(PSec))

Append := \l,x.Pair(Some(l),x)
Single := \x.Pair(None, x)
IsSingle := \l.IsNone(l(PFst))
Len := Y(\|len.\l.S(IsSingle(l)(Z,len(l(PFst)(PFst)))))
Get := Y(\|get.\l.\i.IsZero(i)(l(PSec),get(l(PFst)(PFst),S(i))))
"""
        val lexer = new UnlambdaLexer(CharStreams.fromString(str))

        val parser = new UnlambdaParser(new CommonTokenStream(lexer))

        val visitor = new InterpertingVisitor

        visitor.addFunction((expressions) => {
            val nothing = visitor.currentScope.get("N").get
            for(expression <- expressions.zipWithIndex) {
                var i = 0
                val inc = new Expr {
                    def apply(expressions: () => Expr*): Expr = {
                        i += 1
                        expressions(0)()
                    }
                    def laziers: Seq[Boolean] = Stream.iterate(false)(x => x)
                }
                var current = expression._1()(() => inc)(nothing)
                println(s"#${expression._2}: ${i}")
            }
            null
        }, () => Stream.iterate(false)(x => x), "printeger")


        visitor.addFunction((expressions) => {
            val nothing = visitor.currentScope.get("N").get
            for(expression <- expressions.zipWithIndex) {
                var b = false
                if(expression._1()(nothing, () => null) == nothing()) b = true
                println(s"#${expression._2}: ${b}")
            }
            null
        }, () => Stream.iterate(false)(x => x), "printBoolean")

        visitor.addFunction((expressions) => {
            print("A(")
            if(expressions.length > 0) {
                expressions(0)()
                for(expression <- expressions.drop(1)) {
                    print(", ")
                    expression()
                }
            }
            print(")")
            visitor.currentScope.get("A").get()
        }, () => Stream.iterate(true)(x => x), "A")

        val file = parser.file

        /*val printer = new UnlambdaPrinter(parser)

        ParseTreeWalker.DEFAULT.walk(printer, file)

        println(printer.str)*/

        println(visitor.visit(file))
        
        /*print("> ")
        val charstream = new UnbufferedCharStream(System.in)
        val lexer = new UnlambdaLexer(charstream)
        lexer.setTokenFactory(new CommonTokenFactory(true))
        val stream = new CommonTokenStream(lexer)
        val parser = new UnlambdaParser(stream)
        val visitor = new InterpertingVisitor
        while (true) {
            val tree = parser.single
            println("Found")
            println("--> " + visitor.visit(tree))
            print("> ")
        }*/
    }
}*/