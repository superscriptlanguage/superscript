// Generated from Unlambda.g4 by ANTLR 4.7.2
package org.unlambda.parser;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class UnlambdaParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, LPAREN=9, 
		RPAREN=10, NAME=11, WS=12, NEWLINE=13, INDENT=14, DEDENT=15;
	public static final int
		RULE_file = 0, RULE_statement = 1, RULE_expression = 2, RULE_nullexp = 3, 
		RULE_lazyname = 4;
	private static String[] makeRuleNames() {
		return new String[] {
			"file", "statement", "expression", "nullexp", "lazyname"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "','", "'->'", "'\\'", "'.'", "'return'", "':='", "'=>'", "'|'", 
			"'('", "')'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, "LPAREN", "RPAREN", 
			"NAME", "WS", "NEWLINE", "INDENT", "DEDENT"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Unlambda.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public UnlambdaParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class FileContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(UnlambdaParser.EOF, 0); }
		public List<TerminalNode> NEWLINE() { return getTokens(UnlambdaParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(UnlambdaParser.NEWLINE, i);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public FileContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_file; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).enterFile(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).exitFile(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UnlambdaVisitor ) return ((UnlambdaVisitor<? extends T>)visitor).visitFile(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FileContext file() throws RecognitionException {
		FileContext _localctx = new FileContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_file);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(13);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(10);
					match(NEWLINE);
					}
					} 
				}
				setState(15);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			}
			setState(19);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__2) | (1L << T__4) | (1L << T__6) | (1L << T__7) | (1L << LPAREN) | (1L << NAME) | (1L << NEWLINE))) != 0)) {
				{
				{
				setState(16);
				statement();
				}
				}
				setState(21);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(22);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
	 
		public StatementContext() { }
		public void copyFrom(StatementContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ExprstmtContext extends StatementContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<TerminalNode> NEWLINE() { return getTokens(UnlambdaParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(UnlambdaParser.NEWLINE, i);
		}
		public ExprstmtContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).enterExprstmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).exitExprstmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UnlambdaVisitor ) return ((UnlambdaVisitor<? extends T>)visitor).visitExprstmt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NullstmtContext extends StatementContext {
		public NullexpContext nullexp() {
			return getRuleContext(NullexpContext.class,0);
		}
		public List<TerminalNode> NEWLINE() { return getTokens(UnlambdaParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(UnlambdaParser.NEWLINE, i);
		}
		public NullstmtContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).enterNullstmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).exitNullstmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UnlambdaVisitor ) return ((UnlambdaVisitor<? extends T>)visitor).visitNullstmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_statement);
		try {
			int _alt;
			setState(36);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				_localctx = new ExprstmtContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(24);
				expression(0);
				setState(26); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(25);
						match(NEWLINE);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(28); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			case 2:
				_localctx = new NullstmtContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(30);
				nullexp();
				setState(32); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(31);
						match(NEWLINE);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(34); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	 
		public ExpressionContext() { }
		public void copyFrom(ExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class CallContext extends ExpressionContext {
		public ExpressionContext first;
		public ExpressionContext expression;
		public List<ExpressionContext> call = new ArrayList<ExpressionContext>();
		public TerminalNode LPAREN() { return getToken(UnlambdaParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(UnlambdaParser.RPAREN, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public CallContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).enterCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).exitCall(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UnlambdaVisitor ) return ((UnlambdaVisitor<? extends T>)visitor).visitCall(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParensContext extends ExpressionContext {
		public TerminalNode LPAREN() { return getToken(UnlambdaParser.LPAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(UnlambdaParser.RPAREN, 0); }
		public ParensContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).enterParens(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).exitParens(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UnlambdaVisitor ) return ((UnlambdaVisitor<? extends T>)visitor).visitParens(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AccessContext extends ExpressionContext {
		public TerminalNode NAME() { return getToken(UnlambdaParser.NAME, 0); }
		public AccessContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).enterAccess(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).exitAccess(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UnlambdaVisitor ) return ((UnlambdaVisitor<? extends T>)visitor).visitAccess(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DefineContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode LPAREN() { return getToken(UnlambdaParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(UnlambdaParser.RPAREN, 0); }
		public List<LazynameContext> lazyname() {
			return getRuleContexts(LazynameContext.class);
		}
		public LazynameContext lazyname(int i) {
			return getRuleContext(LazynameContext.class,i);
		}
		public DefineContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).enterDefine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).exitDefine(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UnlambdaVisitor ) return ((UnlambdaVisitor<? extends T>)visitor).visitDefine(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BlockContext extends ExpressionContext {
		public List<TerminalNode> NEWLINE() { return getTokens(UnlambdaParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(UnlambdaParser.NEWLINE, i);
		}
		public TerminalNode INDENT() { return getToken(UnlambdaParser.INDENT, 0); }
		public TerminalNode DEDENT() { return getToken(UnlambdaParser.DEDENT, 0); }
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public BlockContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).enterBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).exitBlock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UnlambdaVisitor ) return ((UnlambdaVisitor<? extends T>)visitor).visitBlock(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ReturnContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ReturnContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).enterReturn(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).exitReturn(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UnlambdaVisitor ) return ((UnlambdaVisitor<? extends T>)visitor).visitReturn(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 4;
		enterRecursionRule(_localctx, 4, RULE_expression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(100);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				{
				_localctx = new AccessContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(39);
				match(NAME);
				}
				break;
			case 2:
				{
				_localctx = new DefineContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(62);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case LPAREN:
					{
					setState(40);
					match(LPAREN);
					setState(49);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__6) | (1L << T__7) | (1L << NAME))) != 0)) {
						{
						setState(41);
						lazyname();
						setState(46);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==T__0) {
							{
							{
							setState(42);
							match(T__0);
							setState(43);
							lazyname();
							}
							}
							setState(48);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						}
					}

					setState(51);
					match(RPAREN);
					}
					break;
				case T__1:
				case T__6:
				case T__7:
				case NAME:
					{
					setState(60);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__6) | (1L << T__7) | (1L << NAME))) != 0)) {
						{
						setState(52);
						lazyname();
						setState(57);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==T__0) {
							{
							{
							setState(53);
							match(T__0);
							setState(54);
							lazyname();
							}
							}
							setState(59);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						}
					}

					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(64);
				match(T__1);
				setState(65);
				expression(5);
				}
				break;
			case 3:
				{
				_localctx = new DefineContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(66);
				match(T__2);
				setState(75);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__6) | (1L << T__7) | (1L << NAME))) != 0)) {
					{
					setState(67);
					lazyname();
					setState(72);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__0) {
						{
						{
						setState(68);
						match(T__0);
						setState(69);
						lazyname();
						}
						}
						setState(74);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(77);
				match(T__3);
				setState(78);
				expression(4);
				}
				break;
			case 4:
				{
				_localctx = new BlockContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(79);
				match(NEWLINE);
				setState(80);
				match(INDENT);
				setState(84);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(81);
						match(NEWLINE);
						}
						} 
					}
					setState(86);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
				}
				setState(88); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(87);
					statement();
					}
					}
					setState(90); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__2) | (1L << T__4) | (1L << T__6) | (1L << T__7) | (1L << LPAREN) | (1L << NAME) | (1L << NEWLINE))) != 0) );
				setState(92);
				match(DEDENT);
				}
				break;
			case 5:
				{
				_localctx = new ParensContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(94);
				match(LPAREN);
				setState(95);
				expression(0);
				setState(96);
				match(RPAREN);
				}
				break;
			case 6:
				{
				_localctx = new ReturnContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(98);
				match(T__4);
				setState(99);
				expression(1);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(126);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,19,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(124);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
					case 1:
						{
						_localctx = new CallContext(new ExpressionContext(_parentctx, _parentState));
						((CallContext)_localctx).first = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(102);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(103);
						match(LPAREN);
						setState(112);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__2) | (1L << T__4) | (1L << T__6) | (1L << T__7) | (1L << LPAREN) | (1L << NAME) | (1L << NEWLINE))) != 0)) {
							{
							setState(104);
							((CallContext)_localctx).expression = expression(0);
							((CallContext)_localctx).call.add(((CallContext)_localctx).expression);
							setState(109);
							_errHandler.sync(this);
							_la = _input.LA(1);
							while (_la==T__0) {
								{
								{
								setState(105);
								match(T__0);
								setState(106);
								((CallContext)_localctx).expression = expression(0);
								((CallContext)_localctx).call.add(((CallContext)_localctx).expression);
								}
								}
								setState(111);
								_errHandler.sync(this);
								_la = _input.LA(1);
							}
							}
						}

						setState(114);
						match(RPAREN);
						}
						break;
					case 2:
						{
						_localctx = new CallContext(new ExpressionContext(_parentctx, _parentState));
						((CallContext)_localctx).first = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(115);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(116);
						((CallContext)_localctx).expression = expression(0);
						((CallContext)_localctx).call.add(((CallContext)_localctx).expression);
						setState(121);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(117);
								match(T__0);
								setState(118);
								((CallContext)_localctx).expression = expression(0);
								((CallContext)_localctx).call.add(((CallContext)_localctx).expression);
								}
								} 
							}
							setState(123);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
						}
						}
						break;
					}
					} 
				}
				setState(128);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,19,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class NullexpContext extends ParserRuleContext {
		public NullexpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nullexp; }
	 
		public NullexpContext() { }
		public void copyFrom(NullexpContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class CreateContext extends NullexpContext {
		public LazynameContext lazyname() {
			return getRuleContext(LazynameContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public CreateContext(NullexpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).enterCreate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).exitCreate(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UnlambdaVisitor ) return ((UnlambdaVisitor<? extends T>)visitor).visitCreate(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NullexpContext nullexp() throws RecognitionException {
		NullexpContext _localctx = new NullexpContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_nullexp);
		try {
			_localctx = new CreateContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(129);
			lazyname();
			setState(130);
			match(T__5);
			setState(131);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LazynameContext extends ParserRuleContext {
		public LazynameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lazyname; }
	 
		public LazynameContext() { }
		public void copyFrom(LazynameContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class LazynContext extends LazynameContext {
		public TerminalNode NAME() { return getToken(UnlambdaParser.NAME, 0); }
		public LazynContext(LazynameContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).enterLazyn(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).exitLazyn(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UnlambdaVisitor ) return ((UnlambdaVisitor<? extends T>)visitor).visitLazyn(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LazyyContext extends LazynameContext {
		public TerminalNode NAME() { return getToken(UnlambdaParser.NAME, 0); }
		public LazyyContext(LazynameContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).enterLazyy(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof UnlambdaListener ) ((UnlambdaListener)listener).exitLazyy(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof UnlambdaVisitor ) return ((UnlambdaVisitor<? extends T>)visitor).visitLazyy(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LazynameContext lazyname() throws RecognitionException {
		LazynameContext _localctx = new LazynameContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_lazyname);
		int _la;
		try {
			setState(136);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__6:
			case T__7:
				_localctx = new LazyyContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(133);
				_la = _input.LA(1);
				if ( !(_la==T__6 || _la==T__7) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(134);
				match(NAME);
				}
				break;
			case NAME:
				_localctx = new LazynContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(135);
				match(NAME);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 2:
			return expression_sempred((ExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 7);
		case 1:
			return precpred(_ctx, 6);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\21\u008d\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\3\2\7\2\16\n\2\f\2\16\2\21\13\2\3\2\7"+
		"\2\24\n\2\f\2\16\2\27\13\2\3\2\3\2\3\3\3\3\6\3\35\n\3\r\3\16\3\36\3\3"+
		"\3\3\6\3#\n\3\r\3\16\3$\5\3\'\n\3\3\4\3\4\3\4\3\4\3\4\3\4\7\4/\n\4\f\4"+
		"\16\4\62\13\4\5\4\64\n\4\3\4\3\4\3\4\3\4\7\4:\n\4\f\4\16\4=\13\4\5\4?"+
		"\n\4\5\4A\n\4\3\4\3\4\3\4\3\4\3\4\3\4\7\4I\n\4\f\4\16\4L\13\4\5\4N\n\4"+
		"\3\4\3\4\3\4\3\4\3\4\7\4U\n\4\f\4\16\4X\13\4\3\4\6\4[\n\4\r\4\16\4\\\3"+
		"\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4g\n\4\3\4\3\4\3\4\3\4\3\4\7\4n\n\4\f"+
		"\4\16\4q\13\4\5\4s\n\4\3\4\3\4\3\4\3\4\3\4\7\4z\n\4\f\4\16\4}\13\4\7\4"+
		"\177\n\4\f\4\16\4\u0082\13\4\3\5\3\5\3\5\3\5\3\6\3\6\3\6\5\6\u008b\n\6"+
		"\3\6\2\3\6\7\2\4\6\b\n\2\3\3\2\t\n\2\u00a0\2\17\3\2\2\2\4&\3\2\2\2\6f"+
		"\3\2\2\2\b\u0083\3\2\2\2\n\u008a\3\2\2\2\f\16\7\17\2\2\r\f\3\2\2\2\16"+
		"\21\3\2\2\2\17\r\3\2\2\2\17\20\3\2\2\2\20\25\3\2\2\2\21\17\3\2\2\2\22"+
		"\24\5\4\3\2\23\22\3\2\2\2\24\27\3\2\2\2\25\23\3\2\2\2\25\26\3\2\2\2\26"+
		"\30\3\2\2\2\27\25\3\2\2\2\30\31\7\2\2\3\31\3\3\2\2\2\32\34\5\6\4\2\33"+
		"\35\7\17\2\2\34\33\3\2\2\2\35\36\3\2\2\2\36\34\3\2\2\2\36\37\3\2\2\2\37"+
		"\'\3\2\2\2 \"\5\b\5\2!#\7\17\2\2\"!\3\2\2\2#$\3\2\2\2$\"\3\2\2\2$%\3\2"+
		"\2\2%\'\3\2\2\2&\32\3\2\2\2& \3\2\2\2\'\5\3\2\2\2()\b\4\1\2)g\7\r\2\2"+
		"*\63\7\13\2\2+\60\5\n\6\2,-\7\3\2\2-/\5\n\6\2.,\3\2\2\2/\62\3\2\2\2\60"+
		".\3\2\2\2\60\61\3\2\2\2\61\64\3\2\2\2\62\60\3\2\2\2\63+\3\2\2\2\63\64"+
		"\3\2\2\2\64\65\3\2\2\2\65A\7\f\2\2\66;\5\n\6\2\678\7\3\2\28:\5\n\6\29"+
		"\67\3\2\2\2:=\3\2\2\2;9\3\2\2\2;<\3\2\2\2<?\3\2\2\2=;\3\2\2\2>\66\3\2"+
		"\2\2>?\3\2\2\2?A\3\2\2\2@*\3\2\2\2@>\3\2\2\2AB\3\2\2\2BC\7\4\2\2Cg\5\6"+
		"\4\7DM\7\5\2\2EJ\5\n\6\2FG\7\3\2\2GI\5\n\6\2HF\3\2\2\2IL\3\2\2\2JH\3\2"+
		"\2\2JK\3\2\2\2KN\3\2\2\2LJ\3\2\2\2ME\3\2\2\2MN\3\2\2\2NO\3\2\2\2OP\7\6"+
		"\2\2Pg\5\6\4\6QR\7\17\2\2RV\7\20\2\2SU\7\17\2\2TS\3\2\2\2UX\3\2\2\2VT"+
		"\3\2\2\2VW\3\2\2\2WZ\3\2\2\2XV\3\2\2\2Y[\5\4\3\2ZY\3\2\2\2[\\\3\2\2\2"+
		"\\Z\3\2\2\2\\]\3\2\2\2]^\3\2\2\2^_\7\21\2\2_g\3\2\2\2`a\7\13\2\2ab\5\6"+
		"\4\2bc\7\f\2\2cg\3\2\2\2de\7\7\2\2eg\5\6\4\3f(\3\2\2\2f@\3\2\2\2fD\3\2"+
		"\2\2fQ\3\2\2\2f`\3\2\2\2fd\3\2\2\2g\u0080\3\2\2\2hi\f\t\2\2ir\7\13\2\2"+
		"jo\5\6\4\2kl\7\3\2\2ln\5\6\4\2mk\3\2\2\2nq\3\2\2\2om\3\2\2\2op\3\2\2\2"+
		"ps\3\2\2\2qo\3\2\2\2rj\3\2\2\2rs\3\2\2\2st\3\2\2\2t\177\7\f\2\2uv\f\b"+
		"\2\2v{\5\6\4\2wx\7\3\2\2xz\5\6\4\2yw\3\2\2\2z}\3\2\2\2{y\3\2\2\2{|\3\2"+
		"\2\2|\177\3\2\2\2}{\3\2\2\2~h\3\2\2\2~u\3\2\2\2\177\u0082\3\2\2\2\u0080"+
		"~\3\2\2\2\u0080\u0081\3\2\2\2\u0081\7\3\2\2\2\u0082\u0080\3\2\2\2\u0083"+
		"\u0084\5\n\6\2\u0084\u0085\7\b\2\2\u0085\u0086\5\6\4\2\u0086\t\3\2\2\2"+
		"\u0087\u0088\t\2\2\2\u0088\u008b\7\r\2\2\u0089\u008b\7\r\2\2\u008a\u0087"+
		"\3\2\2\2\u008a\u0089\3\2\2\2\u008b\13\3\2\2\2\27\17\25\36$&\60\63;>@J"+
		"MV\\for{~\u0080\u008a";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}