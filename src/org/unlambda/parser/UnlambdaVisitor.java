// Generated from Unlambda.g4 by ANTLR 4.7.2
package org.unlambda.parser;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link UnlambdaParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface UnlambdaVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link UnlambdaParser#file}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFile(UnlambdaParser.FileContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exprstmt}
	 * labeled alternative in {@link UnlambdaParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprstmt(UnlambdaParser.ExprstmtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nullstmt}
	 * labeled alternative in {@link UnlambdaParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNullstmt(UnlambdaParser.NullstmtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code call}
	 * labeled alternative in {@link UnlambdaParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCall(UnlambdaParser.CallContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parens}
	 * labeled alternative in {@link UnlambdaParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParens(UnlambdaParser.ParensContext ctx);
	/**
	 * Visit a parse tree produced by the {@code access}
	 * labeled alternative in {@link UnlambdaParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAccess(UnlambdaParser.AccessContext ctx);
	/**
	 * Visit a parse tree produced by the {@code define}
	 * labeled alternative in {@link UnlambdaParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefine(UnlambdaParser.DefineContext ctx);
	/**
	 * Visit a parse tree produced by the {@code block}
	 * labeled alternative in {@link UnlambdaParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(UnlambdaParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by the {@code return}
	 * labeled alternative in {@link UnlambdaParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturn(UnlambdaParser.ReturnContext ctx);
	/**
	 * Visit a parse tree produced by the {@code create}
	 * labeled alternative in {@link UnlambdaParser#nullexp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreate(UnlambdaParser.CreateContext ctx);
	/**
	 * Visit a parse tree produced by the {@code lazyy}
	 * labeled alternative in {@link UnlambdaParser#lazyname}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLazyy(UnlambdaParser.LazyyContext ctx);
	/**
	 * Visit a parse tree produced by the {@code lazyn}
	 * labeled alternative in {@link UnlambdaParser#lazyname}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLazyn(UnlambdaParser.LazynContext ctx);
}