// Generated from Unlambda.g4 by ANTLR 4.7.2
package org.unlambda.parser;

import java.util.Stack;
import java.util.LinkedList;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class UnlambdaLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, LPAREN=9, 
		RPAREN=10, NAME=11, WS=12, NEWLINE=13;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "LPAREN", 
			"RPAREN", "NAME", "WS", "NEWLINE", "NL"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "','", "'->'", "'\\'", "'.'", "'return'", "':='", "'=>'", "'|'", 
			"'('", "')'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, "LPAREN", "RPAREN", 
			"NAME", "WS", "NEWLINE"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	    private int opening = 0;
	    private Stack<Integer> indents = new Stack<Integer>();
	  
	  	private Token lastToken = null;
		private LinkedList<Token> tokens = new LinkedList<>();
	    @Override
	    public void emit(Token t) {
	        super.setToken(t);
	        tokens.offer(t);
	    }
	  
	    @Override
	    public Token nextToken() {
	        if (_input.LA(1) == EOF && !this.indents.isEmpty()) {
	            // Remove any trailing EOF tokens from our buffer.
	            for (int i = tokens.size() - 1; i >= 0; i--) {
	                if (tokens.get(i).getType() == EOF) {
	                    tokens.remove(i);
	                }
	            }
	    
	            this.emit(commonToken(UnlambdaParser.NEWLINE, "\n"));
	    
	            while (!indents.isEmpty()) {
	                dedent();
	                indents.pop();
	            }
	    
	            this.emit(commonToken(UnlambdaParser.EOF, "<EOF>"));
	        }
	    
	        Token next = super.nextToken();
	    
	        if (next.getChannel() == Token.DEFAULT_CHANNEL) {
	            this.lastToken = next;
	        }
	    
	        return tokens.isEmpty() ? next : tokens.poll();
	    }
	  
	    private void dedent() {
	        CommonToken dedent = commonToken(UnlambdaParser.DEDENT, "\n");
	        dedent.setLine(this.lastToken.getLine());
	        emit(dedent);
	        emit(commonToken(NEWLINE, "\n"));
	    }
	  
	    private CommonToken commonToken(int type, String text) {
	        int stop = this.getCharIndex() - 1;
	        int start = text.isEmpty() ? stop : stop - text.length() + 1;
	        return new CommonToken(this._tokenFactorySourcePair, type, DEFAULT_TOKEN_CHANNEL, start, stop);
	    }
	 
	    static int getIndentationCount(String spaces) {
	        int count = 0;
	        for (char ch : spaces.toCharArray()) {
	            switch (ch) {
	                case '\t':
	                    count += 4;
	                    break;
	                default:
	                    count++;
	            }
	        }
	    
	        return count;
	    }


	public UnlambdaLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Unlambda.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 8:
			LPAREN_action((RuleContext)_localctx, actionIndex);
			break;
		case 9:
			RPAREN_action((RuleContext)_localctx, actionIndex);
			break;
		case 12:
			NEWLINE_action((RuleContext)_localctx, actionIndex);
			break;
		}
	}
	private void LPAREN_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0:
			opening++;
			break;
		}
	}
	private void RPAREN_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 1:
			opening--;
			break;
		}
	}
	private void NEWLINE_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 2:

				String spaces = getText().replaceAll("[\r\n\f]+", "");
				int next = _input.LA(1);
				if(opening > 0 || next == '\r' || next == '\n' || next == '\f' || next == '#') {
			        skip();
				} else {
			        emit(commonToken(NEWLINE, "\n"));
					int indent = getIndentationCount(spaces);
					int previous = indents.isEmpty() ? 0 : indents.peek();
					if(indent != previous) {
						if(indent > previous) {
							indents.push(indent);
							emit(commonToken(UnlambdaParser.INDENT, spaces));
						} else if(indent < previous) {
							while(!indents.isEmpty() && indents.peek() > indent) {
			                    dedent();
								indents.pop();
							}
						}
					}
				}

			break;
		}
	}
	@Override
	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 11:
			return WS_sempred((RuleContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean WS_sempred(RuleContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return opening > 0;
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\17u\b\1\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\3\2\3\2\3\3\3\3\3\3\3\4\3\4\3"+
		"\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\b\3\b\3\b\3\t\3\t\3\n"+
		"\3\n\3\n\3\13\3\13\3\13\3\f\3\f\7\f@\n\f\f\f\16\fC\13\f\3\f\3\f\3\f\3"+
		"\f\6\fI\n\f\r\f\16\fJ\3\f\5\fN\n\f\3\r\6\rQ\n\r\r\r\16\rR\3\r\3\r\3\r"+
		"\3\r\3\r\3\r\7\r[\n\r\f\r\16\r^\13\r\3\r\3\r\5\rb\n\r\3\r\3\r\3\16\3\16"+
		"\7\16h\n\16\f\16\16\16k\13\16\3\16\3\16\3\17\5\17p\n\17\3\17\3\17\5\17"+
		"t\n\17\2\2\20\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16"+
		"\33\17\35\2\3\2\7\6\2&&C\\aac|\7\2&&\62;C\\aac|\3\2bb\4\2\13\13\"\"\4"+
		"\2\f\f\16\17\2\177\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13"+
		"\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2"+
		"\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\3\37\3\2\2\2\5!\3\2\2\2\7$\3"+
		"\2\2\2\t&\3\2\2\2\13(\3\2\2\2\r/\3\2\2\2\17\62\3\2\2\2\21\65\3\2\2\2\23"+
		"\67\3\2\2\2\25:\3\2\2\2\27M\3\2\2\2\31a\3\2\2\2\33e\3\2\2\2\35s\3\2\2"+
		"\2\37 \7.\2\2 \4\3\2\2\2!\"\7/\2\2\"#\7@\2\2#\6\3\2\2\2$%\7^\2\2%\b\3"+
		"\2\2\2&\'\7\60\2\2\'\n\3\2\2\2()\7t\2\2)*\7g\2\2*+\7v\2\2+,\7w\2\2,-\7"+
		"t\2\2-.\7p\2\2.\f\3\2\2\2/\60\7<\2\2\60\61\7?\2\2\61\16\3\2\2\2\62\63"+
		"\7?\2\2\63\64\7@\2\2\64\20\3\2\2\2\65\66\7~\2\2\66\22\3\2\2\2\678\7*\2"+
		"\289\b\n\2\29\24\3\2\2\2:;\7+\2\2;<\b\13\3\2<\26\3\2\2\2=A\t\2\2\2>@\t"+
		"\3\2\2?>\3\2\2\2@C\3\2\2\2A?\3\2\2\2AB\3\2\2\2BN\3\2\2\2CA\3\2\2\2DH\7"+
		"b\2\2EI\n\4\2\2FG\7^\2\2GI\13\2\2\2HE\3\2\2\2HF\3\2\2\2IJ\3\2\2\2JH\3"+
		"\2\2\2JK\3\2\2\2KL\3\2\2\2LN\7b\2\2M=\3\2\2\2MD\3\2\2\2N\30\3\2\2\2OQ"+
		"\t\5\2\2PO\3\2\2\2QR\3\2\2\2RP\3\2\2\2RS\3\2\2\2Sb\3\2\2\2TU\7^\2\2UV"+
		"\5\31\r\2VW\5\35\17\2Wb\3\2\2\2X\\\7%\2\2Y[\n\6\2\2ZY\3\2\2\2[^\3\2\2"+
		"\2\\Z\3\2\2\2\\]\3\2\2\2]b\3\2\2\2^\\\3\2\2\2_`\6\r\2\2`b\5\35\17\2aP"+
		"\3\2\2\2aT\3\2\2\2aX\3\2\2\2a_\3\2\2\2bc\3\2\2\2cd\b\r\4\2d\32\3\2\2\2"+
		"ei\5\35\17\2fh\t\5\2\2gf\3\2\2\2hk\3\2\2\2ig\3\2\2\2ij\3\2\2\2jl\3\2\2"+
		"\2ki\3\2\2\2lm\b\16\5\2m\34\3\2\2\2np\7\17\2\2on\3\2\2\2op\3\2\2\2pq\3"+
		"\2\2\2qt\7\f\2\2rt\4\16\17\2so\3\2\2\2sr\3\2\2\2t\36\3\2\2\2\r\2AHJMR"+
		"\\aios\6\3\n\2\3\13\3\b\2\2\3\16\4";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}