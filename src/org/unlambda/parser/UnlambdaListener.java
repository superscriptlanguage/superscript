// Generated from Unlambda.g4 by ANTLR 4.7.2
package org.unlambda.parser;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link UnlambdaParser}.
 */
public interface UnlambdaListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link UnlambdaParser#file}.
	 * @param ctx the parse tree
	 */
	void enterFile(UnlambdaParser.FileContext ctx);
	/**
	 * Exit a parse tree produced by {@link UnlambdaParser#file}.
	 * @param ctx the parse tree
	 */
	void exitFile(UnlambdaParser.FileContext ctx);
	/**
	 * Enter a parse tree produced by the {@code exprstmt}
	 * labeled alternative in {@link UnlambdaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterExprstmt(UnlambdaParser.ExprstmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code exprstmt}
	 * labeled alternative in {@link UnlambdaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitExprstmt(UnlambdaParser.ExprstmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nullstmt}
	 * labeled alternative in {@link UnlambdaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterNullstmt(UnlambdaParser.NullstmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nullstmt}
	 * labeled alternative in {@link UnlambdaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitNullstmt(UnlambdaParser.NullstmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code call}
	 * labeled alternative in {@link UnlambdaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterCall(UnlambdaParser.CallContext ctx);
	/**
	 * Exit a parse tree produced by the {@code call}
	 * labeled alternative in {@link UnlambdaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitCall(UnlambdaParser.CallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parens}
	 * labeled alternative in {@link UnlambdaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterParens(UnlambdaParser.ParensContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parens}
	 * labeled alternative in {@link UnlambdaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitParens(UnlambdaParser.ParensContext ctx);
	/**
	 * Enter a parse tree produced by the {@code access}
	 * labeled alternative in {@link UnlambdaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAccess(UnlambdaParser.AccessContext ctx);
	/**
	 * Exit a parse tree produced by the {@code access}
	 * labeled alternative in {@link UnlambdaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAccess(UnlambdaParser.AccessContext ctx);
	/**
	 * Enter a parse tree produced by the {@code define}
	 * labeled alternative in {@link UnlambdaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterDefine(UnlambdaParser.DefineContext ctx);
	/**
	 * Exit a parse tree produced by the {@code define}
	 * labeled alternative in {@link UnlambdaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitDefine(UnlambdaParser.DefineContext ctx);
	/**
	 * Enter a parse tree produced by the {@code block}
	 * labeled alternative in {@link UnlambdaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBlock(UnlambdaParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by the {@code block}
	 * labeled alternative in {@link UnlambdaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBlock(UnlambdaParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by the {@code return}
	 * labeled alternative in {@link UnlambdaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterReturn(UnlambdaParser.ReturnContext ctx);
	/**
	 * Exit a parse tree produced by the {@code return}
	 * labeled alternative in {@link UnlambdaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitReturn(UnlambdaParser.ReturnContext ctx);
	/**
	 * Enter a parse tree produced by the {@code create}
	 * labeled alternative in {@link UnlambdaParser#nullexp}.
	 * @param ctx the parse tree
	 */
	void enterCreate(UnlambdaParser.CreateContext ctx);
	/**
	 * Exit a parse tree produced by the {@code create}
	 * labeled alternative in {@link UnlambdaParser#nullexp}.
	 * @param ctx the parse tree
	 */
	void exitCreate(UnlambdaParser.CreateContext ctx);
	/**
	 * Enter a parse tree produced by the {@code lazyy}
	 * labeled alternative in {@link UnlambdaParser#lazyname}.
	 * @param ctx the parse tree
	 */
	void enterLazyy(UnlambdaParser.LazyyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code lazyy}
	 * labeled alternative in {@link UnlambdaParser#lazyname}.
	 * @param ctx the parse tree
	 */
	void exitLazyy(UnlambdaParser.LazyyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code lazyn}
	 * labeled alternative in {@link UnlambdaParser#lazyname}.
	 * @param ctx the parse tree
	 */
	void enterLazyn(UnlambdaParser.LazynContext ctx);
	/**
	 * Exit a parse tree produced by the {@code lazyn}
	 * labeled alternative in {@link UnlambdaParser#lazyname}.
	 * @param ctx the parse tree
	 */
	void exitLazyn(UnlambdaParser.LazynContext ctx);
}