package org.superscript.template

import Template._
import scala.util.control.TailCalls._
import scala.language.implicitConversions
import scala.collection.immutable.Stream
type Template = Seq[Extension] => TailRec[String]

object Template {
    type Extension = PartialFunction[(String, () => TailRec[String], String), TailRec[String]]
    val bif = """(?s)^(.*)(?!>\\)>(-?\d*)$""".r
    val bmerge = """(?s)^(.*)(?!>\\)@$""".r
    def folder(before: String, arg: Template, after: String, extensions: Seq[Extension]): TailRec[String] = {
        (before, after) match {
            case (bif(start, question), _) => if(question.toInt > 0) tailcall(arg(extensions)).flatMap((x) => done(start + x + after))
                else done(start + after)
            case (bmerge(start), _) => tailcall(arg(extensions)).flatMap((x) => done(start + x + after))
            case _ => {
                def loop(extensions: Seq[Extension]): TailRec[String] = {
                    val e = extensions.head
                    if(e.isDefinedAt((before, () => tailcall(arg(extensions)), after)))
                        e(before, () => tailcall(arg(extensions)), after)
                    else loop(extensions.tail) 
                }
                loop(extensions)
            }
        }
    }
    def (sc: StringContext) $(args: => Template*): Template = (extensions) => {
        sc.checkLengths(args)
        var result = done(sc.parts(0))
        args.zipWithIndex.foreach((arg, i) => {
            result = result.flatMap((x) => folder(x, arg, sc.parts(i + 1), extensions))
        })
        result
    }
    private val emptyTemplate: Template = (e) => done("")
    def empty(x: => Unit = ()): Template = {
        x
        emptyTemplate
    }
    def >(x: => Any = ""): Template = (e) => done(x.toString)

    def (seq: Seq[Template]) toTemplate(separator: Template = empty()) = {
        if(separator == empty()) seq.toPrefixTemplate() else {
            var isFirst = true
            seq.foldLeft(empty())((old, a) => {
                if(isFirst) {
                    isFirst = false
                    $"@$a"
                } else {
                    $"@$old@$separator@$a"
                }
            })
        }
    }

    def (seq: Seq[Template]) toPrefixTemplate(separator: Template = empty()) = seq.foldLeft(empty())((old, a) => $"@$old@$separator@$a")

    def (seq: Seq[Template]) toPostfixTemplate(separator: Template = empty()) = seq.foldLeft(empty())((old, a) => $"@$old@$a@$separator")

    def (first: => Template) + (second: => Template) = $"@$first@$second"

    def (first: => Template) * (second: Int) = Stream.fill(second)(first).toTemplate()
}
object Extensions {
    val anyMerge: Extension = {
        case (start, arg, after) => arg().flatMap((x) => done(start + x + after))
    }
    object Conversions {
        implicit def boolean(x: Boolean): Template = (e) => x match {
            case true => done("1")
            case false => done("0")
        }
        implicit def any(x: Any): Template = (e) => done(x.toString)
    }
}