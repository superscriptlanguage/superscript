package org.superscript.template

import org.superscript.Lazy

object LazyString {
    type LazyString = Lazy[String]

    def (sc: StringContext) l(args: LazyString*): LazyString = {
        Lazy {
            val strings = args.map(x => x.get)
            sc.s(strings: _*)
        }
    }

    val empty: LazyString = l""

    def (x: Seq[LazyString]) toLS(y: LazyString = empty): LazyString = {
        var first = true
        x.foldLeft(empty)((old, a) => {
            if(first) {
                first = false
                a
            } else {
                old + y + a
            }
        })
    }

    def (x: Seq[LazyString]) toPrefixLS(y: LazyString = empty): LazyString = if(x.isEmpty) empty else y + x.toLS(y)

    def (x: Seq[LazyString]) toPostfixLS(y: LazyString = empty): LazyString = if(x.isEmpty) empty else x.toLS(y) + y

    def (x: LazyString) + (y: LazyString) = l"$x$y"
}