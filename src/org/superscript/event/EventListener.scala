package org.superscript.event

trait EventListener[T] {
    def apply(value: Event[T]): Unit
}