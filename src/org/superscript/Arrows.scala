package org.superscript

object Arrows {

    abstract class Arrow[-F <: Tuple, +G <: Tuple] extends (F => G) {
        def compose[H <: Tuple](x: G ~~> H): F ~~> H = f => x(this(f))

        def andThen[H <: Tuple](x: H ~~> F): H ~~> G = x.compose(this)

        def >>>[H <: Tuple](x: G ~~> H): F ~~> H = compose(x)

        def <<<[H<: Tuple](x: H ~~> F): H ~~> G = andThen(x)
    }

    object Arrow {
        given given_1[F <: Tuple, G <: Tuple]: Conversion[F => G, F ~~> G] {
            def apply(x: F => G): F ~~> G = Arrow(x)
        }

        def apply[F <: Tuple, G <: Tuple](x: F => G): F ~~> G = new Arrow[F, G] {
            def apply(f: F): G = x(f)
        }
    }

    type ~~>[F <: Tuple, G <: Tuple] = Arrow[F, G]

}