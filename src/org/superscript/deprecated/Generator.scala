package org.superscript.deprecated.generator
//gen"foo%22${g"bar${$$"A"}baz"}A*"

import GeneratorHelper._
import scala.collection.mutable.Buffer
import scala.collection.mutable.Map

import java.io._

@deprecated("See org.superscript.template.Template", "1.0.0")
implicit class GeneratorHelper(sc: StringContext) extends AnyVal {
    def func(before: String, arg: () => String, after: String): String = {
        val beginIter = """(?s)^(.*)(?!>\\)%(-?\d*)(,-?\d*)?$""".r
        val endIter = """(?s)^(\w*)\*(.*)$""".r
        val beginAt = """(?s)^(.*)(?!>\\)@$""".r
        val ifAt = """(?s)^(.*)(?!>\\)\|(\w*)>$""".r
        (before, after) match {
            case (beginAt(start), _) => {
                start + arg() + after
            }
            case (ifAt(start, name), _) => {
                if(stack.get(name).get() > 0) {
                    start + arg() + after
                } else {
                    start + after
                }
            }
            case (beginIter(start, count, opt), endIter(name, end)) => {
                var first = 0
                var last = count.toInt
                if(opt != null) {
                    first = last
                    last = opt.drop(1).toInt
                }
                var result = ""
                var n = first
                if(name != "") stack += ((name, () => n))
                for(i <- first until last) {
                    n = i
                    result += arg()
                }
                stack -= name
                start + result + end
            }
            case _ => before + arg() + after
        }
    }
    def g(args: () => String*): () => String = () => {
        sc.checkLengths(args)
        var parts = sc.parts
        var result = parts(0)
        args.zipWithIndex.foreach((arg, i) => {
            result = func(result, arg, parts(i + 1))
        })
        result
    }
    def gen(args: () => String*): String = g(args: _*)()
    def $(args: () => String*): Int = {
        sc.checkLengths(args)
        var parts = sc.parts
        assert {
            parts.length == 1
        }
        stack.get(parts(0)).get()
    }
    def $$(args: () => String*): () => String = {
        >($(args: _*))
    }

}
object GeneratorHelper {
    val stack: Map[String, () => Int] = Map()
    def >[T](f: => T): () => String = () => f.toString
}
object Generator {
    def main(args: Array[String]) = {
        inline val base = "src/org/superscript/generated/"
        inline val end = ".scala"
        val generated: Array[Template] = Array(
            Template(
gen"""package org.superscript.function

import org.superscript._%23${g"""

trait Function${$$"A"}[%@${$$"A"}${g"-T${$$"B"}, "}B*+R] {
    def apply(%@${>($"A" - 1)}${g"v${$$"B"}: T${$$"B"}, "}B*|A>${g"v${>($"A" - 1)}: T${>($"A" - 1)}"}): R
}"""}A*

%2,23${g"""
def (f: Function${$$"A"}[%@${$$"A"}${g"T${$$"B"}, "}B*R]) tupled[%@${$$"A"}${g"T${$$"B"}, "}B*R]: Function1[(%@${>($"A" - 1)}${g"T${$$"B"}, "}B*|A>${g"T${>($"A" - 1)}"}), R] = new Function1[(%@${>($"A" - 1)}${g"T${$$"B"}, "}B*|A>${g"T${>($"A" - 1)}"}), R] {
    def apply(x: (%@${>($"A" - 1)}${g"T${$$"B"}, "}B*|A>${g"T${>($"A" - 1)}"})): R = f(%1,@${$$"A"}${g"x._${$$"B"}, "}B*x._${$$"A"})
}"""}A*

type ->[A, +R] = A match {%2,23${g"""
    case Tuple${$$"A"}[%@${>($"A" - 1)}${g"t${$$"B"}, "}B*t${>($"A" - 1)}] => Function${$$"A"}[%@${$$"A"}${g"t${$$"B"}, "}B*R]"""}A*
    case %[t0] => Function1[t0, R]
    case >|< => Function0[R]
}""",
"Functions"),
            Template(
gen"""package org.superscript.function

import org.superscript._%1,23${g"""

trait PartialFunction${$$"A"}[%@${$$"A"}${g"-T${$$"B"}, "}B*+R] {
    def apply(%@${>($"A" - 1)}${g"v${$$"B"}: T${$$"B"}, "}B*|A>${g"v${>($"A" - 1)}: T${>($"A" - 1)}"}): R
    def isDefinedAt(%@${>($"A" - 1)}${g"v${$$"B"}: T${$$"B"}, "}B*|A>${g"v${>($"A" - 1)}: T${>($"A" - 1)}"}): Boolean
}"""}A*

type -?>[A <: Tuple, +R] = A match {%2,23${g"""
    case Tuple${$$"A"}[%@${>($"A" - 1)}${g"t${$$"B"}, "}B*t${>($"A" - 1)}] => PartialFunction${$$"A"}[%@${$$"A"}${g"t${$$"B"}, "}B*R]"""}A*
    case %[t0] => Function1[t0, R]
}""",
"PartialFunctions"),
            Template(
gen"""package org.superscript.function

import org.superscript._%23${g"""

trait PolymorphicFunction${$$"A"}[%@${$$"A"}${g"-T${$$"B"}[_], "}B*+R[_]] {
    def apply[A](%@${>($"A" - 1)}${g"v${$$"B"}: T${$$"B"}[A], "}B*|A>${g"v${>($"A" - 1)}: T${>($"A" - 1)}[A]"}): R[A]
}"""}A*

type ~>[A[_], +R[_]] = >[A] match {%2,23${g"""
    case >[%@${>($"A" - 1)}${g"t${$$"B"} * "}B*t${>($"A" - 1)}] => PolymorphicFunction${$$"A"}[%@${$$"A"}${g"t${$$"B"}, "}B*R]"""}A*
    case >[$$[t0]] => PolymorphicFunction1[t0, R]
    case >[<|>] => PolymorphicFunction0[R]
}""",
"PolymorphicFunctions"),
            Template(
gen"""package org.superscript.function

import org.superscript._%1,23${g"""

trait PartialPolymorphicFunction${$$"A"}[%@${$$"A"}${g"-T${$$"B"}[_], "}B*+R[_]] {
    def apply[A](%@${>($"A" - 1)}${g"v${$$"B"}: T${$$"B"}[A], "}B*|A>${g"v${>($"A" - 1)}: T${>($"A" - 1)}[A]"}): R[A]
    def isDefinedAt[A](%@${>($"A" - 1)}${g"v${$$"B"}: T${$$"B"}[A], "}B*|A>${g"v${>($"A" - 1)}: T${>($"A" - 1)}[A]"}): Boolean
}"""}A*

type ~?>[A[_], +R[_]] = >[A] match {%2,23${g"""
    case >[%@${>($"A" - 1)}${g"t${$$"B"} * "}B*t${>($"A" - 1)}] => PartialPolymorphicFunction${$$"A"}[%@${$$"A"}${g"t${$$"B"}, "}B*R]"""}A*
    case >[$$[t0]] => PolymorphicFunction1[t0, R]
}""",
"PartialPolymorphicFunctions")
        )
/*
type ~>[A, +R[_]] = A match {%23${g"""
    case < || %@${$$"A"}${g"t${$$"B"} || "}B*> => PolymorphicFunction${$$"A"}[%@${$$"A"}${g"t${$$"B"}, "}B*R]"""}A*
}
*/
        //println(generated)
        generated.foreach(x => {
            val writer = new BufferedWriter(new FileWriter(base + x.filename + end))
            writer.write(x.contents)
            writer.close()
        })
    }
    case class Template(contents: String, filename: String)
}