package org.superscript

import PredefImplicits.given

object Predef {
    type Id[A] = A
    type Optional[+T[_]] = [X] =>> Option[T[X]]

    def (f: A => B) +> [A, B, C] (g: B => C): A => C = x => g(f(x))

    def (f: B => C) <+ [A, B, C] (g: A => B): A => C = x => f(g(x))

    def (x: A) |> [A, B] (f: A => B): B = f(x)

    def because[T](to: Boolean)(further: => T): Option[T] = if(to) Some(further) else None

    def (option: Option[T]) otherwise[T] (cont: => T) = option.getOrElse(cont)


    case class Or[F, G](first: Option[F], second: Option[G])

    type ||[F, G] = Or[F, G]


    case class And[F, G](first: F, second: G)

    type &&[F, G] = And[F, G]


    type =%=[F, G] = F =:= G && G =:= F


    abstract class IsFunction[F, A, B] {
        def toUnary(f: F): A => B

        def fromUnary(f: A => B): F
    }
}