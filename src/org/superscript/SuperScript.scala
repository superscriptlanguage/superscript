package org.superscript

import org.superscript.eventloop.EventLoop
import org.superscript.eventloop.DefaultEventLoop

trait SuperScript[T] {
    def main(args: Array[String]): Unit = {
        init()
        SuperScript.init(args)
        exports
        SuperScript.destroy()
    }
    def init(): Unit = {}
    def exports: T
}

object SuperScript {
    var eventloop: EventLoop[_, _, _] = new DefaultEventLoop(10)
    lazy val loop = eventloop
    def init(args: Array[String]): Unit = {
        loop
    }
    def destroy(): Unit = {
        loop.start()
    }
}