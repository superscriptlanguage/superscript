package org.superscript.compiler

import ast.AstVisitor
import ast.AstImpl
import parser.SuperScriptParser
import org.antlr.v4.runtime.ParserRuleContext
import java.nio.file.Files
import java.nio.file.Paths
import java.io.FileWriter
import java.nio.file.FileSystems
import java.nio.file.FileVisitResult
import java.nio.file.Path
import java.nio.file.PathMatcher
import java.nio.file.SimpleFileVisitor
import java.nio.file.attribute.BasicFileAttributes
import scala.jdk.StreamConverters._

object Compile {
    val help = """
Usage: ssc <input files> [-o <output directory>]
     | ssc <input files> [--output <output directory>]
     | ssc --help
     | ssc -h
"""

    def compute(x: String): String = (compile(AstVisitor.visit(AstVisitor.parse(x + "\n").file), "")(given AstImpl)).get

    def compfull(clean: String, x: String, prefix: String): Unit = {
        println(s"Compiling ${clean}.ss to ${prefix}/${clean}.scala")
        val writer = new FileWriter(prefix + "/" + x + ".scala")
        writer.write(compute(new String(Files.readAllBytes(Paths.get(x + ".ss")))))
        writer.close()
    }

    def glob(glob: String, location: String): List[String] =
        Files.walk(Paths.get(location)).toScala(List)
            .filter(FileSystems.getDefault().getPathMatcher(glob).matches(_))
            .map(_.toString).filter(_.endsWith(".ss")).map(_.dropRight(3))

    def main(args: Array[String]): Unit = {
        if(args.isEmpty || args(0) == "--help" || args(0) == "-h") println(help)
        val loc = "."
        var input: List[String] = Nil
        var stop = false
        var output: Option[String] = None
        args.foreach(a => if(a == "-o" || a == "--output") stop = true else if(!stop) input = a :: input else if(stop && output.isEmpty) output = Some(a))
        var outputNoption = output.getOrElse(loc)
        input.foreach(a => glob("glob:./" + a, loc).foreach(b => compfull(b.drop(2), b, outputNoption)))
    }
}