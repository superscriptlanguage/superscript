package org.superscript.compiler

import org.superscript.test.Test._
import ast.AstImpl
import AstImpl._
import ast.AstVisitor
import parser.SuperScriptParser
import org.antlr.v4.runtime.ParserRuleContext
object GeneratorTests {
    val none = NamedType(List("null"))

    def parse(x: String): SuperScriptParser = AstVisitor.parse(x + "\n")

    def parseN(x: String): SuperScriptParser = AstVisitor.parse(x)

    def visit(f: ParserRuleContext): Ast = AstVisitor.visit(f)

    def comp(ast: Ast): String = (compile(ast, "    ")(given AstImpl)).get

    def compute(x: String) = (compile(visit(parse(x).file), "")(given AstImpl)).get

    def run: Unit = {
        import DefaultEqualities._
        import TypeSafe._
        tag("The parser:") {
            tag("Can output correct Ast for a File:") {
                test {
                    assert(visit(parse("").file) === (File(Nil): Ast))
                }
            }
            tag("Can output correct Ast for an Import:") {
                test {
                    import Import._
                    assert(visit(parse("import null.first.second.third.{a, _, c => d, e => _}").stmt) === (Import(
                        List("null", "first", "second", "third"),
                        List(ImportType.Simple("a"), ImportType.Wildcard, ImportType.Rename("c", Some("d")), ImportType.Rename("e", None))): Ast))
                }
            }
            tag("Can output correct Ast for a Package:") {
                test {
                    assert(visit(parse("package first.second.third").stmt) === (Package(List("first", "second", "third")): Ast))
                }
            }
            tag("Can output correct Ast for an Expr:") {
                test {
                    assert(visit(parse("null").stmt) === (Expr(Ident("null")): Ast))
                }
            }
            tag("Can output correct Ast for a TypeDef:") {
                test {
                    import TypeBounds._
                    assert(visit(parse("type first <: null >: null : null = null").stmt) === (TypeDef("first", TypeBounds(List(BoundType.Subclass(none), BoundType.Superclass(none), BoundType.ExistsImplicit(none))), Some(none)): Ast))
                }
            }
            tag("Can output correct Ast for a ValDef:") {
                test {
                    assert(visit(parse("val first").stmt) === (ValDef(true, "first", None, None): Ast))
                    assert(visit(parse("var first = null").stmt) === (ValDef(false, "first", None, Some(Ident("null"))): Ast))
                }
            }

            //! INSERT STUFF HERE

            tag("Can output correct Ast for a TraitDef:") {
                test {
                    assert(visit(parse("trait A").definition) === (TraitDef(None, "A", List(), Block(List())): Ast))
                    assert(visit(parse("trait A\n    a\n").definition) === (TraitDef(None, "A", List(), Block(List(Expr(Ident("a"))))): Ast))
                    assert(visit(parse("trait A[b]").definition) === (TraitDef(Some(VariantTypeArgs(List((0, NamedType(List("b")),TypeBounds(List()))))), "A", List(), Block(List())): Ast))
                    assert(visit(parse("trait A extends B").definition) === (TraitDef(None, "A", List((List("B"), None)), Block(List())): Ast))
                    assert(visit(parse("trait A extends B with C[D]").definition) === (TraitDef(None, "A", List((List("B"), None), (List("C"), Some(CallTypeArgs(List(NamedType(List("D"))))))), Block(List())): Ast))
                }
            }
            tag("Can output correct Ast for an ImplicitDef:") {
                test {
                    assert(visit(parse("given as first = second").stmt) === (ImplicitDef(None, NamedType(List("first")), Ident("second")): Ast))
                    assert(visit(parse("given foo as first = second").stmt) === (ImplicitDef(Some("foo"), NamedType(List("first")), Ident("second")): Ast))
                }
            }
            tag("Can output correct Ast for a MethodDef:") {
                test {
                    assert(visit(parse("def first").definition) === (MethodDef("first", None, Nil, None, None): Ast))
                    import OptionalArgs.ArgType
                    import TypeBounds._
                    val ntb = TypeBounds(List())
                    val tbd = TypeBounds(List(BoundType.Subclass(NamedType(List("D")))))
                    val ta = TypeArgs(List((NamedType(List("A")), ntb), (NamedType(List("B")), ntb), (NamedType(List("C")), tbd)))
                    val args = List(Args(List(("x", NamedType(List("A"))), ("y", NamedType(List("B")))), ArgType.Normal), Args(List(("z", NamedType(List("C")))), ArgType.Given))
                    assert(visit(parse("    def first[A, B, C <: D](x: A, y: B) given (z: C): E = f").definition) === (MethodDef("first", Some(ta), args, Some(NamedType(List("E"))), Some(Ident("f"))): Ast))
                }
            }
            tag("Can output correct Ast for a NamedArg:") {
                test {
                    assert(visit(parse("first: null").appliable) === (NamedArg("first", Ident("null")): Ast))
                }
            }
            tag("Can output correct Ast for a Ident:") {
                test {
                    assert(visit(parse("first").expression) === (Ident("first"): Ast))
                }
            }
            tag("Can output correct Ast for a Select:") {
                test {
                    assert(visit(parse("first.second").expression) === (Select(Ident("first"), "second"): Ast))
                }
            }

            //! INSERT STUFF HERE

            tag("Can output correct Ast for a This:") {
                test {
                    assert(visit(parse("this").expression) === (This: Ast))
                }
            }
            tag("Can output correct Ast for a Super:") {
                test {
                    assert(visit(parse("super").expression) === (Super: Ast))
                }
            }
            tag("Can output correct Ast for a Typed:") {
                test {
                    assert(visit(parse("null: null").expression) === (Typed(Ident("null"), NamedType(List("null"))): Ast))
                }
            }
            tag("Can output correct Ast for an Apply:") {
                test {
                    assert(visit(parse("null()").expression) === (Apply(Ident("null"), List()): Ast))
                    assert(visit(parse("null(null)").expression) === (Apply(Ident("null"), List(Ident("null"))): Ast))
                    assert(visit(parse("null(null, null)").expression) === (Apply(Ident("null"), List(Ident("null"), Ident("null"))): Ast))
                }
            }
            tag("Can output correct Ast for a GivenApply:") {
                test {
                    assert(visit(parse("null(given null)").expression) === (GivenApply(Ident("null"), List(Ident("null"))): Ast))
                    assert(visit(parse("null(given null, null)").expression) === (GivenApply(Ident("null"), List(Ident("null"), Ident("null"))): Ast))
                }
            }
            tag("Can output correct Ast for a TypeApply") {
                test {
                    assert(visit(parse("null[null]").expression) === (TypeApply(Ident("null"), CallTypeArgs(List(NamedType(List("null"))))): Ast))
                }
            }
            tag("Can output correct Ast for a Block:") {
                test {
                    assert(visit(parse("\n    null\n    package first.second\n").expressioneof) === (Block(List(Expr(Ident("null")), Package(List("first", "second")))): Ast))
                }
            }
            tag("Can output correct Ast for a With:") {
                test {
                    assert(visit(parse("with(null) null").expression) === (With(Ident("null"), Ident("null")): Ast))
                }
            }
            tag("Can output correct Ast for an If:") {
                test {
                    assert(visit(parse("if(null) null").expression) === (If(Ident("null"), Ident("null"), None): Ast))
                    assert(visit(parse("if(null) null else null").expression) === (If(Ident("null"), Ident("null"), Some(Ident("null"))): Ast))
                }
            }
            tag("Can output correct Ast for a Function:") {
                test {
                    assert(visit(parse("() => a").expression) === (Function(OptionalArgs(List(), OptionalArgs.ArgType.Normal), Ident("a")): Ast))
                }
            }
            tag("Can output correct Ast for a Match:") {
                test {
                    assert(visit(parseN("\n    a => b\n    c => d\n").expressioneof) === (Match(List((Value("a"), Ident("b")), (Value("c"), Ident("d")))): Ast))
                }
            }
            tag("Can output correct Ast for a Try:") {
                test {
                    val m = Match(List((Value("a"), Ident("b")), (Value("c"), Ident("d"))))
                    assert(visit(parseN("try x catch\n    a => b\n    c => d\n").expression) === (Try(Ident("x"), m, None): Ast))
                    assert(visit(parseN("try x catch\n    a => b\n    c => d\nfinally y").expressioneof) === (Try(Ident("x"), m, Some(Ident("y"))): Ast))
                }
            }
            tag("Can output correct Ast for a Value:") {
                test {
                    assert(visit(parse("first").pattern) === (Value("first"): Ast))
                }
            }
            tag("Can output correct Ast for a Bind:") {
                test {
                    assert(visit(parse("first @ second").pattern) === (Bind(Value("first"), Value("second")): Ast))
                }
            }
            tag("Can output correct Ast for a Unapply:") {
                test {
                    assert(visit(parse("null(null)").pattern) === (Unapply(Ident("null"), List(Value("null"))): Ast))
                }
            }
            tag("Can output correct Ast for an Alternatives:") {
                test {
                    assert(visit(parse("null | null").pattern) === (Alternatives(List(Value("null"), Value("null"))): Ast))
                }
            }
            tag("Can output correct Ast for a TypeTest:") {
                test {
                    assert(visit(parse("null: null").pattern) === (TypeTest(Value("null"), NamedType(List("null"))): Ast))
                }
            }
            tag("Can output correct Ast for a Wildcard:") {
                test {
                    assert(visit(parse("_").pattern) === (Wildcard(): Ast))
                }
            }
            tag("Can output correct Ast for a TypeBounds:") {
                test {
                    import TypeBounds._
                    assert(visit(parse(" <: null >: null : null").typebounds) === (TypeBounds(List(BoundType.Subclass(none), BoundType.Superclass(none), BoundType.ExistsImplicit(none))): Ast))
                }
            }
            tag("Can output correct Ast for a TypeSelect:") {
                test {
                    assert(visit(parse("first / second").t) === (TypeSelect(Ident("first"), "second"): Ast))
                }
            }
            tag("Can output correct Ast for a TypeLambda:") {
                test {
                    assert(visit(parse("[+ null] =>> a").t) === (TypeLambda(VariantTypeArgs(List((+1, none, TypeBounds(List())))), NamedType(List("a"))): Ast))
                }
            }
            tag("Can output correct Ast for a SingletonType:") {
                test {
                    assert(visit(parse("x.type").t) === (SingletonType(Ident("x")): Ast))
                }
            }
            tag("Can output correct Ast for a ByNameType:") {
                test {
                    assert(visit(parse("=> x").t) === (ByNameType(NamedType(List("x"))): Ast))
                }
            }
            tag("Can output correct Ast for a MatchType:") {
                test {
                    val a = NamedType(List("a"))
                    val b = NamedType(List("b"))
                    assert(visit(parse("c match\n    a => a\n    b => b\n").t) === (MatchType(NamedType(List("c")), List((TypeValue(a), a), (TypeValue(b), b))): Ast))
                }
            }
            tag("Can output correct Ast for an OrType:") {
                test {
                    assert(visit(parse("a | b").t) === (OrType(NamedType(List("a")), NamedType(List("b"))): Ast))
                }
            }
            tag("Can output correct Ast for an AndType:") {
                test {
                    assert(visit(parse("a & b").t) === (AndType(NamedType(List("a")), NamedType(List("b"))): Ast))
                }
            }
            tag("Can output correct Ast for an AppliedType:") {
                test {
                    val args = CallTypeArgs(List(NamedType(List("b")), NamedType(List("c"))))
                    assert(visit(parse("a[b, c]").t) === (AppliedType(NamedType(List("a")), args): Ast))
                }
            }
            tag("Can output correct Ast for a NamedType:") {
                test {
                    assert(visit(parse("first.second").t) === (NamedType(List("first", "second")): Ast))
                }
            }
            tag("Can output correct Ast for an ExtractType:") {
                test {
                    assert(visit(parse("first[_, _]").tp) === (ExtractType(NamedType(List("first")), List(TypeWildcard(), TypeWildcard())): Ast))
                }
            }
            tag("Can output correct Ast for a TypeWildcard:") {
                test {
                    assert(visit(parse("_").tp) === (TypeWildcard(): Ast))
                }
            }
            tag("Can output correct Ast for a TypeValue:") {
                test {
                    assert(visit(parse("a").tp) === (TypeValue(NamedType(List("a"))): Ast))
                }
            }

            //! INSERT STUFF HERE

            tag("Can output correct Ast for an OptionalArgs:") {
                test {
                    import OptionalArgs._
                    val x = Some(NamedType(List("second")))
                    assert(visit(parse("given (first: second, third)").goptionalargs) === (OptionalArgs(List(("first", x), ("third", None)), ArgType.Given): Ast))
                    assert(visit(parse("(first: second, third)").goptionalargs) === (OptionalArgs(List(("first", x), ("third", None)), ArgType.Normal): Ast))
                }
            }
            tag("Can output correct Ast for an Args:") {
                test {
                    import OptionalArgs._
                    val x = NamedType(List("second"))
                    val y = NamedType(List("fourth"))
                    assert(visit(parse("given (first: second, third: fourth)").gargs) === (Args(List(("first", x), ("third", y)), ArgType.Given): Ast))
                    assert(visit(parse("(first: second, third: fourth)").gargs) === (Args(List(("first", x), ("third", y)), ArgType.Normal): Ast))
                }
            }
            tag("Can output correct Ast for a CallTypeArgs:") {
                test {
                    assert(visit(parse("[null, null, null]").calltypeargs) === (CallTypeArgs(List(none, none, none)): Ast))
                }
            }
            tag("Can output correct Ast for a TypeArgs:") {
                test {
                    import TypeBounds._
                    val tb1 = TypeBounds(List(BoundType.Subclass(none)))
                    val tb2 = TypeBounds(List(BoundType.Superclass(none)))
                    val tb3 = TypeBounds(List(BoundType.Subclass(none), BoundType.Superclass(none), BoundType.ExistsImplicit(none)))
                    assert(visit(parse("[null <: null, null >: null, null <: null >: null : null]").typeargs) === (TypeArgs(List((none, tb1), (none, tb2), (none, tb3))): Ast))
                }
            }
            tag("Can output correct Ast for a VariantTypeArgs:") {
                test {
                    import TypeBounds._
                    val tb1 = TypeBounds(List(BoundType.Subclass(none)))
                    val tb2 = TypeBounds(List(BoundType.Superclass(none)))
                    val tb3 = TypeBounds(List(BoundType.Subclass(none), BoundType.Superclass(none), BoundType.ExistsImplicit(none)))
                    assert(visit(parse("[+ null <: null, null >: null, - null <: null >: null : null]").varianttypeargs) === (VariantTypeArgs(List((+1, none, tb1), (0, none, tb2), (-1, none, tb3))): Ast))
                }
            }
        }
        tag("The compiler:") {
            tag("Can output correct File code:") {
                test {
                    assert(comp(File(Nil)) === "    import org.superscript._;\n    import Predef._;\n    import SuperScript.loop;")
                }
            }
            tag("Can output correct Import code:") {
                test {
                    import Import._
                    assert(comp(Import(
                        List("null", "first", "second", "third"),
                        List(ImportType.Simple("a"), ImportType.Wildcard, ImportType.Rename("c", Some("d")), ImportType.Rename("e", None))))
                    ===
                        "    import null.first.second.third.{a, _, c => d, e => _};")
                }
            }
            tag("Can output correct Package code:") {
                test {
                    assert(comp(Package(List("first", "second", "third"))) === "    package first.second.third;")
                }
            }
            tag("Can output correct Expr code:") {
                test {
                    assert(comp(Expr(null)) === "    null;")
                }
            }
            tag("Can output correct TypeDef code:") {
                test {
                    import TypeBounds._
                    assert(comp(TypeDef("first", TypeBounds(List(BoundType.Subclass(null), BoundType.Superclass(null), BoundType.ExistsImplicit(null))), Some(null))) === "    type first <: null >: null : null = null;")
                }
            }
            tag("Can output correct ValDef code:") {
                test {
                    assert(comp(ValDef(true, "first", None, None)) === "    val first;")
                    assert(comp(ValDef(false, "first", None, Some(null))) === "    var first = null;")
                }
            }
            tag("Can output correct ClassDef code:") {
                test {
                }
            }
            tag("Can output correct TraitDef code:") {
                test {
                    assert(comp(TraitDef(None, "A", List(), Block(List()))) === "    trait A {\n    }")
                    assert(comp(TraitDef(Some(VariantTypeArgs(List())), "A", List(), Block(List()))) === "    trait A[] {\n    }")
                    assert(comp(TraitDef(Some(VariantTypeArgs(List())), "A", List((List("B"), None)), Block(List()))) === "    trait A[] extends B {\n    }")
                    assert(comp(TraitDef(Some(VariantTypeArgs(List())), "A", List((List("B"), None), (List("C"), Some(CallTypeArgs(List())))), Block(List()))) === "    trait A[] extends B with C[] {\n    }")
                }
            }
            tag("Can output correct ImplicitDef code:") {
                test {
                    assert(comp(ImplicitDef(Some("first"), null, null)) === "    delegate first for null = null;")
                    assert(comp(ImplicitDef(None, null, null)) === "    delegate for null = null;")
                }
            }
            tag("Can output correct MethodDef code:") {
                test {
                    assert(comp(MethodDef("first", None, Nil, None, None)) === "    def first;")
                    import OptionalArgs.ArgType
                    import TypeBounds._
                    val ntb = TypeBounds(List())
                    val tbd = TypeBounds(List(BoundType.Subclass(NamedType(List("D")))))
                    val ta = TypeArgs(List((NamedType(List("A")), ntb), (NamedType(List("B")), ntb), (NamedType(List("C")), tbd)))
                    val args = List(Args(List(("x", NamedType(List("A"))), ("y", NamedType(List("B")))), ArgType.Normal), Args(List(("z", NamedType(List("C")))), ArgType.Given))
                    assert(comp(MethodDef("first", Some(ta), args, Some(NamedType(List("E"))), Some(Ident("f")))) === "    def first[A, B, C <: D](x: A, y: B) given (z: C): E = f;")
                }
            }
            tag("Can output correct NamedArg code:") {
                test {
                    assert(comp(NamedArg("first", null)) === "first: null")
                }
            }
            tag("Can output correct Ident code:") {
                test {
                    assert(comp(Ident("first")) === "first")
                }
            }
            tag("Can output correct Select code:") {
                test {
                    assert(comp(Select(Ident("first"), "second")) === "first.second")
                }
            }
            tag("Can output correct This code:") {
                test {
                    assert(comp(This) === "this")
                }
            }
            tag("Can output correct Super code:") {
                test {
                    assert(comp(Super) === "super")
                }
            }
            tag("Can output correct Typed code:") {
                test {
                    assert(comp(Typed(null, null)) === "null: null")
                }
            }
            tag("Can output correct Literal code:") {
                test {
                    assert(comp(Literal("f129[[[<- ")) === "f129[[[<- ")
                }
            }
            tag("Can output correct Apply code:") {
                test {
                    assert(comp(Apply(null, Seq())) === "null()")
                    assert(comp(Apply(null, Seq(null))) === "null(null)")
                    assert(comp(Apply(null, Seq(null, null))) === "null(null, null)")
                }
            }
            tag("Can output correct GivenApply code:") {
                test {
                    assert(comp(GivenApply(null, Seq(null))) === "null(given null)")
                    assert(comp(GivenApply(null, Seq(null, null))) === "null(given null, null)")
                }
            }
            tag("Can output correct TypeApply code:") {
                test {
                    assert(comp(TypeApply(null, null)) === "nullnull")
                }
            }
            tag("Can output correct Block code:") {
                test {
                    assert(comp(Block(List(Expr(null), Package(List("first", "second"))))) === "{\n        null;\n        package first.second;\n    }")
                }
            }
            tag("Can output correct With code:") {
                test {
                    assert(comp(With(null, null)) === "    {\n        val __0_with = null;\n        import __0_with._;\nnull\n    };")
                }
            }
            tag("Can output correct If code:") {
                test {
                    assert(comp(If(null, null, None)) === "if(null) null")
                    assert(comp(If(null, null, Some(null))) === "if(null) null else null")
                }
            }
            tag("Can output correct Function code:") {
                test {
                    assert(comp(Function(null, null)) === "null => null")
                }
            }
            tag("Can output correct Match code:") {
                test {
                    assert(comp(Match(List((null, null), (null, null)))) === "{\n        null => null\n        null => null\n    }")
                }
            }
            tag("Can output correct Try code:") {
                test {
                    assert(comp(Try(null, Match(List((null, null), (null, null))), None)) === "try null catch {\n        null => null\n        null => null\n    }")
                    assert(comp(Try(null, Match(List((null, null), (null, null))), Some(null))) === "try null catch {\n        null => null\n        null => null\n    } finally null")
                }
            }
            tag("Can output correct Value code:") {
                test {
                    assert(comp(Value("null")) === "null")
                }
            }
            tag("Can output correct Bind code:") {
                test {
                    assert(comp(Bind(null, null)) === "null @ null")
                }
            }
            tag("Can output correct Unapply code:") {
                test {
                    assert(comp(Unapply(null, List(null))) === "null(null)")
                }
            }
            tag("Can output correct Alternatives code:") {
                test {
                    assert(comp(Alternatives(List(null))) === "null")
                }
            }
            tag("Can output correct TypeTest code:") {
                test {
                    assert(comp(TypeTest(null, null)) === "null: null")
                }
            }
            tag("Can output correct Wildcard code:") {
                test {
                    assert(comp(Wildcard()) === "_")
                }
            }

            //! INSERT STUFF HERE

            tag("Can output correct TypeBounds code:") {
                test {
                    import TypeBounds._
                    assert(comp(TypeBounds(List(BoundType.Subclass(null), BoundType.Superclass(null), BoundType.ExistsImplicit(null)))) === " <: null >: null : null")
                }
            }
            tag("Can output correct TypeSelect code:") {
                test {
                    assert(comp(TypeSelect(Ident("null"), "first")) === "null#first")
                }
            }
            tag("Can output correct TypeLambda code:") {
                test {
                    assert(comp(TypeLambda(null, null)) === "null =>> null")
                }
            }
            tag("Can output correct SingletonType code:") {
                test {
                    assert(comp(SingletonType(null)) === "null.type")
                }
            }
            tag("Can output correct ByNameType code:") {
                test {
                    assert(comp(ByNameType(null)) === "=> null")
                }
            }
            tag("Can output correct MatchType code:") {
                test {
                    val a = NamedType(List("a"))
                    val b = NamedType(List("b"))
                    assert(comp(MatchType(NamedType(List("c")), List((TypeValue(a), a), (TypeValue(b), b)))) === "c match {\n        a => a\n        b => b\n    }")
                }
            }
            tag("Can output correct OrType code:") {
                test {
                    assert(comp(OrType(null, null)) === "null | null")
                }
            }
            tag("Can output correct AndType code:") {
                test {
                    assert(comp(AndType(null, null)) === "null & null")
                }
            }
            tag("Can output correct AppliedType code:") {
                test {
                    assert(comp(AppliedType(null, null)) === "nullnull")
                }
            }
            tag("Can output correct NamedType code:") {
                test {
                    assert(comp(NamedType(List("first", "second", "third"))) === "first.second.third")
                }
            }
            tag("Can output correct ExtractType code:") {
                test {
                    assert(comp(ExtractType(null, List(null, null))) === "null[null, null]")
                }
            }
            tag("Can output correct TypeWildcard code:") {
                test {
                    assert(comp(TypeWildcard()) === "_")
                }
            }
            tag("Can output correct TypeValue code:") {
                test {
                    assert(comp(TypeValue(null)) === "null")
                }
            }

            //! INSERT STUFF HERE

            tag("Can output correct OptionalArgs code:") {
                test {
                    import OptionalArgs._
                    val x = Some(NamedType(List("second")))
                    assert(comp(OptionalArgs(List(("first", x), ("third", None)), ArgType.Given)) === " given (first: second, third)")
                    assert(comp(OptionalArgs(List(("first", x), ("third", None)), ArgType.Normal)) === "(first: second, third)")
                }
            }
            tag("Can output correct CallTypeArgs code:") {
                test {
                    assert(comp(CallTypeArgs(List(null, null, null))) === "[null, null, null]")
                }
            }
            tag("Can output correct TypeArgs code:") {
                test {
                    import TypeBounds._
                    val tb1 = TypeBounds(List(BoundType.Subclass(null)))
                    val tb2 = TypeBounds(List(BoundType.Superclass(null)))
                    val tb3 = TypeBounds(List(BoundType.Subclass(null), BoundType.Superclass(null), BoundType.ExistsImplicit(null)))
                    assert(comp(TypeArgs(List((null, tb1), (null, tb2), (null, tb3)))) === "[null <: null, null >: null, null <: null >: null : null]")
                }
            }
            tag("Can output correct VariantTypeArgs code:") {
                test {
                    import TypeBounds._
                    val tb1 = TypeBounds(List(BoundType.Subclass(null)))
                    val tb2 = TypeBounds(List(BoundType.Superclass(null)))
                    val tb3 = TypeBounds(List(BoundType.Subclass(null), BoundType.Superclass(null), BoundType.ExistsImplicit(null)))
                    assert(comp(VariantTypeArgs(List((+1, null, tb1), (0, null, tb2), (-1, null, tb3)))) === "[+null <: null, null >: null, -null <: null >: null : null]")
                }
            }
        }
    }

    def main(args: Array[String]): Unit = {
        run
        val file = """
class TableSearch[F, G <: Tuple]
    val x: F

    def equals(that: Any): Boolean = if (that.isInstanceOf[TableSearch]) that.asInstanceOf[TableSearch].x.`==`(x) else false

class TableNode[F, G <: Tuple] extends TableSearch[F, G]
    val others: G

def createTableSearch[F, G <: Tuple](a: F): TableSearch[F, G] = new TableSearch[F, G]
    val x: F = a

(1 + 2)
"""
        System.out.println(visit(parse(file).file))
        System.out.println(compute(file))
    }
}