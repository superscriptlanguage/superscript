package org.superscript.compiler.ast

given AstLike = AstImpl

object AstImpl extends AstLike {
    import AstLike._

    sealed abstract class Ast

    protected def isInstanceOfAst(x: Any): Boolean = x.isInstanceOf[Ast]

    protected val getAstOps: AstOps = new AstOps {

    }



    sealed class File(val a: Seq[Statement]) extends Ast {
        override def equals(x: Any): Boolean = x match {
            case that: File => a == that.a
            case _ => false
        }

        override def toString: String = s"File(${a})"
    }

    protected def isInstanceOfFile(x: Any): Boolean = x.isInstanceOf[File]

    protected val getFileOps: FileOps = new FileOps {
        def (x: File) contains: Seq[Statement] = x.a
    }

    protected def toFile(a: Seq[Statement]) = new File(a)



    abstract class Statement extends Ast

    protected def isInstanceOfStatement(x: Any): Boolean = x.isInstanceOf[Statement]

    protected val getStatementOps: StatementOps = new StatementOps {

    }



    import Import.ImportType

    sealed class Import(val a: Seq[String], val b: Seq[ImportType]) extends Statement {
        override def equals(x: Any): Boolean = x match {
            case that: Import => a == that.a && b == that.b
            case _ => false
        }

        override def toString: String = s"Import(${a}, ${b})"
    }

    protected def isInstanceOfImport(x: Any): Boolean = x.isInstanceOf[Import]

    protected val getImportOps: ImportOps = new ImportOps {
        def (x: Import) tail: Seq[String] = x.a
        def (x: Import) head: Seq[ImportType] = x.b
    }

    protected def toImport(a: Seq[String], b: Seq[ImportType]) = new Import(a, b)



    sealed class Package(val a: Seq[String]) extends Statement {
        override def equals(x: Any): Boolean = x match {
            case that: Package => a == that.a
            case _ => false
        }

        override def toString: String = s"Package(${a})"
    }

    protected def isInstanceOfPackage(x: Any): Boolean = x.isInstanceOf[Package]

    protected val getPackageOps: PackageOps = new PackageOps {
        def (x: Package) path: Seq[String] = x.a
    }

    protected def toPackage(a: Seq[String]) = new Package(a)


    sealed class Expr(val a: Expression) extends Statement {
        override def equals(x: Any): Boolean = x match {
            case that: Expr => a == that.a
            case _ => false
        }

        override def toString: String = s"Expr(${a})"
    }

    protected def isInstanceOfExpr(x: Any): Boolean = x.isInstanceOf[Expr]

    protected val getExprOps: ExprOps = new ExprOps {
        def (x: Expr) expression: Expression = x.a
    }

    protected def toExpr(a: Expression) = new Expr(a)



    sealed abstract class Definition extends Statement

    protected def isInstanceOfDefinition(x: Any): Boolean = x.isInstanceOf[Definition]

    protected def getDefinitionOps: DefinitionOps = new DefinitionOps {

    }



    sealed class TypeDef(val a: String, val b: TypeBounds, val c: Option[Type]) extends Definition {
        override def equals(x: Any): Boolean = x match {
            case that: TypeDef => a == that.a && b == that.b && c == that.c
            case _ => false
        }

        override def toString: String = s"TypeDef(${a}, ${b}, ${c})"
    }

    protected def isInstanceOfTypeDef(x: Any): Boolean = x.isInstanceOf[TypeDef]

    protected def getTypeDefOps: TypeDefOps = new TypeDefOps {
        def (x: TypeDef) name: String = x.a
        def (x: TypeDef) bounds: TypeBounds = x.b
        def (x: TypeDef) t: Option[Type] = x.c
    }

    protected def toTypeDef(a: String, b: TypeBounds, c: Option[Type]) = new TypeDef(a, b, c)



    sealed class ValDef(val a: Boolean, val b: String, val c: Option[Type], val d: Option[Expression]) extends Definition {
        override def equals(x: Any): Boolean = x match {
            case that: ValDef => a == that.a && b == that.b && c == that.c && d == that.d
            case _ => false
        }

        override def toString: String = s"ValDef(${a}, ${b}, ${c})"
    }

    protected def isInstanceOfValDef(x: Any): Boolean = x.isInstanceOf[ValDef]

    protected def getValDefOps: ValDefOps = new ValDefOps {
        def (x: ValDef) constant: Boolean = x.a
        def (x: ValDef) name: String = x.b
        def (x: ValDef) t: Option[Type] = x.c
        def (x: ValDef) value: Option[Expression] = x.d
    }

    protected def toValDef(a: Boolean, b: String, c: Option[Type], d: Option[Expression]) = new ValDef(a, b, c, d)



    sealed class ClassDef(val a: Option[VariantTypeArgs], val b: String, val c: Seq[(Seq[String], Option[CallTypeArgs])], val d: Block) extends Definition

    protected def isInstanceOfClassDef(x: Any): Boolean = x.isInstanceOf[ClassDef]

    protected def getClassDefOps: ClassDefOps = new ClassDefOps {
        def (x: ClassDef) types: Option[VariantTypeArgs] = x.a

        def (x: ClassDef) name: String = x.b

        def (x: ClassDef) higher: Seq[(Seq[String], Option[CallTypeArgs])] = x.c

        def (x: ClassDef) inner: Block = x.d
    }

    protected def toClassDef(a: Option[VariantTypeArgs], b: String, c: Seq[(Seq[String], Option[CallTypeArgs])], d: Block): ClassDef = new ClassDef(a, b, c, d)



    sealed class ObjectDef(val a: String, val b: Seq[(Seq[String], Option[CallTypeArgs])], val c: Block, val d: Expression) extends Definition

    protected def isInstanceOfObjectDef(x: Any): Boolean = x.isInstanceOf[ObjectDef]

    protected def getObjectDefOps: ObjectDefOps = new ObjectDefOps {
        def (x: ObjectDef) name: String = x.a

        def (x: ObjectDef) higher: Seq[(Seq[String], Option[CallTypeArgs])] = x.b

        def (x: ObjectDef) inner: Block = x.c

        def (x: ObjectDef) proto: Expression = x.d
    }

    protected def toObjectDef(a: String, b: Seq[(Seq[String], Option[CallTypeArgs])], c: Block, d: Expression): ObjectDef = new ObjectDef(a, b, c, d)



    sealed class TraitDef(val a: Option[VariantTypeArgs], val b: String, val c: Seq[(Seq[String], Option[CallTypeArgs])], val d: Block) extends Definition {
        override def equals(x: Any): Boolean = x match {
            case that: TraitDef => a == that.a && b == that.b && c == that.c && d == that.d
            case _ => false
        }

        override def toString: String = s"TraitDef(${a}, ${b}, ${c}, ${d})"
    }

    protected def isInstanceOfTraitDef(x: Any): Boolean = x.isInstanceOf[TraitDef]

    protected def getTraitDefOps: TraitDefOps = new TraitDefOps {
        def (x: TraitDef) types: Option[VariantTypeArgs] = x.a
        def (x: TraitDef) name: String = x.b
        def (x: TraitDef) higher: Seq[(Seq[String], Option[CallTypeArgs])] = x.c
        def (x: TraitDef) inner: Block = x.d
    }

    protected def toTraitDef(a: Option[VariantTypeArgs], b: String, c: Seq[(Seq[String], Option[CallTypeArgs])], d: Block): TraitDef = new TraitDef(a, b, c, d)



    sealed class ImplicitDef(val a: Option[String], val b: Type, val c: Expression) extends Definition {
        override def equals(x: Any): Boolean = x match {
            case that: ImplicitDef => a == that.a && b == that.b && c == that.c
            case _ => false
        }

        override def toString: String = s"ImplicitDef(${a}, ${b}, ${c})"
    }

    protected def isInstanceOfImplicitDef(x: Any): Boolean = x.isInstanceOf[ImplicitDef]

    protected val getImplicitDefOps: ImplicitDefOps = new ImplicitDefOps {
        def (x: ImplicitDef) name: Option[String] = x.a
        def (x: ImplicitDef) t: Type = x.b
        def (x: ImplicitDef) args: Expression = x.c
    }

    protected def toImplicitDef(a: Option[String], b: Type, c: Expression): ImplicitDef = new ImplicitDef(a, b, c)



    sealed class MethodDef(val a: String, val b: Option[TypeArgs], val c: Seq[Args], val d: Option[Type], val e: Option[Expression]) extends Definition {
        override def equals(x: Any): Boolean = x match {
            case that: MethodDef => a == that.a && b == that.b && c == that.c && d == that.d && e == that.e
            case _ => false
        }

        override def toString: String = s"MethodDef(${a}, ${b}, ${c}, ${d}, ${e})"
    }

    protected def isInstanceOfMethodDef(x: Any): Boolean = x.isInstanceOf[MethodDef]

    protected val getMethodDefOps: MethodDefOps = new MethodDefOps {
        def (x: MethodDef) name: String = x.a
        def (x: MethodDef) types: Option[TypeArgs] = x.b
        def (x: MethodDef) args: Seq[Args] = x.c
        def (x: MethodDef) t: Option[Type] = x.d
        def (x: MethodDef) result: Option[Expression] = x.e
    }

    protected def toMethodDef(a: String, b: Option[TypeArgs], c: Seq[Args], d: Option[Type], e: Option[Expression]): MethodDef = new MethodDef(a, b, c, d, e)



    import ModifiedDef.Modifier

    sealed class ModifiedDef(val a: Modifier, val b: Definition) extends Definition {
        override def equals(x: Any): Boolean = x match {
            case that: ModifiedDef => a == that.a && b == that.b
            case _ => false
        }

        override def toString: String = s"ModifiedDef(${a}, ${b})"
    }

    protected def isInstanceOfModifiedDef(x: Any): Boolean = x.isInstanceOf[ModifiedDef]

    protected val getModifiedDefOps: ModifiedDefOps = new ModifiedDefOps {
        def (x: ModifiedDef) modifier: Modifier = x.a
        def (x: ModifiedDef) definition: Definition = x.b
    }

    protected def toModifiedDef(a: Modifier, b: Definition): ModifiedDef = new ModifiedDef(a, b)



    sealed abstract class Appliable extends Ast

    protected def isInstanceOfAppliable(x: Any): Boolean = x.isInstanceOf[Appliable]

    protected val getAppliableOps: AppliableOps = new AppliableOps {

    }

    sealed class NamedArg(val a: String, val b: Expression) extends Appliable {
        override def equals(x: Any): Boolean = x match {
            case that: NamedArg => a == that.a && b == that.b
            case _ => false
        }

        override def toString: String = s"NamedArg(${a}, ${b})"
    }

    protected def isInstanceOfNamedArg(x: Any): Boolean = x.isInstanceOf[NamedArg]

    protected val getNamedArgOps: NamedArgOps = new NamedArgOps {
        def (x: NamedArg) name: String = x.a
        def (x: NamedArg) value: Expression = x.b
    }

    protected def toNamedArg(a: String, b: Expression) = new NamedArg(a, b)

    sealed abstract class Expression extends Appliable

    protected def isInstanceOfExpression(x: Any): Boolean = x.isInstanceOf[Expression]

    protected val getExpressionOps: ExpressionOps = new ExpressionOps {

    }



    sealed abstract class Ref extends Expression

    protected def isInstanceOfRef(x: Any): Boolean = x.isInstanceOf[Ref]

    protected val getRefOps: RefOps = new RefOps {

    }



    sealed class Ident(val a: String) extends Ref {
        override def equals(x: Any): Boolean = x match {
            case that: Ident => a == that.a
            case _ => false
        }

        override def toString: String = s"Ident(${a})"
    }

    protected def isInstanceOfIdent(x: Any): Boolean = x.isInstanceOf[Ident]

    protected def getIdentOps: IdentOps = new IdentOps {
        def (x: Ident) name: String = x.a
    }

    protected def toIdent(a: String): Ident = new Ident(a)



    sealed class Select(val a: Expression, val b: String) extends Ref {
        override def equals(x: Any): Boolean = x match {
            case that: Select => a == that.a && b == that.b
            case _ => false
        }

        override def toString: String = s"Select(${a}, ${b})"
    }

    protected def isInstanceOfSelect(x: Any): Boolean = x.isInstanceOf[Select]

    protected def getSelectOps: SelectOps = new SelectOps {
        def (x: Select) refined: Expression = x.a
        def (x: Select) refinement: String = x.b
    }

    protected def toSelect(a: Expression, b: String) = new Select(a, b)

    //! INSERT STUFF HERE

    sealed class This extends Expression {
        override def equals(x: Any): Boolean = x.isInstanceOf[This]

        override def toString: String = s"This"
    }

    protected def isInstanceOfThis(x: Any): Boolean = x.isInstanceOf[This]

    protected def getThisOps: ThisOps = new ThisOps {

    }

    protected def toThis: This = new This



    sealed class Super extends Expression {
        override def equals(x: Any): Boolean = x.isInstanceOf[Super]

        override def toString: String = s"Super"
    }

    protected def isInstanceOfSuper(x: Any): Boolean = x.isInstanceOf[Super]

    protected def getSuperOps: SuperOps = new SuperOps {

    }

    protected def toSuper: Super = new Super



    sealed class Typed(val a: Expression, val b: Type) extends Expression {
        override def equals(x: Any): Boolean = x match {
            case that: Typed => a == that.a && b == that.b
            case _ => false
        }

        override def toString: String = s"Typed(${a}, ${b})"
    }

    protected def isInstanceOfTyped(x: Any): Boolean = x.isInstanceOf[Typed]

    protected def getTypedOps: TypedOps = new TypedOps {
        def (x: Typed) value: Expression = x.a
        def (x: Typed) t: Type = x.b
    }

    protected def toTyped(a: Expression, b: Type): Typed = new Typed(a, b)



    sealed class Literal(val a: String) extends Expression {
        override def equals(x: Any): Boolean = x match {
            case that: Literal => a == that.a
            case _ => false
        }

        override def toString: String = s"Literal(${a})"
    }

    protected def isInstanceOfLiteral(x: Any): Boolean = x.isInstanceOf[Literal]

    protected def getLiteralOps: LiteralOps = new LiteralOps {
        def (x: Literal) value: String = x.a
    }

    protected def toLiteral(a: String): Literal = new Literal(a)



    sealed class Apply(val a: Expression, val b: Seq[Appliable]) extends Expression {
        override def equals(x: Any): Boolean = x match {
            case that: Apply => a == that.a && b == that.b
            case _ => false
        }

        override def toString: String = s"Apply(${a}, ${b})"
    }

    protected def isInstanceOfApply(x: Any): Boolean = x.isInstanceOf[Apply]

    protected def getApplyOps: ApplyOps = new ApplyOps {
        def (x: Apply) caller: Expression = x.a
        def (x: Apply) invocation: Seq[Appliable] = x.b
    }

    protected def toApply(a: Expression, b: Seq[Appliable]): Apply = new Apply(a, b)



    sealed class Create(val a: Type, val b: Expression, val c: Block) extends Expression {
        override def equals(x: Any): Boolean = x match {
            case that: Create => a == that.a && b == that.b && c == that.c
            case _ => false
        }

        override def toString: String = s"Create(${a}, ${b}, ${c})"
    }

    protected def isInstanceOfCreate(x: Any): Boolean = x.isInstanceOf[Create]

    protected def getCreateOps: CreateOps = new CreateOps {
        def (x: Create) name: Type = x.a
        def (x: Create) ext: Expression = x.b
        def (x: Create) block: Block = x.c
    }

    protected def toCreate(a: Type, b: Expression, c: Block): Create = new Create(a, b, c)



    sealed class GivenApply(val a: Expression, val b: Seq[Appliable]) extends Expression {
        override def equals(x: Any): Boolean = x match {
            case that: GivenApply => a == that.a && b == that.b
            case _ => false
        }

        override def toString: String = s"GivenApply(${a}, ${b})"
    }

    protected def isInstanceOfGivenApply(x: Any): Boolean = x.isInstanceOf[GivenApply]

    protected def getGivenApplyOps: GivenApplyOps = new GivenApplyOps {
        def (x: GivenApply) caller: Expression = x.a
        def (x: GivenApply) invocation: Seq[Appliable] = x.b
    }

    protected def toGivenApply(a: Expression, b: Seq[Appliable]): GivenApply = new GivenApply(a, b)



    sealed class TypeApply(val a: Expression, val b: CallTypeArgs) extends Expression {
        override def equals(x: Any): Boolean = x match {
            case that: TypeApply => a == that.a && b == that.b
            case _ => false
        }

        override def toString: String = s"TypeApply(${a}, ${b})"
    }

    protected def isInstanceOfTypeApply(x: Any): Boolean = x.isInstanceOf[TypeApply]

    protected def getTypeApplyOps: TypeApplyOps = new TypeApplyOps {
        def (x: TypeApply) caller: Expression = x.a
        def (x: TypeApply) invocation: CallTypeArgs = x.b
    }

    protected def toTypeApply(a: Expression, b: CallTypeArgs): TypeApply = new TypeApply(a, b)



    sealed class Infix(val a: Expression, val b: String, val c: Expression) extends Expression {
        override def equals(x: Any): Boolean = x match {
            case that: Infix => a == that.a && b == that.b && c == that.c
            case _ => false
        }

        override def toString: String = s"Infix(${a}, ${b}, ${c})"
    }

    protected def isInstanceOfInfix(x: Any): Boolean = x.isInstanceOf[Infix]

    protected def getInfixOps: InfixOps = new InfixOps {
        def (x: Infix) first: Expression = x.a
        def (x: Infix) middle: String = x.b
        def (x: Infix) second: Expression = x.c
    }

    protected def toInfix(a: Expression, b: String, c: Expression): Infix = new Infix(a, b, c)



    sealed class Parens(val a: Expression) extends Expression {
        override def equals(x: Any): Boolean = x match {
            case that: Parens => a == that.a
            case _ => false
        }

        override def toString: String = s"Parens(${a})"
    }

    protected def isInstanceOfParens(x: Any): Boolean = x.isInstanceOf[Parens]

    protected def getParensOps: ParensOps = new ParensOps {
        def (x: Parens) exp: Expression = x.a
    }

    protected def toParens(a: Expression): Parens = new Parens(a)



    sealed class Block(val a: Seq[Statement]) extends Expression {
        override def equals(x: Any): Boolean = x match {
            case that: Block => a == that.a
            case _ => false
        }

        override def toString: String = s"Block(${a})"
    }

    protected def isInstanceOfBlock(x: Any): Boolean = x.isInstanceOf[Block]

    protected def getBlockOps: BlockOps = new BlockOps {
        def (x: Block) statements: Seq[Statement] = x.a
    }

    protected def toBlock(a: Seq[Statement]): Block = new Block(a)



    sealed class With(val a: Expression, val b: Expression) extends Expression {
        override def equals(x: Any): Boolean = x match {
            case that: With => a == that.a && b == that.b
            case _ => false
        }

        override def toString: String = s"With(${a}, ${b})"
    }

    protected def isInstanceOfWith(x: Any): Boolean = x.isInstanceOf[With]

    protected def getWithOps: WithOps = new WithOps {
        def (x: With) scope: Expression = x.a
        def (x: With) value: Expression = x.b
    }

    protected def toWith(a: Expression, b: Expression): With = new With(a, b)



    sealed class If(val a: Expression, val b: Expression, val c: Option[Expression]) extends Expression {
        override def equals(x: Any): Boolean = x match {
            case that: If => a == that.a && b == that.b && c == that.c
            case _ => false
        }

        override def toString: String = s"If(${a}, ${b})"
    }

    protected def isInstanceOfIf(x: Any): Boolean = x.isInstanceOf[If]

    protected def getIfOps: IfOps = new IfOps {
        def (x: If) toggle: Expression = x.a
        def (x: If) therefore: Expression = x.b
        def (x: If) otherwise: Option[Expression] = x.c
    }

    protected def toIf(a: Expression, b: Expression, c: Option[Expression]) = new If(a, b, c)



    sealed class Function(val a: OptionalArgs, val b: Expression) extends Expression {
        override def equals(x: Any): Boolean = x match {
            case that: Function => a == that.a && b == that.b
            case _ => false
        }

        override def toString: String = s"Function(${a}, ${b})"
    }

    protected def isInstanceOfFunction(x: Any): Boolean = x.isInstanceOf[Function]

    protected def getFunctionOps: FunctionOps = new FunctionOps {
        def (x: Function) args: OptionalArgs = x.a
        def (x: Function) expression: Expression = x.b
    }

    protected def toFunction(a: OptionalArgs, b: Expression): Function = new Function(a, b)



    sealed class Match(val a: Seq[(Pattern, Expression)]) extends Expression {
        override def equals(x: Any): Boolean = x match {
            case that: Match => a == that.a
            case _ => false
        }

        override def toString: String = s"Match(${a})"
    }

    protected def isInstanceOfMatch(x: Any): Boolean = x.isInstanceOf[Match]

    protected def getMatchOps: MatchOps = new MatchOps {
        def (x: Match) cases: Seq[(Pattern, Expression)] = x.a
    }

    protected def toMatch(a: Seq[(Pattern, Expression)]): Match = new Match(a)



    sealed class Try(val a: Expression, val b: Match, val c: Option[Expression]) extends Expression {
        override def equals(x: Any): Boolean = x match {
            case that: Try => a == that.a && b == that.b && c == that.c
            case _ => false
        }

        override def toString: String = s"Try(${a}, ${b}, ${c})"
    }

    protected def isInstanceOfTry(x: Any): Boolean = x.isInstanceOf[Try]

    protected def getTryOps: TryOps = new TryOps {
        def (x: Try) throwing: Expression = x.a
        def (x: Try) catching: Match = x.b
        def (x: Try) always: Option[Expression] = x.c
    }

    protected def toTry(a: Expression, b: Match, c: Option[Expression]) = new Try(a, b, c)



    sealed abstract class Pattern extends Ast {

    }

    protected def isInstanceOfPattern(x: Any): Boolean = x.isInstanceOf[Pattern]

    protected def getPatternOps: PatternOps = new PatternOps {

    }



    sealed class Value(val a: String) extends Pattern {
        override def equals(x: Any): Boolean = x match {
            case that: Value => a == that.a
            case _ => false
        }

        override def toString: String = s"Value(${a})"
    }

    protected def isInstanceOfValue(x: Any): Boolean = x.isInstanceOf[Value]

    protected def getValueOps: ValueOps = new ValueOps {
        def (x: Value) name: String = x.a
    }

    protected def toValue(a: String): Value = new Value(a)



    sealed class Bind(val a: Pattern, val b: Pattern) extends Pattern {
        override def equals(x: Any): Boolean = x match {
            case that: Bind => a == that.a && b == that.b
            case _ => false
        }

        override def toString: String = s"Bind(${a}, ${b})"
    }

    protected def isInstanceOfBind(x: Any): Boolean = x.isInstanceOf[Bind]

    protected def getBindOps: BindOps = new BindOps {
        def (x: Bind) first: Pattern = x.a
        def (x: Bind) second: Pattern = x.b
    }

    protected def toBind(a: Pattern, b: Pattern): Bind = new Bind(a, b)



    sealed class Unapply(val a: Expression, val b: Seq[Pattern]) extends Pattern {
        override def equals(x: Any): Boolean = x match {
            case that: Unapply => a == that.a && b == that.b
            case _ => false
        }

        override def toString: String = s"Unapply(${a}, ${b})"
    }

    protected def isInstanceOfUnapply(x: Any): Boolean = x.isInstanceOf[Unapply]

    protected def getUnapplyOps: UnapplyOps = new UnapplyOps {
        def (x: Unapply) unapplier: Expression = x.a
        def (x: Unapply) args: Seq[Pattern] = x.b
    }

    protected def toUnapply(a: Expression, b: Seq[Pattern]): Unapply = new Unapply(a, b)



    sealed class Alternatives(val a: Seq[Pattern]) extends Pattern {
        override def equals(x: Any): Boolean = x match {
            case that: Alternatives => a == that.a
            case _ => false
        }

        override def toString: String = s"Alternatives(${a})"
    }

    protected def isInstanceOfAlternatives(x: Any): Boolean = x.isInstanceOf[Alternatives]

    protected def getAlternativesOps: AlternativesOps = new AlternativesOps {
        def (x: Alternatives) alternatives: Seq[Pattern] = x.a
    }

    protected def toAlternatives(a: Seq[Pattern]): Alternatives = new Alternatives(a)



    sealed class TypeTest(val a: Type, val b: Pattern) extends Pattern {
        override def equals(x: Any): Boolean = x match {
            case that: TypeTest => a == that.a && b == that.b
            case _ => false
        }

        override def toString: String = s"TypeTest(${a}, ${b})"
    }

    protected def isInstanceOfTypeTest(x: Any): Boolean = x.isInstanceOf[TypeTest]

    protected def getTypeTestOps: TypeTestOps = new TypeTestOps {
        def (x: TypeTest) t: Type = x.a
        def (x: TypeTest) pattern: Pattern = x.b
    }

    protected def toTypeTest(a: Pattern, b: Type) = new TypeTest(b, a)



    sealed class Wildcard() extends Pattern {
        override def equals(x: Any): Boolean = x.isInstanceOf[Wildcard]

        override def toString: String = s"Wildcard"
    }

    protected def isInstanceOfWildcard(x: Any): Boolean = x.isInstanceOf[Wildcard]

    protected def getWildcardOps: WildcardOps = new WildcardOps {

    }

    protected def toWildcard(): Wildcard = new Wildcard()

    //! INSERT STUFF HERE

    import TypeBounds.BoundType

    sealed class TypeBounds(val a: Seq[BoundType]) extends Ast {
        override def equals(x: Any): Boolean = x match {
            case that: TypeBounds => a == that.a
            case _ => false
        }

        override def toString: String = s"TypeBounds(${a})"
    }

    protected def isInstanceOfTypeBounds(x: Any): Boolean = x.isInstanceOf[TypeBounds]

    protected val getTypeBoundsOps: TypeBoundsOps = new TypeBoundsOps {
        def (x: TypeBounds) t: Seq[BoundType] = x.a
    }

    protected def toTypeBounds(a: Seq[BoundType]) = new TypeBounds(a)



    sealed abstract class Type extends Ast {

    }

    protected def isInstanceOfType(x: Any): Boolean = x.isInstanceOf[Type]

    protected val getTypeOps: TypeOps = new TypeOps {

    }



    sealed class TypeSelect(val a: Expression, val b: String) extends Type {
        override def equals(x: Any): Boolean = x match {
            case that: TypeSelect => a == that.a && b == that.b
            case _ => false
        }

        override def toString: String = s"TypeSelect(${a}, ${b})"
    }

    protected def isInstanceOfTypeSelect(x: Any): Boolean = x.isInstanceOf[TypeSelect]

    protected val getTypeSelectOps: TypeSelectOps = new TypeSelectOps {
        def (x: TypeSelect) expression: Expression = x.a

        def (x: TypeSelect) name: String = x.b
    }

    protected def toTypeSelect(a: Expression, b: String) = new TypeSelect(a, b)



    sealed class TypeLambda(val a: VariantTypeArgs, val b: Type) extends Type {
        override def equals(x: Any): Boolean = x match {
            case that: TypeLambda => a == that.a && b == that.b
            case _ => false
        }

        override def toString: String = s"TypeLambda(${a}, ${b})"
    }

    protected def isInstanceOfTypeLambda(x: Any): Boolean = x.isInstanceOf[TypeLambda]

    protected val getTypeLambdaOps: TypeLambdaOps = new TypeLambdaOps {
        def (x: TypeLambda) args: VariantTypeArgs = x.a
        def (x: TypeLambda) t: Type = x.b
    }

    protected def toTypeLambda(a: VariantTypeArgs, b: Type): TypeLambda = new TypeLambda(a, b)



    sealed class SingletonType(val a: Expression) extends Type {
        override def equals(x: Any): Boolean = x match {
            case that: SingletonType => a == that.a
            case _ => false
        }

        override def toString: String = s"SingletonType(${a})"
    }

    protected def isInstanceOfSingletonType(x: Any): Boolean = x.isInstanceOf[SingletonType]

    protected val getSingletonTypeOps: SingletonTypeOps = new SingletonTypeOps {
        def (x: SingletonType) value: Expression = x.a
    }

    protected def toSingletonType(a: Expression): SingletonType = new SingletonType(a)



    sealed class ByNameType(val a: Type) extends Type {
        override def equals(x: Any): Boolean = x match {
            case that: ByNameType => a == that.a
            case _ => false
        }

        override def toString: String = s"ByNameType(${a})"
    }

    protected def isInstanceOfByNameType(x: Any): Boolean = x.isInstanceOf[ByNameType]

    protected val getByNameTypeOps: ByNameTypeOps = new ByNameTypeOps {
        def (x: ByNameType) t: Type = x.a
    }

    protected def toByNameType(a: Type): ByNameType = new ByNameType(a)



    sealed class MatchType(val a: Type, val b: Seq[(TypePattern, Type)]) extends Type {
        override def equals(x: Any): Boolean = x match {
            case that: MatchType => a == that.a
            case _ => false
        }

        override def toString: String = s"MatchType(${a}, ${b})"
    }

    protected def isInstanceOfMatchType(x: Any): Boolean = x.isInstanceOf[MatchType]

    protected val getMatchTypeOps: MatchTypeOps = new MatchTypeOps {
        def (x: MatchType) first: Type = x.a
        def (x: MatchType) cases: Seq[(TypePattern, Type)] = x.b
    }

    protected def toMatchType(a: Type, b: Seq[(TypePattern, Type)]): MatchType = new MatchType(a, b)

    sealed class OrType(val a: Type, val b: Type) extends Type {
        override def equals(x: Any): Boolean = x match {
            case that: OrType => a == that.a && b == that.b
            case _ => false
        }

        override def toString: String = s"OrType(${a}, ${b})"
    }

    protected def isInstanceOfOrType(x: Any): Boolean = x.isInstanceOf[OrType]

    protected val getOrTypeOps: OrTypeOps = new OrTypeOps {
        def (x: OrType) first: Type = x.a
        def (x: OrType) second: Type = x.b
    }

    protected def toOrType(a: Type, b: Type) = new OrType(a, b)



    sealed class AndType(val a: Type, val b: Type) extends Type {
        override def equals(x: Any): Boolean = x match {
            case that: AndType => a == that.a && b == that.b
            case _ => false
        }

        override def toString: String = s"AndType(${a}, ${b})"
    }

    protected def isInstanceOfAndType(x: Any): Boolean = x.isInstanceOf[AndType]

    protected val getAndTypeOps: AndTypeOps = new AndTypeOps {
        def (x: AndType) first: Type = x.a
        def (x: AndType) second: Type = x.b
    }

    protected def toAndType(a: Type, b: Type) = new AndType(a, b)



    sealed class AppliedType(val a: Type, val b: CallTypeArgs) extends Type {
        override def equals(x: Any): Boolean = x match {
            case that: AppliedType => a == that.a && b == that.b
            case _ => false
        }

        override def toString: String = s"AppliedType(${a}, ${b})"
    }

    protected def isInstanceOfAppliedType(x: Any): Boolean = x.isInstanceOf[AppliedType]

    protected val getAppliedTypeOps: AppliedTypeOps = new AppliedTypeOps {
        def (x: AppliedType) t: Type = x.a
        def (x: AppliedType) call: CallTypeArgs = x.b
    }

    protected def toAppliedType(a: Type, b: CallTypeArgs): AppliedType = new AppliedType(a, b)



    sealed class NamedType(val a: Seq[String]) extends Type {
        override def equals(x: Any): Boolean = x match {
            case that: NamedType => a == that.a
            case _ => false
        }

        override def toString: String = s"NamedType(${a})"
    }

    protected def isInstanceOfNamedType(x: Any): Boolean = x.isInstanceOf[NamedType]

    protected val getNamedTypeOps: NamedTypeOps = new NamedTypeOps {
        def (x: NamedType) name: Seq[String] = x.a
    }

    protected def toNamedType(a: Seq[String]) = new NamedType(a)



    sealed abstract class TypePattern extends Ast {
    }

    protected def isInstanceOfTypePattern(x: Any): Boolean = x.isInstanceOf[TypePattern]

    protected val getTypePatternOps: TypePatternOps = new TypePatternOps {

    }



    sealed class ExtractType(val a: Type, val b: Seq[TypePattern]) extends TypePattern {
        override def equals(x: Any): Boolean = x match {
            case that: ExtractType => a == that.a && b == that.b
            case _ => false
        }

        override def toString: String = s"ExtractType(${a}, ${b})"
    }

    protected def isInstanceOfExtractType(x: Any): Boolean = x.isInstanceOf[ExtractType]

    protected val getExtractTypeOps: ExtractTypeOps = new ExtractTypeOps {
        def (x: ExtractType) extract: Type = x.a
        def (x: ExtractType) types: Seq[TypePattern] = x.b
    }

    protected def toExtractType(a: Type, b: Seq[TypePattern]) = new ExtractType(a, b)



    sealed class TypeWildcard() extends TypePattern {
        override def equals(x: Any): Boolean = x.isInstanceOf[TypeWildcard]

        override def toString: String = s"TypeWildcard"
    }

    protected def isInstanceOfTypeWildcard(x: Any): Boolean = x.isInstanceOf[TypeWildcard]

    protected val getTypeWildcardOps: TypeWildcardOps = new TypeWildcardOps {
    }

    protected def toTypeWildcard() = new TypeWildcard()



    sealed class TypeValue(val a: Type) extends TypePattern {
        override def equals(x: Any): Boolean = x match {
            case that: TypeValue => a == that.a
        }

        override def toString: String = s"TypeValue(${a})"
    }

    protected def isInstanceOfTypeValue(x: Any): Boolean = x.isInstanceOf[TypeValue]

    protected val getTypeValueOps: TypeValueOps = new TypeValueOps {
        def (x: TypeValue) name: Type = x.a
    }

    protected def toTypeValue(a: Type) = new TypeValue(a)

    //! INSERT STUFF HERE

    import OptionalArgs._

    sealed class OptionalArgs(val a: Seq[(String, Option[Type])], val b: ArgType) extends Ast {
        def canEqual(x: Any): Boolean = x.isInstanceOf[OptionalArgs]

        override def equals(x: Any): Boolean = x match {
            case that: OptionalArgs => that.canEqual(this) && a == that.a && b == that.b
            case _ => false
        }
    }

    protected def isInstanceOfOptionalArgs(x: Any): Boolean = x.isInstanceOf[OptionalArgs]

    protected val getOptionalArgsOps: OptionalArgsOps = new OptionalArgsOps {
        def (x: OptionalArgs) oargs: Seq[(String, Option[Type])] = x.a
        def (x: OptionalArgs) t: ArgType = x.b
    }

    protected def toOptionalArgs(a: Seq[(String, Option[Type])], b: ArgType) = new OptionalArgs(a, b)



    sealed class Args(val c: Seq[(String, Type)], val d: ArgType) extends OptionalArgs(c.map(x => (x._1, Some(x._2))), d) {
        override def canEqual(x: Any): Boolean = x.isInstanceOf[Args]

        override def equals(x: Any): Boolean = x match {
            case that: Args => that.canEqual(this) && c == that.c && d == that.d
            case _ => false
        }
    }

    protected def isInstanceOfArgs(x: Any): Boolean = x.isInstanceOf[Args]

    protected val getArgsOps: ArgsOps = new ArgsOps {
        def (x: Args) args: Seq[(String, Type)] = x.c
    }

    protected def toArgs(a: Seq[(String, Type)], b: ArgType): Args = new Args(a, b)



    sealed class CallTypeArgs(val a: Seq[Type]) extends Ast {
        override def equals(x: Any): Boolean = x match {
            case that: CallTypeArgs => a == that.a
            case _ => false
        }

        override def toString: String = s"CallTypeArgs(${a})"
    }

    protected def isInstanceOfCallTypeArgs(x: Any): Boolean = x.isInstanceOf[CallTypeArgs]

    protected val getCallTypeArgsOps: CallTypeArgsOps = new CallTypeArgsOps {
        def (x: CallTypeArgs) types: Seq[Type] = x.a
    }

    protected def toCallTypeArgs(a: Seq[Type]) = new CallTypeArgs(a)



    sealed class TypeArgs(val b: Seq[(Type, TypeBounds)]) extends Ast {
        override def equals(x: Any): Boolean = x match {
            case that: TypeArgs => b == that.b
            case _ => false
        }

        override def toString: String = s"TypeArgs(${b})"
    }

    protected def isInstanceOfTypeArgs(x: Any): Boolean = x.isInstanceOf[TypeArgs]

    protected val getTypeArgsOps: TypeArgsOps = new TypeArgsOps {
        def (x: TypeArgs) types: Seq[(Type, TypeBounds)] = x.b

        def (x: TypeArgs) convertToCallTypeArgs: CallTypeArgs = CallTypeArgs(x.types.map(a => a._1))
    }

    protected def toTypeArgs(a: Seq[(Type, TypeBounds)]) = new TypeArgs(a)



    sealed class VariantTypeArgs(val c: Seq[(Int, Type, TypeBounds)]) extends Ast {
        override def equals(x: Any): Boolean = x match {
            case that: VariantTypeArgs => c == that.c
            case _ => false
        }

        override def toString: String = s"VariantTypeArgs(${c})"
    }

    protected def isInstanceOfVariantTypeArgs(x: Any): Boolean = x.isInstanceOf[VariantTypeArgs]

    protected val getVariantTypeArgsOps: VariantTypeArgsOps = new VariantTypeArgsOps {
        def (x: VariantTypeArgs) types: Seq[(Int, Type, TypeBounds)] = x.c

        def (x: VariantTypeArgs) convertToTypeArgs: TypeArgs = TypeArgs(x.types.map(a => (a._2, a._3)))
    }

    protected def toVariantTypeArgs(a: Seq[(Int, Type, TypeBounds)]) = new VariantTypeArgs(a)
}