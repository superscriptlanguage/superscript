package org.superscript.compiler.ast

import AstImpl._
import scala.collection.JavaConverters._
import org.superscript.compiler.parser.SuperScriptParser._
import org.superscript.compiler.parser.SuperScriptBaseVisitor
import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.CommonTokenStream
import org.superscript.compiler.parser.SuperScriptParser
import org.superscript.compiler.parser.SuperScriptLexer
import scala.language.implicitConversions

def (x: java.util.List[A]) toScala[A]: Seq[A] = Seq.from(x.asScala)

object AstVisitor extends SuperScriptBaseVisitor[Ast] {
    override def visitFile(ctx: FileContext): File = File(ctx.statement.toScala.map(a => visitStatement(a)))

    override def visitStatement(ctx: StatementContext): Statement = visit(ctx.stmt).asInstanceOf[Statement]

    override def visitNameimp(ctx: NameimpContext): Import = {
        import Import.ImportType._
        val path = parseImpPath(ctx.imppath)
        Import(path, List(Simple(parseName(ctx.name))))
    }

    override def visitAnyimp(ctx: AnyimpContext): Import = {
        import Import.ImportType._
        val path = parseImpPath(ctx.imppath)
        Import(path, List(Import.ImportType.Wildcard))
    }

    override def visitTypeimp(ctx: TypeimpContext): Import = {
        import Import.ImportType._
        val path = parseImpPath(ctx.imppath)
        Import(path, ctx.imptype.toScala.map(parseTypeImp))
    }

    override def visitPkg(ctx: PkgContext): Package = Package(ctx.name.toScala.map(x => parseName(x)))

    override def visitExprstmt(ctx: ExprstmtContext): Expr = Expr(visit(ctx.expression).asInstanceOf[Expression])

    override def visitValdef(ctx: ValdefContext): ValDef = ValDef(true, parseName(ctx.infixname), Option(ctx.t).map(a => visit(a).asInstanceOf[Type]), Option(ctx.expression).map(a => visit(a).asInstanceOf[Expression]))

    override def visitTypedef(ctx: TypedefContext): TypeDef = TypeDef(parseName(ctx.name), visitTypebounds(ctx.typebounds), Option(ctx.t).map(a => visit(a).asInstanceOf[Type]))

    override def visitVardef(ctx: VardefContext): ValDef = ValDef(false, parseName(ctx.infixname), Option(ctx.t).map(a => visit(a).asInstanceOf[Type]), Option(ctx.expression).map(a => visit(a).asInstanceOf[Expression]))

    override def visitTypebounds(ctx: TypeboundsContext): TypeBounds = TypeBounds(ctx.boundtype.toScala.map(a => parseBoundType(a)))

    override def visitCalltypeargs(ctx: CalltypeargsContext): CallTypeArgs = CallTypeArgs(ctx.t.toScala.map(a => visit(a).asInstanceOf[Type]))

    override def visitTypeargs(ctx: TypeargsContext): TypeArgs = TypeArgs(ctx.t.toScala.map(a => visit(a).asInstanceOf[Type]) zip ctx.typebounds.toScala.map(a => visitTypebounds(a)))

    override def visitVarianttypeargs(ctx: VarianttypeargsContext): VariantTypeArgs = VariantTypeArgs((
        ctx.t.toScala.map(visit(_).asInstanceOf[Type]) zip
        ctx.typebounds.toScala.map(visitTypebounds(_)) zip
        ctx.variance.toScala.map(parseVariance(_))
    ).map(a => {
        val ((b, c), d) = a
        (d, b, c)
    }))

    override def visitBlock(ctx: BlockContext): Block = Block(ctx.statement.toScala.map(a => visit(a).asInstanceOf[Statement]))

    override def visitThisexp(ctx: ThisexpContext): This = This

    override def visitSuperexp(ctx: SuperexpContext): Super = Super

    override def visitTypedexp(ctx: TypedexpContext): Typed = Typed(visit(ctx.expression).asInstanceOf[Expression], visit(ctx.t).asInstanceOf[Type])

    override def visitIdentexp(ctx: IdentexpContext): Ident = Ident(parseName(ctx.name))

    override def visitSelectexp(ctx: SelectexpContext): Select = Select(visit(ctx.expression).asInstanceOf[Expression], parseName(ctx.infixname))

    override def visitNamedt(ctx: NamedtContext): NamedType = NamedType(ctx.name.toScala.map(a => parseName(a)))

    override def visitSelectt(ctx: SelecttContext): TypeSelect = TypeSelect(visit(ctx.expression).asInstanceOf[Expression], parseName(ctx.name))

    override def visitGivenoptionalargs(ctx: GivenoptionalargsContext): OptionalArgs = OptionalArgs(parseOptionalargs(ctx.optionalargs), OptionalArgs.ArgType.Given)

    override def visitNormaloptionalargs(ctx: NormaloptionalargsContext): OptionalArgs = OptionalArgs(parseOptionalargs(ctx.optionalargs), OptionalArgs.ArgType.Normal)

    override def visitGivenargs(ctx: GivenargsContext): Args = Args(parseArgs(ctx.args), OptionalArgs.ArgType.Given)

    override def visitNormalargs(ctx: NormalargsContext): Args = Args(parseArgs(ctx.args), OptionalArgs.ArgType.Normal)

    override def visitMethoddef(ctx: MethoddefContext): MethodDef = MethodDef(
        Option(ctx.infixname).map(a => parseName(a)).getOrElse(parseName(ctx.name)),
        Option(ctx.typeargs).map(visitTypeargs), ctx.gargs.toScala.map(visit(_).asInstanceOf[Args]),
        Option(ctx.t).map(visit(_).asInstanceOf[Type]), Option(ctx.expression).map(visit(_).asInstanceOf[Expression]))

    override def visitExprappliable(ctx: ExprappliableContext): Expression = visit(ctx.expression).asInstanceOf[Expression]

    override def visitNameappliable(ctx: NameappliableContext): NamedArg = NamedArg(parseName(ctx.name), visit(ctx.expression).asInstanceOf[Expression])

    override def visitW(ctx: WContext): With = {
        val expressions = ctx.expression.toScala
        With(visit(expressions.head).asInstanceOf[Expression], visit(expressions.tail.head).asInstanceOf[Expression])
    }

    override def visitIf(ctx: IfContext): If = {
        val expressions = ctx.expression.toScala
        val expr = visit(expressions.head).asInstanceOf[Expression]
        expressions.tail.asInstanceOf[Seq[org.antlr.v4.runtime.tree.ParseTree]] match {
            case Seq(a) => If(expr, visit(a).asInstanceOf[Expression], None)
            case Seq(a, b) => If(expr, visit(a).asInstanceOf[Expression], Some(visit(b).asInstanceOf[Expression]))
        }
    }

    override def visitOrt(ctx: OrtContext): OrType = OrType(visit(ctx.t.get(0)).asInstanceOf[Type], visit(ctx.t.get(1)).asInstanceOf[Type])

    override def visitAndt(ctx: AndtContext): AndType = AndType(visit(ctx.t.get(0)).asInstanceOf[Type], visit(ctx.t.get(1)).asInstanceOf[Type])

    override def visitAppliedt(ctx: AppliedtContext): AppliedType = AppliedType(visit(ctx.t).asInstanceOf[Type], visitCalltypeargs(ctx.calltypeargs))

    override def visitFunctionexp(ctx: FunctionexpContext): Function = Function(visit(ctx.goptionalargs).asInstanceOf[OptionalArgs], visit(ctx.expression).asInstanceOf[Expression])

    override def visitImplicitdef(ctx: ImplicitdefContext): ImplicitDef = ImplicitDef(Option(ctx.infixname).map(a => parseName(a)), visit(ctx.t).asInstanceOf[Type], visit(ctx.expression).asInstanceOf[Expression])

    override def visitLambdat(ctx: LambdatContext): TypeLambda = TypeLambda(visitVarianttypeargs(ctx.varianttypeargs), visit(ctx.t).asInstanceOf[Type])

    override def visitApply(ctx: ApplyContext): Apply = {
        val expressions = ctx.expression.toScala
        Apply(visit(expressions.head).asInstanceOf[Expression], expressions.tail.map(a => visit(a).asInstanceOf[Expression]))
    }

    override def visitGapply(ctx: GapplyContext): GivenApply = {
        val expressions = ctx.expression.toScala
        GivenApply(visit(expressions.head).asInstanceOf[Expression], expressions.tail.map(a => visit(a).asInstanceOf[Expression]))
    }

    override def visitTapply(ctx: TapplyContext): TypeApply = TypeApply(visit(ctx.expression).asInstanceOf[Expression], visitCalltypeargs(ctx.calltypeargs))

    override def visitBindpat(ctx: BindpatContext): Bind = {
        val patterns = ctx.pattern.toScala
        Bind(visit(patterns.head).asInstanceOf[Pattern], visit(patterns.tail.head).asInstanceOf[Pattern])
    }

    override def visitValuepat(ctx: ValuepatContext): Value = Value(parseName(ctx.name))

    override def visitUnapplypat(ctx: UnapplypatContext): Unapply = Unapply(visit(ctx.expression).asInstanceOf[Expression], ctx.pattern.toScala.map(a => visit(a).asInstanceOf[Pattern]))

    override def visitAltpat(ctx: AltpatContext): Alternatives = Alternatives(ctx.pattern.toScala.map(a => visit(a).asInstanceOf[Pattern]))

    override def visitTypepat(ctx: TypepatContext): TypeTest = TypeTest(visit(ctx.pattern).asInstanceOf[Pattern], visit(ctx.t).asInstanceOf[Type])

    override def visitWildpat(ctx: WildpatContext): Wildcard = Wildcard()

    override def visitSinglet(ctx: SingletContext): SingletonType = SingletonType(visit(ctx.expression).asInstanceOf[Expression])

    override def visitBynamet(ctx: BynametContext): ByNameType = ByNameType(visit(ctx.t).asInstanceOf[Type])

    override def visitExtracttp(ctx: ExtracttpContext): ExtractType = ExtractType(visit(ctx.t).asInstanceOf[Type], ctx.tp.toScala.map(a => visit(a).asInstanceOf[TypePattern]))

    override def visitTpwildcard(ctx: TpwildcardContext): TypeWildcard = TypeWildcard()

    override def visitValuetp(ctx: ValuetpContext): TypeValue = TypeValue(visit(ctx.t).asInstanceOf[Type])

    override def visitMat(ctx: MatContext): Match = Match(ctx.pattern.toScala.map(a => visit(a).asInstanceOf[Pattern]) zip ctx.expression.toScala.map(a => visit(a).asInstanceOf[Expression]))

    override def visitTry(ctx: TryContext): Try = {
        val exprs = ctx.expression.toScala
        Try(visit(exprs.head).asInstanceOf[Expression], visitMat(ctx.mat), exprs.tail.headOption.map(a => visit(a).asInstanceOf[Expression]))
    }

    override def visitExpressioneof(ctx: ExpressioneofContext): Expression = visit(ctx.expression).asInstanceOf[Expression]

    override def visitMatcht(ctx: MatchtContext): MatchType = MatchType(visit(ctx.t).asInstanceOf[Type], parseMatchType(ctx.tm))

    override def visitTraitdef(ctx: TraitdefContext): TraitDef = {
        val argnames = ctx.urlwitharg.toScala.map(a => parseUrlwitharg(a))
        TraitDef(Option(ctx.varianttypeargs).map(a => visitVarianttypeargs(a)), parseName(ctx.name), argnames, Option(ctx.block).map(a => visitBlock(a)).getOrElse(Block(List())))
    }

    override def visitClassdef(ctx: ClassdefContext): ClassDef = {
        val simplenames = ctx.urlwitharg.toScala.map(a => parseUrlwitharg(a))
        val argnames = if(simplenames.nonEmpty) simplenames else List((List("AnyRef"), None))
        ClassDef(Option(ctx.varianttypeargs).map(a => visitVarianttypeargs(a)), parseName(ctx.name), argnames, Option(ctx.block).map(a => visitBlock(a)).getOrElse(Block(List())))
    }

    override def visitObjectdef(ctx: ObjectdefContext): ObjectDef = {
        val simplenames = ctx.urlwitharg.toScala.map(a => parseUrlwitharg(a))
        val argnames = if(simplenames.nonEmpty) simplenames else List((List("AnyRef"), None))
        ObjectDef(parseName(ctx.name), argnames, Option(ctx.block).map(a => visitBlock(a)).getOrElse(Block(List())), Option(ctx.expression).map(visit(_).asInstanceOf[Expression]).getOrElse(Literal("null")))
    }

    override def visitStrexp(ctx: StrexpContext): Literal = Literal(ctx.STRING.getText)

    override def visitNumexp(ctx: NumexpContext): Literal = Literal(ctx.NUM.getText)

    override def visitOverdef(ctx: OverdefContext): ModifiedDef = ModifiedDef(ModifiedDef.Modifier.Override, visit(ctx.definition).asInstanceOf[Definition])

    override def visitPrivatedef(ctx: PrivatedefContext): ModifiedDef = ModifiedDef(ModifiedDef.Modifier.Private, visit(ctx.definition).asInstanceOf[Definition])

    override def visitProtecteddef(ctx: ProtecteddefContext): ModifiedDef = ModifiedDef(ModifiedDef.Modifier.Protected, visit(ctx.definition).asInstanceOf[Definition])

    override def visitCreateexp(ctx: CreateexpContext): Create = Create(visit(ctx.t).asInstanceOf[Type], Option(ctx.expression).map(a => visit(a).asInstanceOf[Expression]).getOrElse(Ident("null")), Option(ctx.block).map(a => visitBlock(a)).getOrElse(Block(List())))

    override def visitInfixexp(ctx: InfixexpContext): Infix = {
        val exprs = ctx.expression.toScala.map(a => visit(a).asInstanceOf[Expression])
        Infix(exprs.head, parseName(ctx.infixname), exprs.tail.head)
    }

    override def visitParensexp(ctx: ParensexpContext): Parens = Parens(visit(ctx.expression).asInstanceOf[Expression])


    def parseUrlwitharg(ctx: UrlwithargContext): (Seq[String], Option[CallTypeArgs]) = (ctx.name.toScala.map(a => parseName(a)), Option(ctx.calltypeargs).map(a => visitCalltypeargs(a)))

    def parseMatchType(ctx: TmContext): Seq[(TypePattern, Type)] = ctx.tp.toScala.map(a => visit(a).asInstanceOf[TypePattern]) zip ctx.t.toScala.map(a => visit(a).asInstanceOf[Type])

    def parseName(ctx: InfixnameContext): String = ctx.getText

    def parseName(ctx: NameContext): String = ctx.NAME.getText

    def parseImpPath(ctx: ImppathContext): Seq[String] = ctx.name.toScala.map(a => parseName(a))

    def parseTypeImp(x: ImptypeContext): Import.ImportType = x match {
        case ctx: AllimptypeContext => Import.ImportType.Wildcard
        case ctx: NameimptypeContext => Import.ImportType.Simple(parseName(ctx.name))
        case ctx: GivenimptypeContext => Import.ImportType.Simple("given")
        case ctx: IgnoreimptypeContext => Import.ImportType.Rename(parseName(ctx.name), None)
        case ctx: RenameimptypeContext => Import.ImportType.Rename(parseName(ctx.name(0)), Some(parseName(ctx.name(1))))
    }

    def parseBoundType(x: BoundtypeContext): TypeBounds.BoundType = x match {
        case ctx: SubclassboundtypeContext => TypeBounds.BoundType.Subclass(visit(ctx.t).asInstanceOf[Type])
        case ctx: SuperclassboundtypeContext => TypeBounds.BoundType.Superclass(visit(ctx.t).asInstanceOf[Type])
        case ctx: ExistsimplicitboundtypeContext => TypeBounds.BoundType.ExistsImplicit(visit(ctx.t).asInstanceOf[Type])
    }

    def parseVariance(x: VarianceContext): Int = x match {
        case ctx: CovarianceContext => +1
        case ctx: InvarianceContext => 0
        case ctx: ContravarianceContext => -1
    }

    def parseOptionalargs(ctx: OptionalargsContext): Seq[(String, Option[Type])] = ctx.oarg.toScala.map(a => parseOarg(a))

    def parseOarg(ctx: OargContext): (String, Option[Type]) = (parseName(ctx.name), Option(ctx.t).map(a => visit(a).asInstanceOf[Type]))

    def parseArgs(ctx: ArgsContext): Seq[(String, Type)] = ctx.arg.toScala.map(a => parseArg(a))

    def parseArg(ctx: ArgContext): (String, Type) = (parseName(ctx.name), visit(ctx.t).asInstanceOf[Type])


    def parse(x: String): SuperScriptParser = {
        val lexer = new SuperScriptLexer(CharStreams.fromString(x))
        new SuperScriptParser(new CommonTokenStream(lexer))
    }
}