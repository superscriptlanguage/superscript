package org.superscript.compiler.ast

abstract class AstLike {
    import AstLike._

    type Ast

    protected def isInstanceOfAst(x: Any): Boolean

    protected def getAstOps: AstOps

    given AstOps = getAstOps

    given (given ops: AstOps): Conversion[AstLike, AstOps] {
        def apply(x: AstLike): AstOps = ops
    }

    abstract class AstOps {

    }

    object Ast {
        def unapply(x: Any): Boolean = isInstanceOfAst(x)
    }



    type File <: Ast

    protected def isInstanceOfFile(x: Any): Boolean

    protected def getFileOps: FileOps

    given FileOps = getFileOps

    given (given ops: FileOps): Conversion[AstLike, FileOps] {
        def apply(x: AstLike): FileOps = ops
    }

    abstract class FileOps {
        def (x: File) contains: Seq[Statement]
    }

    protected def toFile(a: Seq[Statement]): File

    object File {
        def apply(a: Seq[Statement]): File = toFile(a)

        def unapply(x: File): Option[Seq[Statement]] = when(isInstanceOfFile(x))(x.contains)
    }



    type Statement <: Ast

    protected def isInstanceOfStatement(x: Any): Boolean

    protected def getStatementOps: StatementOps

    given StatementOps = getStatementOps

    given (given ops: StatementOps): Conversion[AstLike, StatementOps] {
        def apply(x: AstLike): StatementOps = ops
    }

    abstract class StatementOps {

    }

    object Statement {
        def unapply(x: Any): Boolean = isInstanceOfStatement(x)
    }



    type Import <: Statement

    protected def isInstanceOfImport(x: Any): Boolean

    protected def getImportOps: ImportOps

    given ImportOps = getImportOps

    given (given ops: ImportOps): Conversion[AstLike, ImportOps] {
        def apply(x: AstLike): ImportOps = ops
    }

    abstract class ImportOps {
        import Import._

        def (x: Import) tail: Seq[String]

        def (x: Import) head: Seq[ImportType]
    }

    protected def toImport(a: Seq[String], b: Seq[Import.ImportType]): Import

    object Import {
        def apply(a: Seq[String], b: Seq[ImportType]): Import = toImport(a, b)

        def unapply(x: Import): Option[(Seq[String], Seq[ImportType])] = when(isInstanceOfImport(x))((x.tail, x.head))

        enum ImportType {
            case Simple(name: String)
            case Wildcard
            case Rename(name: String, to: Option[String])
        }
    }



    type Package <: Statement

    protected def isInstanceOfPackage(x: Any): Boolean

    protected def getPackageOps: PackageOps

    given PackageOps = getPackageOps

    given (given ops: PackageOps): Conversion[AstLike, PackageOps] {
        def apply(x: AstLike): PackageOps = ops
    }

    abstract class PackageOps {
        def (x: Package) path: Seq[String]
    }

    protected def toPackage(a: Seq[String]): Package

    object Package {
        def apply(a: Seq[String]): Package = toPackage(a)

        def unapply(x: Package): Option[Seq[String]] = when(isInstanceOfPackage(x))(x.path)
    }



    type Expr <: Statement

    protected def isInstanceOfExpr(x: Any): Boolean

    protected def getExprOps: ExprOps

    given ExprOps = getExprOps

    given (given ops: ExprOps): Conversion[AstLike, ExprOps] {
        def apply(x: AstLike): ExprOps = ops
    }

    abstract class ExprOps {
        def (x: Expr) expression: Expression
    }

    protected def toExpr(a: Expression): Expr

    object Expr {
        def apply(a: Expression): Expr = toExpr(a)

        def unapply(x: Expr): Option[Expression] = when(isInstanceOfExpr(x))(x.expression)
    }



    type Definition <: Statement

    protected def isInstanceOfDefinition(x: Any): Boolean

    protected def getDefinitionOps: DefinitionOps

    given DefinitionOps = getDefinitionOps

    given (given ops: DefinitionOps): Conversion[AstLike, DefinitionOps] {
        def apply(x: AstLike): DefinitionOps = ops
    }

    abstract class DefinitionOps {

    }

    object Definition {
        def unapply(x: Definition): Boolean = isInstanceOfDefinition(x)
    }



    type TypeDef <: Definition

    protected def isInstanceOfTypeDef(x: Any): Boolean

    protected def getTypeDefOps: TypeDefOps

    given TypeDefOps = getTypeDefOps

    given (given ops: TypeDefOps): Conversion[AstLike, TypeDefOps] {
        def apply(x: AstLike): TypeDefOps = ops
    }

    abstract class TypeDefOps {
        def (x: TypeDef) name: String

        def (x: TypeDef) bounds: TypeBounds

        def (x: TypeDef) t: Option[Type]
    }

    protected def toTypeDef(a: String, b: TypeBounds, c: Option[Type]): TypeDef

    object TypeDef {
        def apply(a: String, b: TypeBounds, c: Option[Type]): TypeDef = toTypeDef(a, b, c)

        def unapply(x: TypeDef): Option[(String, TypeBounds, Option[Type])] = when(isInstanceOfTypeDef(x))((x.name, x.bounds, x.t))
    }



    type ValDef <: Definition

    protected def isInstanceOfValDef(x: Any): Boolean

    protected def getValDefOps: ValDefOps

    given ValDefOps = getValDefOps

    given (given ops: ValDefOps): Conversion[AstLike, ValDefOps] {
        def apply(x: AstLike): ValDefOps = ops
    }

    abstract class ValDefOps {
        def (x: ValDef) constant: Boolean

        def (x: ValDef) name: String

        def (x: ValDef) t: Option[Type]

        def (x: ValDef) value: Option[Expression]
    }

    protected def toValDef(a: Boolean, b: String, c: Option[Type], d: Option[Expression]): ValDef

    object ValDef {
        def apply(a: Boolean, b: String, c: Option[Type], d: Option[Expression]): ValDef = toValDef(a, b, c, d)

        def unapply(x: ValDef): Option[(Boolean, String, Option[Type], Option[Expression])] = when(isInstanceOfValDef(x))((x.constant, x.name, x.t, x.value))
    }



    type ClassDef <: Definition

    protected def isInstanceOfClassDef(x: Any): Boolean

    protected def getClassDefOps: ClassDefOps

    given ClassDefOps = getClassDefOps

    given (given ops: ClassDefOps): Conversion[AstLike, ClassDefOps] {
        def apply(x: AstLike): ClassDefOps = ops
    }

    abstract class ClassDefOps {
        def (x: ClassDef) types: Option[VariantTypeArgs]

        def (x: ClassDef) name: String

        def (x: ClassDef) higher: Seq[(Seq[String], Option[CallTypeArgs])]

        def (x: ClassDef) inner: Block
    }

    protected def toClassDef(a: Option[VariantTypeArgs], b: String, c: Seq[(Seq[String], Option[CallTypeArgs])], d: Block): ClassDef

    object ClassDef {
        def apply(a: Option[VariantTypeArgs], b: String, c: Seq[(Seq[String], Option[CallTypeArgs])], d: Block): ClassDef = toClassDef(a, b, c, d)

        def unapply(x: ClassDef): Option[(Option[VariantTypeArgs], String, Seq[(Seq[String], Option[CallTypeArgs])], Block)] = when(isInstanceOfClassDef(x))((x.types, x.name, x.higher, x.inner))
    }



    type ObjectDef <: Definition

    protected def isInstanceOfObjectDef(x: Any): Boolean

    protected def getObjectDefOps: ObjectDefOps

    given ObjectDefOps = getObjectDefOps

    given (given ops: ObjectDefOps): Conversion[AstLike, ObjectDefOps] {
        def apply(x: AstLike): ObjectDefOps = ops
    }

    abstract class ObjectDefOps {
        def (x: ObjectDef) name: String

        def (x: ObjectDef) higher: Seq[(Seq[String], Option[CallTypeArgs])]

        def (x: ObjectDef) inner: Block

        def (x: ObjectDef) proto: Expression
    }

    protected def toObjectDef(a: String, b: Seq[(Seq[String], Option[CallTypeArgs])], c: Block, d: Expression): ObjectDef

    object ObjectDef {
        def apply(a: String, b: Seq[(Seq[String], Option[CallTypeArgs])], c: Block, d: Expression): ObjectDef = toObjectDef(a, b, c, d)

        def unapply(x: ObjectDef): Option[(String, Seq[(Seq[String], Option[CallTypeArgs])], Block, Expression)] = when(isInstanceOfObjectDef(x))((x.name, x.higher, x.inner, x.proto))
    }



    type TraitDef <: Definition

    protected def isInstanceOfTraitDef(x: Any): Boolean

    protected def getTraitDefOps: TraitDefOps

    given TraitDefOps = getTraitDefOps

    given (given ops: TraitDefOps): Conversion[AstLike, TraitDefOps] {
        def apply(x: AstLike): TraitDefOps = ops
    }

    abstract class TraitDefOps {
        def (x: TraitDef) types: Option[VariantTypeArgs]

        def (x: TraitDef) name: String

        def (x: TraitDef) higher: Seq[(Seq[String], Option[CallTypeArgs])]

        def (x: TraitDef) inner: Block
    }

    protected def toTraitDef(a: Option[VariantTypeArgs], b: String, c: Seq[(Seq[String], Option[CallTypeArgs])], d: Block): TraitDef

    object TraitDef {
        def apply(a: Option[VariantTypeArgs], b: String, c: Seq[(Seq[String], Option[CallTypeArgs])], d: Block): TraitDef = toTraitDef(a, b, c, d)

        def unapply(x: TraitDef): Option[(Option[VariantTypeArgs], String, Seq[(Seq[String], Option[CallTypeArgs])], Block)] = when(isInstanceOfTraitDef(x))((x.types, x.name, x.higher, x.inner))
    }



    type ImplicitDef <: Definition

    protected def isInstanceOfImplicitDef(x: Any): Boolean

    protected def getImplicitDefOps: ImplicitDefOps

    given ImplicitDefOps = getImplicitDefOps

    given (given ops: ImplicitDefOps): Conversion[AstLike, ImplicitDefOps] {
        def apply(x: AstLike): ImplicitDefOps = ops
    }

    abstract class ImplicitDefOps {
        def (x: ImplicitDef) name: Option[String]

        def (x: ImplicitDef) t: Type

        def (x: ImplicitDef) args: Expression
    }

    protected def toImplicitDef(a: Option[String], b: Type, c: Expression): ImplicitDef

    object ImplicitDef {
        def apply(a: Option[String], b: Type, c: Expression): ImplicitDef = toImplicitDef(a, b, c)

        def unapply(x: ImplicitDef): Option[(Option[String], Type, Expression)] = when(isInstanceOfImplicitDef(x))((x.name, x.t, x.args))
    }



    type MethodDef <: Definition

    protected def isInstanceOfMethodDef(x: Any): Boolean

    protected def getMethodDefOps: MethodDefOps

    given MethodDefOps = getMethodDefOps

    given (given ops: MethodDefOps): Conversion[AstLike, MethodDefOps] {
        def apply(x: AstLike): MethodDefOps = ops
    }

    abstract class MethodDefOps {
        def (x: MethodDef) name: String

        def (x: MethodDef) types: Option[TypeArgs]

        def (x: MethodDef) args: Seq[Args]

        def (x: MethodDef) t: Option[Type]

        def (x: MethodDef) result: Option[Expression]
    }

    protected def toMethodDef(a: String, b: Option[TypeArgs], c: Seq[Args], d: Option[Type], e: Option[Expression]): MethodDef

    object MethodDef {
        def apply(a: String, b: Option[TypeArgs], c: Seq[Args], d: Option[Type], e: Option[Expression]): MethodDef = toMethodDef(a, b, c, d, e)

        def unapply(x: MethodDef): Option[(String, Option[TypeArgs], Seq[Args], Option[Type], Option[Expression])] = when(isInstanceOfMethodDef(x))((x.name, x.types, x.args, x.t, x.result))
    }



    import ModifiedDef.Modifier

    type ModifiedDef <: Definition

    protected def isInstanceOfModifiedDef(x: Any): Boolean

    protected def getModifiedDefOps: ModifiedDefOps

    given ModifiedDefOps = getModifiedDefOps

    given (given ops: ModifiedDefOps): Conversion[AstLike, ModifiedDefOps] {
        def apply(x: AstLike): ModifiedDefOps = ops
    }

    abstract class ModifiedDefOps {

        def (x: ModifiedDef) modifier: Modifier
        def (x: ModifiedDef) definition: Definition
    }

    protected def toModifiedDef(a: Modifier, b: Definition): ModifiedDef

    object ModifiedDef {
        def apply(a: Modifier, b: Definition): ModifiedDef = toModifiedDef(a, b)

        def unapply(x: ModifiedDef): Option[(Modifier, Definition)] = when(isInstanceOfModifiedDef(x))((x.modifier, x.definition))

        enum Modifier {
            case Override
            case Private
            case Protected
        }
    }



    type Appliable <: Ast

    protected def isInstanceOfAppliable(x: Any): Boolean

    protected def getAppliableOps: AppliableOps

    given AppliableOps = getAppliableOps

    given (given ops: AppliableOps): Conversion[AstLike, AppliableOps] {
        def apply(x: AstLike): AppliableOps = ops
    }

    abstract class AppliableOps {

    }

    object Appliable {
        def unapply(x: Appliable): Boolean = isInstanceOfAppliable(x)
    }



    type NamedArg <: Appliable

    protected def isInstanceOfNamedArg(x: Any): Boolean

    protected def getNamedArgOps: NamedArgOps

    given NamedArgOps = getNamedArgOps

    given (given ops: NamedArgOps): Conversion[AstLike, NamedArgOps] {
        def apply(x: AstLike): NamedArgOps = ops
    }

    abstract class NamedArgOps {
        def (x: NamedArg) name: String

        def (x: NamedArg) value: Expression
    }

    protected def toNamedArg(a: String, b: Expression): NamedArg

    object NamedArg {
        def apply(a: String, b: Expression): NamedArg = toNamedArg(a, b)

        def unapply(x: NamedArg): Option[(String, Expression)] = when(isInstanceOfNamedArg(x))((x.name, x.value))
    }



    type Expression <: Appliable

    protected def isInstanceOfExpression(x: Any): Boolean

    protected def getExpressionOps: ExpressionOps

    given ExpressionOps = getExpressionOps

    given (given ops: ExpressionOps): Conversion[AstLike, ExpressionOps] {
        def apply(x: AstLike): ExpressionOps = ops
    }

    abstract class ExpressionOps {

    }

    object Expression {
        def unapply(x: Expression): Boolean = isInstanceOfExpression(x)
    }



    type Ref <: Expression

    protected def isInstanceOfRef(x: Any): Boolean

    protected def getRefOps: RefOps

    given RefOps = getRefOps

    given (given ops: RefOps): Conversion[AstLike, RefOps] {
        def apply(x: AstLike): RefOps = ops
    }

    abstract class RefOps {

    }

    object Ref {
        def unapply(x: Ref): Boolean = isInstanceOfRef(x)
    }



    type Ident <: Ref

    protected def isInstanceOfIdent(x: Any): Boolean

    protected def getIdentOps: IdentOps

    given IdentOps = getIdentOps

    given (given ops: IdentOps): Conversion[AstLike, IdentOps] {
        def apply(x: AstLike): IdentOps = ops
    }

    abstract class IdentOps {
        def (x: Ident) name: String
    }

    protected def toIdent(a: String): Ident

    object Ident {
        def apply(a: String): Ident = toIdent(a)

        def unapply(x: Ident): Option[String] = when(isInstanceOfIdent(x))(x.name)
    }



    type Select <: Ref

    protected def isInstanceOfSelect(x: Any): Boolean

    protected def getSelectOps: SelectOps

    given SelectOps = getSelectOps

    given (given ops: SelectOps): Conversion[AstLike, SelectOps] {
        def apply(x: AstLike): SelectOps = ops
    }

    abstract class SelectOps {
        def (x: Select) refined: Expression

        def (x: Select) refinement: String
    }

    protected def toSelect(a: Expression, b: String): Select

    object Select {
        def apply(a: Expression, b: String): Select = toSelect(a, b)

        def unapply(x: Select): Option[(Expression, String)] = when(isInstanceOfSelect(x))((x.refined, x.refinement))
    }



    type This <: Expression

    protected def isInstanceOfThis(x: Any): Boolean

    protected def getThisOps: ThisOps

    given ThisOps = getThisOps

    given (given ops: ThisOps): Conversion[AstLike, ThisOps] {
        def apply(x: AstLike): ThisOps = ops
    }

    abstract class ThisOps {

    }

    protected def toThis: This

    lazy val This = toThis



    type Super <: Expression

    protected def isInstanceOfSuper(x: Any): Boolean

    protected def getSuperOps: SuperOps

    given SuperOps = getSuperOps

    given (given ops: SuperOps): Conversion[AstLike, SuperOps] {
        def apply(x: AstLike): SuperOps = ops
    }

    abstract class SuperOps {

    }

    protected def toSuper: Super

    lazy val Super = toSuper



    type Typed <: Expression

    protected def isInstanceOfTyped(x: Any): Boolean

    protected def getTypedOps: TypedOps

    given TypedOps = getTypedOps

    given (given ops: TypedOps): Conversion[AstLike, TypedOps] {
        def apply(x: AstLike): TypedOps = ops
    }

    abstract class TypedOps {
        def (x: Typed) value: Expression

        def (x: Typed) t: Type
    }

    protected def toTyped(a: Expression, b: Type): Typed

    object Typed {
        def apply(a: Expression, b: Type): Typed = toTyped(a, b)

        def unapply(x: Typed): Option[(Expression, Type)] = when(isInstanceOfTyped(x))((x.value, x.t))
    }



    type Literal <: Expression

    protected def isInstanceOfLiteral(x: Any): Boolean

    protected def getLiteralOps: LiteralOps

    given LiteralOps = getLiteralOps

    given (given ops: LiteralOps): Conversion[AstLike, LiteralOps] {
        def apply(x: AstLike): LiteralOps = ops
    }

    abstract class LiteralOps {
        def (x: Literal) value: String
    }

    protected def toLiteral(x: String): Literal

    object Literal {
        def apply(x: String): Literal = toLiteral(x)

        def unapply(x: Literal): Option[String] = when(isInstanceOfLiteral(x))(x.value)
    }



    type Apply <: Expression

    protected def isInstanceOfApply(x: Any): Boolean

    protected def getApplyOps: ApplyOps

    given ApplyOps = getApplyOps

    given (given ops: ApplyOps): Conversion[AstLike, ApplyOps] {
        def apply(x: AstLike): ApplyOps = ops
    }

    abstract class ApplyOps {
        def (x: Apply) caller: Expression

        def (x: Apply) invocation: Seq[Appliable]
    }

    protected def toApply(a: Expression, b: Seq[Appliable]): Apply

    object Apply {
        def apply(a: Expression, b: Seq[Appliable]): Apply = toApply(a, b)

        def unapply(x: Apply): Option[(Expression, Seq[Appliable])] = when(isInstanceOfApply(x))((x.caller, x.invocation))
    }



    type Create <: Expression

    protected def isInstanceOfCreate(x: Any): Boolean

    protected def getCreateOps: CreateOps

    given CreateOps = getCreateOps

    given (given ops: CreateOps): Conversion[AstLike, CreateOps] {
        def apply(x: AstLike): CreateOps = ops
    }

    abstract class CreateOps {
        def (x: Create) name: Type

        def (x: Create) ext: Expression

        def (x: Create) block: Block
    }

    protected def toCreate(a: Type, b: Expression, c: Block): Create

    object Create {
        def apply(a: Type, b: Expression, c: Block): Create = toCreate(a, b, c)

        def unapply(x: Create): Option[(Type, Expression, Block)] = when(isInstanceOfCreate(x))((x.name, x.ext, x.block))
    }



    type GivenApply <: Expression

    protected def isInstanceOfGivenApply(x: Any): Boolean

    protected def getGivenApplyOps: GivenApplyOps

    given GivenApplyOps = getGivenApplyOps

    given (given ops: GivenApplyOps): Conversion[AstLike, GivenApplyOps] {
        def apply(x: AstLike): GivenApplyOps = ops
    }

    abstract class GivenApplyOps {
        def (x: GivenApply) caller: Expression

        def (x: GivenApply) invocation: Seq[Appliable]
    }

    protected def toGivenApply(a: Expression, b: Seq[Appliable]): GivenApply

    object GivenApply {
        def apply(a: Expression, b: Seq[Appliable]): GivenApply = toGivenApply(a, b)

        def unapply(x: GivenApply): Option[(Expression, Seq[Appliable])] = when(isInstanceOfGivenApply(x))((x.caller, x.invocation))
    }



    type TypeApply <: Expression

    protected def isInstanceOfTypeApply(x: Any): Boolean

    protected def getTypeApplyOps: TypeApplyOps

    given TypeApplyOps = getTypeApplyOps

    given (given ops: TypeApplyOps): Conversion[AstLike, TypeApplyOps] {
        def apply(x: AstLike): TypeApplyOps = ops
    }

    abstract class TypeApplyOps {
        def (x: TypeApply) caller: Expression

        def (x: TypeApply) invocation: CallTypeArgs
    }

    protected def toTypeApply(a: Expression, b: CallTypeArgs): TypeApply

    object TypeApply {
        def apply(a: Expression, b: CallTypeArgs): TypeApply = toTypeApply(a, b)

        def unapply(x: TypeApply): Option[(Expression, CallTypeArgs)] = when(isInstanceOfTypeApply(x))((x.caller, x.invocation))
    }



    type Infix <: Expression

    protected def isInstanceOfInfix(x: Any): Boolean

    protected def getInfixOps: InfixOps

    given InfixOps = getInfixOps

    given (given ops: InfixOps): Conversion[AstLike, InfixOps] {
        def apply(x: AstLike): InfixOps = ops
    }

    abstract class InfixOps {
        def (x: Infix) first: Expression

        def (x: Infix) middle: String

        def (x: Infix) second: Expression
    }

    protected def toInfix(a: Expression, b: String, c: Expression): Infix

    object Infix {
        def apply(a: Expression, b: String, c: Expression): Infix = toInfix(a, b, c)

        def unapply(x: Infix): Option[(Expression, String, Expression)] = when(isInstanceOfInfix(x))((x.first, x.middle, x.second))
    }



    type Parens <: Expression

    protected def isInstanceOfParens(x: Any): Boolean

    protected def getParensOps: ParensOps

    given ParensOps = getParensOps

    given (given ops: ParensOps): Conversion[AstLike, ParensOps] {
        def apply(x: AstLike): ParensOps = ops
    }

    abstract class ParensOps {
        def (x: Parens) exp: Expression
    }

    protected def toParens(a: Expression): Parens

    object Parens {
        def apply(a: Expression): Parens = toParens(a)

        def unapply(x: Parens): Option[Expression] = when(isInstanceOfParens(x))(x.exp)
    }



    type Block <: Expression

    protected def isInstanceOfBlock(x: Any): Boolean

    protected def getBlockOps: BlockOps

    given BlockOps = getBlockOps

    given (given ops: BlockOps): Conversion[AstLike, BlockOps] {
        def apply(x: AstLike): BlockOps = ops
    }

    abstract class BlockOps {
        def (x: Block) statements: Seq[Statement]
    }

    protected def toBlock(x: Seq[Statement]): Block

    object Block {
        def apply(x: Seq[Statement]): Block = toBlock(x)

        def unapply(x: Block): Option[Seq[Statement]] = when(isInstanceOfBlock(x))(x.statements)
    }



    type With <: Expression

    protected def isInstanceOfWith(x: Any): Boolean

    protected def getWithOps: WithOps

    given WithOps = getWithOps

    given (given ops: WithOps): Conversion[AstLike, WithOps] {
        def apply(x: AstLike): WithOps = ops
    }

    abstract class WithOps {
        def (x: With) scope: Expression

        def (x: With) value: Expression
    }

    protected def toWith(a: Expression, b: Expression): With

    object With {
        def apply(a: Expression, b: Expression): With = toWith(a, b)

        def unapply(x: With): Option[(Expression, Expression)] = when(isInstanceOfWith(x))((x.scope, x.value))
    }



    type If <: Expression

    protected def isInstanceOfIf(x: Any): Boolean

    protected def getIfOps: IfOps

    given IfOps = getIfOps

    given (given ops: IfOps): Conversion[AstLike, IfOps] {
        def apply(x: AstLike): IfOps = ops
    }

    abstract class IfOps {
        def (x: If) toggle: Expression

        def (x: If) therefore: Expression

        def (x: If) otherwise: Option[Expression]
    }

    protected def toIf(a: Expression, b: Expression, c: Option[Expression]): If

    object If {
        def apply(a: Expression, b: Expression, c: Option[Expression]): If = toIf(a, b, c)

        def unapply(x: If): Option[(Expression, Expression, Option[Expression])] = when(isInstanceOfIf(x))((x.toggle, x.therefore, x.otherwise))
    }



    type Function <: Expression

    protected def isInstanceOfFunction(x: Any): Boolean

    protected def getFunctionOps: FunctionOps

    given FunctionOps = getFunctionOps

    given (given ops: FunctionOps): Conversion[AstLike, FunctionOps] {
        def apply(x: AstLike): FunctionOps = ops
    }

    abstract class FunctionOps {
        def (x: Function) args: OptionalArgs

        def (x: Function) expression: Expression
    }

    protected def toFunction(a: OptionalArgs, b: Expression): Function

    object Function {
        def apply(a: OptionalArgs, b: Expression): Function = toFunction(a, b)

        def unapply(x: Function): Option[(OptionalArgs, Expression)] = when(isInstanceOfFunction(x))((x.args, x.expression))
    }



    type Match <: Expression

    protected def isInstanceOfMatch(x: Any): Boolean

    protected def getMatchOps: MatchOps

    given MatchOps = getMatchOps

    given (given ops: MatchOps): Conversion[AstLike, MatchOps] {
        def apply(x: AstLike): MatchOps = ops
    }

    abstract class MatchOps {
        def (x: Match) cases: Seq[(Pattern, Expression)]
    }

    protected def toMatch(a: Seq[(Pattern, Expression)]): Match

    object Match {
        def apply(a: Seq[(Pattern, Expression)]): Match = toMatch(a)

        def unapply(x: Match): Option[Seq[(Pattern, Expression)]] = when(isInstanceOfMatch(x))(x.cases)
    }



    type Try <: Expression

    protected def isInstanceOfTry(x: Any): Boolean

    protected def getTryOps: TryOps

    given TryOps = getTryOps

    given (given ops: TryOps): Conversion[AstLike, TryOps] {
        def apply(x: AstLike): TryOps = ops
    }

    abstract class TryOps {
        def (x: Try) throwing: Expression

        def (x: Try) catching: Match

        def (x: Try) always: Option[Expression]
    }

    protected def toTry(a: Expression, b: Match, c: Option[Expression]): Try

    object Try {
        def apply(a: Expression, b: Match, c: Option[Expression]): Try = toTry(a, b, c)

        def unapply(x: Try): Option[(Expression, Match, Option[Expression])] = when(isInstanceOfTry(x))((x.throwing, x.catching, x.always))
    }



    type Pattern <: Ast

    protected def isInstanceOfPattern(x: Any): Boolean

    protected def getPatternOps: PatternOps

    given PatternOps = getPatternOps

    given (given ops: PatternOps): Conversion[AstLike, PatternOps] {
        def apply(x: AstLike): PatternOps = ops
    }

    abstract class PatternOps {

    }

    object Pattern {
        def unapply(x: Pattern): Boolean = isInstanceOfPattern(x)
    }



    type Value <: Pattern

    protected def isInstanceOfValue(x: Any): Boolean

    protected def getValueOps: ValueOps

    given ValueOps = getValueOps

    given (given ops: ValueOps): Conversion[AstLike, ValueOps] {
        def apply(x: AstLike): ValueOps = ops
    }

    abstract class ValueOps {
        def (x: Value) name: String
    }

    protected def toValue(a: String): Value

    object Value {
        def apply(a: String): Value = toValue(a)

        def unapply(x: Value): Option[String] = when(isInstanceOfValue(x))(x.name)
    }



    type Bind <: Pattern

    protected def isInstanceOfBind(x: Any): Boolean

    protected def getBindOps: BindOps

    given BindOps = getBindOps

    given (given ops: BindOps): Conversion[AstLike, BindOps] {
        def apply(x: AstLike): BindOps = ops
    }

    abstract class BindOps {
        def (x: Bind) first: Pattern

        def (x: Bind) second: Pattern
    }

    protected def toBind(a: Pattern, b: Pattern): Bind

    object Bind {
        def apply(a: Pattern, b: Pattern): Bind = toBind(a, b)

        def unapply(x: Bind): Option[(Pattern, Pattern)] = when(isInstanceOfBind(x))((x.first, x.second))
    }



    type Unapply <: Pattern

    protected def isInstanceOfUnapply(x: Any): Boolean

    protected def getUnapplyOps: UnapplyOps

    given UnapplyOps = getUnapplyOps

    given (given ops: UnapplyOps): Conversion[AstLike, UnapplyOps] {
        def apply(x: AstLike): UnapplyOps = ops
    }

    abstract class UnapplyOps {
        def (x: Unapply) unapplier: Expression

        def (x: Unapply) args: Seq[Pattern]
    }

    protected def toUnapply(a: Expression, b: Seq[Pattern]): Unapply

    object Unapply {
        def apply(a: Expression, b: Seq[Pattern]): Unapply = toUnapply(a, b)

        def unapply(x: Unapply): Option[(Expression, Seq[Pattern])] = when(isInstanceOfUnapply(x))((x.unapplier, x.args))
    }



    type Alternatives <: Pattern

    protected def isInstanceOfAlternatives(x: Any): Boolean

    protected def getAlternativesOps: AlternativesOps

    given AlternativesOps = getAlternativesOps

    given (given ops: AlternativesOps): Conversion[AstLike, AlternativesOps] {
        def apply(x: AstLike): AlternativesOps = ops
    }

    abstract class AlternativesOps {
        def (x: Alternatives) alternatives: Seq[Pattern]
    }

    protected def toAlternatives(x: Seq[Pattern]): Alternatives

    object Alternatives {
        def apply(x: Seq[Pattern]): Alternatives = toAlternatives(x)

        def unapply(x: Alternatives): Option[Seq[Pattern]] = when(isInstanceOfAlternatives(x))(x.alternatives)
    }



    type TypeTest <: Pattern

    protected def isInstanceOfTypeTest(x: Any): Boolean

    protected def getTypeTestOps: TypeTestOps

    given TypeTestOps = getTypeTestOps

    given (given ops: TypeTestOps): Conversion[AstLike, TypeTestOps] {
        def apply(x: AstLike): TypeTestOps = ops
    }

    abstract class TypeTestOps {
        def (x: TypeTest) t: Type

        def (x: TypeTest) pattern: Pattern
    }

    protected def toTypeTest(a: Pattern, b: Type): TypeTest

    object TypeTest {
        def apply(a: Pattern, b: Type): TypeTest = toTypeTest(a, b)

        def unapply(x: TypeTest): Option[(Pattern, Type)] = when(isInstanceOfTypeTest(x))((x.pattern, x.t))
    }



    type Wildcard <: Pattern

    protected def isInstanceOfWildcard(x: Any): Boolean

    protected def getWildcardOps: WildcardOps

    given WildcardOps = getWildcardOps

    given (given ops: WildcardOps): Conversion[AstLike, WildcardOps] {
        def apply(x: AstLike): WildcardOps = ops
    }

    abstract class WildcardOps {

    }

    protected def toWildcard(): Wildcard

    object Wildcard {
        def apply(): Wildcard = toWildcard()

        def unapply(x: Wildcard): Boolean = isInstanceOfWildcard(x)
    }



    type TypeBounds <: Ast

    protected def isInstanceOfTypeBounds(x: Any): Boolean

    protected def getTypeBoundsOps: TypeBoundsOps

    given TypeBoundsOps = getTypeBoundsOps

    given (given ops: TypeBoundsOps): Conversion[AstLike, TypeBoundsOps] {
        def apply(x: AstLike): TypeBoundsOps = ops
    }

    abstract class TypeBoundsOps {
        import TypeBounds._

        def (x: TypeBounds) t: Seq[BoundType]
    }

    protected def toTypeBounds(a: Seq[TypeBounds.BoundType]): TypeBounds

    object TypeBounds {
        def apply(a: Seq[BoundType]): TypeBounds = toTypeBounds(a)

        def unapply(x: TypeBounds): Option[Seq[BoundType]] = when(isInstanceOfTypeBounds(x))(x.t)

        enum BoundType {
            case Subclass(t: Type)
            case Superclass(t: Type)
            case ExistsImplicit(t: Type)
        }
    }



    type Type <: Ast

    protected def isInstanceOfType(x: Any): Boolean

    protected def getTypeOps: TypeOps

    given TypeOps = getTypeOps

    given (given ops: TypeOps): Conversion[AstLike, TypeOps] {
        def apply(x: AstLike): TypeOps = ops
    }

    abstract class TypeOps {

    }

    object Type {
        def unapply(x: Type): Boolean = isInstanceOfType(x)
    }



    type TypeSelect <: Type

    protected def isInstanceOfTypeSelect(x: Any): Boolean

    protected def getTypeSelectOps: TypeSelectOps

    given TypeSelectOps = getTypeSelectOps

    given (given ops: TypeSelectOps): Conversion[AstLike, TypeSelectOps] {
        def apply(x: AstLike): TypeSelectOps = ops
    }

    abstract class TypeSelectOps {
        def (x: TypeSelect) expression: Expression

        def (x: TypeSelect) name: String
    }

    protected def toTypeSelect(a: Expression, b: String): TypeSelect

    object TypeSelect {
        def apply(a: Expression, b: String): TypeSelect = toTypeSelect(a, b)

        def unapply(x: TypeSelect): Option[(Expression, String)] = when(isInstanceOfTypeSelect(x))((x.expression, x.name))
    }



    type TypeLambda <: Type

    protected def isInstanceOfTypeLambda(x: Any): Boolean

    protected def getTypeLambdaOps: TypeLambdaOps

    given TypeLambdaOps = getTypeLambdaOps

    given (given ops: TypeLambdaOps): Conversion[AstLike, TypeLambdaOps] {
        def apply(x: AstLike): TypeLambdaOps = ops
    }

    abstract class TypeLambdaOps {
        def (x: TypeLambda) args: VariantTypeArgs

        def (x: TypeLambda) t: Type
    }

    protected def toTypeLambda(a: VariantTypeArgs, b: Type): TypeLambda

    object TypeLambda {
        def apply(a: VariantTypeArgs, b: Type): TypeLambda = toTypeLambda(a, b)

        def unapply(x: TypeLambda): Option[(VariantTypeArgs, Type)] = when(isInstanceOfTypeLambda(x))((x.args, x.t))
    }



    type SingletonType <: Type

    protected def isInstanceOfSingletonType(x: Any): Boolean

    protected def getSingletonTypeOps: SingletonTypeOps

    given SingletonTypeOps = getSingletonTypeOps

    given (given ops: SingletonTypeOps): Conversion[AstLike, SingletonTypeOps] {
        def apply(x: AstLike): SingletonTypeOps = ops
    }

    abstract class SingletonTypeOps {
        def (x: SingletonType) value: Expression
    }

    protected def toSingletonType(a: Expression): SingletonType

    object SingletonType {
        def apply(a: Expression): SingletonType = toSingletonType(a)

        def unapply(x: SingletonType): Option[Expression] = when(isInstanceOfSingletonType(x))(x.value)
    }



    type ByNameType <: Type

    protected def isInstanceOfByNameType(x: Any): Boolean

    protected def getByNameTypeOps: ByNameTypeOps

    given ByNameTypeOps = getByNameTypeOps

    given (given ops: ByNameTypeOps): Conversion[AstLike, ByNameTypeOps] {
        def apply(x: AstLike): ByNameTypeOps = ops
    }

    abstract class ByNameTypeOps {
        def (x: ByNameType) t: Type
    }

    protected def toByNameType(a: Type): ByNameType

    object ByNameType {
        def apply(a: Type): ByNameType = toByNameType(a)

        def unapply(x: ByNameType): Option[Type] = when(isInstanceOfByNameType(x))(x.t)
    }



    type MatchType <: Type

    protected def isInstanceOfMatchType(x: Any): Boolean

    protected def getMatchTypeOps: MatchTypeOps

    given MatchTypeOps = getMatchTypeOps

    given (given ops: MatchTypeOps): Conversion[AstLike, MatchTypeOps] {
        def apply(x: AstLike): MatchTypeOps = ops
    }

    abstract class MatchTypeOps {
        def (x: MatchType) first: Type
        def (x: MatchType) cases: Seq[(TypePattern, Type)]
    }

    protected def toMatchType(a: Type, b: Seq[(TypePattern, Type)]): MatchType

    object MatchType {
        def apply(a: Type, b: Seq[(TypePattern, Type)]): MatchType = toMatchType(a, b)

        def unapply(x: MatchType): Option[(Type, Seq[(TypePattern, Type)])] = when(isInstanceOfMatchType(x))((x.first, x.cases))
    }



    type OrType <: Type

    protected def isInstanceOfOrType(x: Any): Boolean

    protected def getOrTypeOps: OrTypeOps

    given OrTypeOps = getOrTypeOps

    given (given ops: OrTypeOps): Conversion[AstLike, OrTypeOps] {
        def apply(x: AstLike): OrTypeOps = ops
    }

    abstract class OrTypeOps {
        def (x: OrType) first: Type

        def (x: OrType) second: Type
    }

    protected def toOrType(a: Type, b: Type): OrType

    object OrType {
        def apply(a: Type, b: Type): OrType = toOrType(a, b)

        def unapply(x: OrType): Option[(Type, Type)] = when(isInstanceOfOrType(x))((x.first, x.second))
    }



    type AndType <: Type

    protected def isInstanceOfAndType(x: Any): Boolean

    protected def getAndTypeOps: AndTypeOps

    given AndTypeOps = getAndTypeOps

    given (given ops: AndTypeOps): Conversion[AstLike, AndTypeOps] {
        def apply(x: AstLike): AndTypeOps = ops
    }

    abstract class AndTypeOps {
        def (x: AndType) first: Type

        def (x: AndType) second: Type
    }

    protected def toAndType(a: Type, b: Type): AndType

    object AndType {
        def apply(a: Type, b: Type): AndType = toAndType(a, b)

        def unapply(x: AndType): Option[(Type, Type)] = when(isInstanceOfAndType(x))((x.first, x.second))
    }



    type AppliedType <: Type

    protected def isInstanceOfAppliedType(x: Any): Boolean

    protected def getAppliedTypeOps: AppliedTypeOps

    given AppliedTypeOps = getAppliedTypeOps

    given (given ops: AppliedTypeOps): Conversion[AstLike, AppliedTypeOps] {
        def apply(x: AstLike): AppliedTypeOps = ops
    }

    abstract class AppliedTypeOps {
        def (x: AppliedType) t: Type

        def (x: AppliedType) call: CallTypeArgs
    }

    protected def toAppliedType(a: Type, b: CallTypeArgs): AppliedType

    object AppliedType {
        def apply(a: Type, b: CallTypeArgs): AppliedType = toAppliedType(a, b)

        def unapply(x: AppliedType): Option[(Type, CallTypeArgs)] = when(isInstanceOfAppliedType(x))((x.t, x.call))
    }



    type NamedType <: Type

    protected def isInstanceOfNamedType(x: Any): Boolean

    protected def getNamedTypeOps: NamedTypeOps

    given NamedTypeOps = getNamedTypeOps

    given (given ops: NamedTypeOps): Conversion[AstLike, NamedTypeOps] {
        def apply(x: AstLike): NamedTypeOps = ops
    }

    abstract class NamedTypeOps {
        def (x: NamedType) name: Seq[String]
    }

    protected def toNamedType(a: Seq[String]): NamedType

    object NamedType {
        def apply(a: Seq[String]): NamedType = toNamedType(a)

        def unapply(x: NamedType): Option[Seq[String]] = when(isInstanceOfNamedType(x))(x.name)
    }



    type TypePattern <: Ast

    protected def isInstanceOfTypePattern(x: Any): Boolean

    protected def getTypePatternOps: TypePatternOps

    given TypePatternOps = getTypePatternOps

    given (given ops: TypePatternOps): Conversion[AstLike, TypePatternOps] {
        def apply(x: AstLike): TypePatternOps = ops
    }

    abstract class TypePatternOps {

    }

    object TypePattern {
        def unapply(x: TypePattern): Boolean = isInstanceOfTypePattern(x)
    }



    type ExtractType <: TypePattern

    protected def isInstanceOfExtractType(x: Any): Boolean

    protected def getExtractTypeOps: ExtractTypeOps

    given ExtractTypeOps = getExtractTypeOps

    given (given ops: ExtractTypeOps): Conversion[AstLike, ExtractTypeOps] {
        def apply(x: AstLike): ExtractTypeOps = ops
    }

    abstract class ExtractTypeOps {
        def (x: ExtractType) extract: Type

        def (x: ExtractType) types: Seq[TypePattern]
    }

    protected def toExtractType(a: Type, b: Seq[TypePattern]): ExtractType

    object ExtractType {
        def apply(a: Type, b: Seq[TypePattern]): ExtractType = toExtractType(a, b)

        def unapply(x: ExtractType): Option[(Type, Seq[TypePattern])] = when(isInstanceOfExtractType(x))((x.extract, x.types))
    }



    type TypeWildcard <: TypePattern

    protected def isInstanceOfTypeWildcard(x: Any): Boolean

    protected def getTypeWildcardOps: TypeWildcardOps

    given TypeWildcardOps = getTypeWildcardOps

    given (given ops: TypeWildcardOps): Conversion[AstLike, TypeWildcardOps] {
        def apply(x: AstLike): TypeWildcardOps = ops
    }

    abstract class TypeWildcardOps {

    }

    protected def toTypeWildcard(): TypeWildcard

    object TypeWildcard {
        def apply(): TypeWildcard = toTypeWildcard()

        def unapply(x: TypeWildcard): Boolean = isInstanceOfTypeWildcard(x)
    }



    type TypeValue <: TypePattern

    protected def isInstanceOfTypeValue(x: Any): Boolean

    protected def getTypeValueOps: TypeValueOps

    given TypeValueOps = getTypeValueOps

    given (given ops: TypeValueOps): Conversion[AstLike, TypeValueOps] {
        def apply(x: AstLike): TypeValueOps = ops
    }

    abstract class TypeValueOps {
        def (x: TypeValue) name: Type
    }

    protected def toTypeValue(a: Type): TypeValue

    object TypeValue {
        def apply(a: Type): TypeValue = toTypeValue(a)

        def unapply(x: TypeValue): Option[Type] = when(isInstanceOfTypeValue(x))(x.name)
    }



    type OptionalArgs <: Ast

    protected def isInstanceOfOptionalArgs(x: Any): Boolean

    protected def getOptionalArgsOps: OptionalArgsOps

    given OptionalArgsOps = getOptionalArgsOps

    given (given ops: OptionalArgsOps): Conversion[AstLike, OptionalArgsOps] {
        def apply(x: AstLike): OptionalArgsOps = ops
    }

    abstract class OptionalArgsOps {
        import OptionalArgs.ArgType

        def (x: OptionalArgs) oargs: Seq[(String, Option[Type])]

        def (x: OptionalArgs) t: ArgType
    }

    protected def toOptionalArgs(a: Seq[(String, Option[Type])], b: OptionalArgs.ArgType): OptionalArgs

    object OptionalArgs {
        def apply(a: Seq[(String, Option[Type])], b: ArgType): OptionalArgs = toOptionalArgs(a, b)

        def unapply(x: OptionalArgs): Option[(Seq[(String, Option[Type])], ArgType)] = when(isInstanceOfOptionalArgs(x))((x.oargs, x.t))

        enum ArgType {
            case Given
            case Normal
        }
    }



    type Args <: OptionalArgs

    protected def isInstanceOfArgs(x: Any): Boolean

    protected def getArgsOps: ArgsOps

    given ArgsOps = getArgsOps

    given (given ops: ArgsOps): Conversion[AstLike, ArgsOps] {
        def apply(x: AstLike): ArgsOps = ops
    }

    abstract class ArgsOps {
        def (x: Args) args: Seq[(String, Type)]
    }

    protected def toArgs(a: Seq[(String, Type)], b: OptionalArgs.ArgType): Args

    object Args {
        import OptionalArgs._

        def apply(a: Seq[(String, Type)], b: ArgType): Args = toArgs(a, b)

        def unapply(x: Args): Option[(Seq[(String, Type)], ArgType)] = when(isInstanceOfArgs(x))((x.args, x.t))
    }



    type CallTypeArgs <: Ast

    protected def isInstanceOfCallTypeArgs(x: Any): Boolean

    protected def getCallTypeArgsOps: CallTypeArgsOps

    given CallTypeArgsOps = getCallTypeArgsOps

    given (given ops: CallTypeArgsOps): Conversion[AstLike, CallTypeArgsOps] {
        def apply(x: AstLike): CallTypeArgsOps = ops
    }

    abstract class CallTypeArgsOps {
        def (x: CallTypeArgs) types: Seq[Type]
    }

    protected def toCallTypeArgs(a: Seq[Type]): CallTypeArgs

    object CallTypeArgs {
        def apply(a: Seq[Type]): CallTypeArgs = toCallTypeArgs(a)

        def unapply(x: CallTypeArgs): Option[Seq[Type]] = when(isInstanceOfCallTypeArgs(x))(x.types)
    }



    type TypeArgs <: Ast

    protected def isInstanceOfTypeArgs(x: Any): Boolean

    protected def getTypeArgsOps: TypeArgsOps

    given TypeArgsOps = getTypeArgsOps

    given (given ops: TypeArgsOps): Conversion[AstLike, TypeArgsOps] {
        def apply(x: AstLike): TypeArgsOps = ops
    }

    abstract class TypeArgsOps {
        def (x: TypeArgs) types: Seq[(Type, TypeBounds)]

        def (x: TypeArgs) convertToCallTypeArgs: CallTypeArgs
    }

    protected def toTypeArgs(a: Seq[(Type, TypeBounds)]): TypeArgs

    object TypeArgs {
        def apply(a: Seq[(Type, TypeBounds)]): TypeArgs = toTypeArgs(a)

        def unapply(x: TypeArgs): Option[Seq[(Type, TypeBounds)]] = when(isInstanceOfTypeArgs(x))(x.types)
    }



    type VariantTypeArgs <: Ast

    protected def isInstanceOfVariantTypeArgs(x: Any): Boolean

    protected def getVariantTypeArgsOps: VariantTypeArgsOps

    given VariantTypeArgsOps = getVariantTypeArgsOps

    given (given ops: VariantTypeArgsOps): Conversion[AstLike, VariantTypeArgsOps] {
        def apply(x: AstLike): VariantTypeArgsOps = ops
    }

    abstract class VariantTypeArgsOps {
        def (x: VariantTypeArgs) types: Seq[(Int, Type, TypeBounds)]

        def (x: VariantTypeArgs) convertToTypeArgs: TypeArgs
    }

    protected def toVariantTypeArgs(a: Seq[(Int, Type, TypeBounds)]): VariantTypeArgs


    object VariantTypeArgs {
        def apply(a: Seq[(Int, Type, TypeBounds)]): VariantTypeArgs = toVariantTypeArgs(a)

        def unapply(x: VariantTypeArgs): Option[Seq[(Int, Type, TypeBounds)]] = {
            x.convertToTypeArgs
            when(isInstanceOfVariantTypeArgs(x))(x.types)
        }
    }
}

object AstLike {
    def when[T](x: Boolean)(t: => T): Option[T] = if(x) Some(t) else None
}