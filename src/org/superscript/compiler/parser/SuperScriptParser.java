// Generated from /home/rouli-freeman/Documents/SuperScript/SuperScript.g4 by ANTLR 4.7.1
package org.superscript.compiler.parser;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SuperScriptParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		T__38=39, T__39=40, T__40=41, T__41=42, T__42=43, T__43=44, STRING=45, 
		LCURLY=46, RCURLY=47, LPAREN=48, RPAREN=49, LBRACK=50, RBRACK=51, NAME=52, 
		STARTNAMEINFIX=53, WS=54, NEWLINE=55, NUM=56, INDENT=57, DEDENT=58;
	public static final int
		RULE_file = 0, RULE_statement = 1, RULE_stmt = 2, RULE_imp = 3, RULE_imppath = 4, 
		RULE_imptype = 5, RULE_pkg = 6, RULE_appliable = 7, RULE_expressioneof = 8, 
		RULE_expression = 9, RULE_block = 10, RULE_mat = 11, RULE_pattern = 12, 
		RULE_definition = 13, RULE_urlwitharg = 14, RULE_t = 15, RULE_tm = 16, 
		RULE_tp = 17, RULE_typebounds = 18, RULE_boundtype = 19, RULE_calltypeargs = 20, 
		RULE_typeargs = 21, RULE_varianttypeargs = 22, RULE_variance = 23, RULE_goptionalargs = 24, 
		RULE_optionalargs = 25, RULE_oarg = 26, RULE_gargs = 27, RULE_args = 28, 
		RULE_arg = 29, RULE_infixname = 30, RULE_name = 31;
	public static final String[] ruleNames = {
		"file", "statement", "stmt", "imp", "imppath", "imptype", "pkg", "appliable", 
		"expressioneof", "expression", "block", "mat", "pattern", "definition", 
		"urlwitharg", "t", "tm", "tp", "typebounds", "boundtype", "calltypeargs", 
		"typeargs", "varianttypeargs", "variance", "goptionalargs", "optionalargs", 
		"oarg", "gargs", "args", "arg", "infixname", "name"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'import'", "'_'", "','", "'.'", "'given'", "'=>'", "'package'", 
		"':'", "'this'", "'super'", "'with'", "'do'", "'if'", "'else'", "'then'", 
		"'new'", "'from'", "'try'", "'catch'", "'finally'", "'case'", "'@'", "'|'", 
		"'val'", "'='", "'var'", "'type'", "'def'", "'as'", "'class'", "'extends'", 
		"'trait'", "'object'", "'override'", "'private'", "'protected'", "'/'", 
		"'&'", "'=>>'", "'match'", "'<:'", "'>:'", "'+'", "'-'", null, "'{'", 
		"'}'", "'('", "')'", "'['", "']'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, "STRING", "LCURLY", 
		"RCURLY", "LPAREN", "RPAREN", "LBRACK", "RBRACK", "NAME", "STARTNAMEINFIX", 
		"WS", "NEWLINE", "NUM", "INDENT", "DEDENT"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "SuperScript.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public SuperScriptParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class FileContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(SuperScriptParser.EOF, 0); }
		public List<TerminalNode> NEWLINE() { return getTokens(SuperScriptParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(SuperScriptParser.NEWLINE, i);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public FileContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_file; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterFile(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitFile(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitFile(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FileContext file() throws RecognitionException {
		FileContext _localctx = new FileContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_file);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(67);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(64);
					match(NEWLINE);
					}
					} 
				}
				setState(69);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			}
			setState(73);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__4) | (1L << T__6) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__12) | (1L << T__15) | (1L << T__17) | (1L << T__23) | (1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__29) | (1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__34) | (1L << T__35) | (1L << STRING) | (1L << LPAREN) | (1L << NAME) | (1L << NEWLINE) | (1L << NUM))) != 0)) {
				{
				{
				setState(70);
				statement();
				}
				}
				setState(75);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(76);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public StmtContext stmt() {
			return getRuleContext(StmtContext.class,0);
		}
		public List<TerminalNode> NEWLINE() { return getTokens(SuperScriptParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(SuperScriptParser.NEWLINE, i);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_statement);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(78);
			stmt();
			setState(80); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(79);
					match(NEWLINE);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(82); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StmtContext extends ParserRuleContext {
		public StmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stmt; }
	 
		public StmtContext() { }
		public void copyFrom(StmtContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ExprstmtContext extends StmtContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ExprstmtContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterExprstmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitExprstmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitExprstmt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DefstmtContext extends StmtContext {
		public DefinitionContext definition() {
			return getRuleContext(DefinitionContext.class,0);
		}
		public DefstmtContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterDefstmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitDefstmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitDefstmt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PkgstmtContext extends StmtContext {
		public PkgContext pkg() {
			return getRuleContext(PkgContext.class,0);
		}
		public PkgstmtContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterPkgstmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitPkgstmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitPkgstmt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ImpstmtContext extends StmtContext {
		public ImpContext imp() {
			return getRuleContext(ImpContext.class,0);
		}
		public ImpstmtContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterImpstmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitImpstmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitImpstmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StmtContext stmt() throws RecognitionException {
		StmtContext _localctx = new StmtContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_stmt);
		try {
			setState(88);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				_localctx = new ExprstmtContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(84);
				expression(0);
				}
				break;
			case 2:
				_localctx = new DefstmtContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(85);
				definition();
				}
				break;
			case 3:
				_localctx = new ImpstmtContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(86);
				imp();
				}
				break;
			case 4:
				_localctx = new PkgstmtContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(87);
				pkg();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ImpContext extends ParserRuleContext {
		public ImpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_imp; }
	 
		public ImpContext() { }
		public void copyFrom(ImpContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class NameimpContext extends ImpContext {
		public ImppathContext imppath() {
			return getRuleContext(ImppathContext.class,0);
		}
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public NameimpContext(ImpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterNameimp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitNameimp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitNameimp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TypeimpContext extends ImpContext {
		public ImppathContext imppath() {
			return getRuleContext(ImppathContext.class,0);
		}
		public List<ImptypeContext> imptype() {
			return getRuleContexts(ImptypeContext.class);
		}
		public ImptypeContext imptype(int i) {
			return getRuleContext(ImptypeContext.class,i);
		}
		public TypeimpContext(ImpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterTypeimp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitTypeimp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitTypeimp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AnyimpContext extends ImpContext {
		public ImppathContext imppath() {
			return getRuleContext(ImppathContext.class,0);
		}
		public AnyimpContext(ImpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterAnyimp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitAnyimp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitAnyimp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ImpContext imp() throws RecognitionException {
		ImpContext _localctx = new ImpContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_imp);
		int _la;
		try {
			setState(111);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				_localctx = new NameimpContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(90);
				match(T__0);
				setState(91);
				imppath();
				setState(92);
				name();
				}
				break;
			case 2:
				_localctx = new AnyimpContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(94);
				match(T__0);
				setState(95);
				imppath();
				setState(96);
				match(T__1);
				}
				break;
			case 3:
				_localctx = new TypeimpContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(98);
				match(T__0);
				setState(99);
				imppath();
				setState(100);
				match(LCURLY);
				setState(101);
				imptype();
				setState(106);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__2) {
					{
					{
					setState(102);
					match(T__2);
					setState(103);
					imptype();
					}
					}
					setState(108);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(109);
				match(RCURLY);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ImppathContext extends ParserRuleContext {
		public List<NameContext> name() {
			return getRuleContexts(NameContext.class);
		}
		public NameContext name(int i) {
			return getRuleContext(NameContext.class,i);
		}
		public ImppathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_imppath; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterImppath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitImppath(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitImppath(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ImppathContext imppath() throws RecognitionException {
		ImppathContext _localctx = new ImppathContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_imppath);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(113);
			name();
			setState(118);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(114);
					match(T__3);
					setState(115);
					name();
					}
					} 
				}
				setState(120);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
			}
			setState(121);
			match(T__3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ImptypeContext extends ParserRuleContext {
		public ImptypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_imptype; }
	 
		public ImptypeContext() { }
		public void copyFrom(ImptypeContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AllimptypeContext extends ImptypeContext {
		public AllimptypeContext(ImptypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterAllimptype(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitAllimptype(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitAllimptype(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IgnoreimptypeContext extends ImptypeContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public IgnoreimptypeContext(ImptypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterIgnoreimptype(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitIgnoreimptype(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitIgnoreimptype(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GivenimptypeContext extends ImptypeContext {
		public GivenimptypeContext(ImptypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterGivenimptype(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitGivenimptype(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitGivenimptype(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NameimptypeContext extends ImptypeContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public NameimptypeContext(ImptypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterNameimptype(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitNameimptype(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitNameimptype(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RenameimptypeContext extends ImptypeContext {
		public List<NameContext> name() {
			return getRuleContexts(NameContext.class);
		}
		public NameContext name(int i) {
			return getRuleContext(NameContext.class,i);
		}
		public RenameimptypeContext(ImptypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterRenameimptype(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitRenameimptype(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitRenameimptype(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ImptypeContext imptype() throws RecognitionException {
		ImptypeContext _localctx = new ImptypeContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_imptype);
		try {
			setState(134);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				_localctx = new AllimptypeContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(123);
				match(T__1);
				}
				break;
			case 2:
				_localctx = new GivenimptypeContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(124);
				match(T__4);
				}
				break;
			case 3:
				_localctx = new NameimptypeContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(125);
				name();
				}
				break;
			case 4:
				_localctx = new IgnoreimptypeContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(126);
				name();
				setState(127);
				match(T__5);
				setState(128);
				match(T__1);
				}
				break;
			case 5:
				_localctx = new RenameimptypeContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(130);
				name();
				setState(131);
				match(T__5);
				setState(132);
				name();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PkgContext extends ParserRuleContext {
		public List<NameContext> name() {
			return getRuleContexts(NameContext.class);
		}
		public NameContext name(int i) {
			return getRuleContext(NameContext.class,i);
		}
		public PkgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pkg; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterPkg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitPkg(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitPkg(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PkgContext pkg() throws RecognitionException {
		PkgContext _localctx = new PkgContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_pkg);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(136);
			match(T__6);
			setState(137);
			name();
			setState(142);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__3) {
				{
				{
				setState(138);
				match(T__3);
				setState(139);
				name();
				}
				}
				setState(144);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AppliableContext extends ParserRuleContext {
		public AppliableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_appliable; }
	 
		public AppliableContext() { }
		public void copyFrom(AppliableContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ExprappliableContext extends AppliableContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ExprappliableContext(AppliableContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterExprappliable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitExprappliable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitExprappliable(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NameappliableContext extends AppliableContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public NameappliableContext(AppliableContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterNameappliable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitNameappliable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitNameappliable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AppliableContext appliable() throws RecognitionException {
		AppliableContext _localctx = new AppliableContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_appliable);
		try {
			setState(150);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				_localctx = new NameappliableContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(145);
				name();
				setState(146);
				match(T__7);
				setState(147);
				expression(0);
				}
				break;
			case 2:
				_localctx = new ExprappliableContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(149);
				expression(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressioneofContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode EOF() { return getToken(SuperScriptParser.EOF, 0); }
		public ExpressioneofContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expressioneof; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterExpressioneof(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitExpressioneof(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitExpressioneof(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressioneofContext expressioneof() throws RecognitionException {
		ExpressioneofContext _localctx = new ExpressioneofContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_expressioneof);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(152);
			expression(0);
			setState(153);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	 
		public ExpressionContext() { }
		public void copyFrom(ExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class SuperexpContext extends ExpressionContext {
		public SuperexpContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterSuperexp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitSuperexp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitSuperexp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BlkexpContext extends ExpressionContext {
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public BlkexpContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterBlkexp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitBlkexp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitBlkexp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TypedexpContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TContext t() {
			return getRuleContext(TContext.class,0);
		}
		public TypedexpContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterTypedexp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitTypedexp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitTypedexp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IdentexpContext extends ExpressionContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public IdentexpContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterIdentexp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitIdentexp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitIdentexp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ApplyContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ApplyContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterApply(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitApply(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitApply(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TapplyContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public CalltypeargsContext calltypeargs() {
			return getRuleContext(CalltypeargsContext.class,0);
		}
		public TapplyContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterTapply(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitTapply(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitTapply(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MatchContext extends ExpressionContext {
		public MatContext mat() {
			return getRuleContext(MatContext.class,0);
		}
		public MatchContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterMatch(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitMatch(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitMatch(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GapplyContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public GapplyContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterGapply(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitGapply(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitGapply(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CreateexpContext extends ExpressionContext {
		public TContext t() {
			return getRuleContext(TContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public CreateexpContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterCreateexp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitCreateexp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitCreateexp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumexpContext extends ExpressionContext {
		public TerminalNode NUM() { return getToken(SuperScriptParser.NUM, 0); }
		public NumexpContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterNumexp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitNumexp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitNumexp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParensexpContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ParensexpContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterParensexp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitParensexp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitParensexp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ThisexpContext extends ExpressionContext {
		public ThisexpContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterThisexp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitThisexp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitThisexp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class WContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public WContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterW(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitW(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitW(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StrexpContext extends ExpressionContext {
		public TerminalNode STRING() { return getToken(SuperScriptParser.STRING, 0); }
		public StrexpContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterStrexp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitStrexp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitStrexp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SelectexpContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public InfixnameContext infixname() {
			return getRuleContext(InfixnameContext.class,0);
		}
		public SelectexpContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterSelectexp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitSelectexp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitSelectexp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TryContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public MatContext mat() {
			return getRuleContext(MatContext.class,0);
		}
		public TryContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterTry(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitTry(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitTry(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class InfixexpContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public InfixnameContext infixname() {
			return getRuleContext(InfixnameContext.class,0);
		}
		public InfixexpContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterInfixexp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitInfixexp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitInfixexp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IfContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public IfContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterIf(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitIf(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitIf(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FunctionexpContext extends ExpressionContext {
		public GoptionalargsContext goptionalargs() {
			return getRuleContext(GoptionalargsContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public FunctionexpContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterFunctionexp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitFunctionexp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitFunctionexp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 18;
		enterRecursionRule(_localctx, 18, RULE_expression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(216);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				{
				_localctx = new BlkexpContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(156);
				block();
				}
				break;
			case 2:
				{
				_localctx = new ParensexpContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(157);
				match(LPAREN);
				setState(158);
				expression(0);
				setState(159);
				match(RPAREN);
				}
				break;
			case 3:
				{
				_localctx = new IdentexpContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(161);
				name();
				}
				break;
			case 4:
				{
				_localctx = new ThisexpContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(162);
				match(T__8);
				}
				break;
			case 5:
				{
				_localctx = new SuperexpContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(163);
				match(T__9);
				}
				break;
			case 6:
				{
				_localctx = new WContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(164);
				match(T__10);
				setState(165);
				match(LPAREN);
				setState(166);
				expression(0);
				setState(167);
				match(RPAREN);
				setState(168);
				expression(14);
				}
				break;
			case 7:
				{
				_localctx = new WContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(170);
				match(T__10);
				setState(171);
				expression(0);
				setState(172);
				match(T__11);
				setState(173);
				expression(13);
				}
				break;
			case 8:
				{
				_localctx = new IfContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(175);
				match(T__12);
				setState(176);
				match(LPAREN);
				setState(177);
				expression(0);
				setState(178);
				match(RPAREN);
				setState(179);
				expression(0);
				setState(182);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
				case 1:
					{
					setState(180);
					match(T__13);
					setState(181);
					expression(0);
					}
					break;
				}
				}
				break;
			case 9:
				{
				_localctx = new IfContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(184);
				match(T__12);
				setState(185);
				expression(0);
				setState(186);
				match(T__14);
				setState(187);
				expression(0);
				setState(190);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
				case 1:
					{
					setState(188);
					match(T__13);
					setState(189);
					expression(0);
					}
					break;
				}
				}
				break;
			case 10:
				{
				_localctx = new CreateexpContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(192);
				match(T__15);
				setState(193);
				t(0);
				setState(196);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
				case 1:
					{
					setState(194);
					match(T__16);
					setState(195);
					expression(0);
					}
					break;
				}
				setState(199);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
				case 1:
					{
					setState(198);
					block();
					}
					break;
				}
				}
				break;
			case 11:
				{
				_localctx = new MatchContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(201);
				mat();
				}
				break;
			case 12:
				{
				_localctx = new TryContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(202);
				match(T__17);
				setState(203);
				expression(0);
				setState(204);
				match(T__18);
				setState(205);
				mat();
				setState(208);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
				case 1:
					{
					setState(206);
					match(T__19);
					setState(207);
					expression(0);
					}
					break;
				}
				}
				break;
			case 13:
				{
				_localctx = new FunctionexpContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(210);
				goptionalargs();
				setState(211);
				match(T__5);
				setState(212);
				expression(4);
				}
				break;
			case 14:
				{
				_localctx = new StrexpContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(214);
				match(STRING);
				}
				break;
			case 15:
				{
				_localctx = new NumexpContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(215);
				match(NUM);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(258);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,20,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(256);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,19,_ctx) ) {
					case 1:
						{
						_localctx = new InfixexpContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(218);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(219);
						infixname();
						setState(220);
						expression(4);
						}
						break;
					case 2:
						{
						_localctx = new SelectexpContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(222);
						if (!(precpred(_ctx, 18))) throw new FailedPredicateException(this, "precpred(_ctx, 18)");
						setState(223);
						match(T__3);
						setState(224);
						infixname();
						}
						break;
					case 3:
						{
						_localctx = new TypedexpContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(225);
						if (!(precpred(_ctx, 15))) throw new FailedPredicateException(this, "precpred(_ctx, 15)");
						setState(226);
						match(T__7);
						setState(227);
						t(0);
						}
						break;
					case 4:
						{
						_localctx = new ApplyContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(228);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(229);
						match(LPAREN);
						setState(238);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__12) | (1L << T__15) | (1L << T__17) | (1L << STRING) | (1L << LPAREN) | (1L << NAME) | (1L << NEWLINE) | (1L << NUM))) != 0)) {
							{
							setState(230);
							expression(0);
							setState(235);
							_errHandler.sync(this);
							_la = _input.LA(1);
							while (_la==T__2) {
								{
								{
								setState(231);
								match(T__2);
								setState(232);
								expression(0);
								}
								}
								setState(237);
								_errHandler.sync(this);
								_la = _input.LA(1);
							}
							}
						}

						setState(240);
						match(RPAREN);
						}
						break;
					case 5:
						{
						_localctx = new GapplyContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(241);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(242);
						match(LPAREN);
						setState(243);
						match(T__4);
						setState(244);
						expression(0);
						setState(249);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==T__2) {
							{
							{
							setState(245);
							match(T__2);
							setState(246);
							expression(0);
							}
							}
							setState(251);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						setState(252);
						match(RPAREN);
						}
						break;
					case 6:
						{
						_localctx = new TapplyContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(254);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(255);
						calltypeargs();
						}
						break;
					}
					} 
				}
				setState(260);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,20,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public List<TerminalNode> NEWLINE() { return getTokens(SuperScriptParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(SuperScriptParser.NEWLINE, i);
		}
		public TerminalNode INDENT() { return getToken(SuperScriptParser.INDENT, 0); }
		public TerminalNode DEDENT() { return getToken(SuperScriptParser.DEDENT, 0); }
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitBlock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_block);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(261);
			match(NEWLINE);
			setState(262);
			match(INDENT);
			setState(266);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(263);
					match(NEWLINE);
					}
					} 
				}
				setState(268);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			}
			setState(270); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(269);
				statement();
				}
				}
				setState(272); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__4) | (1L << T__6) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__12) | (1L << T__15) | (1L << T__17) | (1L << T__23) | (1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__29) | (1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__34) | (1L << T__35) | (1L << STRING) | (1L << LPAREN) | (1L << NAME) | (1L << NEWLINE) | (1L << NUM))) != 0) );
			setState(274);
			match(DEDENT);
			setState(278);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(275);
					match(NEWLINE);
					}
					} 
				}
				setState(280);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MatContext extends ParserRuleContext {
		public List<TerminalNode> NEWLINE() { return getTokens(SuperScriptParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(SuperScriptParser.NEWLINE, i);
		}
		public TerminalNode INDENT() { return getToken(SuperScriptParser.INDENT, 0); }
		public List<PatternContext> pattern() {
			return getRuleContexts(PatternContext.class);
		}
		public PatternContext pattern(int i) {
			return getRuleContext(PatternContext.class,i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode DEDENT() { return getToken(SuperScriptParser.DEDENT, 0); }
		public MatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mat; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterMat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitMat(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitMat(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MatContext mat() throws RecognitionException {
		MatContext _localctx = new MatContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_mat);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(281);
			match(NEWLINE);
			setState(282);
			match(INDENT);
			setState(283);
			match(T__20);
			setState(284);
			pattern(0);
			setState(285);
			match(T__5);
			setState(286);
			expression(0);
			setState(295);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(287);
					match(NEWLINE);
					setState(288);
					match(T__20);
					setState(289);
					pattern(0);
					setState(290);
					match(T__5);
					setState(291);
					expression(0);
					}
					} 
				}
				setState(297);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
			}
			setState(298);
			match(NEWLINE);
			setState(299);
			match(DEDENT);
			setState(303);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,25,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(300);
					match(NEWLINE);
					}
					} 
				}
				setState(305);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,25,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PatternContext extends ParserRuleContext {
		public PatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pattern; }
	 
		public PatternContext() { }
		public void copyFrom(PatternContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ValuepatContext extends PatternContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public ValuepatContext(PatternContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterValuepat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitValuepat(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitValuepat(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class UnapplypatContext extends PatternContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<PatternContext> pattern() {
			return getRuleContexts(PatternContext.class);
		}
		public PatternContext pattern(int i) {
			return getRuleContext(PatternContext.class,i);
		}
		public UnapplypatContext(PatternContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterUnapplypat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitUnapplypat(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitUnapplypat(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AltpatContext extends PatternContext {
		public List<PatternContext> pattern() {
			return getRuleContexts(PatternContext.class);
		}
		public PatternContext pattern(int i) {
			return getRuleContext(PatternContext.class,i);
		}
		public AltpatContext(PatternContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterAltpat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitAltpat(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitAltpat(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class WildpatContext extends PatternContext {
		public WildpatContext(PatternContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterWildpat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitWildpat(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitWildpat(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BindpatContext extends PatternContext {
		public List<PatternContext> pattern() {
			return getRuleContexts(PatternContext.class);
		}
		public PatternContext pattern(int i) {
			return getRuleContext(PatternContext.class,i);
		}
		public BindpatContext(PatternContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterBindpat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitBindpat(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitBindpat(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TypepatContext extends PatternContext {
		public PatternContext pattern() {
			return getRuleContext(PatternContext.class,0);
		}
		public TContext t() {
			return getRuleContext(TContext.class,0);
		}
		public TypepatContext(PatternContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterTypepat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitTypepat(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitTypepat(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PatternContext pattern() throws RecognitionException {
		return pattern(0);
	}

	private PatternContext pattern(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		PatternContext _localctx = new PatternContext(_ctx, _parentState);
		PatternContext _prevctx = _localctx;
		int _startState = 24;
		enterRecursionRule(_localctx, 24, RULE_pattern, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(323);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,28,_ctx) ) {
			case 1:
				{
				_localctx = new ValuepatContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(307);
				name();
				}
				break;
			case 2:
				{
				_localctx = new UnapplypatContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(308);
				expression(0);
				setState(309);
				match(LPAREN);
				setState(318);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__4) | (1L << T__8) | (1L << T__9) | (1L << T__10) | (1L << T__12) | (1L << T__15) | (1L << T__17) | (1L << STRING) | (1L << LPAREN) | (1L << NAME) | (1L << NEWLINE) | (1L << NUM))) != 0)) {
					{
					setState(310);
					pattern(0);
					setState(315);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__2) {
						{
						{
						setState(311);
						match(T__2);
						setState(312);
						pattern(0);
						}
						}
						setState(317);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(320);
				match(RPAREN);
				}
				break;
			case 3:
				{
				_localctx = new WildpatContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(322);
				match(T__1);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(336);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(334);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,29,_ctx) ) {
					case 1:
						{
						_localctx = new BindpatContext(new PatternContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_pattern);
						setState(325);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(326);
						match(T__21);
						setState(327);
						pattern(6);
						}
						break;
					case 2:
						{
						_localctx = new AltpatContext(new PatternContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_pattern);
						setState(328);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(329);
						match(T__22);
						setState(330);
						pattern(4);
						}
						break;
					case 3:
						{
						_localctx = new TypepatContext(new PatternContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_pattern);
						setState(331);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(332);
						match(T__7);
						setState(333);
						t(0);
						}
						break;
					}
					} 
				}
				setState(338);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class DefinitionContext extends ParserRuleContext {
		public DefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_definition; }
	 
		public DefinitionContext() { }
		public void copyFrom(DefinitionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ObjectdefContext extends DefinitionContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public List<UrlwithargContext> urlwitharg() {
			return getRuleContexts(UrlwithargContext.class);
		}
		public UrlwithargContext urlwitharg(int i) {
			return getRuleContext(UrlwithargContext.class,i);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public ObjectdefContext(DefinitionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterObjectdef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitObjectdef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitObjectdef(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ValdefContext extends DefinitionContext {
		public InfixnameContext infixname() {
			return getRuleContext(InfixnameContext.class,0);
		}
		public TContext t() {
			return getRuleContext(TContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ValdefContext(DefinitionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterValdef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitValdef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitValdef(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TraitdefContext extends DefinitionContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public VarianttypeargsContext varianttypeargs() {
			return getRuleContext(VarianttypeargsContext.class,0);
		}
		public List<UrlwithargContext> urlwitharg() {
			return getRuleContexts(UrlwithargContext.class);
		}
		public UrlwithargContext urlwitharg(int i) {
			return getRuleContext(UrlwithargContext.class,i);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public TraitdefContext(DefinitionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterTraitdef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitTraitdef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitTraitdef(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OverdefContext extends DefinitionContext {
		public DefinitionContext definition() {
			return getRuleContext(DefinitionContext.class,0);
		}
		public OverdefContext(DefinitionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterOverdef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitOverdef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitOverdef(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VardefContext extends DefinitionContext {
		public InfixnameContext infixname() {
			return getRuleContext(InfixnameContext.class,0);
		}
		public TContext t() {
			return getRuleContext(TContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public VardefContext(DefinitionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterVardef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitVardef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitVardef(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ClassdefContext extends DefinitionContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public VarianttypeargsContext varianttypeargs() {
			return getRuleContext(VarianttypeargsContext.class,0);
		}
		public List<UrlwithargContext> urlwitharg() {
			return getRuleContexts(UrlwithargContext.class);
		}
		public UrlwithargContext urlwitharg(int i) {
			return getRuleContext(UrlwithargContext.class,i);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public ClassdefContext(DefinitionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterClassdef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitClassdef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitClassdef(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MethoddefContext extends DefinitionContext {
		public InfixnameContext infixname() {
			return getRuleContext(InfixnameContext.class,0);
		}
		public TypeargsContext typeargs() {
			return getRuleContext(TypeargsContext.class,0);
		}
		public List<GargsContext> gargs() {
			return getRuleContexts(GargsContext.class);
		}
		public GargsContext gargs(int i) {
			return getRuleContext(GargsContext.class,i);
		}
		public TContext t() {
			return getRuleContext(TContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public MethoddefContext(DefinitionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterMethoddef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitMethoddef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitMethoddef(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TypedefContext extends DefinitionContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public TypeboundsContext typebounds() {
			return getRuleContext(TypeboundsContext.class,0);
		}
		public TContext t() {
			return getRuleContext(TContext.class,0);
		}
		public TypedefContext(DefinitionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterTypedef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitTypedef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitTypedef(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ImplicitdefContext extends DefinitionContext {
		public TContext t() {
			return getRuleContext(TContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public InfixnameContext infixname() {
			return getRuleContext(InfixnameContext.class,0);
		}
		public ImplicitdefContext(DefinitionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterImplicitdef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitImplicitdef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitImplicitdef(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ProtecteddefContext extends DefinitionContext {
		public DefinitionContext definition() {
			return getRuleContext(DefinitionContext.class,0);
		}
		public ProtecteddefContext(DefinitionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterProtecteddef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitProtecteddef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitProtecteddef(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PrivatedefContext extends DefinitionContext {
		public DefinitionContext definition() {
			return getRuleContext(DefinitionContext.class,0);
		}
		public PrivatedefContext(DefinitionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterPrivatedef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitPrivatedef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitPrivatedef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DefinitionContext definition() throws RecognitionException {
		DefinitionContext _localctx = new DefinitionContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_definition);
		int _la;
		try {
			setState(470);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,56,_ctx) ) {
			case 1:
				_localctx = new ValdefContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(339);
				match(T__23);
				setState(340);
				infixname();
				setState(343);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__7) {
					{
					setState(341);
					match(T__7);
					setState(342);
					t(0);
					}
				}

				setState(347);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__24) {
					{
					setState(345);
					match(T__24);
					setState(346);
					expression(0);
					}
				}

				}
				break;
			case 2:
				_localctx = new VardefContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(349);
				match(T__25);
				setState(350);
				infixname();
				setState(353);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__7) {
					{
					setState(351);
					match(T__7);
					setState(352);
					t(0);
					}
				}

				setState(357);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__24) {
					{
					setState(355);
					match(T__24);
					setState(356);
					expression(0);
					}
				}

				}
				break;
			case 3:
				_localctx = new TypedefContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(359);
				match(T__26);
				setState(360);
				name();
				setState(361);
				typebounds();
				setState(364);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__24) {
					{
					setState(362);
					match(T__24);
					setState(363);
					t(0);
					}
				}

				}
				break;
			case 4:
				_localctx = new MethoddefContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(366);
				match(T__27);
				setState(367);
				infixname();
				setState(369);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LBRACK) {
					{
					setState(368);
					typeargs();
					}
				}

				setState(372); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(371);
					gargs();
					}
					}
					setState(374); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==T__4 || _la==LPAREN );
				setState(378);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__7) {
					{
					setState(376);
					match(T__7);
					setState(377);
					t(0);
					}
				}

				setState(382);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__24) {
					{
					setState(380);
					match(T__24);
					setState(381);
					expression(0);
					}
				}

				}
				break;
			case 5:
				_localctx = new MethoddefContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(384);
				match(T__27);
				setState(385);
				name();
				setState(387);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LBRACK) {
					{
					setState(386);
					typeargs();
					}
				}

				setState(391);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__7) {
					{
					setState(389);
					match(T__7);
					setState(390);
					t(0);
					}
				}

				setState(395);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__24) {
					{
					setState(393);
					match(T__24);
					setState(394);
					expression(0);
					}
				}

				}
				break;
			case 6:
				_localctx = new ImplicitdefContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(397);
				match(T__4);
				setState(399);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__36) | (1L << T__37) | (1L << T__39) | (1L << T__42) | (1L << T__43) | (1L << NAME) | (1L << STARTNAMEINFIX))) != 0)) {
					{
					setState(398);
					infixname();
					}
				}

				setState(401);
				match(T__28);
				setState(402);
				t(0);
				setState(403);
				match(T__24);
				setState(404);
				expression(0);
				}
				break;
			case 7:
				_localctx = new ClassdefContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(406);
				match(T__29);
				setState(407);
				name();
				setState(409);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LBRACK) {
					{
					setState(408);
					varianttypeargs();
					}
				}

				setState(420);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__30) {
					{
					setState(411);
					match(T__30);
					setState(412);
					urlwitharg();
					setState(417);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__10) {
						{
						{
						setState(413);
						match(T__10);
						setState(414);
						urlwitharg();
						}
						}
						setState(419);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(423);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,47,_ctx) ) {
				case 1:
					{
					setState(422);
					block();
					}
					break;
				}
				}
				break;
			case 8:
				_localctx = new TraitdefContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(425);
				match(T__31);
				setState(426);
				name();
				setState(428);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LBRACK) {
					{
					setState(427);
					varianttypeargs();
					}
				}

				setState(439);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__30) {
					{
					setState(430);
					match(T__30);
					setState(431);
					urlwitharg();
					setState(436);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__10) {
						{
						{
						setState(432);
						match(T__10);
						setState(433);
						urlwitharg();
						}
						}
						setState(438);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(442);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,51,_ctx) ) {
				case 1:
					{
					setState(441);
					block();
					}
					break;
				}
				}
				break;
			case 9:
				_localctx = new ObjectdefContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(444);
				match(T__32);
				setState(445);
				name();
				setState(455);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__30) {
					{
					setState(446);
					match(T__30);
					setState(447);
					urlwitharg();
					setState(452);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__10) {
						{
						{
						setState(448);
						match(T__10);
						setState(449);
						urlwitharg();
						}
						}
						setState(454);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(459);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__16) {
					{
					setState(457);
					match(T__16);
					setState(458);
					expression(0);
					}
				}

				setState(462);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,55,_ctx) ) {
				case 1:
					{
					setState(461);
					block();
					}
					break;
				}
				}
				break;
			case 10:
				_localctx = new OverdefContext(_localctx);
				enterOuterAlt(_localctx, 10);
				{
				setState(464);
				match(T__33);
				setState(465);
				definition();
				}
				break;
			case 11:
				_localctx = new PrivatedefContext(_localctx);
				enterOuterAlt(_localctx, 11);
				{
				setState(466);
				match(T__34);
				setState(467);
				definition();
				}
				break;
			case 12:
				_localctx = new ProtecteddefContext(_localctx);
				enterOuterAlt(_localctx, 12);
				{
				setState(468);
				match(T__35);
				setState(469);
				definition();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UrlwithargContext extends ParserRuleContext {
		public List<NameContext> name() {
			return getRuleContexts(NameContext.class);
		}
		public NameContext name(int i) {
			return getRuleContext(NameContext.class,i);
		}
		public CalltypeargsContext calltypeargs() {
			return getRuleContext(CalltypeargsContext.class,0);
		}
		public UrlwithargContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_urlwitharg; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterUrlwitharg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitUrlwitharg(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitUrlwitharg(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UrlwithargContext urlwitharg() throws RecognitionException {
		UrlwithargContext _localctx = new UrlwithargContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_urlwitharg);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(472);
			name();
			setState(477);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__3) {
				{
				{
				setState(473);
				match(T__3);
				setState(474);
				name();
				}
				}
				setState(479);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(481);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LBRACK) {
				{
				setState(480);
				calltypeargs();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TContext extends ParserRuleContext {
		public TContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_t; }
	 
		public TContext() { }
		public void copyFrom(TContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class OrtContext extends TContext {
		public List<TContext> t() {
			return getRuleContexts(TContext.class);
		}
		public TContext t(int i) {
			return getRuleContext(TContext.class,i);
		}
		public OrtContext(TContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterOrt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitOrt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitOrt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AndtContext extends TContext {
		public List<TContext> t() {
			return getRuleContexts(TContext.class);
		}
		public TContext t(int i) {
			return getRuleContext(TContext.class,i);
		}
		public AndtContext(TContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterAndt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitAndt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitAndt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MatchtContext extends TContext {
		public TContext t() {
			return getRuleContext(TContext.class,0);
		}
		public TmContext tm() {
			return getRuleContext(TmContext.class,0);
		}
		public MatchtContext(TContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterMatcht(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitMatcht(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitMatcht(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NamedtContext extends TContext {
		public List<NameContext> name() {
			return getRuleContexts(NameContext.class);
		}
		public NameContext name(int i) {
			return getRuleContext(NameContext.class,i);
		}
		public NamedtContext(TContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterNamedt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitNamedt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitNamedt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SingletContext extends TContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public SingletContext(TContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterSinglet(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitSinglet(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitSinglet(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LambdatContext extends TContext {
		public VarianttypeargsContext varianttypeargs() {
			return getRuleContext(VarianttypeargsContext.class,0);
		}
		public TContext t() {
			return getRuleContext(TContext.class,0);
		}
		public LambdatContext(TContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterLambdat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitLambdat(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitLambdat(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BynametContext extends TContext {
		public TContext t() {
			return getRuleContext(TContext.class,0);
		}
		public BynametContext(TContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterBynamet(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitBynamet(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitBynamet(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AppliedtContext extends TContext {
		public TContext t() {
			return getRuleContext(TContext.class,0);
		}
		public CalltypeargsContext calltypeargs() {
			return getRuleContext(CalltypeargsContext.class,0);
		}
		public AppliedtContext(TContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterAppliedt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitAppliedt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitAppliedt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SelecttContext extends TContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public SelecttContext(TContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterSelectt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitSelectt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitSelectt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TContext t() throws RecognitionException {
		return t(0);
	}

	private TContext t(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		TContext _localctx = new TContext(_ctx, _parentState);
		TContext _prevctx = _localctx;
		int _startState = 30;
		enterRecursionRule(_localctx, 30, RULE_t, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(506);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,60,_ctx) ) {
			case 1:
				{
				_localctx = new NamedtContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(484);
				name();
				setState(489);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,59,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(485);
						match(T__3);
						setState(486);
						name();
						}
						} 
					}
					setState(491);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,59,_ctx);
				}
				}
				break;
			case 2:
				{
				_localctx = new SelecttContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(492);
				expression(0);
				setState(493);
				match(T__36);
				setState(494);
				name();
				}
				break;
			case 3:
				{
				_localctx = new LambdatContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(496);
				varianttypeargs();
				setState(497);
				match(T__38);
				setState(498);
				t(4);
				}
				break;
			case 4:
				{
				_localctx = new SingletContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(500);
				expression(0);
				setState(501);
				match(T__3);
				setState(502);
				match(T__26);
				}
				break;
			case 5:
				{
				_localctx = new BynametContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(504);
				match(T__5);
				setState(505);
				t(2);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(521);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,62,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(519);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,61,_ctx) ) {
					case 1:
						{
						_localctx = new OrtContext(new TContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_t);
						setState(508);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(509);
						match(T__22);
						setState(510);
						t(8);
						}
						break;
					case 2:
						{
						_localctx = new AndtContext(new TContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_t);
						setState(511);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(512);
						match(T__37);
						setState(513);
						t(7);
						}
						break;
					case 3:
						{
						_localctx = new AppliedtContext(new TContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_t);
						setState(514);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(515);
						calltypeargs();
						}
						break;
					case 4:
						{
						_localctx = new MatchtContext(new TContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_t);
						setState(516);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(517);
						match(T__39);
						setState(518);
						tm();
						}
						break;
					}
					} 
				}
				setState(523);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,62,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class TmContext extends ParserRuleContext {
		public List<TerminalNode> NEWLINE() { return getTokens(SuperScriptParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(SuperScriptParser.NEWLINE, i);
		}
		public TerminalNode INDENT() { return getToken(SuperScriptParser.INDENT, 0); }
		public List<TpContext> tp() {
			return getRuleContexts(TpContext.class);
		}
		public TpContext tp(int i) {
			return getRuleContext(TpContext.class,i);
		}
		public List<TContext> t() {
			return getRuleContexts(TContext.class);
		}
		public TContext t(int i) {
			return getRuleContext(TContext.class,i);
		}
		public TerminalNode DEDENT() { return getToken(SuperScriptParser.DEDENT, 0); }
		public TmContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterTm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitTm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitTm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TmContext tm() throws RecognitionException {
		TmContext _localctx = new TmContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_tm);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(524);
			match(NEWLINE);
			setState(525);
			match(INDENT);
			setState(526);
			match(T__20);
			setState(527);
			tp();
			setState(528);
			match(T__5);
			setState(529);
			t(0);
			setState(538);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,63,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(530);
					match(NEWLINE);
					setState(531);
					match(T__20);
					setState(532);
					tp();
					setState(533);
					match(T__5);
					setState(534);
					t(0);
					}
					} 
				}
				setState(540);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,63,_ctx);
			}
			setState(541);
			match(NEWLINE);
			setState(542);
			match(DEDENT);
			setState(543);
			match(NEWLINE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TpContext extends ParserRuleContext {
		public TpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tp; }
	 
		public TpContext() { }
		public void copyFrom(TpContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ExtracttpContext extends TpContext {
		public TContext t() {
			return getRuleContext(TContext.class,0);
		}
		public List<TpContext> tp() {
			return getRuleContexts(TpContext.class);
		}
		public TpContext tp(int i) {
			return getRuleContext(TpContext.class,i);
		}
		public ExtracttpContext(TpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterExtracttp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitExtracttp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitExtracttp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ValuetpContext extends TpContext {
		public TContext t() {
			return getRuleContext(TContext.class,0);
		}
		public ValuetpContext(TpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterValuetp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitValuetp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitValuetp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TpwildcardContext extends TpContext {
		public TpwildcardContext(TpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterTpwildcard(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitTpwildcard(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitTpwildcard(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TpContext tp() throws RecognitionException {
		TpContext _localctx = new TpContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_tp);
		int _la;
		try {
			setState(559);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,65,_ctx) ) {
			case 1:
				_localctx = new ExtracttpContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(545);
				t(0);
				setState(546);
				match(LBRACK);
				setState(547);
				tp();
				setState(552);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__2) {
					{
					{
					setState(548);
					match(T__2);
					setState(549);
					tp();
					}
					}
					setState(554);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(555);
				match(RBRACK);
				}
				break;
			case 2:
				_localctx = new TpwildcardContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(557);
				match(T__1);
				}
				break;
			case 3:
				_localctx = new ValuetpContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(558);
				t(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeboundsContext extends ParserRuleContext {
		public List<BoundtypeContext> boundtype() {
			return getRuleContexts(BoundtypeContext.class);
		}
		public BoundtypeContext boundtype(int i) {
			return getRuleContext(BoundtypeContext.class,i);
		}
		public TypeboundsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typebounds; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterTypebounds(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitTypebounds(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitTypebounds(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeboundsContext typebounds() throws RecognitionException {
		TypeboundsContext _localctx = new TypeboundsContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_typebounds);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(564);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__7) | (1L << T__40) | (1L << T__41))) != 0)) {
				{
				{
				setState(561);
				boundtype();
				}
				}
				setState(566);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoundtypeContext extends ParserRuleContext {
		public BoundtypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boundtype; }
	 
		public BoundtypeContext() { }
		public void copyFrom(BoundtypeContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class SuperclassboundtypeContext extends BoundtypeContext {
		public TContext t() {
			return getRuleContext(TContext.class,0);
		}
		public SuperclassboundtypeContext(BoundtypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterSuperclassboundtype(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitSuperclassboundtype(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitSuperclassboundtype(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExistsimplicitboundtypeContext extends BoundtypeContext {
		public TContext t() {
			return getRuleContext(TContext.class,0);
		}
		public ExistsimplicitboundtypeContext(BoundtypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterExistsimplicitboundtype(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitExistsimplicitboundtype(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitExistsimplicitboundtype(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SubclassboundtypeContext extends BoundtypeContext {
		public TContext t() {
			return getRuleContext(TContext.class,0);
		}
		public SubclassboundtypeContext(BoundtypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterSubclassboundtype(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitSubclassboundtype(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitSubclassboundtype(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BoundtypeContext boundtype() throws RecognitionException {
		BoundtypeContext _localctx = new BoundtypeContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_boundtype);
		try {
			setState(573);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__40:
				_localctx = new SubclassboundtypeContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(567);
				match(T__40);
				setState(568);
				t(0);
				}
				break;
			case T__41:
				_localctx = new SuperclassboundtypeContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(569);
				match(T__41);
				setState(570);
				t(0);
				}
				break;
			case T__7:
				_localctx = new ExistsimplicitboundtypeContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(571);
				match(T__7);
				setState(572);
				t(0);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CalltypeargsContext extends ParserRuleContext {
		public List<TContext> t() {
			return getRuleContexts(TContext.class);
		}
		public TContext t(int i) {
			return getRuleContext(TContext.class,i);
		}
		public CalltypeargsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_calltypeargs; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterCalltypeargs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitCalltypeargs(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitCalltypeargs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CalltypeargsContext calltypeargs() throws RecognitionException {
		CalltypeargsContext _localctx = new CalltypeargsContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_calltypeargs);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(575);
			match(LBRACK);
			setState(576);
			t(0);
			setState(581);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__2) {
				{
				{
				setState(577);
				match(T__2);
				setState(578);
				t(0);
				}
				}
				setState(583);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(584);
			match(RBRACK);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeargsContext extends ParserRuleContext {
		public List<TContext> t() {
			return getRuleContexts(TContext.class);
		}
		public TContext t(int i) {
			return getRuleContext(TContext.class,i);
		}
		public List<TypeboundsContext> typebounds() {
			return getRuleContexts(TypeboundsContext.class);
		}
		public TypeboundsContext typebounds(int i) {
			return getRuleContext(TypeboundsContext.class,i);
		}
		public TypeargsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeargs; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterTypeargs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitTypeargs(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitTypeargs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeargsContext typeargs() throws RecognitionException {
		TypeargsContext _localctx = new TypeargsContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_typeargs);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(586);
			match(LBRACK);
			{
			setState(587);
			t(0);
			setState(588);
			typebounds();
			}
			setState(596);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__2) {
				{
				{
				setState(590);
				match(T__2);
				{
				setState(591);
				t(0);
				setState(592);
				typebounds();
				}
				}
				}
				setState(598);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(599);
			match(RBRACK);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarianttypeargsContext extends ParserRuleContext {
		public List<VarianceContext> variance() {
			return getRuleContexts(VarianceContext.class);
		}
		public VarianceContext variance(int i) {
			return getRuleContext(VarianceContext.class,i);
		}
		public List<TContext> t() {
			return getRuleContexts(TContext.class);
		}
		public TContext t(int i) {
			return getRuleContext(TContext.class,i);
		}
		public List<TypeboundsContext> typebounds() {
			return getRuleContexts(TypeboundsContext.class);
		}
		public TypeboundsContext typebounds(int i) {
			return getRuleContext(TypeboundsContext.class,i);
		}
		public VarianttypeargsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_varianttypeargs; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterVarianttypeargs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitVarianttypeargs(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitVarianttypeargs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VarianttypeargsContext varianttypeargs() throws RecognitionException {
		VarianttypeargsContext _localctx = new VarianttypeargsContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_varianttypeargs);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(601);
			match(LBRACK);
			{
			setState(602);
			variance();
			setState(603);
			t(0);
			setState(604);
			typebounds();
			}
			setState(613);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__2) {
				{
				{
				setState(606);
				match(T__2);
				{
				setState(607);
				variance();
				setState(608);
				t(0);
				setState(609);
				typebounds();
				}
				}
				}
				setState(615);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(616);
			match(RBRACK);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarianceContext extends ParserRuleContext {
		public VarianceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variance; }
	 
		public VarianceContext() { }
		public void copyFrom(VarianceContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ContravarianceContext extends VarianceContext {
		public ContravarianceContext(VarianceContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterContravariance(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitContravariance(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitContravariance(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CovarianceContext extends VarianceContext {
		public CovarianceContext(VarianceContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterCovariance(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitCovariance(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitCovariance(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class InvarianceContext extends VarianceContext {
		public InvarianceContext(VarianceContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterInvariance(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitInvariance(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitInvariance(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VarianceContext variance() throws RecognitionException {
		VarianceContext _localctx = new VarianceContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_variance);
		try {
			setState(621);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__42:
				_localctx = new CovarianceContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(618);
				match(T__42);
				}
				break;
			case T__4:
			case T__5:
			case T__8:
			case T__9:
			case T__10:
			case T__12:
			case T__15:
			case T__17:
			case STRING:
			case LPAREN:
			case LBRACK:
			case NAME:
			case NEWLINE:
			case NUM:
				_localctx = new InvarianceContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				}
				break;
			case T__43:
				_localctx = new ContravarianceContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(620);
				match(T__43);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GoptionalargsContext extends ParserRuleContext {
		public GoptionalargsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_goptionalargs; }
	 
		public GoptionalargsContext() { }
		public void copyFrom(GoptionalargsContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class GivenoptionalargsContext extends GoptionalargsContext {
		public OptionalargsContext optionalargs() {
			return getRuleContext(OptionalargsContext.class,0);
		}
		public GivenoptionalargsContext(GoptionalargsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterGivenoptionalargs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitGivenoptionalargs(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitGivenoptionalargs(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NormaloptionalargsContext extends GoptionalargsContext {
		public OptionalargsContext optionalargs() {
			return getRuleContext(OptionalargsContext.class,0);
		}
		public NormaloptionalargsContext(GoptionalargsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterNormaloptionalargs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitNormaloptionalargs(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitNormaloptionalargs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GoptionalargsContext goptionalargs() throws RecognitionException {
		GoptionalargsContext _localctx = new GoptionalargsContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_goptionalargs);
		try {
			setState(626);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__4:
				_localctx = new GivenoptionalargsContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(623);
				match(T__4);
				setState(624);
				optionalargs();
				}
				break;
			case LPAREN:
				_localctx = new NormaloptionalargsContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(625);
				optionalargs();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OptionalargsContext extends ParserRuleContext {
		public List<OargContext> oarg() {
			return getRuleContexts(OargContext.class);
		}
		public OargContext oarg(int i) {
			return getRuleContext(OargContext.class,i);
		}
		public OptionalargsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_optionalargs; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterOptionalargs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitOptionalargs(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitOptionalargs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OptionalargsContext optionalargs() throws RecognitionException {
		OptionalargsContext _localctx = new OptionalargsContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_optionalargs);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(628);
			match(LPAREN);
			setState(637);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==NAME) {
				{
				setState(629);
				oarg();
				setState(634);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__2) {
					{
					{
					setState(630);
					match(T__2);
					setState(631);
					oarg();
					}
					}
					setState(636);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(639);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OargContext extends ParserRuleContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public TContext t() {
			return getRuleContext(TContext.class,0);
		}
		public OargContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_oarg; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterOarg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitOarg(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitOarg(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OargContext oarg() throws RecognitionException {
		OargContext _localctx = new OargContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_oarg);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(641);
			name();
			setState(644);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__7) {
				{
				setState(642);
				match(T__7);
				setState(643);
				t(0);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GargsContext extends ParserRuleContext {
		public GargsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_gargs; }
	 
		public GargsContext() { }
		public void copyFrom(GargsContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class GivenargsContext extends GargsContext {
		public ArgsContext args() {
			return getRuleContext(ArgsContext.class,0);
		}
		public GivenargsContext(GargsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterGivenargs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitGivenargs(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitGivenargs(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NormalargsContext extends GargsContext {
		public ArgsContext args() {
			return getRuleContext(ArgsContext.class,0);
		}
		public NormalargsContext(GargsContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterNormalargs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitNormalargs(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitNormalargs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GargsContext gargs() throws RecognitionException {
		GargsContext _localctx = new GargsContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_gargs);
		try {
			setState(649);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__4:
				_localctx = new GivenargsContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(646);
				match(T__4);
				setState(647);
				args();
				}
				break;
			case LPAREN:
				_localctx = new NormalargsContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(648);
				args();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgsContext extends ParserRuleContext {
		public List<ArgContext> arg() {
			return getRuleContexts(ArgContext.class);
		}
		public ArgContext arg(int i) {
			return getRuleContext(ArgContext.class,i);
		}
		public ArgsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_args; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterArgs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitArgs(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitArgs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgsContext args() throws RecognitionException {
		ArgsContext _localctx = new ArgsContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_args);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(651);
			match(LPAREN);
			setState(660);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==NAME) {
				{
				setState(652);
				arg();
				setState(657);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__2) {
					{
					{
					setState(653);
					match(T__2);
					setState(654);
					arg();
					}
					}
					setState(659);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(662);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgContext extends ParserRuleContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public TContext t() {
			return getRuleContext(TContext.class,0);
		}
		public ArgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arg; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterArg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitArg(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitArg(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgContext arg() throws RecognitionException {
		ArgContext _localctx = new ArgContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_arg);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(664);
			name();
			setState(665);
			match(T__7);
			setState(666);
			t(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InfixnameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(SuperScriptParser.NAME, 0); }
		public TerminalNode STARTNAMEINFIX() { return getToken(SuperScriptParser.STARTNAMEINFIX, 0); }
		public InfixnameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_infixname; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterInfixname(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitInfixname(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitInfixname(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InfixnameContext infixname() throws RecognitionException {
		InfixnameContext _localctx = new InfixnameContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_infixname);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(668);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__22) | (1L << T__36) | (1L << T__37) | (1L << T__39) | (1L << T__42) | (1L << T__43) | (1L << NAME) | (1L << STARTNAMEINFIX))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NameContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(SuperScriptParser.NAME, 0); }
		public NameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).enterName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptListener ) ((SuperScriptListener)listener).exitName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptVisitor ) return ((SuperScriptVisitor<? extends T>)visitor).visitName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NameContext name() throws RecognitionException {
		NameContext _localctx = new NameContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(670);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 9:
			return expression_sempred((ExpressionContext)_localctx, predIndex);
		case 12:
			return pattern_sempred((PatternContext)_localctx, predIndex);
		case 15:
			return t_sempred((TContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 3);
		case 1:
			return precpred(_ctx, 18);
		case 2:
			return precpred(_ctx, 15);
		case 3:
			return precpred(_ctx, 10);
		case 4:
			return precpred(_ctx, 9);
		case 5:
			return precpred(_ctx, 5);
		}
		return true;
	}
	private boolean pattern_sempred(PatternContext _localctx, int predIndex) {
		switch (predIndex) {
		case 6:
			return precpred(_ctx, 5);
		case 7:
			return precpred(_ctx, 3);
		case 8:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean t_sempred(TContext _localctx, int predIndex) {
		switch (predIndex) {
		case 9:
			return precpred(_ctx, 7);
		case 10:
			return precpred(_ctx, 6);
		case 11:
			return precpred(_ctx, 5);
		case 12:
			return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3<\u02a3\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\3\2\7\2D\n\2\f\2\16\2G\13\2\3\2\7\2J\n\2\f\2\16\2M\13\2\3\2\3\2\3"+
		"\3\3\3\6\3S\n\3\r\3\16\3T\3\4\3\4\3\4\3\4\5\4[\n\4\3\5\3\5\3\5\3\5\3\5"+
		"\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\7\5k\n\5\f\5\16\5n\13\5\3\5\3\5\5"+
		"\5r\n\5\3\6\3\6\3\6\7\6w\n\6\f\6\16\6z\13\6\3\6\3\6\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7\u0089\n\7\3\b\3\b\3\b\3\b\7\b\u008f\n\b"+
		"\f\b\16\b\u0092\13\b\3\t\3\t\3\t\3\t\3\t\5\t\u0099\n\t\3\n\3\n\3\n\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\5\13\u00b9"+
		"\n\13\3\13\3\13\3\13\3\13\3\13\3\13\5\13\u00c1\n\13\3\13\3\13\3\13\3\13"+
		"\5\13\u00c7\n\13\3\13\5\13\u00ca\n\13\3\13\3\13\3\13\3\13\3\13\3\13\3"+
		"\13\5\13\u00d3\n\13\3\13\3\13\3\13\3\13\3\13\3\13\5\13\u00db\n\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\7\13\u00ec\n\13\f\13\16\13\u00ef\13\13\5\13\u00f1\n\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\7\13\u00fa\n\13\f\13\16\13\u00fd\13\13\3\13\3\13"+
		"\3\13\3\13\7\13\u0103\n\13\f\13\16\13\u0106\13\13\3\f\3\f\3\f\7\f\u010b"+
		"\n\f\f\f\16\f\u010e\13\f\3\f\6\f\u0111\n\f\r\f\16\f\u0112\3\f\3\f\7\f"+
		"\u0117\n\f\f\f\16\f\u011a\13\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r"+
		"\3\r\3\r\7\r\u0128\n\r\f\r\16\r\u012b\13\r\3\r\3\r\3\r\7\r\u0130\n\r\f"+
		"\r\16\r\u0133\13\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16\7\16\u013c\n\16"+
		"\f\16\16\16\u013f\13\16\5\16\u0141\n\16\3\16\3\16\3\16\5\16\u0146\n\16"+
		"\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\7\16\u0151\n\16\f\16\16"+
		"\16\u0154\13\16\3\17\3\17\3\17\3\17\5\17\u015a\n\17\3\17\3\17\5\17\u015e"+
		"\n\17\3\17\3\17\3\17\3\17\5\17\u0164\n\17\3\17\3\17\5\17\u0168\n\17\3"+
		"\17\3\17\3\17\3\17\3\17\5\17\u016f\n\17\3\17\3\17\3\17\5\17\u0174\n\17"+
		"\3\17\6\17\u0177\n\17\r\17\16\17\u0178\3\17\3\17\5\17\u017d\n\17\3\17"+
		"\3\17\5\17\u0181\n\17\3\17\3\17\3\17\5\17\u0186\n\17\3\17\3\17\5\17\u018a"+
		"\n\17\3\17\3\17\5\17\u018e\n\17\3\17\3\17\5\17\u0192\n\17\3\17\3\17\3"+
		"\17\3\17\3\17\3\17\3\17\3\17\5\17\u019c\n\17\3\17\3\17\3\17\3\17\7\17"+
		"\u01a2\n\17\f\17\16\17\u01a5\13\17\5\17\u01a7\n\17\3\17\5\17\u01aa\n\17"+
		"\3\17\3\17\3\17\5\17\u01af\n\17\3\17\3\17\3\17\3\17\7\17\u01b5\n\17\f"+
		"\17\16\17\u01b8\13\17\5\17\u01ba\n\17\3\17\5\17\u01bd\n\17\3\17\3\17\3"+
		"\17\3\17\3\17\3\17\7\17\u01c5\n\17\f\17\16\17\u01c8\13\17\5\17\u01ca\n"+
		"\17\3\17\3\17\5\17\u01ce\n\17\3\17\5\17\u01d1\n\17\3\17\3\17\3\17\3\17"+
		"\3\17\3\17\5\17\u01d9\n\17\3\20\3\20\3\20\7\20\u01de\n\20\f\20\16\20\u01e1"+
		"\13\20\3\20\5\20\u01e4\n\20\3\21\3\21\3\21\3\21\7\21\u01ea\n\21\f\21\16"+
		"\21\u01ed\13\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21"+
		"\3\21\3\21\3\21\5\21\u01fd\n\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21"+
		"\3\21\3\21\3\21\7\21\u020a\n\21\f\21\16\21\u020d\13\21\3\22\3\22\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\7\22\u021b\n\22\f\22\16"+
		"\22\u021e\13\22\3\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\7\23\u0229"+
		"\n\23\f\23\16\23\u022c\13\23\3\23\3\23\3\23\3\23\5\23\u0232\n\23\3\24"+
		"\7\24\u0235\n\24\f\24\16\24\u0238\13\24\3\25\3\25\3\25\3\25\3\25\3\25"+
		"\5\25\u0240\n\25\3\26\3\26\3\26\3\26\7\26\u0246\n\26\f\26\16\26\u0249"+
		"\13\26\3\26\3\26\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\7\27\u0255\n"+
		"\27\f\27\16\27\u0258\13\27\3\27\3\27\3\30\3\30\3\30\3\30\3\30\3\30\3\30"+
		"\3\30\3\30\3\30\7\30\u0266\n\30\f\30\16\30\u0269\13\30\3\30\3\30\3\31"+
		"\3\31\3\31\5\31\u0270\n\31\3\32\3\32\3\32\5\32\u0275\n\32\3\33\3\33\3"+
		"\33\3\33\7\33\u027b\n\33\f\33\16\33\u027e\13\33\5\33\u0280\n\33\3\33\3"+
		"\33\3\34\3\34\3\34\5\34\u0287\n\34\3\35\3\35\3\35\5\35\u028c\n\35\3\36"+
		"\3\36\3\36\3\36\7\36\u0292\n\36\f\36\16\36\u0295\13\36\5\36\u0297\n\36"+
		"\3\36\3\36\3\37\3\37\3\37\3\37\3 \3 \3!\3!\3!\2\5\24\32 \"\2\4\6\b\n\f"+
		"\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@\2\3\7\2\31\31\'"+
		"(**-.\66\67\2\u02fc\2E\3\2\2\2\4P\3\2\2\2\6Z\3\2\2\2\bq\3\2\2\2\ns\3\2"+
		"\2\2\f\u0088\3\2\2\2\16\u008a\3\2\2\2\20\u0098\3\2\2\2\22\u009a\3\2\2"+
		"\2\24\u00da\3\2\2\2\26\u0107\3\2\2\2\30\u011b\3\2\2\2\32\u0145\3\2\2\2"+
		"\34\u01d8\3\2\2\2\36\u01da\3\2\2\2 \u01fc\3\2\2\2\"\u020e\3\2\2\2$\u0231"+
		"\3\2\2\2&\u0236\3\2\2\2(\u023f\3\2\2\2*\u0241\3\2\2\2,\u024c\3\2\2\2."+
		"\u025b\3\2\2\2\60\u026f\3\2\2\2\62\u0274\3\2\2\2\64\u0276\3\2\2\2\66\u0283"+
		"\3\2\2\28\u028b\3\2\2\2:\u028d\3\2\2\2<\u029a\3\2\2\2>\u029e\3\2\2\2@"+
		"\u02a0\3\2\2\2BD\79\2\2CB\3\2\2\2DG\3\2\2\2EC\3\2\2\2EF\3\2\2\2FK\3\2"+
		"\2\2GE\3\2\2\2HJ\5\4\3\2IH\3\2\2\2JM\3\2\2\2KI\3\2\2\2KL\3\2\2\2LN\3\2"+
		"\2\2MK\3\2\2\2NO\7\2\2\3O\3\3\2\2\2PR\5\6\4\2QS\79\2\2RQ\3\2\2\2ST\3\2"+
		"\2\2TR\3\2\2\2TU\3\2\2\2U\5\3\2\2\2V[\5\24\13\2W[\5\34\17\2X[\5\b\5\2"+
		"Y[\5\16\b\2ZV\3\2\2\2ZW\3\2\2\2ZX\3\2\2\2ZY\3\2\2\2[\7\3\2\2\2\\]\7\3"+
		"\2\2]^\5\n\6\2^_\5@!\2_r\3\2\2\2`a\7\3\2\2ab\5\n\6\2bc\7\4\2\2cr\3\2\2"+
		"\2de\7\3\2\2ef\5\n\6\2fg\7\60\2\2gl\5\f\7\2hi\7\5\2\2ik\5\f\7\2jh\3\2"+
		"\2\2kn\3\2\2\2lj\3\2\2\2lm\3\2\2\2mo\3\2\2\2nl\3\2\2\2op\7\61\2\2pr\3"+
		"\2\2\2q\\\3\2\2\2q`\3\2\2\2qd\3\2\2\2r\t\3\2\2\2sx\5@!\2tu\7\6\2\2uw\5"+
		"@!\2vt\3\2\2\2wz\3\2\2\2xv\3\2\2\2xy\3\2\2\2y{\3\2\2\2zx\3\2\2\2{|\7\6"+
		"\2\2|\13\3\2\2\2}\u0089\7\4\2\2~\u0089\7\7\2\2\177\u0089\5@!\2\u0080\u0081"+
		"\5@!\2\u0081\u0082\7\b\2\2\u0082\u0083\7\4\2\2\u0083\u0089\3\2\2\2\u0084"+
		"\u0085\5@!\2\u0085\u0086\7\b\2\2\u0086\u0087\5@!\2\u0087\u0089\3\2\2\2"+
		"\u0088}\3\2\2\2\u0088~\3\2\2\2\u0088\177\3\2\2\2\u0088\u0080\3\2\2\2\u0088"+
		"\u0084\3\2\2\2\u0089\r\3\2\2\2\u008a\u008b\7\t\2\2\u008b\u0090\5@!\2\u008c"+
		"\u008d\7\6\2\2\u008d\u008f\5@!\2\u008e\u008c\3\2\2\2\u008f\u0092\3\2\2"+
		"\2\u0090\u008e\3\2\2\2\u0090\u0091\3\2\2\2\u0091\17\3\2\2\2\u0092\u0090"+
		"\3\2\2\2\u0093\u0094\5@!\2\u0094\u0095\7\n\2\2\u0095\u0096\5\24\13\2\u0096"+
		"\u0099\3\2\2\2\u0097\u0099\5\24\13\2\u0098\u0093\3\2\2\2\u0098\u0097\3"+
		"\2\2\2\u0099\21\3\2\2\2\u009a\u009b\5\24\13\2\u009b\u009c\7\2\2\3\u009c"+
		"\23\3\2\2\2\u009d\u009e\b\13\1\2\u009e\u00db\5\26\f\2\u009f\u00a0\7\62"+
		"\2\2\u00a0\u00a1\5\24\13\2\u00a1\u00a2\7\63\2\2\u00a2\u00db\3\2\2\2\u00a3"+
		"\u00db\5@!\2\u00a4\u00db\7\13\2\2\u00a5\u00db\7\f\2\2\u00a6\u00a7\7\r"+
		"\2\2\u00a7\u00a8\7\62\2\2\u00a8\u00a9\5\24\13\2\u00a9\u00aa\7\63\2\2\u00aa"+
		"\u00ab\5\24\13\20\u00ab\u00db\3\2\2\2\u00ac\u00ad\7\r\2\2\u00ad\u00ae"+
		"\5\24\13\2\u00ae\u00af\7\16\2\2\u00af\u00b0\5\24\13\17\u00b0\u00db\3\2"+
		"\2\2\u00b1\u00b2\7\17\2\2\u00b2\u00b3\7\62\2\2\u00b3\u00b4\5\24\13\2\u00b4"+
		"\u00b5\7\63\2\2\u00b5\u00b8\5\24\13\2\u00b6\u00b7\7\20\2\2\u00b7\u00b9"+
		"\5\24\13\2\u00b8\u00b6\3\2\2\2\u00b8\u00b9\3\2\2\2\u00b9\u00db\3\2\2\2"+
		"\u00ba\u00bb\7\17\2\2\u00bb\u00bc\5\24\13\2\u00bc\u00bd\7\21\2\2\u00bd"+
		"\u00c0\5\24\13\2\u00be\u00bf\7\20\2\2\u00bf\u00c1\5\24\13\2\u00c0\u00be"+
		"\3\2\2\2\u00c0\u00c1\3\2\2\2\u00c1\u00db\3\2\2\2\u00c2\u00c3\7\22\2\2"+
		"\u00c3\u00c6\5 \21\2\u00c4\u00c5\7\23\2\2\u00c5\u00c7\5\24\13\2\u00c6"+
		"\u00c4\3\2\2\2\u00c6\u00c7\3\2\2\2\u00c7\u00c9\3\2\2\2\u00c8\u00ca\5\26"+
		"\f\2\u00c9\u00c8\3\2\2\2\u00c9\u00ca\3\2\2\2\u00ca\u00db\3\2\2\2\u00cb"+
		"\u00db\5\30\r\2\u00cc\u00cd\7\24\2\2\u00cd\u00ce\5\24\13\2\u00ce\u00cf"+
		"\7\25\2\2\u00cf\u00d2\5\30\r\2\u00d0\u00d1\7\26\2\2\u00d1\u00d3\5\24\13"+
		"\2\u00d2\u00d0\3\2\2\2\u00d2\u00d3\3\2\2\2\u00d3\u00db\3\2\2\2\u00d4\u00d5"+
		"\5\62\32\2\u00d5\u00d6\7\b\2\2\u00d6\u00d7\5\24\13\6\u00d7\u00db\3\2\2"+
		"\2\u00d8\u00db\7/\2\2\u00d9\u00db\7:\2\2\u00da\u009d\3\2\2\2\u00da\u009f"+
		"\3\2\2\2\u00da\u00a3\3\2\2\2\u00da\u00a4\3\2\2\2\u00da\u00a5\3\2\2\2\u00da"+
		"\u00a6\3\2\2\2\u00da\u00ac\3\2\2\2\u00da\u00b1\3\2\2\2\u00da\u00ba\3\2"+
		"\2\2\u00da\u00c2\3\2\2\2\u00da\u00cb\3\2\2\2\u00da\u00cc\3\2\2\2\u00da"+
		"\u00d4\3\2\2\2\u00da\u00d8\3\2\2\2\u00da\u00d9\3\2\2\2\u00db\u0104\3\2"+
		"\2\2\u00dc\u00dd\f\5\2\2\u00dd\u00de\5> \2\u00de\u00df\5\24\13\6\u00df"+
		"\u0103\3\2\2\2\u00e0\u00e1\f\24\2\2\u00e1\u00e2\7\6\2\2\u00e2\u0103\5"+
		"> \2\u00e3\u00e4\f\21\2\2\u00e4\u00e5\7\n\2\2\u00e5\u0103\5 \21\2\u00e6"+
		"\u00e7\f\f\2\2\u00e7\u00f0\7\62\2\2\u00e8\u00ed\5\24\13\2\u00e9\u00ea"+
		"\7\5\2\2\u00ea\u00ec\5\24\13\2\u00eb\u00e9\3\2\2\2\u00ec\u00ef\3\2\2\2"+
		"\u00ed\u00eb\3\2\2\2\u00ed\u00ee\3\2\2\2\u00ee\u00f1\3\2\2\2\u00ef\u00ed"+
		"\3\2\2\2\u00f0\u00e8\3\2\2\2\u00f0\u00f1\3\2\2\2\u00f1\u00f2\3\2\2\2\u00f2"+
		"\u0103\7\63\2\2\u00f3\u00f4\f\13\2\2\u00f4\u00f5\7\62\2\2\u00f5\u00f6"+
		"\7\7\2\2\u00f6\u00fb\5\24\13\2\u00f7\u00f8\7\5\2\2\u00f8\u00fa\5\24\13"+
		"\2\u00f9\u00f7\3\2\2\2\u00fa\u00fd\3\2\2\2\u00fb\u00f9\3\2\2\2\u00fb\u00fc"+
		"\3\2\2\2\u00fc\u00fe\3\2\2\2\u00fd\u00fb\3\2\2\2\u00fe\u00ff\7\63\2\2"+
		"\u00ff\u0103\3\2\2\2\u0100\u0101\f\7\2\2\u0101\u0103\5*\26\2\u0102\u00dc"+
		"\3\2\2\2\u0102\u00e0\3\2\2\2\u0102\u00e3\3\2\2\2\u0102\u00e6\3\2\2\2\u0102"+
		"\u00f3\3\2\2\2\u0102\u0100\3\2\2\2\u0103\u0106\3\2\2\2\u0104\u0102\3\2"+
		"\2\2\u0104\u0105\3\2\2\2\u0105\25\3\2\2\2\u0106\u0104\3\2\2\2\u0107\u0108"+
		"\79\2\2\u0108\u010c\7;\2\2\u0109\u010b\79\2\2\u010a\u0109\3\2\2\2\u010b"+
		"\u010e\3\2\2\2\u010c\u010a\3\2\2\2\u010c\u010d\3\2\2\2\u010d\u0110\3\2"+
		"\2\2\u010e\u010c\3\2\2\2\u010f\u0111\5\4\3\2\u0110\u010f\3\2\2\2\u0111"+
		"\u0112\3\2\2\2\u0112\u0110\3\2\2\2\u0112\u0113\3\2\2\2\u0113\u0114\3\2"+
		"\2\2\u0114\u0118\7<\2\2\u0115\u0117\79\2\2\u0116\u0115\3\2\2\2\u0117\u011a"+
		"\3\2\2\2\u0118\u0116\3\2\2\2\u0118\u0119\3\2\2\2\u0119\27\3\2\2\2\u011a"+
		"\u0118\3\2\2\2\u011b\u011c\79\2\2\u011c\u011d\7;\2\2\u011d\u011e\7\27"+
		"\2\2\u011e\u011f\5\32\16\2\u011f\u0120\7\b\2\2\u0120\u0129\5\24\13\2\u0121"+
		"\u0122\79\2\2\u0122\u0123\7\27\2\2\u0123\u0124\5\32\16\2\u0124\u0125\7"+
		"\b\2\2\u0125\u0126\5\24\13\2\u0126\u0128\3\2\2\2\u0127\u0121\3\2\2\2\u0128"+
		"\u012b\3\2\2\2\u0129\u0127\3\2\2\2\u0129\u012a\3\2\2\2\u012a\u012c\3\2"+
		"\2\2\u012b\u0129\3\2\2\2\u012c\u012d\79\2\2\u012d\u0131\7<\2\2\u012e\u0130"+
		"\79\2\2\u012f\u012e\3\2\2\2\u0130\u0133\3\2\2\2\u0131\u012f\3\2\2\2\u0131"+
		"\u0132\3\2\2\2\u0132\31\3\2\2\2\u0133\u0131\3\2\2\2\u0134\u0135\b\16\1"+
		"\2\u0135\u0146\5@!\2\u0136\u0137\5\24\13\2\u0137\u0140\7\62\2\2\u0138"+
		"\u013d\5\32\16\2\u0139\u013a\7\5\2\2\u013a\u013c\5\32\16\2\u013b\u0139"+
		"\3\2\2\2\u013c\u013f\3\2\2\2\u013d\u013b\3\2\2\2\u013d\u013e\3\2\2\2\u013e"+
		"\u0141\3\2\2\2\u013f\u013d\3\2\2\2\u0140\u0138\3\2\2\2\u0140\u0141\3\2"+
		"\2\2\u0141\u0142\3\2\2\2\u0142\u0143\7\63\2\2\u0143\u0146\3\2\2\2\u0144"+
		"\u0146\7\4\2\2\u0145\u0134\3\2\2\2\u0145\u0136\3\2\2\2\u0145\u0144\3\2"+
		"\2\2\u0146\u0152\3\2\2\2\u0147\u0148\f\7\2\2\u0148\u0149\7\30\2\2\u0149"+
		"\u0151\5\32\16\b\u014a\u014b\f\5\2\2\u014b\u014c\7\31\2\2\u014c\u0151"+
		"\5\32\16\6\u014d\u014e\f\4\2\2\u014e\u014f\7\n\2\2\u014f\u0151\5 \21\2"+
		"\u0150\u0147\3\2\2\2\u0150\u014a\3\2\2\2\u0150\u014d\3\2\2\2\u0151\u0154"+
		"\3\2\2\2\u0152\u0150\3\2\2\2\u0152\u0153\3\2\2\2\u0153\33\3\2\2\2\u0154"+
		"\u0152\3\2\2\2\u0155\u0156\7\32\2\2\u0156\u0159\5> \2\u0157\u0158\7\n"+
		"\2\2\u0158\u015a\5 \21\2\u0159\u0157\3\2\2\2\u0159\u015a\3\2\2\2\u015a"+
		"\u015d\3\2\2\2\u015b\u015c\7\33\2\2\u015c\u015e\5\24\13\2\u015d\u015b"+
		"\3\2\2\2\u015d\u015e\3\2\2\2\u015e\u01d9\3\2\2\2\u015f\u0160\7\34\2\2"+
		"\u0160\u0163\5> \2\u0161\u0162\7\n\2\2\u0162\u0164\5 \21\2\u0163\u0161"+
		"\3\2\2\2\u0163\u0164\3\2\2\2\u0164\u0167\3\2\2\2\u0165\u0166\7\33\2\2"+
		"\u0166\u0168\5\24\13\2\u0167\u0165\3\2\2\2\u0167\u0168\3\2\2\2\u0168\u01d9"+
		"\3\2\2\2\u0169\u016a\7\35\2\2\u016a\u016b\5@!\2\u016b\u016e\5&\24\2\u016c"+
		"\u016d\7\33\2\2\u016d\u016f\5 \21\2\u016e\u016c\3\2\2\2\u016e\u016f\3"+
		"\2\2\2\u016f\u01d9\3\2\2\2\u0170\u0171\7\36\2\2\u0171\u0173\5> \2\u0172"+
		"\u0174\5,\27\2\u0173\u0172\3\2\2\2\u0173\u0174\3\2\2\2\u0174\u0176\3\2"+
		"\2\2\u0175\u0177\58\35\2\u0176\u0175\3\2\2\2\u0177\u0178\3\2\2\2\u0178"+
		"\u0176\3\2\2\2\u0178\u0179\3\2\2\2\u0179\u017c\3\2\2\2\u017a\u017b\7\n"+
		"\2\2\u017b\u017d\5 \21\2\u017c\u017a\3\2\2\2\u017c\u017d\3\2\2\2\u017d"+
		"\u0180\3\2\2\2\u017e\u017f\7\33\2\2\u017f\u0181\5\24\13\2\u0180\u017e"+
		"\3\2\2\2\u0180\u0181\3\2\2\2\u0181\u01d9\3\2\2\2\u0182\u0183\7\36\2\2"+
		"\u0183\u0185\5@!\2\u0184\u0186\5,\27\2\u0185\u0184\3\2\2\2\u0185\u0186"+
		"\3\2\2\2\u0186\u0189\3\2\2\2\u0187\u0188\7\n\2\2\u0188\u018a\5 \21\2\u0189"+
		"\u0187\3\2\2\2\u0189\u018a\3\2\2\2\u018a\u018d\3\2\2\2\u018b\u018c\7\33"+
		"\2\2\u018c\u018e\5\24\13\2\u018d\u018b\3\2\2\2\u018d\u018e\3\2\2\2\u018e"+
		"\u01d9\3\2\2\2\u018f\u0191\7\7\2\2\u0190\u0192\5> \2\u0191\u0190\3\2\2"+
		"\2\u0191\u0192\3\2\2\2\u0192\u0193\3\2\2\2\u0193\u0194\7\37\2\2\u0194"+
		"\u0195\5 \21\2\u0195\u0196\7\33\2\2\u0196\u0197\5\24\13\2\u0197\u01d9"+
		"\3\2\2\2\u0198\u0199\7 \2\2\u0199\u019b\5@!\2\u019a\u019c\5.\30\2\u019b"+
		"\u019a\3\2\2\2\u019b\u019c\3\2\2\2\u019c\u01a6\3\2\2\2\u019d\u019e\7!"+
		"\2\2\u019e\u01a3\5\36\20\2\u019f\u01a0\7\r\2\2\u01a0\u01a2\5\36\20\2\u01a1"+
		"\u019f\3\2\2\2\u01a2\u01a5\3\2\2\2\u01a3\u01a1\3\2\2\2\u01a3\u01a4\3\2"+
		"\2\2\u01a4\u01a7\3\2\2\2\u01a5\u01a3\3\2\2\2\u01a6\u019d\3\2\2\2\u01a6"+
		"\u01a7\3\2\2\2\u01a7\u01a9\3\2\2\2\u01a8\u01aa\5\26\f\2\u01a9\u01a8\3"+
		"\2\2\2\u01a9\u01aa\3\2\2\2\u01aa\u01d9\3\2\2\2\u01ab\u01ac\7\"\2\2\u01ac"+
		"\u01ae\5@!\2\u01ad\u01af\5.\30\2\u01ae\u01ad\3\2\2\2\u01ae\u01af\3\2\2"+
		"\2\u01af\u01b9\3\2\2\2\u01b0\u01b1\7!\2\2\u01b1\u01b6\5\36\20\2\u01b2"+
		"\u01b3\7\r\2\2\u01b3\u01b5\5\36\20\2\u01b4\u01b2\3\2\2\2\u01b5\u01b8\3"+
		"\2\2\2\u01b6\u01b4\3\2\2\2\u01b6\u01b7\3\2\2\2\u01b7\u01ba\3\2\2\2\u01b8"+
		"\u01b6\3\2\2\2\u01b9\u01b0\3\2\2\2\u01b9\u01ba\3\2\2\2\u01ba\u01bc\3\2"+
		"\2\2\u01bb\u01bd\5\26\f\2\u01bc\u01bb\3\2\2\2\u01bc\u01bd\3\2\2\2\u01bd"+
		"\u01d9\3\2\2\2\u01be\u01bf\7#\2\2\u01bf\u01c9\5@!\2\u01c0\u01c1\7!\2\2"+
		"\u01c1\u01c6\5\36\20\2\u01c2\u01c3\7\r\2\2\u01c3\u01c5\5\36\20\2\u01c4"+
		"\u01c2\3\2\2\2\u01c5\u01c8\3\2\2\2\u01c6\u01c4\3\2\2\2\u01c6\u01c7\3\2"+
		"\2\2\u01c7\u01ca\3\2\2\2\u01c8\u01c6\3\2\2\2\u01c9\u01c0\3\2\2\2\u01c9"+
		"\u01ca\3\2\2\2\u01ca\u01cd\3\2\2\2\u01cb\u01cc\7\23\2\2\u01cc\u01ce\5"+
		"\24\13\2\u01cd\u01cb\3\2\2\2\u01cd\u01ce\3\2\2\2\u01ce\u01d0\3\2\2\2\u01cf"+
		"\u01d1\5\26\f\2\u01d0\u01cf\3\2\2\2\u01d0\u01d1\3\2\2\2\u01d1\u01d9\3"+
		"\2\2\2\u01d2\u01d3\7$\2\2\u01d3\u01d9\5\34\17\2\u01d4\u01d5\7%\2\2\u01d5"+
		"\u01d9\5\34\17\2\u01d6\u01d7\7&\2\2\u01d7\u01d9\5\34\17\2\u01d8\u0155"+
		"\3\2\2\2\u01d8\u015f\3\2\2\2\u01d8\u0169\3\2\2\2\u01d8\u0170\3\2\2\2\u01d8"+
		"\u0182\3\2\2\2\u01d8\u018f\3\2\2\2\u01d8\u0198\3\2\2\2\u01d8\u01ab\3\2"+
		"\2\2\u01d8\u01be\3\2\2\2\u01d8\u01d2\3\2\2\2\u01d8\u01d4\3\2\2\2\u01d8"+
		"\u01d6\3\2\2\2\u01d9\35\3\2\2\2\u01da\u01df\5@!\2\u01db\u01dc\7\6\2\2"+
		"\u01dc\u01de\5@!\2\u01dd\u01db\3\2\2\2\u01de\u01e1\3\2\2\2\u01df\u01dd"+
		"\3\2\2\2\u01df\u01e0\3\2\2\2\u01e0\u01e3\3\2\2\2\u01e1\u01df\3\2\2\2\u01e2"+
		"\u01e4\5*\26\2\u01e3\u01e2\3\2\2\2\u01e3\u01e4\3\2\2\2\u01e4\37\3\2\2"+
		"\2\u01e5\u01e6\b\21\1\2\u01e6\u01eb\5@!\2\u01e7\u01e8\7\6\2\2\u01e8\u01ea"+
		"\5@!\2\u01e9\u01e7\3\2\2\2\u01ea\u01ed\3\2\2\2\u01eb\u01e9\3\2\2\2\u01eb"+
		"\u01ec\3\2\2\2\u01ec\u01fd\3\2\2\2\u01ed\u01eb\3\2\2\2\u01ee\u01ef\5\24"+
		"\13\2\u01ef\u01f0\7\'\2\2\u01f0\u01f1\5@!\2\u01f1\u01fd\3\2\2\2\u01f2"+
		"\u01f3\5.\30\2\u01f3\u01f4\7)\2\2\u01f4\u01f5\5 \21\6\u01f5\u01fd\3\2"+
		"\2\2\u01f6\u01f7\5\24\13\2\u01f7\u01f8\7\6\2\2\u01f8\u01f9\7\35\2\2\u01f9"+
		"\u01fd\3\2\2\2\u01fa\u01fb\7\b\2\2\u01fb\u01fd\5 \21\4\u01fc\u01e5\3\2"+
		"\2\2\u01fc\u01ee\3\2\2\2\u01fc\u01f2\3\2\2\2\u01fc\u01f6\3\2\2\2\u01fc"+
		"\u01fa\3\2\2\2\u01fd\u020b\3\2\2\2\u01fe\u01ff\f\t\2\2\u01ff\u0200\7\31"+
		"\2\2\u0200\u020a\5 \21\n\u0201\u0202\f\b\2\2\u0202\u0203\7(\2\2\u0203"+
		"\u020a\5 \21\t\u0204\u0205\f\7\2\2\u0205\u020a\5*\26\2\u0206\u0207\f\3"+
		"\2\2\u0207\u0208\7*\2\2\u0208\u020a\5\"\22\2\u0209\u01fe\3\2\2\2\u0209"+
		"\u0201\3\2\2\2\u0209\u0204\3\2\2\2\u0209\u0206\3\2\2\2\u020a\u020d\3\2"+
		"\2\2\u020b\u0209\3\2\2\2\u020b\u020c\3\2\2\2\u020c!\3\2\2\2\u020d\u020b"+
		"\3\2\2\2\u020e\u020f\79\2\2\u020f\u0210\7;\2\2\u0210\u0211\7\27\2\2\u0211"+
		"\u0212\5$\23\2\u0212\u0213\7\b\2\2\u0213\u021c\5 \21\2\u0214\u0215\79"+
		"\2\2\u0215\u0216\7\27\2\2\u0216\u0217\5$\23\2\u0217\u0218\7\b\2\2\u0218"+
		"\u0219\5 \21\2\u0219\u021b\3\2\2\2\u021a\u0214\3\2\2\2\u021b\u021e\3\2"+
		"\2\2\u021c\u021a\3\2\2\2\u021c\u021d\3\2\2\2\u021d\u021f\3\2\2\2\u021e"+
		"\u021c\3\2\2\2\u021f\u0220\79\2\2\u0220\u0221\7<\2\2\u0221\u0222\79\2"+
		"\2\u0222#\3\2\2\2\u0223\u0224\5 \21\2\u0224\u0225\7\64\2\2\u0225\u022a"+
		"\5$\23\2\u0226\u0227\7\5\2\2\u0227\u0229\5$\23\2\u0228\u0226\3\2\2\2\u0229"+
		"\u022c\3\2\2\2\u022a\u0228\3\2\2\2\u022a\u022b\3\2\2\2\u022b\u022d\3\2"+
		"\2\2\u022c\u022a\3\2\2\2\u022d\u022e\7\65\2\2\u022e\u0232\3\2\2\2\u022f"+
		"\u0232\7\4\2\2\u0230\u0232\5 \21\2\u0231\u0223\3\2\2\2\u0231\u022f\3\2"+
		"\2\2\u0231\u0230\3\2\2\2\u0232%\3\2\2\2\u0233\u0235\5(\25\2\u0234\u0233"+
		"\3\2\2\2\u0235\u0238\3\2\2\2\u0236\u0234\3\2\2\2\u0236\u0237\3\2\2\2\u0237"+
		"\'\3\2\2\2\u0238\u0236\3\2\2\2\u0239\u023a\7+\2\2\u023a\u0240\5 \21\2"+
		"\u023b\u023c\7,\2\2\u023c\u0240\5 \21\2\u023d\u023e\7\n\2\2\u023e\u0240"+
		"\5 \21\2\u023f\u0239\3\2\2\2\u023f\u023b\3\2\2\2\u023f\u023d\3\2\2\2\u0240"+
		")\3\2\2\2\u0241\u0242\7\64\2\2\u0242\u0247\5 \21\2\u0243\u0244\7\5\2\2"+
		"\u0244\u0246\5 \21\2\u0245\u0243\3\2\2\2\u0246\u0249\3\2\2\2\u0247\u0245"+
		"\3\2\2\2\u0247\u0248\3\2\2\2\u0248\u024a\3\2\2\2\u0249\u0247\3\2\2\2\u024a"+
		"\u024b\7\65\2\2\u024b+\3\2\2\2\u024c\u024d\7\64\2\2\u024d\u024e\5 \21"+
		"\2\u024e\u024f\5&\24\2\u024f\u0256\3\2\2\2\u0250\u0251\7\5\2\2\u0251\u0252"+
		"\5 \21\2\u0252\u0253\5&\24\2\u0253\u0255\3\2\2\2\u0254\u0250\3\2\2\2\u0255"+
		"\u0258\3\2\2\2\u0256\u0254\3\2\2\2\u0256\u0257\3\2\2\2\u0257\u0259\3\2"+
		"\2\2\u0258\u0256\3\2\2\2\u0259\u025a\7\65\2\2\u025a-\3\2\2\2\u025b\u025c"+
		"\7\64\2\2\u025c\u025d\5\60\31\2\u025d\u025e\5 \21\2\u025e\u025f\5&\24"+
		"\2\u025f\u0267\3\2\2\2\u0260\u0261\7\5\2\2\u0261\u0262\5\60\31\2\u0262"+
		"\u0263\5 \21\2\u0263\u0264\5&\24\2\u0264\u0266\3\2\2\2\u0265\u0260\3\2"+
		"\2\2\u0266\u0269\3\2\2\2\u0267\u0265\3\2\2\2\u0267\u0268\3\2\2\2\u0268"+
		"\u026a\3\2\2\2\u0269\u0267\3\2\2\2\u026a\u026b\7\65\2\2\u026b/\3\2\2\2"+
		"\u026c\u0270\7-\2\2\u026d\u0270\3\2\2\2\u026e\u0270\7.\2\2\u026f\u026c"+
		"\3\2\2\2\u026f\u026d\3\2\2\2\u026f\u026e\3\2\2\2\u0270\61\3\2\2\2\u0271"+
		"\u0272\7\7\2\2\u0272\u0275\5\64\33\2\u0273\u0275\5\64\33\2\u0274\u0271"+
		"\3\2\2\2\u0274\u0273\3\2\2\2\u0275\63\3\2\2\2\u0276\u027f\7\62\2\2\u0277"+
		"\u027c\5\66\34\2\u0278\u0279\7\5\2\2\u0279\u027b\5\66\34\2\u027a\u0278"+
		"\3\2\2\2\u027b\u027e\3\2\2\2\u027c\u027a\3\2\2\2\u027c\u027d\3\2\2\2\u027d"+
		"\u0280\3\2\2\2\u027e\u027c\3\2\2\2\u027f\u0277\3\2\2\2\u027f\u0280\3\2"+
		"\2\2\u0280\u0281\3\2\2\2\u0281\u0282\7\63\2\2\u0282\65\3\2\2\2\u0283\u0286"+
		"\5@!\2\u0284\u0285\7\n\2\2\u0285\u0287\5 \21\2\u0286\u0284\3\2\2\2\u0286"+
		"\u0287\3\2\2\2\u0287\67\3\2\2\2\u0288\u0289\7\7\2\2\u0289\u028c\5:\36"+
		"\2\u028a\u028c\5:\36\2\u028b\u0288\3\2\2\2\u028b\u028a\3\2\2\2\u028c9"+
		"\3\2\2\2\u028d\u0296\7\62\2\2\u028e\u0293\5<\37\2\u028f\u0290\7\5\2\2"+
		"\u0290\u0292\5<\37\2\u0291\u028f\3\2\2\2\u0292\u0295\3\2\2\2\u0293\u0291"+
		"\3\2\2\2\u0293\u0294\3\2\2\2\u0294\u0297\3\2\2\2\u0295\u0293\3\2\2\2\u0296"+
		"\u028e\3\2\2\2\u0296\u0297\3\2\2\2\u0297\u0298\3\2\2\2\u0298\u0299\7\63"+
		"\2\2\u0299;\3\2\2\2\u029a\u029b\5@!\2\u029b\u029c\7\n\2\2\u029c\u029d"+
		"\5 \21\2\u029d=\3\2\2\2\u029e\u029f\t\2\2\2\u029f?\3\2\2\2\u02a0\u02a1"+
		"\7\66\2\2\u02a1A\3\2\2\2QEKTZlqx\u0088\u0090\u0098\u00b8\u00c0\u00c6\u00c9"+
		"\u00d2\u00da\u00ed\u00f0\u00fb\u0102\u0104\u010c\u0112\u0118\u0129\u0131"+
		"\u013d\u0140\u0145\u0150\u0152\u0159\u015d\u0163\u0167\u016e\u0173\u0178"+
		"\u017c\u0180\u0185\u0189\u018d\u0191\u019b\u01a3\u01a6\u01a9\u01ae\u01b6"+
		"\u01b9\u01bc\u01c6\u01c9\u01cd\u01d0\u01d8\u01df\u01e3\u01eb\u01fc\u0209"+
		"\u020b\u021c\u022a\u0231\u0236\u023f\u0247\u0256\u0267\u026f\u0274\u027c"+
		"\u027f\u0286\u028b\u0293\u0296";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}