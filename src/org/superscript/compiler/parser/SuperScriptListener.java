// Generated from /home/rouli-freeman/Documents/SuperScript/SuperScript.g4 by ANTLR 4.7.1
package org.superscript.compiler.parser;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SuperScriptParser}.
 */
public interface SuperScriptListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#file}.
	 * @param ctx the parse tree
	 */
	void enterFile(SuperScriptParser.FileContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#file}.
	 * @param ctx the parse tree
	 */
	void exitFile(SuperScriptParser.FileContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(SuperScriptParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(SuperScriptParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code exprstmt}
	 * labeled alternative in {@link SuperScriptParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterExprstmt(SuperScriptParser.ExprstmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code exprstmt}
	 * labeled alternative in {@link SuperScriptParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitExprstmt(SuperScriptParser.ExprstmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code defstmt}
	 * labeled alternative in {@link SuperScriptParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterDefstmt(SuperScriptParser.DefstmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code defstmt}
	 * labeled alternative in {@link SuperScriptParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitDefstmt(SuperScriptParser.DefstmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code impstmt}
	 * labeled alternative in {@link SuperScriptParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterImpstmt(SuperScriptParser.ImpstmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code impstmt}
	 * labeled alternative in {@link SuperScriptParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitImpstmt(SuperScriptParser.ImpstmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code pkgstmt}
	 * labeled alternative in {@link SuperScriptParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterPkgstmt(SuperScriptParser.PkgstmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code pkgstmt}
	 * labeled alternative in {@link SuperScriptParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitPkgstmt(SuperScriptParser.PkgstmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nameimp}
	 * labeled alternative in {@link SuperScriptParser#imp}.
	 * @param ctx the parse tree
	 */
	void enterNameimp(SuperScriptParser.NameimpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nameimp}
	 * labeled alternative in {@link SuperScriptParser#imp}.
	 * @param ctx the parse tree
	 */
	void exitNameimp(SuperScriptParser.NameimpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code anyimp}
	 * labeled alternative in {@link SuperScriptParser#imp}.
	 * @param ctx the parse tree
	 */
	void enterAnyimp(SuperScriptParser.AnyimpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code anyimp}
	 * labeled alternative in {@link SuperScriptParser#imp}.
	 * @param ctx the parse tree
	 */
	void exitAnyimp(SuperScriptParser.AnyimpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code typeimp}
	 * labeled alternative in {@link SuperScriptParser#imp}.
	 * @param ctx the parse tree
	 */
	void enterTypeimp(SuperScriptParser.TypeimpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code typeimp}
	 * labeled alternative in {@link SuperScriptParser#imp}.
	 * @param ctx the parse tree
	 */
	void exitTypeimp(SuperScriptParser.TypeimpContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#imppath}.
	 * @param ctx the parse tree
	 */
	void enterImppath(SuperScriptParser.ImppathContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#imppath}.
	 * @param ctx the parse tree
	 */
	void exitImppath(SuperScriptParser.ImppathContext ctx);
	/**
	 * Enter a parse tree produced by the {@code allimptype}
	 * labeled alternative in {@link SuperScriptParser#imptype}.
	 * @param ctx the parse tree
	 */
	void enterAllimptype(SuperScriptParser.AllimptypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code allimptype}
	 * labeled alternative in {@link SuperScriptParser#imptype}.
	 * @param ctx the parse tree
	 */
	void exitAllimptype(SuperScriptParser.AllimptypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code givenimptype}
	 * labeled alternative in {@link SuperScriptParser#imptype}.
	 * @param ctx the parse tree
	 */
	void enterGivenimptype(SuperScriptParser.GivenimptypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code givenimptype}
	 * labeled alternative in {@link SuperScriptParser#imptype}.
	 * @param ctx the parse tree
	 */
	void exitGivenimptype(SuperScriptParser.GivenimptypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nameimptype}
	 * labeled alternative in {@link SuperScriptParser#imptype}.
	 * @param ctx the parse tree
	 */
	void enterNameimptype(SuperScriptParser.NameimptypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nameimptype}
	 * labeled alternative in {@link SuperScriptParser#imptype}.
	 * @param ctx the parse tree
	 */
	void exitNameimptype(SuperScriptParser.NameimptypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ignoreimptype}
	 * labeled alternative in {@link SuperScriptParser#imptype}.
	 * @param ctx the parse tree
	 */
	void enterIgnoreimptype(SuperScriptParser.IgnoreimptypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ignoreimptype}
	 * labeled alternative in {@link SuperScriptParser#imptype}.
	 * @param ctx the parse tree
	 */
	void exitIgnoreimptype(SuperScriptParser.IgnoreimptypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code renameimptype}
	 * labeled alternative in {@link SuperScriptParser#imptype}.
	 * @param ctx the parse tree
	 */
	void enterRenameimptype(SuperScriptParser.RenameimptypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code renameimptype}
	 * labeled alternative in {@link SuperScriptParser#imptype}.
	 * @param ctx the parse tree
	 */
	void exitRenameimptype(SuperScriptParser.RenameimptypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#pkg}.
	 * @param ctx the parse tree
	 */
	void enterPkg(SuperScriptParser.PkgContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#pkg}.
	 * @param ctx the parse tree
	 */
	void exitPkg(SuperScriptParser.PkgContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nameappliable}
	 * labeled alternative in {@link SuperScriptParser#appliable}.
	 * @param ctx the parse tree
	 */
	void enterNameappliable(SuperScriptParser.NameappliableContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nameappliable}
	 * labeled alternative in {@link SuperScriptParser#appliable}.
	 * @param ctx the parse tree
	 */
	void exitNameappliable(SuperScriptParser.NameappliableContext ctx);
	/**
	 * Enter a parse tree produced by the {@code exprappliable}
	 * labeled alternative in {@link SuperScriptParser#appliable}.
	 * @param ctx the parse tree
	 */
	void enterExprappliable(SuperScriptParser.ExprappliableContext ctx);
	/**
	 * Exit a parse tree produced by the {@code exprappliable}
	 * labeled alternative in {@link SuperScriptParser#appliable}.
	 * @param ctx the parse tree
	 */
	void exitExprappliable(SuperScriptParser.ExprappliableContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#expressioneof}.
	 * @param ctx the parse tree
	 */
	void enterExpressioneof(SuperScriptParser.ExpressioneofContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#expressioneof}.
	 * @param ctx the parse tree
	 */
	void exitExpressioneof(SuperScriptParser.ExpressioneofContext ctx);
	/**
	 * Enter a parse tree produced by the {@code superexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterSuperexp(SuperScriptParser.SuperexpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code superexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitSuperexp(SuperScriptParser.SuperexpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code blkexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBlkexp(SuperScriptParser.BlkexpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code blkexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBlkexp(SuperScriptParser.BlkexpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code typedexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterTypedexp(SuperScriptParser.TypedexpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code typedexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitTypedexp(SuperScriptParser.TypedexpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code identexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterIdentexp(SuperScriptParser.IdentexpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code identexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitIdentexp(SuperScriptParser.IdentexpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code apply}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterApply(SuperScriptParser.ApplyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code apply}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitApply(SuperScriptParser.ApplyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tapply}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterTapply(SuperScriptParser.TapplyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tapply}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitTapply(SuperScriptParser.TapplyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code match}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMatch(SuperScriptParser.MatchContext ctx);
	/**
	 * Exit a parse tree produced by the {@code match}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMatch(SuperScriptParser.MatchContext ctx);
	/**
	 * Enter a parse tree produced by the {@code gapply}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterGapply(SuperScriptParser.GapplyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code gapply}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitGapply(SuperScriptParser.GapplyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code createexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterCreateexp(SuperScriptParser.CreateexpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code createexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitCreateexp(SuperScriptParser.CreateexpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNumexp(SuperScriptParser.NumexpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNumexp(SuperScriptParser.NumexpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parensexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterParensexp(SuperScriptParser.ParensexpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parensexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitParensexp(SuperScriptParser.ParensexpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code thisexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterThisexp(SuperScriptParser.ThisexpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code thisexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitThisexp(SuperScriptParser.ThisexpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code w}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterW(SuperScriptParser.WContext ctx);
	/**
	 * Exit a parse tree produced by the {@code w}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitW(SuperScriptParser.WContext ctx);
	/**
	 * Enter a parse tree produced by the {@code strexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterStrexp(SuperScriptParser.StrexpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code strexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitStrexp(SuperScriptParser.StrexpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code selectexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterSelectexp(SuperScriptParser.SelectexpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code selectexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitSelectexp(SuperScriptParser.SelectexpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code try}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterTry(SuperScriptParser.TryContext ctx);
	/**
	 * Exit a parse tree produced by the {@code try}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitTry(SuperScriptParser.TryContext ctx);
	/**
	 * Enter a parse tree produced by the {@code infixexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterInfixexp(SuperScriptParser.InfixexpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code infixexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitInfixexp(SuperScriptParser.InfixexpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code if}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterIf(SuperScriptParser.IfContext ctx);
	/**
	 * Exit a parse tree produced by the {@code if}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitIf(SuperScriptParser.IfContext ctx);
	/**
	 * Enter a parse tree produced by the {@code functionexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterFunctionexp(SuperScriptParser.FunctionexpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code functionexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitFunctionexp(SuperScriptParser.FunctionexpContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(SuperScriptParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(SuperScriptParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#mat}.
	 * @param ctx the parse tree
	 */
	void enterMat(SuperScriptParser.MatContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#mat}.
	 * @param ctx the parse tree
	 */
	void exitMat(SuperScriptParser.MatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code valuepat}
	 * labeled alternative in {@link SuperScriptParser#pattern}.
	 * @param ctx the parse tree
	 */
	void enterValuepat(SuperScriptParser.ValuepatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code valuepat}
	 * labeled alternative in {@link SuperScriptParser#pattern}.
	 * @param ctx the parse tree
	 */
	void exitValuepat(SuperScriptParser.ValuepatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unapplypat}
	 * labeled alternative in {@link SuperScriptParser#pattern}.
	 * @param ctx the parse tree
	 */
	void enterUnapplypat(SuperScriptParser.UnapplypatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unapplypat}
	 * labeled alternative in {@link SuperScriptParser#pattern}.
	 * @param ctx the parse tree
	 */
	void exitUnapplypat(SuperScriptParser.UnapplypatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code altpat}
	 * labeled alternative in {@link SuperScriptParser#pattern}.
	 * @param ctx the parse tree
	 */
	void enterAltpat(SuperScriptParser.AltpatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code altpat}
	 * labeled alternative in {@link SuperScriptParser#pattern}.
	 * @param ctx the parse tree
	 */
	void exitAltpat(SuperScriptParser.AltpatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code wildpat}
	 * labeled alternative in {@link SuperScriptParser#pattern}.
	 * @param ctx the parse tree
	 */
	void enterWildpat(SuperScriptParser.WildpatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code wildpat}
	 * labeled alternative in {@link SuperScriptParser#pattern}.
	 * @param ctx the parse tree
	 */
	void exitWildpat(SuperScriptParser.WildpatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code bindpat}
	 * labeled alternative in {@link SuperScriptParser#pattern}.
	 * @param ctx the parse tree
	 */
	void enterBindpat(SuperScriptParser.BindpatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code bindpat}
	 * labeled alternative in {@link SuperScriptParser#pattern}.
	 * @param ctx the parse tree
	 */
	void exitBindpat(SuperScriptParser.BindpatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code typepat}
	 * labeled alternative in {@link SuperScriptParser#pattern}.
	 * @param ctx the parse tree
	 */
	void enterTypepat(SuperScriptParser.TypepatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code typepat}
	 * labeled alternative in {@link SuperScriptParser#pattern}.
	 * @param ctx the parse tree
	 */
	void exitTypepat(SuperScriptParser.TypepatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code valdef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 */
	void enterValdef(SuperScriptParser.ValdefContext ctx);
	/**
	 * Exit a parse tree produced by the {@code valdef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 */
	void exitValdef(SuperScriptParser.ValdefContext ctx);
	/**
	 * Enter a parse tree produced by the {@code vardef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 */
	void enterVardef(SuperScriptParser.VardefContext ctx);
	/**
	 * Exit a parse tree produced by the {@code vardef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 */
	void exitVardef(SuperScriptParser.VardefContext ctx);
	/**
	 * Enter a parse tree produced by the {@code typedef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 */
	void enterTypedef(SuperScriptParser.TypedefContext ctx);
	/**
	 * Exit a parse tree produced by the {@code typedef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 */
	void exitTypedef(SuperScriptParser.TypedefContext ctx);
	/**
	 * Enter a parse tree produced by the {@code methoddef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 */
	void enterMethoddef(SuperScriptParser.MethoddefContext ctx);
	/**
	 * Exit a parse tree produced by the {@code methoddef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 */
	void exitMethoddef(SuperScriptParser.MethoddefContext ctx);
	/**
	 * Enter a parse tree produced by the {@code implicitdef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 */
	void enterImplicitdef(SuperScriptParser.ImplicitdefContext ctx);
	/**
	 * Exit a parse tree produced by the {@code implicitdef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 */
	void exitImplicitdef(SuperScriptParser.ImplicitdefContext ctx);
	/**
	 * Enter a parse tree produced by the {@code classdef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 */
	void enterClassdef(SuperScriptParser.ClassdefContext ctx);
	/**
	 * Exit a parse tree produced by the {@code classdef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 */
	void exitClassdef(SuperScriptParser.ClassdefContext ctx);
	/**
	 * Enter a parse tree produced by the {@code traitdef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 */
	void enterTraitdef(SuperScriptParser.TraitdefContext ctx);
	/**
	 * Exit a parse tree produced by the {@code traitdef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 */
	void exitTraitdef(SuperScriptParser.TraitdefContext ctx);
	/**
	 * Enter a parse tree produced by the {@code objectdef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 */
	void enterObjectdef(SuperScriptParser.ObjectdefContext ctx);
	/**
	 * Exit a parse tree produced by the {@code objectdef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 */
	void exitObjectdef(SuperScriptParser.ObjectdefContext ctx);
	/**
	 * Enter a parse tree produced by the {@code overdef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 */
	void enterOverdef(SuperScriptParser.OverdefContext ctx);
	/**
	 * Exit a parse tree produced by the {@code overdef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 */
	void exitOverdef(SuperScriptParser.OverdefContext ctx);
	/**
	 * Enter a parse tree produced by the {@code privatedef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 */
	void enterPrivatedef(SuperScriptParser.PrivatedefContext ctx);
	/**
	 * Exit a parse tree produced by the {@code privatedef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 */
	void exitPrivatedef(SuperScriptParser.PrivatedefContext ctx);
	/**
	 * Enter a parse tree produced by the {@code protecteddef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 */
	void enterProtecteddef(SuperScriptParser.ProtecteddefContext ctx);
	/**
	 * Exit a parse tree produced by the {@code protecteddef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 */
	void exitProtecteddef(SuperScriptParser.ProtecteddefContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#urlwitharg}.
	 * @param ctx the parse tree
	 */
	void enterUrlwitharg(SuperScriptParser.UrlwithargContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#urlwitharg}.
	 * @param ctx the parse tree
	 */
	void exitUrlwitharg(SuperScriptParser.UrlwithargContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ort}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 */
	void enterOrt(SuperScriptParser.OrtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ort}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 */
	void exitOrt(SuperScriptParser.OrtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code andt}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 */
	void enterAndt(SuperScriptParser.AndtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code andt}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 */
	void exitAndt(SuperScriptParser.AndtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code matcht}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 */
	void enterMatcht(SuperScriptParser.MatchtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code matcht}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 */
	void exitMatcht(SuperScriptParser.MatchtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code namedt}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 */
	void enterNamedt(SuperScriptParser.NamedtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code namedt}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 */
	void exitNamedt(SuperScriptParser.NamedtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code singlet}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 */
	void enterSinglet(SuperScriptParser.SingletContext ctx);
	/**
	 * Exit a parse tree produced by the {@code singlet}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 */
	void exitSinglet(SuperScriptParser.SingletContext ctx);
	/**
	 * Enter a parse tree produced by the {@code lambdat}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 */
	void enterLambdat(SuperScriptParser.LambdatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code lambdat}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 */
	void exitLambdat(SuperScriptParser.LambdatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code bynamet}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 */
	void enterBynamet(SuperScriptParser.BynametContext ctx);
	/**
	 * Exit a parse tree produced by the {@code bynamet}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 */
	void exitBynamet(SuperScriptParser.BynametContext ctx);
	/**
	 * Enter a parse tree produced by the {@code appliedt}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 */
	void enterAppliedt(SuperScriptParser.AppliedtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code appliedt}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 */
	void exitAppliedt(SuperScriptParser.AppliedtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code selectt}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 */
	void enterSelectt(SuperScriptParser.SelecttContext ctx);
	/**
	 * Exit a parse tree produced by the {@code selectt}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 */
	void exitSelectt(SuperScriptParser.SelecttContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#tm}.
	 * @param ctx the parse tree
	 */
	void enterTm(SuperScriptParser.TmContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#tm}.
	 * @param ctx the parse tree
	 */
	void exitTm(SuperScriptParser.TmContext ctx);
	/**
	 * Enter a parse tree produced by the {@code extracttp}
	 * labeled alternative in {@link SuperScriptParser#tp}.
	 * @param ctx the parse tree
	 */
	void enterExtracttp(SuperScriptParser.ExtracttpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code extracttp}
	 * labeled alternative in {@link SuperScriptParser#tp}.
	 * @param ctx the parse tree
	 */
	void exitExtracttp(SuperScriptParser.ExtracttpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tpwildcard}
	 * labeled alternative in {@link SuperScriptParser#tp}.
	 * @param ctx the parse tree
	 */
	void enterTpwildcard(SuperScriptParser.TpwildcardContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tpwildcard}
	 * labeled alternative in {@link SuperScriptParser#tp}.
	 * @param ctx the parse tree
	 */
	void exitTpwildcard(SuperScriptParser.TpwildcardContext ctx);
	/**
	 * Enter a parse tree produced by the {@code valuetp}
	 * labeled alternative in {@link SuperScriptParser#tp}.
	 * @param ctx the parse tree
	 */
	void enterValuetp(SuperScriptParser.ValuetpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code valuetp}
	 * labeled alternative in {@link SuperScriptParser#tp}.
	 * @param ctx the parse tree
	 */
	void exitValuetp(SuperScriptParser.ValuetpContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#typebounds}.
	 * @param ctx the parse tree
	 */
	void enterTypebounds(SuperScriptParser.TypeboundsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#typebounds}.
	 * @param ctx the parse tree
	 */
	void exitTypebounds(SuperScriptParser.TypeboundsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code subclassboundtype}
	 * labeled alternative in {@link SuperScriptParser#boundtype}.
	 * @param ctx the parse tree
	 */
	void enterSubclassboundtype(SuperScriptParser.SubclassboundtypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code subclassboundtype}
	 * labeled alternative in {@link SuperScriptParser#boundtype}.
	 * @param ctx the parse tree
	 */
	void exitSubclassboundtype(SuperScriptParser.SubclassboundtypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code superclassboundtype}
	 * labeled alternative in {@link SuperScriptParser#boundtype}.
	 * @param ctx the parse tree
	 */
	void enterSuperclassboundtype(SuperScriptParser.SuperclassboundtypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code superclassboundtype}
	 * labeled alternative in {@link SuperScriptParser#boundtype}.
	 * @param ctx the parse tree
	 */
	void exitSuperclassboundtype(SuperScriptParser.SuperclassboundtypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code existsimplicitboundtype}
	 * labeled alternative in {@link SuperScriptParser#boundtype}.
	 * @param ctx the parse tree
	 */
	void enterExistsimplicitboundtype(SuperScriptParser.ExistsimplicitboundtypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code existsimplicitboundtype}
	 * labeled alternative in {@link SuperScriptParser#boundtype}.
	 * @param ctx the parse tree
	 */
	void exitExistsimplicitboundtype(SuperScriptParser.ExistsimplicitboundtypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#calltypeargs}.
	 * @param ctx the parse tree
	 */
	void enterCalltypeargs(SuperScriptParser.CalltypeargsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#calltypeargs}.
	 * @param ctx the parse tree
	 */
	void exitCalltypeargs(SuperScriptParser.CalltypeargsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#typeargs}.
	 * @param ctx the parse tree
	 */
	void enterTypeargs(SuperScriptParser.TypeargsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#typeargs}.
	 * @param ctx the parse tree
	 */
	void exitTypeargs(SuperScriptParser.TypeargsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#varianttypeargs}.
	 * @param ctx the parse tree
	 */
	void enterVarianttypeargs(SuperScriptParser.VarianttypeargsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#varianttypeargs}.
	 * @param ctx the parse tree
	 */
	void exitVarianttypeargs(SuperScriptParser.VarianttypeargsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code covariance}
	 * labeled alternative in {@link SuperScriptParser#variance}.
	 * @param ctx the parse tree
	 */
	void enterCovariance(SuperScriptParser.CovarianceContext ctx);
	/**
	 * Exit a parse tree produced by the {@code covariance}
	 * labeled alternative in {@link SuperScriptParser#variance}.
	 * @param ctx the parse tree
	 */
	void exitCovariance(SuperScriptParser.CovarianceContext ctx);
	/**
	 * Enter a parse tree produced by the {@code invariance}
	 * labeled alternative in {@link SuperScriptParser#variance}.
	 * @param ctx the parse tree
	 */
	void enterInvariance(SuperScriptParser.InvarianceContext ctx);
	/**
	 * Exit a parse tree produced by the {@code invariance}
	 * labeled alternative in {@link SuperScriptParser#variance}.
	 * @param ctx the parse tree
	 */
	void exitInvariance(SuperScriptParser.InvarianceContext ctx);
	/**
	 * Enter a parse tree produced by the {@code contravariance}
	 * labeled alternative in {@link SuperScriptParser#variance}.
	 * @param ctx the parse tree
	 */
	void enterContravariance(SuperScriptParser.ContravarianceContext ctx);
	/**
	 * Exit a parse tree produced by the {@code contravariance}
	 * labeled alternative in {@link SuperScriptParser#variance}.
	 * @param ctx the parse tree
	 */
	void exitContravariance(SuperScriptParser.ContravarianceContext ctx);
	/**
	 * Enter a parse tree produced by the {@code givenoptionalargs}
	 * labeled alternative in {@link SuperScriptParser#goptionalargs}.
	 * @param ctx the parse tree
	 */
	void enterGivenoptionalargs(SuperScriptParser.GivenoptionalargsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code givenoptionalargs}
	 * labeled alternative in {@link SuperScriptParser#goptionalargs}.
	 * @param ctx the parse tree
	 */
	void exitGivenoptionalargs(SuperScriptParser.GivenoptionalargsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code normaloptionalargs}
	 * labeled alternative in {@link SuperScriptParser#goptionalargs}.
	 * @param ctx the parse tree
	 */
	void enterNormaloptionalargs(SuperScriptParser.NormaloptionalargsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code normaloptionalargs}
	 * labeled alternative in {@link SuperScriptParser#goptionalargs}.
	 * @param ctx the parse tree
	 */
	void exitNormaloptionalargs(SuperScriptParser.NormaloptionalargsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#optionalargs}.
	 * @param ctx the parse tree
	 */
	void enterOptionalargs(SuperScriptParser.OptionalargsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#optionalargs}.
	 * @param ctx the parse tree
	 */
	void exitOptionalargs(SuperScriptParser.OptionalargsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#oarg}.
	 * @param ctx the parse tree
	 */
	void enterOarg(SuperScriptParser.OargContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#oarg}.
	 * @param ctx the parse tree
	 */
	void exitOarg(SuperScriptParser.OargContext ctx);
	/**
	 * Enter a parse tree produced by the {@code givenargs}
	 * labeled alternative in {@link SuperScriptParser#gargs}.
	 * @param ctx the parse tree
	 */
	void enterGivenargs(SuperScriptParser.GivenargsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code givenargs}
	 * labeled alternative in {@link SuperScriptParser#gargs}.
	 * @param ctx the parse tree
	 */
	void exitGivenargs(SuperScriptParser.GivenargsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code normalargs}
	 * labeled alternative in {@link SuperScriptParser#gargs}.
	 * @param ctx the parse tree
	 */
	void enterNormalargs(SuperScriptParser.NormalargsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code normalargs}
	 * labeled alternative in {@link SuperScriptParser#gargs}.
	 * @param ctx the parse tree
	 */
	void exitNormalargs(SuperScriptParser.NormalargsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#args}.
	 * @param ctx the parse tree
	 */
	void enterArgs(SuperScriptParser.ArgsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#args}.
	 * @param ctx the parse tree
	 */
	void exitArgs(SuperScriptParser.ArgsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#arg}.
	 * @param ctx the parse tree
	 */
	void enterArg(SuperScriptParser.ArgContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#arg}.
	 * @param ctx the parse tree
	 */
	void exitArg(SuperScriptParser.ArgContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#infixname}.
	 * @param ctx the parse tree
	 */
	void enterInfixname(SuperScriptParser.InfixnameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#infixname}.
	 * @param ctx the parse tree
	 */
	void exitInfixname(SuperScriptParser.InfixnameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#name}.
	 * @param ctx the parse tree
	 */
	void enterName(SuperScriptParser.NameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#name}.
	 * @param ctx the parse tree
	 */
	void exitName(SuperScriptParser.NameContext ctx);
}