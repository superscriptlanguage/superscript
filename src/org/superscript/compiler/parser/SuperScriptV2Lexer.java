// Generated from /home/rouli-freeman/Documents/SuperScript/SuperScriptV2Lexer.g4 by ANTLR 4.7.1
package org.superscript.compiler.parser;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SuperScriptV2Lexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		LCURLY=1, RCURLY=2, LPAREN=3, RPAREN=4, LBRACK=5, RBRACK=6, LANGLE=7, 
		RANGLE=8, BIGSTRINGSTART=9, STRINGSTART=10, TYPEOF=11, SEMI=12, COMMA=13, 
		PERIOD=14, EQ=15, COLON=16, OTHER=17, BIGSTRINGY=18, STRINGY=19, SSDOC=20, 
		WS=21, LNAME1=22, LNAME2=23, LNAME3=24, LNAME4=25, LNAME5=26, LNAME6=27, 
		LNAME7=28, LNAME8=29, LNAME9=30, LNAME0=31, RNAME1=32, RNAME2=33, RNAME3=34, 
		RNAME4=35, RNAME5=36, RNAME6=37, RNAME7=38, RNAME8=39, RNAME9=40, RNAME0=41, 
		CHAR=42, NUMBER=43, BIGSTRINGEND=44, BIGINTERPEXC=45, BIGSTRINGINTERP=46, 
		BIGSTRINGINTERP2=47, BIGCHUNK=48, BIGCHARACTER=49, STRINGEND=50, INTERPEXC=51, 
		STRINGINTERP=52, STRINGINTERP2=53, CHUNK=54, CHARACTER=55;
	public static final int
		DOCUMENTATION=2;
	public static final int
		BIGSTRING=1, STRING=2;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN", "DOCUMENTATION"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE", "BIGSTRING", "STRING"
	};

	public static final String[] ruleNames = {
		"LCURLY", "RCURLY", "LPAREN", "RPAREN", "LBRACK", "RBRACK", "LANGLE", 
		"RANGLE", "BIGSTRINGSTART", "STRINGSTART", "TYPEOF", "SEMI", "COMMA", 
		"PERIOD", "EQ", "COLON", "OTHER", "BIGSTRINGY", "STRINGY", "SSDOC", "WS", 
		"SPACING", "BLOCKCOMMENT", "INLINECOMMENT", "LNAME1", "LNAME2", "LNAME3", 
		"LNAME4", "LNAME5", "LNAME6", "LNAME7", "LNAME8", "LNAME9", "LNAME0", 
		"RNAME1", "RNAME2", "RNAME3", "RNAME4", "RNAME5", "RNAME6", "RNAME7", 
		"RNAME8", "RNAME9", "RNAME0", "CHAR", "INTNONEG", "INT", "NUMBER", "DIGIT", 
		"HEXDIGIT", "BINDIGIT", "OCTDIGIT", "LLETIFY", "GLLETIFY", "RLETIFY", 
		"GRLETIFY", "LLETTER", "RLETTER", "LETTER", "LETUN", "ALPHA", "BIGSTRINGEND", 
		"BIGINTERPEXC", "BIGSTRINGINTERP", "BIGSTRINGINTERP2", "BIGCHUNK", "BIGCHARACTER", 
		"STRINGEND", "INTERPEXC", "STRINGINTERP", "STRINGINTERP2", "CHUNK", "CHARACTER"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'{'", "'}'", "'('", "')'", "'['", "']'", "'<'", "'>'", null, null, 
		"'typeof'", "';'", "','", "'.'", "'='", "':'", "'_'", null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, "'\"\"\"'", 
		null, null, null, null, null, "'\"'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "LCURLY", "RCURLY", "LPAREN", "RPAREN", "LBRACK", "RBRACK", "LANGLE", 
		"RANGLE", "BIGSTRINGSTART", "STRINGSTART", "TYPEOF", "SEMI", "COMMA", 
		"PERIOD", "EQ", "COLON", "OTHER", "BIGSTRINGY", "STRINGY", "SSDOC", "WS", 
		"LNAME1", "LNAME2", "LNAME3", "LNAME4", "LNAME5", "LNAME6", "LNAME7", 
		"LNAME8", "LNAME9", "LNAME0", "RNAME1", "RNAME2", "RNAME3", "RNAME4", 
		"RNAME5", "RNAME6", "RNAME7", "RNAME8", "RNAME9", "RNAME0", "CHAR", "NUMBER", 
		"BIGSTRINGEND", "BIGINTERPEXC", "BIGSTRINGINTERP", "BIGSTRINGINTERP2", 
		"BIGCHUNK", "BIGCHARACTER", "STRINGEND", "INTERPEXC", "STRINGINTERP", 
		"STRINGINTERP2", "CHUNK", "CHARACTER"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public SuperScriptV2Lexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "SuperScriptV2Lexer.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\29\u020d\b\1\b\1\b"+
		"\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n"+
		"\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21"+
		"\4\22\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30"+
		"\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37"+
		"\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t"+
		"*\4+\t+\4,\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63"+
		"\4\64\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t"+
		"<\4=\t=\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4"+
		"H\tH\4I\tI\4J\tJ\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3"+
		"\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n\6\n\u00ad\n\n\r\n\16\n\u00ae\3\n\3\n\3"+
		"\n\3\n\3\n\3\n\3\13\6\13\u00b8\n\13\r\13\16\13\u00b9\3\13\3\13\3\13\3"+
		"\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20"+
		"\3\21\3\21\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23"+
		"\3\23\3\23\3\23\7\23\u00e0\n\23\f\23\16\23\u00e3\13\23\3\23\3\23\3\23"+
		"\3\23\3\24\3\24\3\24\3\24\7\24\u00ed\n\24\f\24\16\24\u00f0\13\24\3\24"+
		"\3\24\3\25\3\25\3\25\3\25\3\25\7\25\u00f9\n\25\f\25\16\25\u00fc\13\25"+
		"\3\25\3\25\3\25\3\25\3\25\3\26\3\26\3\26\5\26\u0106\n\26\3\26\3\26\3\27"+
		"\3\27\3\30\3\30\3\30\3\30\7\30\u0110\n\30\f\30\16\30\u0113\13\30\3\30"+
		"\3\30\3\30\3\31\3\31\3\31\3\31\7\31\u011c\n\31\f\31\16\31\u011f\13\31"+
		"\3\32\3\32\3\32\3\33\3\33\3\33\3\34\3\34\3\34\3\35\3\35\3\35\3\36\3\36"+
		"\3\36\3\37\3\37\3\37\3 \3 \3 \3!\3!\3!\3\"\3\"\3\"\3#\3#\3$\3$\3$\3%\3"+
		"%\3%\3&\3&\3&\3\'\3\'\3\'\3(\3(\3(\3)\3)\3)\3*\3*\3*\3+\3+\3+\3,\3,\3"+
		",\3-\3-\3.\3.\3.\3.\5.\u015f\n.\3.\3.\3/\3/\7/\u0165\n/\f/\16/\u0168\13"+
		"/\3\60\3\60\5\60\u016c\n\60\3\60\5\60\u016f\n\60\3\61\3\61\3\61\7\61\u0174"+
		"\n\61\f\61\16\61\u0177\13\61\5\61\u0179\n\61\3\61\3\61\5\61\u017d\n\61"+
		"\3\61\3\61\3\61\3\61\3\61\7\61\u0184\n\61\f\61\16\61\u0187\13\61\3\61"+
		"\3\61\3\61\3\61\7\61\u018d\n\61\f\61\16\61\u0190\13\61\3\61\3\61\3\61"+
		"\3\61\7\61\u0196\n\61\f\61\16\61\u0199\13\61\5\61\u019b\n\61\3\61\5\61"+
		"\u019e\n\61\3\62\3\62\3\63\3\63\3\64\3\64\3\65\3\65\3\66\5\66\u01a9\n"+
		"\66\3\67\7\67\u01ac\n\67\f\67\16\67\u01af\13\67\3\67\3\67\38\58\u01b4"+
		"\n8\39\79\u01b7\n9\f9\169\u01ba\139\39\39\3:\3:\3;\3;\3<\3<\3=\3=\5=\u01c6"+
		"\n=\3>\3>\3?\3?\3?\3?\3?\3?\3@\3@\3@\3A\3A\3A\3A\3A\3B\3B\7B\u01da\nB"+
		"\fB\16B\u01dd\13B\3B\3B\3C\3C\3C\3C\3C\3C\3C\3C\3C\6C\u01ea\nC\rC\16C"+
		"\u01eb\3D\3D\3E\3E\3E\3E\3F\3F\3F\3G\3G\3G\3G\3G\3H\3H\7H\u01fe\nH\fH"+
		"\16H\u0201\13H\3H\3H\3I\3I\3I\6I\u0208\nI\rI\16I\u0209\3J\3J\6\u00e1\u00ee"+
		"\u00fa\u0111\2K\5\3\7\4\t\5\13\6\r\7\17\b\21\t\23\n\25\13\27\f\31\r\33"+
		"\16\35\17\37\20!\21#\22%\23\'\24)\25+\26-\27/\2\61\2\63\2\65\30\67\31"+
		"9\32;\33=\34?\35A\36C\37E G!I\"K#M$O%Q&S\'U(W)Y*[+],_\2a\2c-e\2g\2i\2"+
		"k\2m\2o\2q\2s\2u\2w\2y\2{\2}\2\177.\u0081/\u0083\60\u0085\61\u0087\62"+
		"\u0089\63\u008b\64\u008d\65\u008f\66\u0091\67\u00938\u00959\5\2\3\4\25"+
		"\3\2$$\5\2\f\f\17\17$$\5\2\13\f\17\17\"\"\4\2\f\f\17\17\5\2\'\',,\61\61"+
		"\4\2--//\4\2##??\4\2>>@@\5\2\f\f\17\17))\3\2\63;\4\2GGgg\3\2c|\3\2\62"+
		";\5\2\62;CHch\3\2\62\63\3\2\629\f\2##%(,-//\61;>\\``c|~~\u0080\u0080\f"+
		"\2##%(,-//\61<>\\``c|~~\u0080\u0080\4\2C\\c|\2\u0221\2\5\3\2\2\2\2\7\3"+
		"\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2"+
		"\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35"+
		"\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)"+
		"\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2"+
		";\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3"+
		"\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2"+
		"\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2c\3\2\2\2\3"+
		"\177\3\2\2\2\3\u0081\3\2\2\2\3\u0083\3\2\2\2\3\u0085\3\2\2\2\3\u0087\3"+
		"\2\2\2\3\u0089\3\2\2\2\4\u008b\3\2\2\2\4\u008d\3\2\2\2\4\u008f\3\2\2\2"+
		"\4\u0091\3\2\2\2\4\u0093\3\2\2\2\4\u0095\3\2\2\2\5\u0097\3\2\2\2\7\u009b"+
		"\3\2\2\2\t\u009f\3\2\2\2\13\u00a1\3\2\2\2\r\u00a3\3\2\2\2\17\u00a5\3\2"+
		"\2\2\21\u00a7\3\2\2\2\23\u00a9\3\2\2\2\25\u00ac\3\2\2\2\27\u00b7\3\2\2"+
		"\2\31\u00bf\3\2\2\2\33\u00c6\3\2\2\2\35\u00c8\3\2\2\2\37\u00ca\3\2\2\2"+
		"!\u00cc\3\2\2\2#\u00ce\3\2\2\2%\u00d0\3\2\2\2\'\u00d2\3\2\2\2)\u00e8\3"+
		"\2\2\2+\u00f3\3\2\2\2-\u0105\3\2\2\2/\u0109\3\2\2\2\61\u010b\3\2\2\2\63"+
		"\u0117\3\2\2\2\65\u0120\3\2\2\2\67\u0123\3\2\2\29\u0126\3\2\2\2;\u0129"+
		"\3\2\2\2=\u012c\3\2\2\2?\u012f\3\2\2\2A\u0132\3\2\2\2C\u0135\3\2\2\2E"+
		"\u0138\3\2\2\2G\u013b\3\2\2\2I\u013d\3\2\2\2K\u0140\3\2\2\2M\u0143\3\2"+
		"\2\2O\u0146\3\2\2\2Q\u0149\3\2\2\2S\u014c\3\2\2\2U\u014f\3\2\2\2W\u0152"+
		"\3\2\2\2Y\u0155\3\2\2\2[\u0158\3\2\2\2]\u015a\3\2\2\2_\u0162\3\2\2\2a"+
		"\u016e\3\2\2\2c\u019a\3\2\2\2e\u019f\3\2\2\2g\u01a1\3\2\2\2i\u01a3\3\2"+
		"\2\2k\u01a5\3\2\2\2m\u01a8\3\2\2\2o\u01ad\3\2\2\2q\u01b3\3\2\2\2s\u01b8"+
		"\3\2\2\2u\u01bd\3\2\2\2w\u01bf\3\2\2\2y\u01c1\3\2\2\2{\u01c5\3\2\2\2}"+
		"\u01c7\3\2\2\2\177\u01c9\3\2\2\2\u0081\u01cf\3\2\2\2\u0083\u01d2\3\2\2"+
		"\2\u0085\u01d7\3\2\2\2\u0087\u01e9\3\2\2\2\u0089\u01ed\3\2\2\2\u008b\u01ef"+
		"\3\2\2\2\u008d\u01f3\3\2\2\2\u008f\u01f6\3\2\2\2\u0091\u01fb\3\2\2\2\u0093"+
		"\u0207\3\2\2\2\u0095\u020b\3\2\2\2\u0097\u0098\7}\2\2\u0098\u0099\3\2"+
		"\2\2\u0099\u009a\b\2\2\2\u009a\6\3\2\2\2\u009b\u009c\7\177\2\2\u009c\u009d"+
		"\3\2\2\2\u009d\u009e\b\3\3\2\u009e\b\3\2\2\2\u009f\u00a0\7*\2\2\u00a0"+
		"\n\3\2\2\2\u00a1\u00a2\7+\2\2\u00a2\f\3\2\2\2\u00a3\u00a4\7]\2\2\u00a4"+
		"\16\3\2\2\2\u00a5\u00a6\7_\2\2\u00a6\20\3\2\2\2\u00a7\u00a8\7>\2\2\u00a8"+
		"\22\3\2\2\2\u00a9\u00aa\7@\2\2\u00aa\24\3\2\2\2\u00ab\u00ad\5y<\2\u00ac"+
		"\u00ab\3\2\2\2\u00ad\u00ae\3\2\2\2\u00ae\u00ac\3\2\2\2\u00ae\u00af\3\2"+
		"\2\2\u00af\u00b0\3\2\2\2\u00b0\u00b1\7$\2\2\u00b1\u00b2\7$\2\2\u00b2\u00b3"+
		"\7$\2\2\u00b3\u00b4\3\2\2\2\u00b4\u00b5\b\n\4\2\u00b5\26\3\2\2\2\u00b6"+
		"\u00b8\5y<\2\u00b7\u00b6\3\2\2\2\u00b8\u00b9\3\2\2\2\u00b9\u00b7\3\2\2"+
		"\2\u00b9\u00ba\3\2\2\2\u00ba\u00bb\3\2\2\2\u00bb\u00bc\7$\2\2\u00bc\u00bd"+
		"\3\2\2\2\u00bd\u00be\b\13\5\2\u00be\30\3\2\2\2\u00bf\u00c0\7v\2\2\u00c0"+
		"\u00c1\7{\2\2\u00c1\u00c2\7r\2\2\u00c2\u00c3\7g\2\2\u00c3\u00c4\7q\2\2"+
		"\u00c4\u00c5\7h\2\2\u00c5\32\3\2\2\2\u00c6\u00c7\7=\2\2\u00c7\34\3\2\2"+
		"\2\u00c8\u00c9\7.\2\2\u00c9\36\3\2\2\2\u00ca\u00cb\7\60\2\2\u00cb \3\2"+
		"\2\2\u00cc\u00cd\7?\2\2\u00cd\"\3\2\2\2\u00ce\u00cf\7<\2\2\u00cf$\3\2"+
		"\2\2\u00d0\u00d1\7a\2\2\u00d1&\3\2\2\2\u00d2\u00d3\7$\2\2\u00d3\u00d4"+
		"\7$\2\2\u00d4\u00d5\7$\2\2\u00d5\u00e1\3\2\2\2\u00d6\u00e0\n\2\2\2\u00d7"+
		"\u00d8\7$\2\2\u00d8\u00e0\n\2\2\2\u00d9\u00da\7$\2\2\u00da\u00db\7$\2"+
		"\2\u00db\u00dc\3\2\2\2\u00dc\u00e0\n\2\2\2\u00dd\u00de\7^\2\2\u00de\u00e0"+
		"\13\2\2\2\u00df\u00d6\3\2\2\2\u00df\u00d7\3\2\2\2\u00df\u00d9\3\2\2\2"+
		"\u00df\u00dd\3\2\2\2\u00e0\u00e3\3\2\2\2\u00e1\u00e2\3\2\2\2\u00e1\u00df"+
		"\3\2\2\2\u00e2\u00e4\3\2\2\2\u00e3\u00e1\3\2\2\2\u00e4\u00e5\7$\2\2\u00e5"+
		"\u00e6\7$\2\2\u00e6\u00e7\7$\2\2\u00e7(\3\2\2\2\u00e8\u00ee\7$\2\2\u00e9"+
		"\u00ed\n\3\2\2\u00ea\u00eb\7^\2\2\u00eb\u00ed\13\2\2\2\u00ec\u00e9\3\2"+
		"\2\2\u00ec\u00ea\3\2\2\2\u00ed\u00f0\3\2\2\2\u00ee\u00ef\3\2\2\2\u00ee"+
		"\u00ec\3\2\2\2\u00ef\u00f1\3\2\2\2\u00f0\u00ee\3\2\2\2\u00f1\u00f2\7$"+
		"\2\2\u00f2*\3\2\2\2\u00f3\u00f4\7\61\2\2\u00f4\u00f5\7,\2\2\u00f5\u00f6"+
		"\7,\2\2\u00f6\u00fa\3\2\2\2\u00f7\u00f9\13\2\2\2\u00f8\u00f7\3\2\2\2\u00f9"+
		"\u00fc\3\2\2\2\u00fa\u00fb\3\2\2\2\u00fa\u00f8\3\2\2\2\u00fb\u00fd\3\2"+
		"\2\2\u00fc\u00fa\3\2\2\2\u00fd\u00fe\7,\2\2\u00fe\u00ff\7\61\2\2\u00ff"+
		"\u0100\3\2\2\2\u0100\u0101\b\25\6\2\u0101,\3\2\2\2\u0102\u0106\5/\27\2"+
		"\u0103\u0106\5\61\30\2\u0104\u0106\5\63\31\2\u0105\u0102\3\2\2\2\u0105"+
		"\u0103\3\2\2\2\u0105\u0104\3\2\2\2\u0106\u0107\3\2\2\2\u0107\u0108\b\26"+
		"\7\2\u0108.\3\2\2\2\u0109\u010a\t\4\2\2\u010a\60\3\2\2\2\u010b\u010c\7"+
		"\61\2\2\u010c\u010d\7,\2\2\u010d\u0111\3\2\2\2\u010e\u0110\13\2\2\2\u010f"+
		"\u010e\3\2\2\2\u0110\u0113\3\2\2\2\u0111\u0112\3\2\2\2\u0111\u010f\3\2"+
		"\2\2\u0112\u0114\3\2\2\2\u0113\u0111\3\2\2\2\u0114\u0115\7,\2\2\u0115"+
		"\u0116\7\61\2\2\u0116\62\3\2\2\2\u0117\u0118\7\61\2\2\u0118\u0119\7\61"+
		"\2\2\u0119\u011d\3\2\2\2\u011a\u011c\n\5\2\2\u011b\u011a\3\2\2\2\u011c"+
		"\u011f\3\2\2\2\u011d\u011b\3\2\2\2\u011d\u011e\3\2\2\2\u011e\64\3\2\2"+
		"\2\u011f\u011d\3\2\2\2\u0120\u0121\t\6\2\2\u0121\u0122\5m\66\2\u0122\66"+
		"\3\2\2\2\u0123\u0124\t\7\2\2\u0124\u0125\5m\66\2\u01258\3\2\2\2\u0126"+
		"\u0127\7<\2\2\u0127\u0128\5m\66\2\u0128:\3\2\2\2\u0129\u012a\t\b\2\2\u012a"+
		"\u012b\5m\66\2\u012b<\3\2\2\2\u012c\u012d\t\t\2\2\u012d\u012e\5m\66\2"+
		"\u012e>\3\2\2\2\u012f\u0130\7(\2\2\u0130\u0131\5m\66\2\u0131@\3\2\2\2"+
		"\u0132\u0133\7`\2\2\u0133\u0134\5m\66\2\u0134B\3\2\2\2\u0135\u0136\7~"+
		"\2\2\u0136\u0137\5m\66\2\u0137D\3\2\2\2\u0138\u0139\5}>\2\u0139\u013a"+
		"\5m\66\2\u013aF\3\2\2\2\u013b\u013c\5o\67\2\u013cH\3\2\2\2\u013d\u013e"+
		"\t\6\2\2\u013e\u013f\5q8\2\u013fJ\3\2\2\2\u0140\u0141\t\7\2\2\u0141\u0142"+
		"\5q8\2\u0142L\3\2\2\2\u0143\u0144\7<\2\2\u0144\u0145\5q8\2\u0145N\3\2"+
		"\2\2\u0146\u0147\t\b\2\2\u0147\u0148\5q8\2\u0148P\3\2\2\2\u0149\u014a"+
		"\t\t\2\2\u014a\u014b\5q8\2\u014bR\3\2\2\2\u014c\u014d\7(\2\2\u014d\u014e"+
		"\5q8\2\u014eT\3\2\2\2\u014f\u0150\7`\2\2\u0150\u0151\5q8\2\u0151V\3\2"+
		"\2\2\u0152\u0153\7~\2\2\u0153\u0154\5q8\2\u0154X\3\2\2\2\u0155\u0156\5"+
		"}>\2\u0156\u0157\5q8\2\u0157Z\3\2\2\2\u0158\u0159\5s9\2\u0159\\\3\2\2"+
		"\2\u015a\u015e\7)\2\2\u015b\u015f\n\n\2\2\u015c\u015d\7^\2\2\u015d\u015f"+
		"\13\2\2\2\u015e\u015b\3\2\2\2\u015e\u015c\3\2\2\2\u015f\u0160\3\2\2\2"+
		"\u0160\u0161\7)\2\2\u0161^\3\2\2\2\u0162\u0166\t\13\2\2\u0163\u0165\5"+
		"e\62\2\u0164\u0163\3\2\2\2\u0165\u0168\3\2\2\2\u0166\u0164\3\2\2\2\u0166"+
		"\u0167\3\2\2\2\u0167`\3\2\2\2\u0168\u0166\3\2\2\2\u0169\u016f\7\62\2\2"+
		"\u016a\u016c\7/\2\2\u016b\u016a\3\2\2\2\u016b\u016c\3\2\2\2\u016c\u016d"+
		"\3\2\2\2\u016d\u016f\5_/\2\u016e\u0169\3\2\2\2\u016e\u016b\3\2\2\2\u016f"+
		"b\3\2\2\2\u0170\u0178\5_/\2\u0171\u0175\7\60\2\2\u0172\u0174\5e\62\2\u0173"+
		"\u0172\3\2\2\2\u0174\u0177\3\2\2\2\u0175\u0173\3\2\2\2\u0175\u0176\3\2"+
		"\2\2\u0176\u0179\3\2\2\2\u0177\u0175\3\2\2\2\u0178\u0171\3\2\2\2\u0178"+
		"\u0179\3\2\2\2\u0179\u017c\3\2\2\2\u017a\u017b\t\f\2\2\u017b\u017d\5a"+
		"\60\2\u017c\u017a\3\2\2\2\u017c\u017d\3\2\2\2\u017d\u019b\3\2\2\2\u017e"+
		"\u019b\7\62\2\2\u017f\u0180\7\62\2\2\u0180\u0181\7z\2\2\u0181\u0185\3"+
		"\2\2\2\u0182\u0184\5g\63\2\u0183\u0182\3\2\2\2\u0184\u0187\3\2\2\2\u0185"+
		"\u0183\3\2\2\2\u0185\u0186\3\2\2\2\u0186\u019b\3\2\2\2\u0187\u0185\3\2"+
		"\2\2\u0188\u0189\7\62\2\2\u0189\u018a\7d\2\2\u018a\u018e\3\2\2\2\u018b"+
		"\u018d\5i\64\2\u018c\u018b\3\2\2\2\u018d\u0190\3\2\2\2\u018e\u018c\3\2"+
		"\2\2\u018e\u018f\3\2\2\2\u018f\u019b\3\2\2\2\u0190\u018e\3\2\2\2\u0191"+
		"\u0192\7\62\2\2\u0192\u0193\7q\2\2\u0193\u0197\3\2\2\2\u0194\u0196\5k"+
		"\65\2\u0195\u0194\3\2\2\2\u0196\u0199\3\2\2\2\u0197\u0195\3\2\2\2\u0197"+
		"\u0198\3\2\2\2\u0198\u019b\3\2\2\2\u0199\u0197\3\2\2\2\u019a\u0170\3\2"+
		"\2\2\u019a\u017e\3\2\2\2\u019a\u017f\3\2\2\2\u019a\u0188\3\2\2\2\u019a"+
		"\u0191\3\2\2\2\u019b\u019d\3\2\2\2\u019c\u019e\t\r\2\2\u019d\u019c\3\2"+
		"\2\2\u019d\u019e\3\2\2\2\u019ed\3\2\2\2\u019f\u01a0\t\16\2\2\u01a0f\3"+
		"\2\2\2\u01a1\u01a2\t\17\2\2\u01a2h\3\2\2\2\u01a3\u01a4\t\20\2\2\u01a4"+
		"j\3\2\2\2\u01a5\u01a6\t\21\2\2\u01a6l\3\2\2\2\u01a7\u01a9\5o\67\2\u01a8"+
		"\u01a7\3\2\2\2\u01a8\u01a9\3\2\2\2\u01a9n\3\2\2\2\u01aa\u01ac\5{=\2\u01ab"+
		"\u01aa\3\2\2\2\u01ac\u01af\3\2\2\2\u01ad\u01ab\3\2\2\2\u01ad\u01ae\3\2"+
		"\2\2\u01ae\u01b0\3\2\2\2\u01af\u01ad\3\2\2\2\u01b0\u01b1\5u:\2\u01b1p"+
		"\3\2\2\2\u01b2\u01b4\5s9\2\u01b3\u01b2\3\2\2\2\u01b3\u01b4\3\2\2\2\u01b4"+
		"r\3\2\2\2\u01b5\u01b7\5{=\2\u01b6\u01b5\3\2\2\2\u01b7\u01ba\3\2\2\2\u01b8"+
		"\u01b6\3\2\2\2\u01b8\u01b9\3\2\2\2\u01b9\u01bb\3\2\2\2\u01ba\u01b8\3\2"+
		"\2\2\u01bb\u01bc\5w;\2\u01bct\3\2\2\2\u01bd\u01be\t\22\2\2\u01bev\3\2"+
		"\2\2\u01bf\u01c0\7<\2\2\u01c0x\3\2\2\2\u01c1\u01c2\t\23\2\2\u01c2z\3\2"+
		"\2\2\u01c3\u01c6\5y<\2\u01c4\u01c6\7a\2\2\u01c5\u01c3\3\2\2\2\u01c5\u01c4"+
		"\3\2\2\2\u01c6|\3\2\2\2\u01c7\u01c8\t\24\2\2\u01c8~\3\2\2\2\u01c9\u01ca"+
		"\7$\2\2\u01ca\u01cb\7$\2\2\u01cb\u01cc\7$\2\2\u01cc\u01cd\3\2\2\2\u01cd"+
		"\u01ce\b?\3\2\u01ce\u0080\3\2\2\2\u01cf\u01d0\7&\2\2\u01d0\u01d1\7&\2"+
		"\2\u01d1\u0082\3\2\2\2\u01d2\u01d3\7&\2\2\u01d3\u01d4\7}\2\2\u01d4\u01d5"+
		"\3\2\2\2\u01d5\u01d6\bA\2\2\u01d6\u0084\3\2\2\2\u01d7\u01db\7&\2\2\u01d8"+
		"\u01da\5{=\2\u01d9\u01d8\3\2\2\2\u01da\u01dd\3\2\2\2\u01db\u01d9\3\2\2"+
		"\2\u01db\u01dc\3\2\2\2\u01dc\u01de\3\2\2\2\u01dd\u01db\3\2\2\2\u01de\u01df"+
		"\5y<\2\u01df\u0086\3\2\2\2\u01e0\u01ea\n\2\2\2\u01e1\u01e2\7$\2\2\u01e2"+
		"\u01ea\n\2\2\2\u01e3\u01e4\7$\2\2\u01e4\u01e5\7$\2\2\u01e5\u01e6\3\2\2"+
		"\2\u01e6\u01ea\n\2\2\2\u01e7\u01e8\7^\2\2\u01e8\u01ea\13\2\2\2\u01e9\u01e0"+
		"\3\2\2\2\u01e9\u01e1\3\2\2\2\u01e9\u01e3\3\2\2\2\u01e9\u01e7\3\2\2\2\u01ea"+
		"\u01eb\3\2\2\2\u01eb\u01e9\3\2\2\2\u01eb\u01ec\3\2\2\2\u01ec\u0088\3\2"+
		"\2\2\u01ed\u01ee\n\2\2\2\u01ee\u008a\3\2\2\2\u01ef\u01f0\7$\2\2\u01f0"+
		"\u01f1\3\2\2\2\u01f1\u01f2\bE\3\2\u01f2\u008c\3\2\2\2\u01f3\u01f4\7&\2"+
		"\2\u01f4\u01f5\7&\2\2\u01f5\u008e\3\2\2\2\u01f6\u01f7\7&\2\2\u01f7\u01f8"+
		"\7}\2\2\u01f8\u01f9\3\2\2\2\u01f9\u01fa\bG\2\2\u01fa\u0090\3\2\2\2\u01fb"+
		"\u01ff\7&\2\2\u01fc\u01fe\5{=\2\u01fd\u01fc\3\2\2\2\u01fe\u0201\3\2\2"+
		"\2\u01ff\u01fd\3\2\2\2\u01ff\u0200\3\2\2\2\u0200\u0202\3\2\2\2\u0201\u01ff"+
		"\3\2\2\2\u0202\u0203\5y<\2\u0203\u0092\3\2\2\2\u0204\u0208\n\2\2\2\u0205"+
		"\u0206\7^\2\2\u0206\u0208\13\2\2\2\u0207\u0204\3\2\2\2\u0207\u0205\3\2"+
		"\2\2\u0208\u0209\3\2\2\2\u0209\u0207\3\2\2\2\u0209\u020a\3\2\2\2\u020a"+
		"\u0094\3\2\2\2\u020b\u020c\n\2\2\2\u020c\u0096\3\2\2\2&\2\3\4\u00ae\u00b9"+
		"\u00df\u00e1\u00ec\u00ee\u00fa\u0105\u0111\u011d\u015e\u0166\u016b\u016e"+
		"\u0175\u0178\u017c\u0185\u018e\u0197\u019a\u019d\u01a8\u01ad\u01b3\u01b8"+
		"\u01c5\u01db\u01e9\u01eb\u01ff\u0207\u0209\b\7\2\2\6\2\2\7\3\2\7\4\2\2"+
		"\4\2\2\3\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}