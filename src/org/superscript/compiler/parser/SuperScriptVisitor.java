// Generated from /home/rouli-freeman/Documents/SuperScript/SuperScript.g4 by ANTLR 4.7.1
package org.superscript.compiler.parser;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link SuperScriptParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface SuperScriptVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#file}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFile(SuperScriptParser.FileContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(SuperScriptParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exprstmt}
	 * labeled alternative in {@link SuperScriptParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprstmt(SuperScriptParser.ExprstmtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code defstmt}
	 * labeled alternative in {@link SuperScriptParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefstmt(SuperScriptParser.DefstmtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code impstmt}
	 * labeled alternative in {@link SuperScriptParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImpstmt(SuperScriptParser.ImpstmtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code pkgstmt}
	 * labeled alternative in {@link SuperScriptParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPkgstmt(SuperScriptParser.PkgstmtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nameimp}
	 * labeled alternative in {@link SuperScriptParser#imp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNameimp(SuperScriptParser.NameimpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code anyimp}
	 * labeled alternative in {@link SuperScriptParser#imp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnyimp(SuperScriptParser.AnyimpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code typeimp}
	 * labeled alternative in {@link SuperScriptParser#imp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeimp(SuperScriptParser.TypeimpContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#imppath}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImppath(SuperScriptParser.ImppathContext ctx);
	/**
	 * Visit a parse tree produced by the {@code allimptype}
	 * labeled alternative in {@link SuperScriptParser#imptype}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAllimptype(SuperScriptParser.AllimptypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code givenimptype}
	 * labeled alternative in {@link SuperScriptParser#imptype}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGivenimptype(SuperScriptParser.GivenimptypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nameimptype}
	 * labeled alternative in {@link SuperScriptParser#imptype}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNameimptype(SuperScriptParser.NameimptypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ignoreimptype}
	 * labeled alternative in {@link SuperScriptParser#imptype}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIgnoreimptype(SuperScriptParser.IgnoreimptypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code renameimptype}
	 * labeled alternative in {@link SuperScriptParser#imptype}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRenameimptype(SuperScriptParser.RenameimptypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#pkg}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPkg(SuperScriptParser.PkgContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nameappliable}
	 * labeled alternative in {@link SuperScriptParser#appliable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNameappliable(SuperScriptParser.NameappliableContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exprappliable}
	 * labeled alternative in {@link SuperScriptParser#appliable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprappliable(SuperScriptParser.ExprappliableContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#expressioneof}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressioneof(SuperScriptParser.ExpressioneofContext ctx);
	/**
	 * Visit a parse tree produced by the {@code superexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSuperexp(SuperScriptParser.SuperexpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code blkexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlkexp(SuperScriptParser.BlkexpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code typedexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypedexp(SuperScriptParser.TypedexpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code identexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentexp(SuperScriptParser.IdentexpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code apply}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitApply(SuperScriptParser.ApplyContext ctx);
	/**
	 * Visit a parse tree produced by the {@code tapply}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTapply(SuperScriptParser.TapplyContext ctx);
	/**
	 * Visit a parse tree produced by the {@code match}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMatch(SuperScriptParser.MatchContext ctx);
	/**
	 * Visit a parse tree produced by the {@code gapply}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGapply(SuperScriptParser.GapplyContext ctx);
	/**
	 * Visit a parse tree produced by the {@code createexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreateexp(SuperScriptParser.CreateexpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code numexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumexp(SuperScriptParser.NumexpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parensexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParensexp(SuperScriptParser.ParensexpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code thisexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitThisexp(SuperScriptParser.ThisexpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code w}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitW(SuperScriptParser.WContext ctx);
	/**
	 * Visit a parse tree produced by the {@code strexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStrexp(SuperScriptParser.StrexpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code selectexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelectexp(SuperScriptParser.SelectexpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code try}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTry(SuperScriptParser.TryContext ctx);
	/**
	 * Visit a parse tree produced by the {@code infixexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInfixexp(SuperScriptParser.InfixexpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code if}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf(SuperScriptParser.IfContext ctx);
	/**
	 * Visit a parse tree produced by the {@code functionexp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionexp(SuperScriptParser.FunctionexpContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(SuperScriptParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#mat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMat(SuperScriptParser.MatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code valuepat}
	 * labeled alternative in {@link SuperScriptParser#pattern}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValuepat(SuperScriptParser.ValuepatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unapplypat}
	 * labeled alternative in {@link SuperScriptParser#pattern}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnapplypat(SuperScriptParser.UnapplypatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code altpat}
	 * labeled alternative in {@link SuperScriptParser#pattern}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAltpat(SuperScriptParser.AltpatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code wildpat}
	 * labeled alternative in {@link SuperScriptParser#pattern}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWildpat(SuperScriptParser.WildpatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code bindpat}
	 * labeled alternative in {@link SuperScriptParser#pattern}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBindpat(SuperScriptParser.BindpatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code typepat}
	 * labeled alternative in {@link SuperScriptParser#pattern}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypepat(SuperScriptParser.TypepatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code valdef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValdef(SuperScriptParser.ValdefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code vardef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVardef(SuperScriptParser.VardefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code typedef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypedef(SuperScriptParser.TypedefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code methoddef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethoddef(SuperScriptParser.MethoddefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code implicitdef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImplicitdef(SuperScriptParser.ImplicitdefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code classdef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassdef(SuperScriptParser.ClassdefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code traitdef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTraitdef(SuperScriptParser.TraitdefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code objectdef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjectdef(SuperScriptParser.ObjectdefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code overdef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOverdef(SuperScriptParser.OverdefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code privatedef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrivatedef(SuperScriptParser.PrivatedefContext ctx);
	/**
	 * Visit a parse tree produced by the {@code protecteddef}
	 * labeled alternative in {@link SuperScriptParser#definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProtecteddef(SuperScriptParser.ProtecteddefContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#urlwitharg}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUrlwitharg(SuperScriptParser.UrlwithargContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ort}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrt(SuperScriptParser.OrtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code andt}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAndt(SuperScriptParser.AndtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code matcht}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMatcht(SuperScriptParser.MatchtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code namedt}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNamedt(SuperScriptParser.NamedtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code singlet}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSinglet(SuperScriptParser.SingletContext ctx);
	/**
	 * Visit a parse tree produced by the {@code lambdat}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLambdat(SuperScriptParser.LambdatContext ctx);
	/**
	 * Visit a parse tree produced by the {@code bynamet}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBynamet(SuperScriptParser.BynametContext ctx);
	/**
	 * Visit a parse tree produced by the {@code appliedt}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAppliedt(SuperScriptParser.AppliedtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code selectt}
	 * labeled alternative in {@link SuperScriptParser#t}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelectt(SuperScriptParser.SelecttContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#tm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTm(SuperScriptParser.TmContext ctx);
	/**
	 * Visit a parse tree produced by the {@code extracttp}
	 * labeled alternative in {@link SuperScriptParser#tp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExtracttp(SuperScriptParser.ExtracttpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code tpwildcard}
	 * labeled alternative in {@link SuperScriptParser#tp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTpwildcard(SuperScriptParser.TpwildcardContext ctx);
	/**
	 * Visit a parse tree produced by the {@code valuetp}
	 * labeled alternative in {@link SuperScriptParser#tp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValuetp(SuperScriptParser.ValuetpContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#typebounds}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypebounds(SuperScriptParser.TypeboundsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code subclassboundtype}
	 * labeled alternative in {@link SuperScriptParser#boundtype}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubclassboundtype(SuperScriptParser.SubclassboundtypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code superclassboundtype}
	 * labeled alternative in {@link SuperScriptParser#boundtype}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSuperclassboundtype(SuperScriptParser.SuperclassboundtypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code existsimplicitboundtype}
	 * labeled alternative in {@link SuperScriptParser#boundtype}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExistsimplicitboundtype(SuperScriptParser.ExistsimplicitboundtypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#calltypeargs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCalltypeargs(SuperScriptParser.CalltypeargsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#typeargs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeargs(SuperScriptParser.TypeargsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#varianttypeargs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarianttypeargs(SuperScriptParser.VarianttypeargsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code covariance}
	 * labeled alternative in {@link SuperScriptParser#variance}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCovariance(SuperScriptParser.CovarianceContext ctx);
	/**
	 * Visit a parse tree produced by the {@code invariance}
	 * labeled alternative in {@link SuperScriptParser#variance}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInvariance(SuperScriptParser.InvarianceContext ctx);
	/**
	 * Visit a parse tree produced by the {@code contravariance}
	 * labeled alternative in {@link SuperScriptParser#variance}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitContravariance(SuperScriptParser.ContravarianceContext ctx);
	/**
	 * Visit a parse tree produced by the {@code givenoptionalargs}
	 * labeled alternative in {@link SuperScriptParser#goptionalargs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGivenoptionalargs(SuperScriptParser.GivenoptionalargsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code normaloptionalargs}
	 * labeled alternative in {@link SuperScriptParser#goptionalargs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNormaloptionalargs(SuperScriptParser.NormaloptionalargsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#optionalargs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOptionalargs(SuperScriptParser.OptionalargsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#oarg}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOarg(SuperScriptParser.OargContext ctx);
	/**
	 * Visit a parse tree produced by the {@code givenargs}
	 * labeled alternative in {@link SuperScriptParser#gargs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGivenargs(SuperScriptParser.GivenargsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code normalargs}
	 * labeled alternative in {@link SuperScriptParser#gargs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNormalargs(SuperScriptParser.NormalargsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#args}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgs(SuperScriptParser.ArgsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#arg}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArg(SuperScriptParser.ArgContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#infixname}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInfixname(SuperScriptParser.InfixnameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitName(SuperScriptParser.NameContext ctx);
}