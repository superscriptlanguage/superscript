package org.superscript.compiler

import ast._
import org.superscript.template.LazyString._
import org.superscript.Lazy

inline val size = "    "

inline val header = """
class AnyRefDelegator(__0_del: AnyRef);
"""


def compile[T](ast: T, indent: String)(given desc: AstLike)(given ev: T <:< desc.Ast): LazyString = {
    import desc.{given, _}
    val i = Lazy(indent)
    ast match {
        case null => l"null"
        case File(a) => l"${header}${a.map(x => compile(x, indent)).toPrefixLS(l"\n")}"
        case Import(a, b) => {
            import Import.ImportType
            def toLS(x: ImportType): LazyString = x match {
                case ImportType.Simple(a) => a
                case ImportType.Wildcard => l"_"
                case ImportType.Rename(a, b) => l"${a} => ${Lazy(b.getOrElse("_"))}"
            }
            l"${i}import ${a.mkString(".")}.{${b.map(toLS).toLS(l", ")}};"
        }
        case Package(a) => l"${i}package ${a.mkString(".")};"
        case Expr(a) => l"${i}${compile(a, indent)};"
        case TypeDef(a, b, c) => l"${i}type ${a}${compile(b, indent)}${c.map(l" = " + compile(_, indent)).getOrElse(l"")};"
        case ValDef(a, b, c, d) => l"${i}${if(a) l"val" else l"var"} ${b}${c.map(x => l": " + compile(x, indent)).getOrElse(l"")}${d.map(x => l" = ${compile(x, indent)}").getOrElse(l"")};"
        case ClassDef(a, b, c, d) => {
            def removeAbstract(x: Statement): Option[Statement] = x match {
                case ValDef(a, b, c, d) => if(d.isEmpty) Some(ValDef(a, b, c, Some(Select(Ident("__0_del"), b)))) else None
                case MethodDef(a, b, c, d, e) => {
                    val polyFun = b.map(x => TypeApply(Ident(a), x.convertToCallTypeArgs)).getOrElse(Ident(a))
                    def getApplies(base: Expression, remaining: Seq[Args]): Expression = {
                        if(remaining.isEmpty) base else getApplies(Apply(base, remaining.head.args.map(x => Ident(x._1))), remaining.tail)
                    }
                    e.map(a => None).getOrElse(Some(MethodDef(a, b, c, d, Some(getApplies(polyFun, c)))))
                }
                case _ => None
            }
            def unabstract(x: Block): Block = x match {
                case Block(a) => Block(a.flatMap(x => removeAbstract(x)))
            }
            val varargs = a.map(x => compile(x, indent)).getOrElse(l"")
            val bc = b + varargs
            val bcnv = b + a.map(x => compile(x.convertToTypeArgs.convertToCallTypeArgs, indent)).getOrElse(l"")
            val fheadc = Lazy(c.head._1.mkString("."))
            val sheadc = c.head._2.map(x => compile(x, indent)).getOrElse(l"")
            val headc = fheadc + sheadc
            val extensions = l" extends " + fheadc + "Delegator" + sheadc + l"(__0_proto)" + c.tail.map(x => Lazy(x._1.mkString(".")) + x._2.map(y => compile(y, indent)).getOrElse(l"")).toPrefixLS(" with ")
            val first = l"${i}abstract class ${bc}(__0_proto: ${headc})${extensions} ${compile(d, indent)}"
            val second = l"${i}class ${b}Delegator${varargs}(__0_del: ${bcnv}) extends ${bcnv}(__0_del) ${compile(unabstract(d), indent)}"
            first + l"\n" + second
        }
        case ObjectDef(a, b, c, d) => {
            val fheadb = Lazy(b.head._1.mkString("."))
            val sheadb = b.head._2.map(x => compile(x, indent)).getOrElse(l"")
            val headb = fheadb + sheadb
            val extensions = l" extends " + fheadb + "Delegator" + sheadb + l"(${compile(d, indent)})" + b.tail.map(x => Lazy(x._1.mkString(".")) + x._2.map(y => compile(y, indent)).getOrElse(l"")).toPrefixLS(" with ")
            l"${i}object ${a}${extensions} ${compile(c, indent)}"
        }
        case TraitDef(a, b, c, d) => l"${i}trait ${b}${a.map(x => compile(x, indent)).getOrElse(l"")}${if(c.nonEmpty) l" extends " + c.map(x => Lazy(x._1.mkString(".")) + x._2.map(y => compile(y, indent)).getOrElse(l"")).toLS(" with ") else l""} ${compile(d, indent)}"
        case ImplicitDef(a, b, c) => l"${i}delegate ${a.map(x => x + " ").getOrElse("")}for ${compile(b, indent)} = ${compile(c, indent)};"
        case MethodDef(a, b, c, d, e) => l"${i}def " + a + b.map(x => compile(x, indent)).getOrElse(l"") + c.map(x => compile(x, indent)).toLS("") + d.map(a => l": " + compile(a, indent)).getOrElse(l"") + e.map(a => l" = " + compile(a, indent)).getOrElse(l"") + l";"
        case ModifiedDef(a, b) => {
            import ModifiedDef.Modifier
            def compModifier(a: Modifier): LazyString = a match {
                case Modifier.Override => l"override"
                case Modifier.Protected => l"protected"
                case Modifier.Private => l"private"
            }
            compModifier(a) + l" " + compile(b, indent)
        }
        case NamedArg(a, b) => l"${a}: ${compile(b, indent)}"
        case Ident(a) => Lazy(a)
        case Select(a, b) => l"${compile(a, indent)}.${Lazy(b)}"
        case This => l"this"
        case Super => l"super"
        case Typed(a, b) => compile(a, indent) + ": " + compile(b, indent)
        case Literal(a) => a
        case Apply(a, b) => compile(a, indent) + l"(${b.map(x => compile(x, indent)).toLS(", ")})"
        case Create(a, b, c) => l"new " + compile(a, indent) + l"(${compile(b, indent)}) " + compile(c, indent)
        case GivenApply(a, b) => compile(a, indent) + l"(given ${b.map(x => compile(x, indent)).toLS(", ")})"
        case TypeApply(a, b) => compile(a, indent) + compile(b, indent)
        case Infix(a, b, c) => compile(a, indent) + l" ${b} " + compile(c, indent)
        case Parens(a) => l"(${compile(a, indent)})"
        case Block(a) => l"{\n${a.map(x => compile(x, indent + size)).toPostfixLS("\n")}${i}}"
        case With(a, b) => l"${i}{\n${i}${size}val __0_with = ${compile(a, indent + size)};\n${i}${size}import __0_with._;\n${compile(b, indent + size)}\n${i}};"
        case If(a, b, c) => l"if(${compile(a, indent)}) ${compile(b, indent)}${c.map(a => l" else " + compile(a, indent)).getOrElse(l"")}"
        case Function(a, b) => l"${compile(a, indent)} => ${compile(b, indent)}"
        case Match(a) => l"{\n${a.map(x => i + Lazy(size) + l"case " + compile(x._1, indent + size) + l" => " + compile(x._2, indent + size)).toPostfixLS("\n")}${i}}"
        case Try(a, b, c) => l"try ${compile(a, indent)} catch ${compile(b, indent)}${c.map(x => l" finally " + compile(x, indent)).getOrElse(l"")}"
        case Value(a) => a
        case Bind(a, b) => compile(a, indent) + l" @ " + compile(b, indent)
        case Unapply(a, b) => compile(a, indent) + l"(${b.map(x => compile(x, indent)).toLS(l", ")})"
        case Alternatives(a) => a.map(x => compile(x, indent)).toLS(l" | ")
        case TypeTest(a, b) => compile(a, indent) + l": " + compile(b, indent)
        case Wildcard() => l"_"
        case TypeBounds(a) => {
            import TypeBounds.BoundType
            def toLS(x: BoundType): LazyString = x match {
                case BoundType.Subclass(t) => l"<: ${compile(t, indent)}"
                case BoundType.Superclass(t) => l">: ${compile(t, indent)}"
                case BoundType.ExistsImplicit(t) => l": ${compile(t, indent)}"
            }
            a.map(toLS).toPrefixLS(l" ")
        }
        case TypeSelect(a, b) => compile(a, indent) + l"#${b}"
        case TypeLambda(a, b) => compile(a, indent) + l" =>> " + compile(b, indent)
        case SingletonType(a) => compile(a, indent) + l".type"
        case ByNameType(a) => l"=> " + compile(a, indent)
        case MatchType(a, b) => compile(a, indent) + l" match {\n${b.map(x => i + Lazy(size) + compile(x._1, indent + size) + l" => " + compile(x._2, indent + size)).toPostfixLS("\n")}${i}}"
        case OrType(a, b) => compile(a, indent) + l" | " + compile(b, indent)
        case AndType(a, b) => compile(a, indent) + l" & " + compile(b, indent)
        case AppliedType(a, b) => compile(a, indent) + compile(b, indent)
        case NamedType(a) => a.mkString(".")
        case ExtractType(a, b) => compile(a, indent) + l"[${b.map(x => compile(x, indent)).toLS(l", ")}]"
        case TypeWildcard() => l"_"
        case TypeValue(a) => compile(a, indent)
        case OptionalArgs(a, b) => {
            import OptionalArgs.ArgType
            def toLS(x: ArgType): LazyString = x match {
                case ArgType.Given => l"(given "
                case ArgType.Normal => l"("
            }
            toLS(b) + a.map(x => Lazy(x._1) + x._2.map(a => l": " + compile(a, indent)).getOrElse(l"")).toLS(l", ") + ")" //! WRONG FIX ALLOW DESTRUCTURING IN METHODS
        }
        case CallTypeArgs(a) => l"[" + a.map(x => compile(x, indent)).toLS(l", ") + l"]"
        case TypeArgs(a) => l"[" + a.map(x => compile(x._1, indent) + compile(x._2, indent)).toLS(l", ") + l"]"
        case VariantTypeArgs(a) => {
            def toLS(x: Int): LazyString = x match {
                case a if a > 0 => l"+"
                case a if a == 0 => l""
                case a if a < 0 => l"-"
            }
            l"[" + a.map(x => toLS(x._1) + compile(x._2, indent) + compile(x._3, indent)).toLS(l", ") + l"]"
        }

        case _ => l"NOT IMPLEMENTED"
    }
}