package org.superscript

import Predef._

object PredefImplicits {
    given given_0[F, G](given f: F): Or[F, G] = Or[F, G](Some(f), None)

    given given_1[H, J](given j: J): Or[H, J] = Or[H, J](None, Some(j))

    given given_2[F, G]: Conversion[F || G, G || F] {
        def apply(x: F || G) = Or(x.second, x.first)
    }


    given given_3[F, G](given f: F, g: G): And[F, G] = And[F, G](f, g)

    given given_4[F, G]: Conversion[F && G, G && F] {
        def apply(x: F && G) = And(x.second, x.first)
    }

    given given_5[F, A, B](given or: TupledFunction[F, A => B] || ((F =%= (() => B)) && A =%= Unit)): IsFunction[F, A, B] = new IsFunction[F, A, B] {
        def toUnary(f: F): A => B = or match {
            case Or(Some(a), None) => a.tupled(f)
            case Or(None, Some(a)) => x => a.first.first(f)()
        }
        def fromUnary(f: A => B): F = or match {
            case Or(Some(a), None) => a.untupled(f)
            case Or(None, Some(a)) => a.first.second(() => f(a.second.second(())))
        }
    }
}

object Implicits {
    given given_6[F, G <: Tuple => _](given tupled: TupledFunction[F, G]): Conversion[F, G] {
        def apply(f: F): G = tupled.tupled(f)
    }

    given given_7[F, G <: Tuple => _](given tupled: TupledFunction[F, G]): Conversion[G, F] {
        def apply(g: G): F = tupled.untupled(g)
    }
}