package org.superscript.test

import scala.language.implicitConversions

import scala.io.AnsiColor._

import scala.annotation.implicitNotFound
object Test {
    private type State = List[String]

    private val indent = "    "
    private val miniIndent = "  "

    implicit val defaultState: State = Nil

    private case class TestingError(text: String) extends Throwable

    def tag(name: String)(code: (given State) => Unit)(given state: State): Unit = {
        println(indent * state.size + "- " + name)
        code (given name :: state)
    }

    def test(code: (given State) => Unit)(given state: State): Unit = {
        print(indent * state.size + "* ")
        try {
            code
            println(s"${GREEN}Success${RESET}")
        } catch {
            case x: TestingError => {
                println(s"${RED}Failure:${indentify(x.text, indent * (state.size + 1))}$RESET")
            }
        }
    }

    def assert(computation: Assertion, text: String): Assertion = {
        if(!computation.value) throw TestingError(s"Assertion failed:${indentify(s"$text: ${computation}", miniIndent)}")
        computation
    }

    def assert(computation: Assertion): Assertion = {
        if(!computation.value) throw TestingError(s"Assertion failed:${indentify(computation.toString, miniIndent)}")
        computation
    }

    def timed[T](time: Long, text: String)(code: (given State) => T)(given state: State): T = {
        val start = System.currentTimeMillis()
        val comp = code
        val end = System.currentTimeMillis()
        val actual = end - start
        if(actual > time) throw TestingError(s"Out of time: ${text.split("\n").mkString(s"\n$miniIndent")}\n${miniIndent}Expected: $time\n${miniIndent}Actual: $actual")
        comp
    }

    def timed[T](time: Long)(code: (given State) => T)(given state: State): T = {
        val start = System.currentTimeMillis()
        val comp = code
        val end = System.currentTimeMillis()
        val actual = end - start
        if(actual > time) throw TestingError(s"Out of time:\n${miniIndent}Expected: $time\n${miniIndent}Actual: $actual")
        comp
    }

    def throws[T <: Throwable](text: String)(code: (given State) => T)(given state: State): Unit = {
        var thrown = false
        try {
            code
        } catch {
            case _: T => thrown = true
        }
        if(!thrown)
            throw TestingError(s"Fails to throw: ${text.split("\n").mkString(s"\n$miniIndent")}")
    }

    def not(text: String)(code: (given State) => Unit)(given state: State): Unit = {
        var thrown = false
        try {
            code
        } catch {
            case _: TestingError => thrown = true
        }
        if(!thrown)
            throw TestingError(s"Inner test succeeds: ${text.split("\n").mkString(s"\n$miniIndent")}")

    }

    private def indentify(text: String, indent: String) = "\n" + indent + text.split("\n").mkString(s"\n$indent")

    case class Assertion(value: Boolean, reason: String) {
        override def toString: String = reason

        def unary_! : Assertion = Assertion(!value, s"!$reason")
    }

    implicit def booleanToAssertion(value: Boolean): Assertion = Assertion(value, "Incorrect boolean value")

    implicit def assertionToBoolean(value: Assertion): Boolean = value.value

    @implicitNotFound(msg = "Types ${T} and ${S} do not satisfy the strictness object imported for === or =!=, missing a Strictness[${T}, ${S}].")
    class Strictness[T, S]

    trait Equality[T, S] {
        def isEqual(t: T, s: S): Boolean
    }

    type >:>[+A, -B] = <:<[B, A]

    def (t: T) === [T, S](s: S)(given strictness: Strictness[T, S]): (given Equality[T, S]) => Assertion = (given equality: Equality[T, S]) => Assertion(equality.isEqual(t, s), s"$t =!= $s")

    def (t: T) =!= [T, S](s: S)(given strictness: Strictness[T, S]): (given Equality[T, S]) => Assertion = (given equality: Equality[T, S]) => Assertion(!equality.isEqual(t, s), s"$t === $s")


    object DefaultEqualities {
        implicit def defaultEquality[T, S]: Equality[T, S] = (t, s) => t == s
    }

    object TypeSafe {
        implicit def strictness[T]: Strictness[T, T] = Strictness[T, T]
    }

    object TypeUnsafe {
        implicit def strictness[T, S]: Strictness[T, S] = Strictness[T, S]
    }

    object RangeEquality {
        implicit def range[T]: Equality[T, Spread[T]] = (t, s) => s.contains(t)

        case class Spread[T](start: T, stop: T)(given ev: Numeric[T]) {
            def contains(other: T): Boolean = ev.lteq(start, other) && ev.gteq(stop, other)
        }

        def (x: T) +- [T](y: T)(given ev: Numeric[T]) = Spread(ev.minus(x, y), ev.plus(x, y))(given ev)
    }

    def main(args: Array[String]) = {
        tag("The testing framework:") {
            tag("`timed` works:") {
                test {
                    timed(200, "Works fast") {
                        Thread.sleep(100)
                    }
                }
                test {
                    not("Too fast") {
                        timed(100, "Works fast") {
                            Thread.sleep(200)
                        }
                    }
                }
            }
            tag("`assert` works:") {
                test {
                    not("1 === 2") {
                        assert(1 == 2, "Impossible")
                    }
                }
                test {
                    assert(1 == 1, "Always true")
                }
            }
        }

        tag("Equality(`===`):") {
            import DefaultEqualities._
            tag("Can compare two objects, behaving as normal double-equals(`==`):") {
                test {
                    import TypeUnsafe._
                    assert(1 === 1, "Should always be true")
                }
            }
            tag("Can check whether a number is within a range:") {
                test {
                    import TypeUnsafe._
                    import RangeEquality._
                    assert(2 === 3 +- 2, "2 is between 1 and 5")
                    assert(2.0 === 3.0 +- 2.0, "2.0 is between 1.0 and 5.0")
                    assert(2.0 =!= 3.0 +- 0.5, "2.0 is not between 2.5 and 3.5")
                }
            }
        }
    }
}