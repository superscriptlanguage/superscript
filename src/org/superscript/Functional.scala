package org.superscript

import Predef._

trait Lifter[F[_], G[_ <: Tuple]] {
    type Lifted[X <: Tuple] <: Tuple = X match {
        case a *: y => F[a] *: Lifted[y]
    }

    def lift[X <: Tuple](x: F[G[X]]): G[Lifted[X]]
}

trait EnvironmentLifter[F[_], G[_ <: Tuple], H[_]] {
    type Lifted[X <: Tuple] <: Tuple = X match {
        case a *: y => F[a] *: Lifted[y]
    }

    def lift[X <: Tuple](x: F[G[X]])(given a: H[X]): G[Lifted[X]]
}

type TupleFunction[A <: Tuple] = A match {
    case (x, y) => x => y
}

trait Functor[F[_]] {
    def map[A, B](x: F[A])(f: A => B): F[B]
    def lift[A, B](f: A => B): F[A] => F[B] = x => map(x)(f)
}

trait Apply[F[_]] extends Functor[F] with EnvironmentLifter[F, TupleFunction, [X] =>> X =:= (_, _)] {
    def apply[A, B](f: F[A => B])(x: F[A]): F[B]
    def lift[X <: Tuple, A, B](x: F[TupleFunction[X]]): F[A] => F[B] = x match {
        case a: F[A => B] => b => apply(a)(b)
    }
    def lift[X <: Tuple, A, B](f: F[TupleFunction[X]])(given ev: (F[TupleFunction[X]] =:= F[A => B]) && ((F[A] => F[B]) =:= TupleFunction[Lifted[X]])): TupleFunction[Lifted[X]] =
        ev.second(x => apply[A, B](ev.first(f))(x))
}

trait Pure[F[_]] {
    def pure[A](a: A): F[A]
}

trait Applicative[F[_]] extends Apply[F] with Pure[F]

trait FlatMap[F[_]] {
    def flatMap[A, B](x: F[A])(f: A => F[B]): F[B]
    def flatLift[A, B](f: A => F[B]): F[A] => F[B] = x => flatMap(x)(f)
}

trait Monad[F[_]] extends FlatMap[F] with Applicative[F] {
    def flatten[A](x: F[F[A]]): F[A] = flatMap(x)(a => a)
}

// def filterM[A](x: List[A])(f: A => F[Boolean]): F[List[A]]
// def (x: F[A]) filter (a: A => Boolean): F[A]
// def filter(x: F[A], mask: F[Boolean]): F[A]
// def filter(x: F[A])(a: A => Boolean): F[A] = filter(x, map(x)(a))
//
//def filter[A](x: List[A])(f: A => Boolean): List[A] = x match {
//    case a :: b => if(f(a)) a :: filter(b)(f) else filter(b)(f)
//    case Nil => Nil
//}
//
//def filter[A](x: List[A], mask: List[Boolean]): List[A] = x match {
//    case a :: b => mask match {
//        case c :: d => if(c) a :: filter(b, d) else filter(b, d)
//    }
//}
//
//def filter[A](x: List[A])(f: A => Boolean): List[A] = x.flatMap(a => if(f(a)) a :: Nil else Nil)
//
//def filterM[A](x: List[A])(f: A => F[Boolean]): F[List[A]] = flatMap[Boolean, List[A]](f(???))(???)
//
//def interiorFlatten[A](x: F[List[List[A]]]): F[List[A]] = x.map(a => a.flatMap(b => b))