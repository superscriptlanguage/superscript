package org.superscript

import scala.language.implicitConversions

final class Lazy[+A] private (x: => A) {
    lazy val eval = x
    def get: A = eval

    override def toString: String = get.toString
}
object Lazy {
    def apply[A](x: => A): Lazy[A] = new Lazy(x)

    implicit def convert[A](x: => A): Lazy[A] = new Lazy(x)
}