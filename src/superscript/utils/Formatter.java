/**
 * TODO: Make custom printing.
 */

package superscript.utils;

import java.util.ArrayList;
import java.util.TreeSet;
import java.util.NavigableSet;
import org.fusesource.jansi.AnsiConsole;

public class Formatter {
    private String error;
    private String[] lines;
    private NavigableSet<Msg> messages;
    private Colors colors;

    private static enum Msgtype {
        ERROR, INFO;
    }

    private static class Msg {
        public final Msgtype type;
        public final int line;
        public final int cpil;
        public final int start;
        public final int end;
        public final String msg;

        public Msg(Msgtype type, String msg, Formatter.Token token) {
            this.type = type;
            this.msg = msg;
            cpil = token.getCharPositionInLine();
            start = token.getStartIndex();
            end = token.getStopIndex() + 1;
            line = token.getLine() - 1;
        }
    }

    public static enum ColorType {
        NONE, NORMAL, BRIGHT;
    }

    public static class Colors {
        public String RESET;
        public String ERROR;
        public String INFO;
        public String NOTE;

        public Colors(ColorType type) {
            switch (type) {
            case NONE:
                RESET = "";
                ERROR = "";
                INFO = "";
                NOTE = "";
            case NORMAL:
                RESET = "\u001B[0m";
                ERROR = "\u001B[31m";
                INFO = "\u001B[36m";
                NOTE = "\u001B[32m";
            case BRIGHT:
                RESET = "\u001B[0m";
                ERROR = "\u001B[31;1m";
                INFO = "\u001B[36;1m";
                NOTE = "\u001B[32;1m";
            }
        }

        public Colors(String reset, String error, String info, String note) {
            RESET = reset;
            ERROR = error;
            INFO = info;
            NOTE = note;
        }
    }

    public static class Token {
        private final int cpil;
        private final int line;
        private final int start;
        private final int end;

        public Token(int cpil, int line, int start, int end) {
            this.cpil = cpil;
            this.line = line;
            this.start = start;
            this.end = end;
        }

        public Token(org.antlr.v4.runtime.Token token) {
            this.cpil = token.getCharPositionInLine();
            this.start = token.getStartIndex();
            this.end = token.getStopIndex();
            this.line = token.getLine();
        }

        public int getCharPositionInLine() {
            return cpil;
        }

        public int getLine() {
            return line;
        }

        public int getStopIndex() {
            return end;
        }

        public int getStartIndex() {
            return start;
        }

    }

    public Formatter() {
        AnsiConsole.systemInstall();
        error = "";
        messages = createSortedSet();
        colors = new Colors(ColorType.BRIGHT);
    }

    public Formatter(String[] lines) {
        AnsiConsole.systemInstall();
        error = "";
        this.lines = lines;
        messages = createSortedSet();
        colors = new Colors(ColorType.BRIGHT);
    }

    public Formatter(String error, String[] lines) {
        AnsiConsole.systemInstall();
        this.error = error;
        this.lines = lines;
        messages = createSortedSet();
        colors = new Colors(ColorType.BRIGHT);
    }

    public Formatter(Colors c) {
        AnsiConsole.systemInstall();
        error = "";
        messages = createSortedSet();
        colors = c;
    }

    public Formatter(String[] lines, Colors c) {
        AnsiConsole.systemInstall();
        error = "";
        this.lines = lines;
        messages = createSortedSet();
        colors = c;
    }

    public Formatter(String error, String[] lines, Colors c) {
        AnsiConsole.systemInstall();
        this.error = error;
        this.lines = lines;
        messages = createSortedSet();
        colors = c;
    }

    private static NavigableSet<Msg> createSortedSet() {
        return new TreeSet<Msg>((Msg x, Msg y) -> {
            if (x.line != y.line) {
                return x.line - y.line;
            }
            return x.cpil - y.cpil;
        });
    }

    public Formatter format(String where, int window, String[] notes, String basicIndent, int headers, int distance) {
        return format(where, window, notes, basicIndent, headers, distance, true);
    }

    public Formatter format(String where, int window, String[] notes, String basicIndent, int headers, int distance,
            boolean shouldLastMsgInline) {
        if (window < 0)
            throw new Error("Window is less than zero: " + window);
        boolean[] validlines = new boolean[lines.length];
        ArrayList<Integer> msglines = new ArrayList<>();
        int lastline = -1;
        {
            for (int i = 0; i < lines.length; i++) {
                validlines[i] = false;
            }
            int oldline = -1;
            for (Msg msg : messages) {
                int line = msg.line;
                for (int i = Math.max(line - window, 0); i <= Math.min(line + window, lines.length - 1); i++) {
                    validlines[i] = true;
                    lastline = i;
                }
                if (oldline != -1) {
                    if (line - oldline == window * 2 + 1) {
                        validlines[line - window - 1] = true;
                    }
                }
                oldline = line;
                if (msglines.size() > 0) {
                    if (msglines.get(msglines.size() - 1) != line) {
                        msglines.add(line);
                    }
                } else {
                    msglines.add(line);
                }
            }
        }
        int indent = ("" + lastline).toString().length() + 1;
        String defaultIndent = fill(indent, " ");

        String firstline = basicIndent + defaultIndent.substring(0, indent - 1) + colors.INFO + "--> " + colors.RESET;
        String linestart = colors.INFO + "| " + colors.RESET;
        String noteline = defaultIndent + colors.NOTE + "= " + colors.RESET;
        String nl = "\n" + basicIndent;

        error += firstline + where + nl;
        error += defaultIndent + linestart + nl;
        {
            boolean isrepeated = true;
            Msg lastmsg = messages.first();
            for (int i = 0; i < validlines.length; i++) {
                if (validlines[i]) {
                    isrepeated = false;
                    error += colors.INFO + calc(linestart, i, indent) + colors.RESET + lines[i] + nl;
                    if (msglines.contains(i)) {
                        error += defaultIndent + linestart;
                        NavigableSet<Msg> linemsgs = createSortedSet();
                        int currentchar = 0;
                        for (Msg msg : messages.tailSet(lastmsg)) {
                            if (msg.line != i) {
                                lastmsg = msg;
                                break;
                            } else {
                                linemsgs.add(msg);
                                error += fill(msg.cpil - currentchar, " ");
                                String fill;
                                if (msg.type == Msgtype.INFO) {
                                    fill = "-";
                                } else {
                                    fill = "^";
                                }
                                error += getColor(msg);
                                error += fill(msg.end - msg.start, fill);
                                error += colors.RESET;
                                currentchar = msg.end - msg.start + msg.cpil;
                            }
                        }
                        if (shouldLastMsgInline) {
                            Msg last = linemsgs.last();
                            String msg = last.msg;
                            if (msg.split("\n").length == 1) {
                                error += ' ' + getColor(last) + msg + colors.RESET;
                                linemsgs.remove(last);
                            }
                        }
                        error += fill(headers, nl + defaultIndent + linestart + pipes(linemsgs));
                        boolean notfirst = false;
                        for (Msg msg : linemsgs.descendingSet()) {
                            NavigableSet<Msg> msgs = linemsgs.headSet(msg, true);
                            String[] lines = msg.msg.split("\n");
                            if (notfirst)
                                error += fill(distance, nl + defaultIndent + linestart + pipes(msgs));
                            notfirst = true;
                            for (int j = 0; j < lines.length - 1; j++) {
                                error += nl + defaultIndent + linestart + pipes(msgs) + " " + getColor(msg) + lines[j]
                                        + colors.RESET;
                            }
                            error += nl + defaultIndent + linestart + endpipes(msgs) + " " + getColor(msg)
                                    + lines[lines.length - 1] + colors.RESET;
                        }
                        error += nl;
                    }
                } else {
                    if (!isrepeated) {
                        if (!(msglines.get(msglines.size() - 1) < i)) {
                            isrepeated = true;
                            error += fill(indent, ".") + nl;
                        }
                    }
                }
            }
        }
        error += defaultIndent + linestart;
        for (int i = 0; i < notes.length; i++) {
            String[] notelines = notes[i].split("\n");
            error += nl + noteline + notelines[0];
            for (int j = 1; j < notelines.length; j++) {
                error += nl + defaultIndent + "  " + notelines[j];
            }
        }
        return this;
    }

    private String pipes(NavigableSet<Msg> linemsgs) {
        String result = "";
        int tcpil = 0;
        for (Msg msg : linemsgs) {
            int dist = msg.cpil - tcpil;
            result += fill(dist, " ") + getColor(msg) + "|" + colors.RESET;
            tcpil = msg.cpil + 1;
        }
        return result;
    }

    private String endpipes(NavigableSet<Msg> linemsgs) {
        String result = "";
        int tcpil = 0;
        for (Msg msg : linemsgs) {
            int dist = msg.cpil - tcpil;
            result += fill(dist, " ") + getColor(msg) + ((msg == linemsgs.last()) ? '\\' : '|') + colors.RESET;
            tcpil = msg.cpil + 1;
        }
        return result;
    }

    public Formatter clearError() {
        error = "";
        return this;
    }

    public String giveError() {
        String olderr = error;
        error = "";
        return olderr;
    }

    public String giveError(String error) {
        String olderr = this.error;
        this.error = error;
        return olderr;
    }

    public Formatter setLines(String[] lines) {
        this.lines = lines;
        return this;
    }

    public Formatter info(String msg, Token infoToken) {
        messages.add(new Msg(Msgtype.INFO, msg, infoToken));
        return this;
    }

    public Formatter error(String msg, Token offendingToken) {
        messages.add(new Msg(Msgtype.ERROR, msg, offendingToken));
        return this;
    }

    public Formatter clear() {
        messages.clear();
        return this;
    }

    public Formatter uninstall() {
        AnsiConsole.systemUninstall();
        return this;
    }

    private static String fill(int howmany, String val) {
        String result = "";
        for (int i = 0; i < howmany; i++) {
            result += val;
        }
        return result;
    }

    private static String calc(String linestart, int i, int indent) {
        String result = "" + (i + 1);
        for (int val = result.length(); val < indent; val++) {
            result += ' ';
        }
        return result + linestart;
    }

    private String getColor(Msg msg) {
        return msg.type == Msgtype.INFO ? colors.INFO : colors.ERROR;
    }
}