package superscript.utils;

import java.util.HashSet;
import java.util.Set;

public class ErrorHandler {
    public String[] notes;
    public int type;
    public Set<TokPair> infos;
    public Formatter.Token offendingToken;
    public String headers;

    public ErrorHandler(Set<TokPair> infos) {
        notes = null;
        type = -1;
        this.infos = infos;
        offendingToken = null;
        headers = null;
    }

    public ErrorHandler() {
        this(new HashSet<>());
    }

    public static ErrorHandler empty() {
        ErrorHandler error = new ErrorHandler();
        error.notes = new String[0];
        error.type = 0;
        error.offendingToken = new Formatter.Token(0, 0, 0, 0);
        error.headers = "";
        return error;
    }

    public void clear() {
        notes = null;
        type = -1;
        infos.clear();
        offendingToken = null;
        headers = null;
    }

    public boolean has() {
        return !(notes == null || type == -1 || offendingToken == null || headers == null);
    }

    public static class TokPair {
        public String msg;
        public Formatter.Token token;

        TokPair(String msg, Formatter.Token token) {
            this.msg = msg;
            this.token = token;
        }
    }
}