package superscript.interperter;

import java.util.Arrays;
import java.util.Map;

class ArrayValue extends Value {
    private Type type;
    private Value[] values;

    @Override
    public Type getType() {
        return new ArrayType(type);
    }

    @Override
    public Value cast(Type t) {
        if (t instanceof ArrayType) {
            Type type = ((ArrayType) t).getType();
            Value[] newValues = new Value[values.length];
            for (int i = 0; i < values.length; i++) {
                newValues[i] = values[i].cast(type);
            }
            return new ArrayValue(type, newValues);
        }
        throw new InterperterException("Can not cast type of array.");
    }

    // TODO: Implement
    @Override
    public Value infix(String type, Value other) {
        throw new UnsupportedOperationException("Not implemented.");
    }

    // TODO: Implement
    @Override
    public Value postfix(String type) {
        throw new UnsupportedOperationException("Not implemented.");
    }

    // TODO: Implement
    @Override
    public Value prefix(String type) {
        throw new UnsupportedOperationException("Not implemented.");
    }

    /*
     * @Override public Property getProperty(String name) { try { return new
     * Property(true, type, values[Integer.parseInt(name)]); } catch (Exception e) {
     * throw new InterperterException(e); } }
     */

    public Type getArrayValueType() {
        return type;
    }

    public Value getAt(int index) {
        return values[index];
    }

    public int length() {
        return values.length;
    }

    public ArrayValue setAt(int index, Value value) {
        if (type.isCompatiableWith(value)) {
            values[index] = value;
        } else {
            throw new InterperterException("Invalid type for value.");
        }
        return this;
    }

    ArrayValue(Type type, Value[] startingValues) {
        for (int i = 0; i < startingValues.length; i++) {
            Value value = startingValues[i];
            if (value != null)
                if (!type.isCompatiableWith(value))
                    throw new InterperterException("Invalid types.");
        }
        values = startingValues;
        this.type = type;
    }

    @Override
    public Value call(Value[] vals) {
        throw new InterperterException("Can not call an array as a function.");
    }

    @Override
    public String toString() {
        return type + Arrays.toString(values);
    }

    @Override
    public Value call(Map<String, Value> args) {
        throw new InterperterException("Can not call an array as a function.");
    }
}