package superscript.interperter;

class TypeValue extends Item {

    private Type type;
    private String name;

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    TypeValue(Type type, String name) {
        this.type = type;
        this.name = name;
    }

}