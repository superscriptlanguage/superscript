package superscript.interperter;

class Id extends Item {
    private boolean ismut;
    private TypeValue value;

    public void evaluate(Value expression, Scope<Property> current) {
        current.add(value.getName(), new Property(ismut, value.getType(), expression));
    }

    public boolean isMut() {
        return ismut;
    }

    Id(boolean ismut, TypeValue value) {
        this.ismut = ismut;
        this.value = value;
    }
}