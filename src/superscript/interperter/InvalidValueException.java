package superscript.interperter;

class InvalidValueException extends InterperterException {

    private static final long serialVersionUID = 1L;

    InvalidValueException(String message) {
        super(message);
    }

    InvalidValueException() {
        super();
    }
}