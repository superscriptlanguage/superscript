package superscript.interperter;

class InterperterException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    InterperterException() {
        super();
    }

    InterperterException(String message) {
        super(message);
    }

    InterperterException(String message, Throwable throwable) {
        super(message, throwable);
    }

    InterperterException(Throwable throwable) {
        super(throwable);
    }
}