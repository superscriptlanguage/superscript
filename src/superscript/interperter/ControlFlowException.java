package superscript.interperter;

class ControlFlowException extends InterperterException {

    private static final long serialVersionUID = 1L;

    private Value value;

    public Value getValue() {
        return value;
    }

    ControlFlowException(Value value) {
        this.value = value;
    }
}