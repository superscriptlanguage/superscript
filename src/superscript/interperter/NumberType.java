package superscript.interperter;

class NumberType extends Type {

    @Override
    public boolean isCompatiableWith(Type othertype) {
        return othertype instanceof NumberType || othertype instanceof BooleanType;
    }

    @Override
    public String toString() {
        return "number";
    }
}