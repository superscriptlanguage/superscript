package superscript.interperter;

class ContinueException extends ControlFlowException {

    private static final long serialVersionUID = 1L;

    ContinueException(Value value) {
        super(value);
    }
}