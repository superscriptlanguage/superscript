package superscript.interperter;

import java.util.Map;

class ObjectValue extends Value {

    private Type type;

    private Map<String, Property> data;

    private Value[] supers;

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public String toString() {
        Value ts = data.get("toString").get();
        if (ts != null) {
            return ts.call(new Value[0]).toString();
        } else {
            return "[object " + type + "]";
        }
    }

    // TODO: Get this working.
    @Override
    public Value infix(String type, Value other) {
        throw new UnsupportedOperationException("Infix operators are not supported.");
    }

    // TODO: Get this working.
    @Override
    public Value postfix(String type) {
        throw new UnsupportedOperationException("Postfix operators are not supported.");
    }

    // TODO: Get this working.
    @Override
    public Value prefix(String type) {
        throw new UnsupportedOperationException("Prefix operators are not supported.");
    }

    @Override
    public Value call(Value[] vals) {
        for (int i = 0; i < supers.length; i++) {
            if (supers[i].canCall()) {
                return supers[i].call(vals);
            }
        }
        throw new InterperterException("Can not call object as function.");
    }

    @Override
    public Value cast(Type t) {
        if (t instanceof ObjectType) {
            return this;
        }
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public Value call(Map<String, Value> args) {
        for (int i = 0; i < supers.length; i++) {
            if (supers[i].canCall()) {
                return supers[i].call(args);
            }
        }
        throw new InterperterException("Can not call object as function.");
    }

    @Override
    public boolean canCall() {
        for (int i = 0; i < supers.length; i++) {
            if (supers[i].canCall()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Property getProperty(Value name) {
        if (data.containsKey(name.toString())) {
            return data.get(name.toString());
        } else {
            for (int i = 0; i < supers.length; i++) {
                Property property = supers[i].getProperty(name);
                if (property != null) {
                    return property;
                }
            }
        }
        return null;
    }
}