package superscript.interperter;

import java.util.Map;

class NumberValue extends Value {
    double number;

    @Override
    public Type getType() {
        return new NumberType();
    }

    @Override
    public String toString() {
        return "" + number;
    }

    NumberValue(String string) {
        number = Double.parseDouble(string);
    }

    NumberValue(double num) {
        number = num;
    }

    // TODO: Get this working.
    @Override
    public Value infix(String type, Value value) {
        if (value instanceof NumberValue) {
            NumberValue otherNumber = (NumberValue) value;
            double number = otherNumber.number;
            switch (type) {
            case "+":
                return new NumberValue(this.number + number);
            case "-":
                return new NumberValue(this.number - number);
            case "*":
                return new NumberValue(this.number * number);
            case "/":
                if (number == 0)
                    return new NanValue();
                return new NumberValue(this.number / number);
            case "**":
                double val = Math.pow(this.number, number);
                if (val != val)
                    return new NanValue();
                return new NumberValue(val);
            case "<":
                return new BooleanValue(this.number < number);
            case ">":
                return new BooleanValue(this.number > number);
            case "<=":
                return new BooleanValue(this.number <= number);
            case ">=":
                return new BooleanValue(this.number >= number);
            case "==":
            case "===":
                return new BooleanValue(this.number == number);
            default:
                throw new UnsupportedOperationException("Unsupported infix operator.");
            }
        } else if (value instanceof NanValue) {
            return value.infix(type, this);
        } else {
            throw new InterperterException("Can not use an operator on a number and a object of an other type.");
        }
    }

    // TODO: Get this working.
    @Override
    public Value postfix(String type) {
        switch (type) {
        case "--":
            return new NumberValue(this.number--);
        case "++":
            return new NumberValue(this.number++);
        default:
            throw new UnsupportedOperationException("Unsupported infix operator.");
        }
    }

    // TODO: Get this working.
    @Override
    public Value prefix(String type) {
        switch (type) {
        case "--":
            return new NumberValue(--this.number);
        case "++":
            return new NumberValue(++this.number);
        case "+":
            return new NumberValue(this.number);
        case "-":
            return new NumberValue(-this.number);
        default:
            throw new UnsupportedOperationException("Unsupported infix operator.");
        }
    }

    @Override
    public Value call(Value[] vals) {
        throw new InterperterException("Can not call a number as a function.");
    }

    @Override
    public Value cast(Type t) {
        if (t instanceof BooleanType) {
            return new BooleanValue(number != 0);
        } else if (t instanceof NumberType) {
            return this;
        }
        throw new InterperterException("Can not cast number to other types than boolean.");
    }

    @Override
    public Value call(Map<String, Value> args) {
        throw new InterperterException("Can not call a number as a function.");
    }
}