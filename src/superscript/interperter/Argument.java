package superscript.interperter;

import java.util.ArrayList;

class Argument extends Item {
    private Value value;
    private boolean toExpand;

    public void call(ArrayList<Value> values) {
        if (toExpand) {
            ArrayValue array = (ArrayValue) value;
            for (int i = 0; i < array.length(); i++) {
                values.add(array.getAt(i));
            }
        } else {
            values.add(value);
        }
    }

    Argument(Value val, boolean expand) {
        value = val;
        toExpand = expand;
    }
}