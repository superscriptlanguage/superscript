package superscript.interperter;

import java.util.Map;

class StringValue extends Value {

    private String string;

    public StringValue(String string) {
        this.string = string;
    }

    @Override
    public Type getType() {
        return new StringType();
    }

    @Override
    public Value cast(Type type) {
        if (type instanceof StringType) {
            return this;
        }
        throw new InterperterException("Can not cast String Value.");
    }

    @Override
    public Value infix(String type, Value value) {
        switch (type) {
        case "+":
            return new StringValue(this.string + value);
        default:
            throw new InterperterException("Invalid operator for string.");
        }
    }

    @Override
    public Value postfix(String type) {
        throw new InterperterException("Can not use postfixop on String Value.");
    }

    @Override
    public Value prefix(String type) {
        throw new InterperterException("Can not use prefixop on String Value.");
    }

    @Override
    public Value call(Value[] args) {
        throw new InterperterException("Can not call string as function.");
    }

    @Override
    public Value call(Map<String, Value> args) {
        throw new InterperterException("Can not call string as function.");
    }

    @Override
    public String toString() {
        return string;
    }
}