package superscript.interperter;

class BooleanType extends Type {

    @Override
    public boolean isCompatiableWith(Type othertype) {
        return othertype instanceof BooleanType;
    }

    @Override
    public String toString() {
        return "boolean";
    }

}