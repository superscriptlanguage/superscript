package superscript.interperter;

abstract class Matchable extends Item {
    public abstract boolean match(Value value, Scope<Property> current);
}