package superscript.interperter;

class ReturnException extends ControlFlowException {

    private static final long serialVersionUID = 1L;

    ReturnException(Value value) {
        super(value);
    }
}