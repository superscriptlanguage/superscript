package superscript.interperter;

import java.util.function.Supplier;

class Matcher extends Item {
    private Matchable matcher;
    private Supplier<Item> evaluator;

    public Item match(Value expression, Scope<Property> currentScope) {
        return matcher.match(expression, currentScope) ? evaluator.get() : null;
    }

    Matcher(Matchable match, Supplier<Item> evaluator) {
        this.matcher = match;
        this.evaluator = evaluator;
    }
}