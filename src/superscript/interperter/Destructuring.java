package superscript.interperter;

abstract class Destructuring extends Matchable {
    public abstract void evaluate(Value expression, Scope<Property> current);

    public abstract String[] getNecessaryNames();

    public void evaluateConst(Value expression, Scope<Property> current) {
        for (String s : getNecessaryNames()) {
            current.add(s, new Property(false, null, null));
        }
        evaluate(expression, current);
    }

    public boolean match(Value value, Scope<Property> current) {
        try {
            evaluateConst(value, current);
            return true;
        } catch (InvalidValueException e) {
            return false;
        }
    }
}