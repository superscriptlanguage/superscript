package superscript.interperter;

import superscript.parser.*;
import java.io.IOException;
import java.util.Arrays;

import org.antlr.v4.runtime.CommonTokenFactory;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.UnbufferedCharStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

class Visitor {

    private static String lastParse = null;

    public static void main(String[] args) throws IOException {
        System.out.print("> ");
        ParseTreeWalker walker = new ParseTreeWalker();
        UnbufferedCharStream charstream = new UnbufferedCharStream(System.in);
        SuperScriptLexer lexer = new SuperScriptLexer(charstream);
        lexer.setTokenFactory(new CommonTokenFactory(true));
        CommonTokenStream stream = new CommonTokenStream(lexer);
        SuperScriptParser parser = new SuperScriptParser(stream);
        SuperScriptParserEvaluatorVisitor visitor = new SuperScriptParserEvaluatorVisitor();
        SuperScriptGrammarChecker checker = new SuperScriptGrammarChecker(parser);

        parser.removeErrorListeners();
        visitor.addFunction("log", (vals, that) -> {
            System.out.println(Arrays.toString(vals));
            return that;
        }, () -> "[logger name: 'log']");

        visitor.addFunction("parse", (vals, self) -> {
            System.out.println(lastParse);
            return self;
        }, () -> "[parser name: 'parse']");

        while (true) {
            ParseTree tree = parser.exprsemi();

            lastParse = tree.toStringTree(parser);

            try {
                walker.walk(checker, tree);
                System.out.println("--> " + visitor.visit(tree));
            } catch (ReturnException | ContinueException | BreakException | ScopeException e) {
                // End parsing.
            } catch (InterperterException e) {
                System.out.println("Error: " + e.getMessage());
            }

            System.out.print("> ");
        }
    }
}