package superscript.interperter;

class BreakException extends ControlFlowException {

    private static final long serialVersionUID = 1L;

    BreakException(Value value) {
        super(value);
    }
}