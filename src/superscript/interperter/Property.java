package superscript.interperter;

class Property extends Item {
    private Type type;
    private Value inner;
    private boolean ismut;

    public Value get() {
        return inner;
    }

    public Property set(Value v) {
        if (!ismut && inner != null) {
            throw new InterperterException("Attempting to modify Value " + inner + " while it is constant.");
        }
        inner = type == null ? v : v.cast(type);
        return this;
    }

    @Override
    public String toString() {
        return inner.toString();
    }

    public Type getType() {
        return type;
    }

    public Property setType(Type type) {
        this.type = type;
        return this;
    }

    Property(boolean ismut, Type type, Value inner) {
        this.ismut = ismut;
        this.type = type;
        set(inner);
    }
}