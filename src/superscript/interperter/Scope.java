package superscript.interperter;

import java.util.Map;
import java.util.function.UnaryOperator;

class Scope<T> {
    private Map<String, T> props;
    private Scope<T> outer;
    private Scope<T> returnScope;

    private Value thisValue;
    private Value superValue;
    private Value self;

    public Value getSelf() {
        return self;
    }

    public Value getThis() {
        return thisValue;
    }

    public Value getSuper() {
        return superValue;
    }

    public T get(String name) {
        return get(name, this);
    }

    private T get(String name, Scope<T> current) {
        if (current == null)
            return null;
        return current.props.containsKey(name) ? current.props.get(name) : get(name, current.outer);
    }

    public Scope<T> returnScope() {
        if (returnScope == null)
            throw new ScopeException("Invalid return scope.");
        return returnScope;
    }

    public Scope<T> returnScopeNoCheck() {
        return returnScope;
    }

    public Scope<T> outerScope() {
        if (outer == null)
            throw new ScopeException("Invalid outer scope.");
        return outer;
    }

    public Scope<T> outerScopeNoCheck() {
        return outer;
    }

    public Scope<T> add(String name, T value) {
        props.put(name, value);
        return this;
    }

    public Scope<T> modify(String name, UnaryOperator<T> op) {
        modify(name, op, this);
        return this;
    }

    public Scope<T> put(String name, T value) {
        modify(name, (v) -> value, this);
        return this;
    }

    private void modify(String name, UnaryOperator<T> op, Scope<T> current) {
        if (current == null)
            throw new InterperterException("Setting a variable without declaring it.");
        if (current.props.containsKey(name)) {
            current.props.compute(name, (k, v) -> op.apply(v));
        } else {
            modify(name, op, current.outer);
        }
    }

    Scope(Map<String, T> props, Scope<T> outer, Scope<T> returnScope, Value thisValue, Value superValue, Value self) {
        this.props = props;
        this.outer = outer;
        this.returnScope = returnScope;
        this.thisValue = thisValue;
        this.superValue = superValue;
        this.self = self;
    }
}