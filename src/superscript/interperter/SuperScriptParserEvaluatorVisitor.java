package superscript.interperter;

import superscript.parser.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import org.antlr.v4.runtime.tree.ParseTree;

class SuperScriptParserEvaluatorVisitor extends SuperScriptParserBaseVisitor<Item> {
    Scope<Property> global = scopec(null, null);
    Scope<Property> currentScope = global;

    /* ==================UTILITIES============== */

    public static Scope<Property> scopec(Scope<Property> outer, Scope<Property> ret) {
        return new Scope<>(new HashMap<>(), outer, ret, new VoidValue(), new VoidValue(), new VoidValue());
    }

    /* ================PROPERTY MANAGEMENT================= */

    @Override
    public Property visitRefinement(SuperScriptParser.RefinementContext ctx) {
        return visit(ctx.refine).get().getProperty(
                ctx.refinement == null ? new StringValue(ctx.NAME().getText()) : visit(ctx.refinement).get());
    }

    @Override
    public Item visitAccess(SuperScriptParser.AccessContext ctx) {
        String name = ctx.NAME().getText();
        Property property = currentScope.get(name);
        return property == null ? new MissingValue(name) : property;
    }

    /* ====================DESTRUCTURING=================== */

    @Override
    public ValueDestruct visitName(SuperScriptParser.NameContext ctx) {
        SuperScriptParser.ExpressionContext expression = ctx.expression();
        String text = ctx.NAME().getText();
        return expression == null ? new ValueDestruct(text) : new ValueDestruct(text, visit(expression).get());
    }

    /* =====================TYPES========================= */

    @Override
    public Type visitNameType(SuperScriptParser.NameTypeContext ctx) {
        switch (ctx.url().NAME(0).getText()) {
        case "number":
            return new NumberType();
        case "boolean":
            return new BooleanType();
        case "func":
            return new FunctionValue.DefaultFunctionType();
        case "string":
            return new StringType();
        default:
            throw new UnsupportedOperationException("Types are not fully supported yet.");
        }
    }

    @Override
    public Type visitVoidType(SuperScriptParser.VoidTypeContext ctx) {
        return new VoidType();
    }

    @Override
    public Type visitTypeArray(SuperScriptParser.TypeArrayContext ctx) {
        return new ArrayType((Type) visit(ctx.typedec()));
    }

    @Override
    public Value visitCast(SuperScriptParser.CastContext ctx) {
        return visit(ctx.expression()).get().cast((Type) visit(ctx.typedec()));
    }

    /* ======================OPERATORS==================== */

    @Override
    public Value visitInfix(SuperScriptParser.InfixContext ctx) {
        Value left = visit(ctx.expression(0)).get();
        String text = ctx.children.get(1).getText();
        return infix(left, text, () -> visit(ctx.expression(1)).get());
    }

    @Override
    public Value visitHas(SuperScriptParser.HasContext ctx) {
        return new BooleanValue(currentScope.get(ctx.NAME().getText()) != null);
    }

    @Override
    public Value visitInfixHas(SuperScriptParser.InfixHasContext ctx) {
        return new BooleanValue(
                visit(ctx.expression()).get().getProperty(new StringValue(ctx.NAME().getText())).get() != null);
    }

    @Override
    public Value visitPrefix(SuperScriptParser.PrefixContext ctx) {
        Value val = visit(ctx.expression()).get();
        return val.prefix(ctx.children.get(0).getText());
    }

    @Override
    public Value visitPostfix(SuperScriptParser.PostfixContext ctx) {
        Value val = visit(ctx.expression()).get();
        return val.postfix(ctx.children.get(1).getText());
    }

    /* =================LITERALS=============== */

    @Override
    public Value visitValuesSet(SuperScriptParser.ValuesSetContext ctx) {
        int size = ctx.arrparam().size();
        Value[] vals = new Value[size];
        for (int i = 0; i < size; i++) {
            vals[i] = visit(ctx.arrparam(i)).get();
        }
        return new ArrayValue((Type) visit(ctx.typedec()), vals);
    }

    @Override
    public Value visitArrparam(SuperScriptParser.ArrparamContext ctx) {
        if (ctx.DECLARE() != null) {
            return null;
        }
        return visit(ctx.expression()).get();
    }

    @Override
    public Value visitLengthSet(SuperScriptParser.LengthSetContext ctx) {
        return new ArrayValue((Type) visit(ctx.typedec()),
                new Value[(int) ((NumberValue) (visit(ctx.expression())).get().cast(new NumberType())).number]);
    }

    @Override
    public Value visitNumber(SuperScriptParser.NumberContext ctx) {
        String text = ctx.NUMBER().getText();
        return new NumberValue(text);
    }

    @Override
    public Value visitString(SuperScriptParser.StringContext ctx) {
        return new StringValue(ctx.STRING().getText().replaceAll("^\"|\"$", ""));
    }

    @Override
    public Value visitTrue(SuperScriptParser.TrueContext ctx) {
        return new BooleanValue(true);
    }

    @Override
    public Value visitFalse(SuperScriptParser.FalseContext ctx) {
        return new BooleanValue(false);
    }

    @Override
    public NanValue visitNaN(SuperScriptParser.NaNContext ctx) {
        return new NanValue();
    }

    @Override
    public Value visitThis(SuperScriptParser.ThisContext ctx) {
        return currentScope.getThis();
    }

    @Override
    public Value visitSuper(SuperScriptParser.SuperContext ctx) {
        return currentScope.getSuper();
    }

    @Override
    public Value visitSelf(SuperScriptParser.SelfContext ctx) {
        return currentScope.getSelf();
    }

    @Override
    public Value visitVoid(SuperScriptParser.VoidContext ctx) {
        return new VoidValue();
    }

    /* =========================VAR MODIFICATIONS================= */

    @Override
    public Value visitVarManagement(SuperScriptParser.VarManagementContext ctx) {
        Destructuring destruct = (Destructuring) visit(ctx.var());
        Value expression = visit(ctx.expression()).get();
        destruct.evaluate(expression, currentScope);
        return expression;
    }

    @Override
    public Value visitVarset(SuperScriptParser.VarsetContext ctx) {
        Id id = (Id) visit(ctx.id());
        boolean ismut = id.isMut();
        Value lastValue = visit(ctx.first).get();
        id.evaluate(lastValue, currentScope);
        for (int i = 0; i < ctx.values.size(); i++) {
            new Id(ismut, (TypeValue) visit(ctx.values.get(i))).evaluate(lastValue = visit(ctx.further.get(i)).get(),
                    currentScope);
        }
        return lastValue;
    }

    @Override
    public Property visitVarModify(SuperScriptParser.VarModifyContext ctx) {
        String text = ctx.children.get(1).getText();
        String before = text.substring(0, text.length() - 1);
        Property property = (Property) visit(ctx.expression(0));
        Value value;
        if (property != null) {
            value = property.get();
        } else {
            throw new InterperterException("Can not add to a missing value");
        }
        Value infix = infix(value, before, () -> visit(ctx.expression(1)).get());
        property.set(infix);
        return property;
    }

    /* ========================FUNCTIONS====================== */

    @Override
    public Value visitFunction(SuperScriptParser.FunctionContext ctx) {
        Item[] items = ((ItemArray) visit(ctx.args())).getItems();
        Item[] destructures = ((ItemArray) items[1]).getItems();
        Item[] types = ((ItemArray) items[2]).getItems();
        ParseTree expression = ctx.expression();
        Type cast = null;
        if (expression instanceof SuperScriptParser.CastContext) {
            cast = (Type) visit(((SuperScriptParser.CastContext) expression).typedec());
        }
        return new FunctionValue(expression, (x, y) -> {
            Scope<Property> oldScope = currentScope;
            currentScope = y;
            Value ret = visit(x).get();
            currentScope = oldScope;
            return ret;
        }, (int) ((NumberValue) items[0]).number, currentScope,
                Arrays.copyOf(destructures, destructures.length, Destructuring[].class),
                Arrays.copyOf(types, types.length, Type[].class), cast);
    }

    @Override
    public ItemArray visitArgs(SuperScriptParser.ArgsContext ctx) {
        int destructuringposition = -1;
        List<Type> types = new ArrayList<>();
        List<Destructuring> destructures = new ArrayList<>();
        for (int i = 0; i < ctx.arg().size(); i++) {
            SuperScriptParser.ArgContext arg = ctx.arg(i);
            if (arg.RESTSPREAD() != null) {
                destructuringposition = i;
            }
            types.add((Type) visit(arg.functionname().typedec()));
            destructures.add((Destructuring) visit(arg.var()));
        }
        return new ItemArray(new Item[] { new NumberValue(destructuringposition),
                new ItemArray(destructures.toArray(new Item[0])), new ItemArray(types.toArray(new Item[0])) });
    }

    @Override
    public Argument visitFunctionarg(SuperScriptParser.FunctionargContext ctx) {
        return new Argument(visit(ctx.expression()).get(), ctx.RESTSPREAD() != null);
    }

    @Override
    public Value visitEvaluation(SuperScriptParser.EvaluationContext ctx) {
        ArrayList<Value> values = new ArrayList<>();
        Value function = visit(ctx.expression()).get();
        for (SuperScriptParser.FunctionargContext arg : ctx.functionarg()) {
            ((Argument) visit(arg)).call(values);
        }
        return function.call(values.toArray(new Value[0]));
    }

    /* ===================CONTROL FLOW===================== */

    @Override
    public Item visitReturn(SuperScriptParser.ReturnContext ctx) {
        if (ctx.expression() != null) {
            throw new ReturnException(visit(ctx.expression()).get());
        }
        throw new ReturnException(new VoidValue());
    }

    @Override
    public Item visitBreak(SuperScriptParser.BreakContext ctx) {
        if (ctx.expression() != null) {
            throw new BreakException(visit(ctx.expression()).get());
        }
        throw new BreakException(new VoidValue());
    }

    @Override
    public Item visitContinue(SuperScriptParser.ContinueContext ctx) {
        if (ctx.expression() != null) {
            throw new ContinueException(visit(ctx.expression()).get());
        }
        throw new ContinueException(new VoidValue());
    }

    @Override
    public Item visitBlocks(SuperScriptParser.BlocksContext ctx) {
        currentScope = scopec(currentScope, currentScope.returnScopeNoCheck());
        Item value = visit(ctx.block());
        currentScope = currentScope.outerScope();
        return value;
    }

    @Override
    public Item visitBlock(SuperScriptParser.BlockContext ctx) {
        List<SuperScriptParser.ExpressionContext> exprs = ctx.expression();
        Item lastValue = new VoidValue();
        for (int i = 0; i < exprs.size(); i++) {
            lastValue = visit(exprs.get(i));
        }
        return lastValue;
    }

    @Override
    public Item visitIf(SuperScriptParser.IfContext ctx) {
        if (((BooleanValue) visit(ctx.expression(0)).get().cast(new BooleanType())).value) {
            return visit(ctx.expression(1));
        } else {
            if (ctx.ELSE() != null) {
                return visit(ctx.expression(2));
            }
        }
        return new VoidValue();
    }

    @Override
    public Value visitFor(SuperScriptParser.ForContext ctx) {
        currentScope = scopec(currentScope, currentScope.returnScopeNoCheck());
        if (ctx.first != null) {
            visit(ctx.first);
        }
        Value lastValue = new VoidValue();
        while (ctx.second == null || ((BooleanValue) (visit(ctx.second).get().cast(new BooleanType()))).value) {
            try {
                lastValue = visit(ctx.next).get();
            } catch (BreakException e) {
                lastValue = e.getValue();
                break;
            } catch (ContinueException e) {
                lastValue = e.getValue();
                continue;
            }
            visit(ctx.third);
        }
        currentScope = currentScope.outerScope();
        return lastValue;
    }

    @Override
    public Value visitWhile(SuperScriptParser.WhileContext ctx) {
        currentScope = scopec(currentScope, currentScope.returnScopeNoCheck());

        Value lastValue = new VoidValue();
        while (ctx.expression(0) == null
                || ((BooleanValue) (visit(ctx.expression(0)).get().cast(new BooleanType()))).value) {
            try {
                lastValue = visit(ctx.expression(1)).get();
            } catch (BreakException e) {
                lastValue = e.getValue();
                break;
            } catch (ContinueException e) {
                lastValue = e.getValue();
                continue;
            }
        }
        currentScope = currentScope.outerScope();
        return lastValue;
    }

    @Override
    public Value visitDoWhile(SuperScriptParser.DoWhileContext ctx) {
        currentScope = scopec(currentScope, currentScope.returnScopeNoCheck());

        Value lastValue = new VoidValue();
        do {
            try {
                lastValue = visit(ctx.expression(0)).get();
            } catch (BreakException e) {
                lastValue = e.getValue();
                break;
            } catch (ContinueException e) {
                lastValue = e.getValue();
                continue;
            }
        } while (ctx.expression(1) == null
                || ((BooleanValue) (visit(ctx.expression(1)).get().cast(new BooleanType()))).value);
        currentScope = currentScope.outerScope();
        return lastValue;
    }

    @Override
    public Item visitMatch(SuperScriptParser.MatchContext ctx) {
        Value expression = visit(ctx.expression()).get();
        Item returnValue = new VoidValue();
        currentScope = scopec(currentScope, currentScope.returnScopeNoCheck());
        for (int i = 0; i < ctx.matchresult().size(); i++) {
            Item currentValue = ((Matcher) visit(ctx.matchresult(i))).match(expression, currentScope);
            if (currentValue != null) {
                returnValue = currentValue;
                break;
            }
        }
        currentScope = currentScope.outerScope();
        return returnValue;
    }

    @Override
    public Matcher visitMatchresult(SuperScriptParser.MatchresultContext ctx) {
        return new Matcher((Matchable) visit(ctx.cases()), () -> visit(ctx.expression()));
    }

    @Override
    public Matchable visitCases(SuperScriptParser.CasesContext ctx) {
        return new MultiMatch(ctx.acase().stream().map((v) -> visit(v)).toArray(Matchable[]::new));
    }

    @Override
    public Matchable visitCaseExp(SuperScriptParser.CaseExpContext ctx) {
        return visit(ctx.expression()).get();
    }

    @Override
    public Matchable visitCaseDes(SuperScriptParser.CaseDesContext ctx) {
        return (Destructuring) visit(ctx.var());
    }

    @Override
    public Matchable visitCaseDef(SuperScriptParser.CaseDefContext ctx) {
        return new DefaultMatch();
    }

    /* ========================= OTHER ======================= */

    @Override
    public Value visitFile(SuperScriptParser.FileContext ctx) {
        return visit(ctx.block()).get();
    }

    @Override
    public Item visitParens(SuperScriptParser.ParensContext ctx) {
        return visit(ctx.expression());
    }

    @Override
    public TypeValue visitTypevalue(SuperScriptParser.TypevalueContext ctx) {
        return new TypeValue((Type) visit(ctx.typedec()), ctx.NAME().getText());
    }

    @Override
    public Id visitId(SuperScriptParser.IdContext ctx) {
        return new Id(((BooleanValue) visit(ctx.vardec())).value, (TypeValue) visit(ctx.typevalue()));
    }

    @Override
    public BooleanValue visitVardec(SuperScriptParser.VardecContext ctx) {
        if (ctx.CONST() != null) {
            return new BooleanValue(false);
        } else {
            return new BooleanValue(true);
        }
    }

    /* =================EXTERNAL================== */

    public void addProperty(String name, Property prop) {
        currentScope.add(name, prop);
    }

    public void addProperty(String name, Value value) {
        addProperty(name, new Property(false, null, value));
    }

    public void addFunction(String name, Function function) {
        addFunction(name, function, () -> "[missing toString]");
    }

    public void addFunction(String name, Function function, Supplier<String> toString) {
        currentScope.add(name, new Property(true, null, new Value() {

            @Override
            public Type getType() {
                throw new InterperterException("Function has no type.");
            }

            @Override
            public Value cast(Type type) {
                throw new InterperterException("Function has no type.");
            }

            @Override
            public Value infix(String type, Value value) {
                throw new InterperterException("Function does not allow infix operators.");
            }

            @Override
            public Value postfix(String type) {
                throw new InterperterException("Function does not allow postfix operators.");
            }

            @Override
            public Value prefix(String type) {
                throw new InterperterException("Function does not allow prefix operators.");
            }

            @Override
            public Value call(Value[] args) {
                return function.call(args, this);
            }

            @Override
            public String toString() {
                return toString.get();
            }

            @Override
            public Value call(Map<String, Value> args) {
                throw new InterperterException("Function does not allow calling using maps.");
            }
        }));
    }

    static interface Function {
        Value call(Value[] args, Value that);
    }

    /* ==============================INTERNAL========================= */
    private Value infix(Value left, String text, Supplier<Value> other) {
        switch (text) {
        case "": {
            return other.get();
        }
        case "||": {
            boolean value = ((BooleanValue) left.cast(new BooleanType())).value;
            if (value) {
                return left;
            } else {
                return other.get();
            }
        }
        case "&&": {
            boolean value = ((BooleanValue) left.cast(new BooleanType())).value;
            if (value) {
                return other.get();
            } else {
                return left;
            }
        }
        default:
            return left.infix(text, other.get());
        }
    }
}