package superscript.interperter;

import java.util.Map;

class MissingValue extends Value {
    String name;

    @Override
    public Type getType() {
        throw new InterperterException("Value is missing.");
    }

    @Override
    public Value cast(Type type) {
        throw new InterperterException("Value is missing.");
    }

    @Override
    public Value infix(String type, Value other) {
        throw new InterperterException("Value is missing.");
    }

    @Override
    public Value postfix(String type) {
        throw new InterperterException("Value is missing.");
    }

    @Override
    public Value prefix(String type) {
        if (type == "has") {
            return new BooleanValue(false);
        }
        throw new InterperterException("Value is missing.");
    }

    @Override
    public String toString() {
        return "[Missing Value]";
    }

    MissingValue(String name) {
        this.name = name;
    }

    @Override
    public Value call(Value[] vals) {
        throw new InterperterException("Value is missing.");
    }

    @Override
    public Value call(Map<String, Value> args) {
        throw new InterperterException("Value is missing.");
    }

}