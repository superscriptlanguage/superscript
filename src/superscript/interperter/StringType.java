package superscript.interperter;

class StringType extends Type {

    @Override
    public String toString() {
        return "string";
    }

    @Override
    public boolean isCompatiableWith(Type othertype) {
        return othertype instanceof StringType;
    }

}