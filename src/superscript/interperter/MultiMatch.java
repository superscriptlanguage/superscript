package superscript.interperter;

class MultiMatch extends Matchable {

    Matchable[] matches;

    public MultiMatch(Matchable[] array) {
        matches = array;
    }

    @Override
    public boolean match(Value value, Scope<Property> current) {
        for (Matchable match : matches) {
            if (match.match(value, current)) {
                return true;
            }
        }
        return false;
    }

}