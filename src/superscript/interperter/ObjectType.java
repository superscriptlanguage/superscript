package superscript.interperter;

class ObjectType extends Type {

    @Override
    public boolean isCompatiableWith(Type othertype) {
        return othertype instanceof ObjectType;
    }

    @Override
    public String toString() {
        return "object";
    }
}