package superscript.interperter;

import java.util.Map;

class VoidValue extends Value {

    @Override
    public Type getType() {
        return new VoidType();
    }

    @Override
    public String toString() {
        return "void";
    }

    @Override
    public Value infix(String type, Value other) {
        throw new InterperterException("Can not add 'void' to anything.");
    }

    @Override
    public Value postfix(String type) {
        throw new InterperterException("Can not add 'void' to anything.");
    }

    @Override
    public Value prefix(String type) {
        throw new InterperterException("Can not add 'void' to anything.");
    }

    @Override
    public Value call(Value[] vals) {
        throw new InterperterException("Can not call 'void' as a function.");
    }

    @Override
    public Value cast(Type t) {
        if (t instanceof BooleanType) {
            return new BooleanValue(false);
        } else if (t instanceof VoidType) {
            return this;
        } else if (t instanceof NumberType) {
            return new NumberValue(0);
        }
        throw new InterperterException("Can not cast void to value.");
    }

    @Override
    public Value call(Map<String, Value> args) {
        throw new InterperterException("Can not call 'void' as a function.");
    }

}