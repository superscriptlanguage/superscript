package superscript.interperter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

import org.antlr.v4.runtime.tree.ParseTree;

class FunctionValue extends Value {
    public static class DefaultFunctionType extends Type {

        @Override
        public boolean isCompatiableWith(Type othertype) {
            return othertype instanceof DefaultFunctionType;
        }

        @Override
        public String toString() {
            return "func";
        }

    }

    private ParseTree toVisit;
    private BiFunction<ParseTree, Scope<Property>, Value> evaluator;
    private int restpos;
    private Scope<Property> lookScope;
    private Destructuring[] destructures;
    private Type[] types;
    private Type cast;

    FunctionValue(ParseTree toVisit, BiFunction<ParseTree, Scope<Property>, Value> evaluator, int restpos,
            Scope<Property> lookScope, Destructuring[] destructures, Type[] types, Type cast) {
        this.toVisit = toVisit;
        this.evaluator = evaluator;
        this.restpos = restpos;
        this.lookScope = lookScope;
        this.destructures = destructures;
        this.types = types;
        this.cast = cast;
    }

    public Type getReturnType() {
        return cast;
    }

    @Override
    public Type getType() {
        return new DefaultFunctionType();
    }

    @Override
    public Value cast(Type type) {
        if (type instanceof DefaultFunctionType) {
            return this;
        }
        throw new InterperterException("Can not cast function.");
    }

    @Override
    public Value infix(String type, Value value) {
        throw new InterperterException("Can't use things on functions.");
    }

    @Override
    public Value postfix(String type) {
        throw new InterperterException("Can't use things on functions.");
    }

    @Override
    public Value prefix(String type) {
        throw new InterperterException("Can't use things on functions.");
    }

    @Override
    public Value call(Value[] args) {
        Scope<Property> newScope = new Scope<>(new HashMap<>(), lookScope, lookScope.returnScopeNoCheck(),
                new VoidValue(), new VoidValue(), this);
        for (int i = 0; i < restpos; i++) {
            destructures[i].evaluateConst(args[i].cast(types[i]), newScope);
        }
        for (int i = restpos + 1; i < destructures.length; i++) {
            destructures[i].evaluateConst(args[args.length - (destructures.length - i)].cast(types[i]), newScope);
        }
        if (restpos != -1) {
            int startValue = restpos;
            int endValue = args.length - (destructures.length - restpos);
            destructures[restpos].evaluateConst(
                    new ArrayValue(types[restpos], Arrays.copyOfRange(args, startValue, endValue)), newScope);
        }
        try {
            return evaluator.apply(toVisit, newScope);
        } catch (ReturnException e) {
            return e.getValue();
        }
    }

    @Override
    public Value call(Map<String, Value> args) {
        throw new UnsupportedOperationException("Can't use things on functions.");
    }

    @Override
    public String toString() {
        String typestrings = "";
        if (types.length > 0) {
            typestrings = types[0].toString();
            for (int i = 1; i < types.length; i++) {
                typestrings += ", " + types[i].toString();
            }
        }
        return "function(" + typestrings + ")" + (cast == null ? "" : " -> " + cast);
    }

    @Override
    public boolean canCall() {
        return true;
    }

}