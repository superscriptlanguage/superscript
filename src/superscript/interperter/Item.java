package superscript.interperter;

abstract class Item {
    public Value get() {
        throw new InterperterException("Can not cast to value.");
    }
}