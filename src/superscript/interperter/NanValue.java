package superscript.interperter;

import java.util.Map;

class NanValue extends Value {

    @Override
    public String toString() {
        return "NaN";
    }

    @Override
    public Value infix(String type, Value value) {
        if (value instanceof NumberValue || value instanceof NanValue) {
            switch (type) {
            case "+":
            case "-":
            case "*":
            case "/":
            case "**":
                return new NanValue();
            case "<":
            case ">":
            case "<=":
            case ">=":
                throw new InterperterException("Can not compare NaN to a number");
            case "==":
            case "===":
                return new BooleanValue(value instanceof NanValue);
            default:
                throw new UnsupportedOperationException("Unsupported infix operator.");
            }
        } else {
            throw new InterperterException("Can not use an operator on NaN and a object of an other type.");
        }
    }

    // TODO: Get this working.
    @Override
    public Value postfix(String type) {
        switch (type) {
        case "--":
        case "++":
            return new NanValue();
        default:
            throw new UnsupportedOperationException("Unsupported infix operator.");
        }
    }

    // TODO: Get this working.
    @Override
    public Value prefix(String type) {
        switch (type) {
        case "--":
        case "++":
        case "+":
        case "-":
            return new NanValue();
        default:
            throw new UnsupportedOperationException("Unsupported infix operator.");
        }
    }

    @Override
    public Value call(Value[] vals) {
        throw new InterperterException("Can not call NaN as a function.");
    }

    @Override
    public Value cast(Type t) {
        if (t instanceof NumberType) {
            return this;
        }
        throw new InterperterException("Can not cast NaN to other types.");
    }

    @Override
    public Type getType() {
        return new NumberType();
    }

    @Override
    public Value call(Map<String, Value> args) {
        throw new InterperterException("Can not call NaN as a function.");
    }
}