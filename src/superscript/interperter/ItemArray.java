package superscript.interperter;

class ItemArray extends Item {
    private Item[] items;

    ItemArray(Item[] items) {
        this.items = items;
    }

    public Item[] getItems() {
        return items;
    }
}