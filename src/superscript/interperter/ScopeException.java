package superscript.interperter;

class ScopeException extends InterperterException {

    private static final long serialVersionUID = 1L;

    ScopeException() {
        super();
    }

    ScopeException(String message) {
        super(message);
    }

    ScopeException(String message, Throwable throwable) {
        super(message, throwable);
    }

    ScopeException(Throwable throwable) {
        super(throwable);
    }
}