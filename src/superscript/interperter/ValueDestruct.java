package superscript.interperter;

class ValueDestruct extends Destructuring {
    private String name;

    private Value defaultValue;

    public ValueDestruct(String name) {
        this(name, null);
    }

    public ValueDestruct(String name, Value def) {
        this.name = name;
        this.defaultValue = def;
    }

    @Override
    public void evaluate(Value expression, Scope<Property> current) {
        if (expression == null) {
            if (defaultValue == null) {
                throw new InterperterException("Non-existing value, and no default value provided.");
            } else {
                current.modify(name, (v) -> v.set(defaultValue));
            }
        }
        current.modify(name, (v) -> v.set(expression));
    }

    @Override
    public String[] getNecessaryNames() {
        return new String[] { name };
    }

}