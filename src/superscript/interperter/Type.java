package superscript.interperter;

abstract class Type extends Item {

    public abstract String toString();

    public abstract boolean isCompatiableWith(Type othertype);

    public boolean isCompatiableWith(Value v) {
        return v.getType().isCompatiableWith(this);
    }
}