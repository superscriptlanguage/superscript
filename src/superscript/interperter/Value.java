package superscript.interperter;

import java.util.Map;

abstract class Value extends Matchable {
    public abstract Type getType();

    public abstract Value cast(Type type);

    public abstract Value infix(String type, Value value);

    public abstract Value postfix(String type);

    public abstract Value prefix(String type);

    public abstract Value call(Value[] args);

    public abstract Value call(Map<String, Value> args);

    public boolean canCall() {
        return false;
    }

    public abstract String toString();

    public Property getProperty(Value name) {
        throw new InterperterException("Can not get property of a normal value.");
    }

    @Override
    public Value get() {
        return this;
    }

    @Override
    public boolean match(Value value, Scope<Property> current) {
        return ((BooleanValue) value.infix("==", this).cast(new BooleanType())).value;
    }
}