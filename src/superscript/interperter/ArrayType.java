package superscript.interperter;

class ArrayType extends Type {
    private Type type;

    public Type getType() {
        return type;
    }

    ArrayType(Type type) {
        this.type = type;
    }

    @Override
    public boolean isCompatiableWith(Type othertype) {
        return othertype instanceof ArrayType && ((ArrayType) othertype).getType().isCompatiableWith(type);
    }

    @Override
    public String toString() {
        return type + "[]";
    }
}