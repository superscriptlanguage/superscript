package superscript.interperter;

class VoidType extends Type {

    @Override
    public boolean isCompatiableWith(Type othertype) {
        return othertype instanceof BooleanType || othertype instanceof VoidType || othertype instanceof NumberType;
    }

    @Override
    public String toString() {
        return "void";
    }
}