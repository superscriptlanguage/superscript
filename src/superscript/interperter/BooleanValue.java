package superscript.interperter;

import java.util.Map;

class BooleanValue extends Value {
    boolean value;

    @Override
    public Type getType() {
        return new BooleanType();
    }

    @Override
    public Value cast(Type type) {
        if (type instanceof BooleanType) {
            return this;
        }
        throw new InterperterException("Can not set type of BooleanValue to be another type.");
    }

    // TODO: Implement
    @Override
    public Value infix(String type, Value other) {
        if (!(other instanceof BooleanValue))
            throw new InterperterException("Value is not of necessary type.");
        boolean otherValue = ((BooleanValue) other).value;
        switch (type) {
        case "|":
            return new BooleanValue(value | otherValue);
        case "&":
            return new BooleanValue(value & otherValue);
        case "^":
            return new BooleanValue(value ^ otherValue);
        default:
            throw new UnsupportedOperationException("Unsupported infix operator.");
        }
    }

    // TODO: Implement
    @Override
    public Value postfix(String type) {
        throw new UnsupportedOperationException("Unsupported postfix operator.");
    }

    // TODO: Implement
    @Override
    public Value prefix(String type) {
        throw new UnsupportedOperationException("Unsupported prefix operator.");
    }

    @Override
    public String toString() {
        return "" + value;
    }

    BooleanValue(boolean value) {
        this.value = value;
    }

    @Override
    public Value call(Value[] vals) {
        throw new InterperterException("Can not call a boolean as a function.");
    }

    @Override
    public Value call(Map<String, Value> args) {
        throw new InterperterException("Can not call a boolean as a function.");
    }

}