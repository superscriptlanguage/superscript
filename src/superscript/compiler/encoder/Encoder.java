package superscript.compiler.encoder;

/**
 * An encoder, usually paired with {@link Decoder}
 * <p>
 * Encodes objects into JVM-Valid identifiers.
 * 
 * @param <T>
 *                The type of the object to encode.
 * @see Decoder
 */
public interface Encoder<T> {
    /**
     * Encodes an object, which would be decoded by a {@link Decoder}
     * 
     * @param t
     *              The object to encode.
     * @return The encoded string.
     */
    public String encode(T t);
}