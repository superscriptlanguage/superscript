package superscript.compiler.encoder;

/**
 * A decoder, usually paired with {@link Decoder}
 * <p>
 * Decodes strings that are JVM-Valid identifiers to objects.
 * 
 * @param <T>
 *                The type of the object that gets decoded.
 * @see Encoder
 */
public interface Decoder<T> {
    /**
     * Decodes an string, which would be encoded by an {@link Encoder}
     * 
     * @param string
     *                   The encoded string.
     * @return The decoded object.
     * @see Decoder
     */
    public abstract T decode(String string);

}