package superscript.compiler.encoder;

import superscript.compiler.type.MethodType;
import superscript.compiler.type.Type;

/**
 * Encodes functions.
 * 
 * @see Encoder
 */
public final class FunctionEncoder implements Encoder<MethodType> {
    public final String postfix;
    public final String seperator;
    public final Encoder<Type> encoder;

    /**
     * {@inheritDoc}
     */
    @Override
    public String encode(MethodType t) {
        String methodString = "";
        for (Type type : t.inputs) {
            methodString += encoder.encode(type);
        }
        methodString += seperator;
        methodString += encoder.encode(t.output);
        return methodString + postfix;
    }

    public FunctionEncoder(String postfix, String seperator, Encoder<Type> encoder) {
        this.postfix = postfix;
        this.seperator = seperator;
        this.encoder = encoder;
    }

}