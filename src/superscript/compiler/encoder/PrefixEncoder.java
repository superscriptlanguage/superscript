package superscript.compiler.encoder;

/**
 * Encodes and decodes strings by appending a prefix to make two different
 * encoders distingushable.
 * 
 * @see Encoder
 * @see Decoder
 */
public final class PrefixEncoder implements Encoder<String>, Decoder<String> {
    public final String prefix;

    /**
     * {@inheritDoc}
     */
    @Override
    public String encode(String t) {
        return prefix + t;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String decode(String string) {
        return string.substring(prefix.length());
    }

    public PrefixEncoder(String prefix) {
        this.prefix = prefix;
    }
}