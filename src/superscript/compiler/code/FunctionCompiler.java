package superscript.compiler.code;

import superscript.parser.SuperScriptParser;
import superscript.parser.SuperScriptParserBaseVisitor;

public class FunctionCompiler extends SuperScriptParserBaseVisitor<FunctionCompiler.Storage> {
    /**
     * An object that pairs the size of the stack, the size of the locals, and code.
     * 
     * @param <T>
     *                The type of the code.
     */
    public static final class Storage {
        public final int stack;
        public final int locals;
        public final int size;
        public final String str;

        public Storage(int stack, int locals, int size, String str) {
            this.stack = stack;
            this.locals = locals;
            this.size = size;
            this.str = str;
        }
    }

    @Override
    public Storage visitBlock(SuperScriptParser.BlockContext ctx) {
        String str = "";
        int stack = 0;
        int locals = 0;
        for (int i = 0; i < ctx.expression().size() - 1; i++) {
            Storage current = visit(ctx.expression(i));
            str += current.str;
            str += "\n";
            str += "pop\n" + current.size;
            stack = Math.max(stack, current.stack);
            locals += current.locals;
        }
        Storage last = visit(ctx.expression(ctx.expression().size() - 1));
        str += last.str;
        return new Storage(Math.max(stack, last.stack), locals + last.locals, last.size, str);
    }
}