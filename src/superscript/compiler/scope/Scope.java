package superscript.compiler.scope;

import java.util.Map;
import java.util.function.UnaryOperator;

import superscript.compiler.misc.CompilerException;

public class Scope<S, T> {
    private Map<S, T> props;
    private Scope<S, T> outer;

    public T get(S s) {
        return get(s, this);
    }

    private T get(S s, Scope<S, T> current) {
        if (current == null)
            return null;
        return current.props.containsKey(s) ? current.props.get(s) : get(s, current.outer);
    }

    public Scope<S, T> outerScope() {
        if (outer == null)
            throw new CompilerException("Invalid outer scope.");
        return outer;
    }

    public Scope<S, T> outerScopeNoCheck() {
        return outer;
    }

    public Scope<S, T> add(S s, T t) {
        props.put(s, t);
        return this;
    }

    public Scope<S, T> modify(S s, UnaryOperator<T> op) {
        modify(s, op, this);
        return this;
    }

    public Scope<S, T> put(S s, T value) {
        modify(s, (v) -> value, this);
        return this;
    }

    private void modify(S s, UnaryOperator<T> op, Scope<S, T> current) {
        if (current == null)
            throw new CompilerException("Setting a variable without declaring it.");
        if (current.props.containsKey(s)) {
            current.props.compute(s, (k, v) -> op.apply(v));
        } else {
            modify(s, op, current.outer);
        }
    }

    Scope(Map<S, T> props, Scope<S, T> outer) {
        this.props = props;
        this.outer = outer;
    }
}