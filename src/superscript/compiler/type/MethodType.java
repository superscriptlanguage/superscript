package superscript.compiler.type;

public final class MethodType extends Type {
    public final Type[] inputs;
    public final Type output;

    @Override
    public String generic() {
        String inputString = "";
        for (Type t : inputs) {
            inputString += t.generic();
        }
        return '(' + inputString + ')' + output;
    }

    @Override
    public String noGeneric() {
        String inputString = "";
        for (Type t : inputs) {
            inputString += t.noGeneric();
        }
        return '(' + inputString + ')' + output;
    }

    public MethodType(Type[] inputs, Type output) {
        this.inputs = inputs;
        this.output = output;
    }
}