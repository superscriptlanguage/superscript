package superscript.compiler.type;

public final class ObjectType extends Type {
    public final String name;

    @Override
    public String generic() {
        return 'L' + name + ';';
    }

    @Override
    public String noGeneric() {
        return generic();
    }

    public ObjectType(String name) {
        this.name = name;
    }
}