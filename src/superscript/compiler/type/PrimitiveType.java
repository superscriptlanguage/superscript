package superscript.compiler.type;

public final class PrimitiveType extends Type {
    public final Primitive primitive;

    @Override
    public String generic() {
        return primitive.toString();
    }

    @Override
    public String noGeneric() {
        return generic();
    }

    public PrimitiveType(Primitive primitive) {
        this.primitive = primitive;
    }

    public enum Primitive {
        BOOLEAN('Z'), BYTE('B'), SHORT('S'), INT('I'), LONG('J'), FLOAT('F'), DOUBLE('D'), CHAR('C'), VOID('V');
        private final char name;

        private Primitive(char name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return String.valueOf(name);
        }
    }
}