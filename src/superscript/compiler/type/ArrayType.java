package superscript.compiler.type;

public final class ArrayType extends Type {
    public final Type type;

    @Override
    public String generic() {
        return '[' + type.generic();
    }

    @Override
    public String noGeneric() {
        return '[' + type.noGeneric();
    }

    public ArrayType(Type type) {
        this.type = type;
    }

}