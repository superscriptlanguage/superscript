package superscript.compiler.type;

public final class GenericType extends Type {
    public final Type type;
    public final Type[] generics;

    @Override
    public String generic() {
        String typeString = type.generic();
        if (typeString.charAt(typeString.length()) != ';')
            throw new Error("Invalid type passed in.");
        String genericString = "";
        for (Type t : generics) {
            genericString += t.generic();
        }
        return typeString.replaceAll(".$", "") + '<' + genericString + '>' + ';';
    }

    @Override
    public String noGeneric() {
        return type.noGeneric();
    }

    public GenericType(Type type, Type[] generics) {
        this.type = type;
        this.generics = generics;
    }

}