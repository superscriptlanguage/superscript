package superscript.compiler.type;

public final class TypeVariable extends Type {
    public final String[] names;

    @Override
    public String generic() {
        String name = "";
        for (String s : names) {
            name += s;
        }
        return 'T' + name + ';';
    }

    @Override
    public String noGeneric() {
        return generic(); // TODO: Maybe?
    }

    public TypeVariable(String[] names) {
        this.names = names;
    }
}