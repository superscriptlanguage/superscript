package superscript.compiler.type;

public abstract class Type {
    public abstract String generic();

    public abstract String noGeneric();
}