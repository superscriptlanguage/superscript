package superscript.compiler.misc;

public class CompilerException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public CompilerException() {
        super();
    }

    public CompilerException(String message) {
        super(message);
    }

    public CompilerException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public CompilerException(Throwable throwable) {
        super(throwable);
    }
}