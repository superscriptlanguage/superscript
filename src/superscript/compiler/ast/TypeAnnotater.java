package superscript.compiler.ast;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

import superscript.compiler.ast.type.Type;
import superscript.parser.SuperScriptParserBaseVisitor;

public class TypeAnnotater extends SuperScriptParserBaseVisitor<Void> {
    public final ParseTreeProperty<Type> types;

    public static ParseTreeProperty<Type> generate(ParseTree toVisit) {
        TypeAnnotater annotater = new TypeAnnotater();
        annotater.visit(toVisit);
        return annotater.types;
    }

    private TypeAnnotater() {
        types = new ParseTreeProperty<Type>();
    }
}