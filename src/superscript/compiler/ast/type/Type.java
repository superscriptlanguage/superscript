package superscript.compiler.ast.type;

public abstract class Type {
    public abstract boolean matches(Type otherType);
}