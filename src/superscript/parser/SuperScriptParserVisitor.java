// Generated from SuperScriptParser.g4 by ANTLR 4.7.2
package superscript.parser;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link SuperScriptParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface SuperScriptParserVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#file}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFile(SuperScriptParser.FileContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#pkg}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPkg(SuperScriptParser.PkgContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#url}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUrl(SuperScriptParser.UrlContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(SuperScriptParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by the {@code valuesSet}
	 * labeled alternative in {@link SuperScriptParser#arr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValuesSet(SuperScriptParser.ValuesSetContext ctx);
	/**
	 * Visit a parse tree produced by the {@code lengthSet}
	 * labeled alternative in {@link SuperScriptParser#arr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLengthSet(SuperScriptParser.LengthSetContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#obj}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObj(SuperScriptParser.ObjContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#tup}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTup(SuperScriptParser.TupContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#tupparam}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTupparam(SuperScriptParser.TupparamContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#arrparam}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrparam(SuperScriptParser.ArrparamContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#objparam}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjparam(SuperScriptParser.ObjparamContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#id}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitId(SuperScriptParser.IdContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#valdec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValdec(SuperScriptParser.ValdecContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#vardec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVardec(SuperScriptParser.VardecContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#vartype}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVartype(SuperScriptParser.VartypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#typevalue}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypevalue(SuperScriptParser.TypevalueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code typeFunction}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeFunction(SuperScriptParser.TypeFunctionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nameType}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNameType(SuperScriptParser.NameTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code typeExpand}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeExpand(SuperScriptParser.TypeExpandContext ctx);
	/**
	 * Visit a parse tree produced by the {@code typeArray}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeArray(SuperScriptParser.TypeArrayContext ctx);
	/**
	 * Visit a parse tree produced by the {@code noneType}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNoneType(SuperScriptParser.NoneTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code typeTuple}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeTuple(SuperScriptParser.TypeTupleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code typeObject}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeObject(SuperScriptParser.TypeObjectContext ctx);
	/**
	 * Visit a parse tree produced by the {@code voidType}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVoidType(SuperScriptParser.VoidTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code genericType}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGenericType(SuperScriptParser.GenericTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code typeGenFunction}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeGenFunction(SuperScriptParser.TypeGenFunctionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unknownType}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnknownType(SuperScriptParser.UnknownTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parensType}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParensType(SuperScriptParser.ParensTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#functionname}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionname(SuperScriptParser.FunctionnameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#descriptivefunctionname}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDescriptivefunctionname(SuperScriptParser.DescriptivefunctionnameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#typeobj}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeobj(SuperScriptParser.TypeobjContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#typetup}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypetup(SuperScriptParser.TypetupContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#typeobjparam}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeobjparam(SuperScriptParser.TypeobjparamContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#typetupparam}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypetupparam(SuperScriptParser.TypetupparamContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#gentype}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGentype(SuperScriptParser.GentypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#declaredgentype}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaredgentype(SuperScriptParser.DeclaredgentypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#generic}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGeneric(SuperScriptParser.GenericContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#functionoptions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionoptions(SuperScriptParser.FunctionoptionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#infixop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInfixop(SuperScriptParser.InfixopContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#prefixop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrefixop(SuperScriptParser.PrefixopContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#postfixop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostfixop(SuperScriptParser.PostfixopContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parens}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParens(SuperScriptParser.ParensContext ctx);
	/**
	 * Visit a parse tree produced by the {@code access}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAccess(SuperScriptParser.AccessContext ctx);
	/**
	 * Visit a parse tree produced by the {@code declare}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclare(SuperScriptParser.DeclareContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exportObjs}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExportObjs(SuperScriptParser.ExportObjsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code prefix}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrefix(SuperScriptParser.PrefixContext ctx);
	/**
	 * Visit a parse tree produced by the {@code for}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor(SuperScriptParser.ForContext ctx);
	/**
	 * Visit a parse tree produced by the {@code generator}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGenerator(SuperScriptParser.GeneratorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code importObjs}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImportObjs(SuperScriptParser.ImportObjsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code while}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhile(SuperScriptParser.WhileContext ctx);
	/**
	 * Visit a parse tree produced by the {@code literal}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral(SuperScriptParser.LiteralContext ctx);
	/**
	 * Visit a parse tree produced by the {@code result}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitResult(SuperScriptParser.ResultContext ctx);
	/**
	 * Visit a parse tree produced by the {@code evaluation}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEvaluation(SuperScriptParser.EvaluationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code cast}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCast(SuperScriptParser.CastContext ctx);
	/**
	 * Visit a parse tree produced by the {@code lambda}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLambda(SuperScriptParser.LambdaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code varModify}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarModify(SuperScriptParser.VarModifyContext ctx);
	/**
	 * Visit a parse tree produced by the {@code yieldGen}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitYieldGen(SuperScriptParser.YieldGenContext ctx);
	/**
	 * Visit a parse tree produced by the {@code continue}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitContinue(SuperScriptParser.ContinueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code yield}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitYield(SuperScriptParser.YieldContext ctx);
	/**
	 * Visit a parse tree produced by the {@code function}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction(SuperScriptParser.FunctionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code await}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAwait(SuperScriptParser.AwaitContext ctx);
	/**
	 * Visit a parse tree produced by the {@code refinement}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRefinement(SuperScriptParser.RefinementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code has}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHas(SuperScriptParser.HasContext ctx);
	/**
	 * Visit a parse tree produced by the {@code postfix}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostfix(SuperScriptParser.PostfixContext ctx);
	/**
	 * Visit a parse tree produced by the {@code if}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf(SuperScriptParser.IfContext ctx);
	/**
	 * Visit a parse tree produced by the {@code varManagement}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarManagement(SuperScriptParser.VarManagementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code chain}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitChain(SuperScriptParser.ChainContext ctx);
	/**
	 * Visit a parse tree produced by the {@code break}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBreak(SuperScriptParser.BreakContext ctx);
	/**
	 * Visit a parse tree produced by the {@code blocks}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlocks(SuperScriptParser.BlocksContext ctx);
	/**
	 * Visit a parse tree produced by the {@code forEach}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForEach(SuperScriptParser.ForEachContext ctx);
	/**
	 * Visit a parse tree produced by the {@code lambdaGen}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLambdaGen(SuperScriptParser.LambdaGenContext ctx);
	/**
	 * Visit a parse tree produced by the {@code match}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMatch(SuperScriptParser.MatchContext ctx);
	/**
	 * Visit a parse tree produced by the {@code infix}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInfix(SuperScriptParser.InfixContext ctx);
	/**
	 * Visit a parse tree produced by the {@code setType}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSetType(SuperScriptParser.SetTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code pipelineOp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPipelineOp(SuperScriptParser.PipelineOpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code varset}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarset(SuperScriptParser.VarsetContext ctx);
	/**
	 * Visit a parse tree produced by the {@code throw}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitThrow(SuperScriptParser.ThrowContext ctx);
	/**
	 * Visit a parse tree produced by the {@code doWhile}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoWhile(SuperScriptParser.DoWhileContext ctx);
	/**
	 * Visit a parse tree produced by the {@code specificEvaluation}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSpecificEvaluation(SuperScriptParser.SpecificEvaluationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code genericRefining}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGenericRefining(SuperScriptParser.GenericRefiningContext ctx);
	/**
	 * Visit a parse tree produced by the {@code try}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTry(SuperScriptParser.TryContext ctx);
	/**
	 * Visit a parse tree produced by the {@code infixHas}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInfixHas(SuperScriptParser.InfixHasContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ternary}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTernary(SuperScriptParser.TernaryContext ctx);
	/**
	 * Visit a parse tree produced by the {@code return}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturn(SuperScriptParser.ReturnContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#matchresult}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMatchresult(SuperScriptParser.MatchresultContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#urlexp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUrlexp(SuperScriptParser.UrlexpContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#urlname}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUrlname(SuperScriptParser.UrlnameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#specificfunctionarg}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSpecificfunctionarg(SuperScriptParser.SpecificfunctionargContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#functionarg}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionarg(SuperScriptParser.FunctionargContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#cases}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCases(SuperScriptParser.CasesContext ctx);
	/**
	 * Visit a parse tree produced by the {@code caseExp}
	 * labeled alternative in {@link SuperScriptParser#acase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCaseExp(SuperScriptParser.CaseExpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code caseDes}
	 * labeled alternative in {@link SuperScriptParser#acase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCaseDes(SuperScriptParser.CaseDesContext ctx);
	/**
	 * Visit a parse tree produced by the {@code caseDef}
	 * labeled alternative in {@link SuperScriptParser#acase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCaseDef(SuperScriptParser.CaseDefContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#args}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgs(SuperScriptParser.ArgsContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#arg}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArg(SuperScriptParser.ArgContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar(SuperScriptParser.VarContext ctx);
	/**
	 * Visit a parse tree produced by the {@code arrDestruct}
	 * labeled alternative in {@link SuperScriptParser#destruct}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrDestruct(SuperScriptParser.ArrDestructContext ctx);
	/**
	 * Visit a parse tree produced by the {@code objDestructAndStore}
	 * labeled alternative in {@link SuperScriptParser#destruct}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjDestructAndStore(SuperScriptParser.ObjDestructAndStoreContext ctx);
	/**
	 * Visit a parse tree produced by the {@code tupDestruct}
	 * labeled alternative in {@link SuperScriptParser#destruct}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTupDestruct(SuperScriptParser.TupDestructContext ctx);
	/**
	 * Visit a parse tree produced by the {@code name}
	 * labeled alternative in {@link SuperScriptParser#destruct}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitName(SuperScriptParser.NameContext ctx);
	/**
	 * Visit a parse tree produced by the {@code objDestruct}
	 * labeled alternative in {@link SuperScriptParser#destruct}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjDestruct(SuperScriptParser.ObjDestructContext ctx);
	/**
	 * Visit a parse tree produced by the {@code arrDestructAndStore}
	 * labeled alternative in {@link SuperScriptParser#destruct}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrDestructAndStore(SuperScriptParser.ArrDestructAndStoreContext ctx);
	/**
	 * Visit a parse tree produced by the {@code tupDestructAndStore}
	 * labeled alternative in {@link SuperScriptParser#destruct}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTupDestructAndStore(SuperScriptParser.TupDestructAndStoreContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#destructarrparam}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDestructarrparam(SuperScriptParser.DestructarrparamContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#destructtupparam}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDestructtupparam(SuperScriptParser.DestructtupparamContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#destructobjparam}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDestructobjparam(SuperScriptParser.DestructobjparamContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#number}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumber(SuperScriptParser.NumberContext ctx);
	/**
	 * Visit a parse tree produced by the {@code object}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObject(SuperScriptParser.ObjectContext ctx);
	/**
	 * Visit a parse tree produced by the {@code array}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray(SuperScriptParser.ArrayContext ctx);
	/**
	 * Visit a parse tree produced by the {@code tuple}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTuple(SuperScriptParser.TupleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code templateString}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTemplateString(SuperScriptParser.TemplateStringContext ctx);
	/**
	 * Visit a parse tree produced by the {@code numb}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumb(SuperScriptParser.NumbContext ctx);
	/**
	 * Visit a parse tree produced by the {@code string}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitString(SuperScriptParser.StringContext ctx);
	/**
	 * Visit a parse tree produced by the {@code char}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitChar(SuperScriptParser.CharContext ctx);
	/**
	 * Visit a parse tree produced by the {@code true}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTrue(SuperScriptParser.TrueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code false}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFalse(SuperScriptParser.FalseContext ctx);
	/**
	 * Visit a parse tree produced by the {@code NaN}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNaN(SuperScriptParser.NaNContext ctx);
	/**
	 * Visit a parse tree produced by the {@code this}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitThis(SuperScriptParser.ThisContext ctx);
	/**
	 * Visit a parse tree produced by the {@code super}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSuper(SuperScriptParser.SuperContext ctx);
	/**
	 * Visit a parse tree produced by the {@code self}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelf(SuperScriptParser.SelfContext ctx);
	/**
	 * Visit a parse tree produced by the {@code void}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVoid(SuperScriptParser.VoidContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#templatestr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTemplatestr(SuperScriptParser.TemplatestrContext ctx);
	/**
	 * Visit a parse tree produced by {@link SuperScriptParser#exprsemi}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprsemi(SuperScriptParser.ExprsemiContext ctx);
}