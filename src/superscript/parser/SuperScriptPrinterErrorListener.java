package superscript.parser;

import superscript.utils.*;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;

class SuperScriptPrinterErrorListener extends BaseErrorListener {
    private static final int header = 0;
    private static final int distance = 0;
    String errors;
    private String file;
    private ErrorHandler error;
    private Formatter formatter;
    private final String errindent;
    private final String msgindent;
    private Formatter.Colors color = new Formatter.Colors(Formatter.ColorType.BRIGHT);

    SuperScriptPrinterErrorListener(String file, ErrorHandler error) {
        this(file, "", "", error);
    }

    SuperScriptPrinterErrorListener(String file, String errindent, ErrorHandler error) {
        this(file, errindent, "", error);
    }

    SuperScriptPrinterErrorListener(String file, String errindent, String msgindent, ErrorHandler error) {
        errors = "";
        formatter = new Formatter(color);
        this.errindent = errindent;
        this.file = file;
        this.error = error;
        this.msgindent = msgindent;
    }

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine,
            String msg, RecognitionException e) {
        if (!(recognizer instanceof Parser))
            throw new Error("Incorrect 'recognizer' type");
        if (!(offendingSymbol instanceof Token))
            throw new Error("Incorrect 'offendingSymbol' type");
        Token offendingToken = (Token) offendingSymbol;
        Parser parser = (Parser) recognizer;
        TokenStream tokens = parser.getInputStream();
        String[] lines = tokens.getTokenSource().getInputStream().toString().split("\n");
        ErrorHandler error = calcErrors(parser, offendingToken, line, charPositionInLine, msg);
        if (!errors.equals("")) {
            errors += "\r\n";
        }
        if (error.has()) {
            errors += errindent + color.ERROR + "Error[e" + String.format("%04d", error.type) + "]: " + msg
                    + color.RESET;
            errors += error.headers;
            Formatter.Token formatOffToken = error.offendingToken;
            formatter.setLines(lines).error(msg, formatOffToken);
            for (ErrorHandler.TokPair pair : error.infos) {
                formatter.info(pair.msg, pair.token);
            }
            String format = formatter.format(color.INFO + file + ':' + formatOffToken.getLine() + ':'
                    + formatOffToken.getCharPositionInLine() + color.RESET, 1, error.notes, errindent + msgindent,
                    header, distance).clear().giveError();
            errors += "\r\n" + format;
            error.clear();
        } else {
            throw new Error("Invalid 'error'");
        }
    }

    private ErrorHandler calcErrors(Parser parser, Token offendingToken, int line, int charPositionInLine, String msg) {
        ErrorHandler result = ErrorHandler.empty();
        if (error.has()) {
            result = error;
            error.clear();
        } else {
            System.err.println("Warning: 'error' is not valid. Reverting to basic error handling.");
            result.notes = new String[] { color.NOTE + "Note: Error is from parser." + color.RESET };
            result.offendingToken = new Formatter.Token(offendingToken);
        }
        return result;
    }

    public SuperScriptPrinterErrorListener uninstall() {
        formatter.uninstall();
        return this;
    }
}