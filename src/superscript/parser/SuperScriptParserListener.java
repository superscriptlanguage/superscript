// Generated from SuperScriptParser.g4 by ANTLR 4.7.2
package superscript.parser;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SuperScriptParser}.
 */
public interface SuperScriptParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#file}.
	 * @param ctx the parse tree
	 */
	void enterFile(SuperScriptParser.FileContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#file}.
	 * @param ctx the parse tree
	 */
	void exitFile(SuperScriptParser.FileContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#pkg}.
	 * @param ctx the parse tree
	 */
	void enterPkg(SuperScriptParser.PkgContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#pkg}.
	 * @param ctx the parse tree
	 */
	void exitPkg(SuperScriptParser.PkgContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#url}.
	 * @param ctx the parse tree
	 */
	void enterUrl(SuperScriptParser.UrlContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#url}.
	 * @param ctx the parse tree
	 */
	void exitUrl(SuperScriptParser.UrlContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(SuperScriptParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(SuperScriptParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by the {@code valuesSet}
	 * labeled alternative in {@link SuperScriptParser#arr}.
	 * @param ctx the parse tree
	 */
	void enterValuesSet(SuperScriptParser.ValuesSetContext ctx);
	/**
	 * Exit a parse tree produced by the {@code valuesSet}
	 * labeled alternative in {@link SuperScriptParser#arr}.
	 * @param ctx the parse tree
	 */
	void exitValuesSet(SuperScriptParser.ValuesSetContext ctx);
	/**
	 * Enter a parse tree produced by the {@code lengthSet}
	 * labeled alternative in {@link SuperScriptParser#arr}.
	 * @param ctx the parse tree
	 */
	void enterLengthSet(SuperScriptParser.LengthSetContext ctx);
	/**
	 * Exit a parse tree produced by the {@code lengthSet}
	 * labeled alternative in {@link SuperScriptParser#arr}.
	 * @param ctx the parse tree
	 */
	void exitLengthSet(SuperScriptParser.LengthSetContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#obj}.
	 * @param ctx the parse tree
	 */
	void enterObj(SuperScriptParser.ObjContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#obj}.
	 * @param ctx the parse tree
	 */
	void exitObj(SuperScriptParser.ObjContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#tup}.
	 * @param ctx the parse tree
	 */
	void enterTup(SuperScriptParser.TupContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#tup}.
	 * @param ctx the parse tree
	 */
	void exitTup(SuperScriptParser.TupContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#tupparam}.
	 * @param ctx the parse tree
	 */
	void enterTupparam(SuperScriptParser.TupparamContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#tupparam}.
	 * @param ctx the parse tree
	 */
	void exitTupparam(SuperScriptParser.TupparamContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#arrparam}.
	 * @param ctx the parse tree
	 */
	void enterArrparam(SuperScriptParser.ArrparamContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#arrparam}.
	 * @param ctx the parse tree
	 */
	void exitArrparam(SuperScriptParser.ArrparamContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#objparam}.
	 * @param ctx the parse tree
	 */
	void enterObjparam(SuperScriptParser.ObjparamContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#objparam}.
	 * @param ctx the parse tree
	 */
	void exitObjparam(SuperScriptParser.ObjparamContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#id}.
	 * @param ctx the parse tree
	 */
	void enterId(SuperScriptParser.IdContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#id}.
	 * @param ctx the parse tree
	 */
	void exitId(SuperScriptParser.IdContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#valdec}.
	 * @param ctx the parse tree
	 */
	void enterValdec(SuperScriptParser.ValdecContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#valdec}.
	 * @param ctx the parse tree
	 */
	void exitValdec(SuperScriptParser.ValdecContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#vardec}.
	 * @param ctx the parse tree
	 */
	void enterVardec(SuperScriptParser.VardecContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#vardec}.
	 * @param ctx the parse tree
	 */
	void exitVardec(SuperScriptParser.VardecContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#vartype}.
	 * @param ctx the parse tree
	 */
	void enterVartype(SuperScriptParser.VartypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#vartype}.
	 * @param ctx the parse tree
	 */
	void exitVartype(SuperScriptParser.VartypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#typevalue}.
	 * @param ctx the parse tree
	 */
	void enterTypevalue(SuperScriptParser.TypevalueContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#typevalue}.
	 * @param ctx the parse tree
	 */
	void exitTypevalue(SuperScriptParser.TypevalueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code typeFunction}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void enterTypeFunction(SuperScriptParser.TypeFunctionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code typeFunction}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void exitTypeFunction(SuperScriptParser.TypeFunctionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nameType}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void enterNameType(SuperScriptParser.NameTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nameType}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void exitNameType(SuperScriptParser.NameTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code typeExpand}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void enterTypeExpand(SuperScriptParser.TypeExpandContext ctx);
	/**
	 * Exit a parse tree produced by the {@code typeExpand}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void exitTypeExpand(SuperScriptParser.TypeExpandContext ctx);
	/**
	 * Enter a parse tree produced by the {@code typeArray}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void enterTypeArray(SuperScriptParser.TypeArrayContext ctx);
	/**
	 * Exit a parse tree produced by the {@code typeArray}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void exitTypeArray(SuperScriptParser.TypeArrayContext ctx);
	/**
	 * Enter a parse tree produced by the {@code noneType}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void enterNoneType(SuperScriptParser.NoneTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code noneType}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void exitNoneType(SuperScriptParser.NoneTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code typeTuple}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void enterTypeTuple(SuperScriptParser.TypeTupleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code typeTuple}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void exitTypeTuple(SuperScriptParser.TypeTupleContext ctx);
	/**
	 * Enter a parse tree produced by the {@code typeObject}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void enterTypeObject(SuperScriptParser.TypeObjectContext ctx);
	/**
	 * Exit a parse tree produced by the {@code typeObject}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void exitTypeObject(SuperScriptParser.TypeObjectContext ctx);
	/**
	 * Enter a parse tree produced by the {@code voidType}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void enterVoidType(SuperScriptParser.VoidTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code voidType}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void exitVoidType(SuperScriptParser.VoidTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code genericType}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void enterGenericType(SuperScriptParser.GenericTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code genericType}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void exitGenericType(SuperScriptParser.GenericTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code typeGenFunction}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void enterTypeGenFunction(SuperScriptParser.TypeGenFunctionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code typeGenFunction}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void exitTypeGenFunction(SuperScriptParser.TypeGenFunctionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unknownType}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void enterUnknownType(SuperScriptParser.UnknownTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unknownType}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void exitUnknownType(SuperScriptParser.UnknownTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parensType}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void enterParensType(SuperScriptParser.ParensTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parensType}
	 * labeled alternative in {@link SuperScriptParser#typedec}.
	 * @param ctx the parse tree
	 */
	void exitParensType(SuperScriptParser.ParensTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#functionname}.
	 * @param ctx the parse tree
	 */
	void enterFunctionname(SuperScriptParser.FunctionnameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#functionname}.
	 * @param ctx the parse tree
	 */
	void exitFunctionname(SuperScriptParser.FunctionnameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#descriptivefunctionname}.
	 * @param ctx the parse tree
	 */
	void enterDescriptivefunctionname(SuperScriptParser.DescriptivefunctionnameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#descriptivefunctionname}.
	 * @param ctx the parse tree
	 */
	void exitDescriptivefunctionname(SuperScriptParser.DescriptivefunctionnameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#typeobj}.
	 * @param ctx the parse tree
	 */
	void enterTypeobj(SuperScriptParser.TypeobjContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#typeobj}.
	 * @param ctx the parse tree
	 */
	void exitTypeobj(SuperScriptParser.TypeobjContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#typetup}.
	 * @param ctx the parse tree
	 */
	void enterTypetup(SuperScriptParser.TypetupContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#typetup}.
	 * @param ctx the parse tree
	 */
	void exitTypetup(SuperScriptParser.TypetupContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#typeobjparam}.
	 * @param ctx the parse tree
	 */
	void enterTypeobjparam(SuperScriptParser.TypeobjparamContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#typeobjparam}.
	 * @param ctx the parse tree
	 */
	void exitTypeobjparam(SuperScriptParser.TypeobjparamContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#typetupparam}.
	 * @param ctx the parse tree
	 */
	void enterTypetupparam(SuperScriptParser.TypetupparamContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#typetupparam}.
	 * @param ctx the parse tree
	 */
	void exitTypetupparam(SuperScriptParser.TypetupparamContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#gentype}.
	 * @param ctx the parse tree
	 */
	void enterGentype(SuperScriptParser.GentypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#gentype}.
	 * @param ctx the parse tree
	 */
	void exitGentype(SuperScriptParser.GentypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#declaredgentype}.
	 * @param ctx the parse tree
	 */
	void enterDeclaredgentype(SuperScriptParser.DeclaredgentypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#declaredgentype}.
	 * @param ctx the parse tree
	 */
	void exitDeclaredgentype(SuperScriptParser.DeclaredgentypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#generic}.
	 * @param ctx the parse tree
	 */
	void enterGeneric(SuperScriptParser.GenericContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#generic}.
	 * @param ctx the parse tree
	 */
	void exitGeneric(SuperScriptParser.GenericContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#functionoptions}.
	 * @param ctx the parse tree
	 */
	void enterFunctionoptions(SuperScriptParser.FunctionoptionsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#functionoptions}.
	 * @param ctx the parse tree
	 */
	void exitFunctionoptions(SuperScriptParser.FunctionoptionsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#infixop}.
	 * @param ctx the parse tree
	 */
	void enterInfixop(SuperScriptParser.InfixopContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#infixop}.
	 * @param ctx the parse tree
	 */
	void exitInfixop(SuperScriptParser.InfixopContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#prefixop}.
	 * @param ctx the parse tree
	 */
	void enterPrefixop(SuperScriptParser.PrefixopContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#prefixop}.
	 * @param ctx the parse tree
	 */
	void exitPrefixop(SuperScriptParser.PrefixopContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#postfixop}.
	 * @param ctx the parse tree
	 */
	void enterPostfixop(SuperScriptParser.PostfixopContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#postfixop}.
	 * @param ctx the parse tree
	 */
	void exitPostfixop(SuperScriptParser.PostfixopContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parens}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterParens(SuperScriptParser.ParensContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parens}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitParens(SuperScriptParser.ParensContext ctx);
	/**
	 * Enter a parse tree produced by the {@code access}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAccess(SuperScriptParser.AccessContext ctx);
	/**
	 * Exit a parse tree produced by the {@code access}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAccess(SuperScriptParser.AccessContext ctx);
	/**
	 * Enter a parse tree produced by the {@code declare}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterDeclare(SuperScriptParser.DeclareContext ctx);
	/**
	 * Exit a parse tree produced by the {@code declare}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitDeclare(SuperScriptParser.DeclareContext ctx);
	/**
	 * Enter a parse tree produced by the {@code exportObjs}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExportObjs(SuperScriptParser.ExportObjsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code exportObjs}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExportObjs(SuperScriptParser.ExportObjsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code prefix}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPrefix(SuperScriptParser.PrefixContext ctx);
	/**
	 * Exit a parse tree produced by the {@code prefix}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPrefix(SuperScriptParser.PrefixContext ctx);
	/**
	 * Enter a parse tree produced by the {@code for}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterFor(SuperScriptParser.ForContext ctx);
	/**
	 * Exit a parse tree produced by the {@code for}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitFor(SuperScriptParser.ForContext ctx);
	/**
	 * Enter a parse tree produced by the {@code generator}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterGenerator(SuperScriptParser.GeneratorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code generator}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitGenerator(SuperScriptParser.GeneratorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code importObjs}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterImportObjs(SuperScriptParser.ImportObjsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code importObjs}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitImportObjs(SuperScriptParser.ImportObjsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code while}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterWhile(SuperScriptParser.WhileContext ctx);
	/**
	 * Exit a parse tree produced by the {@code while}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitWhile(SuperScriptParser.WhileContext ctx);
	/**
	 * Enter a parse tree produced by the {@code literal}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(SuperScriptParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by the {@code literal}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(SuperScriptParser.LiteralContext ctx);
	/**
	 * Enter a parse tree produced by the {@code result}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterResult(SuperScriptParser.ResultContext ctx);
	/**
	 * Exit a parse tree produced by the {@code result}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitResult(SuperScriptParser.ResultContext ctx);
	/**
	 * Enter a parse tree produced by the {@code evaluation}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterEvaluation(SuperScriptParser.EvaluationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code evaluation}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitEvaluation(SuperScriptParser.EvaluationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code cast}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterCast(SuperScriptParser.CastContext ctx);
	/**
	 * Exit a parse tree produced by the {@code cast}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitCast(SuperScriptParser.CastContext ctx);
	/**
	 * Enter a parse tree produced by the {@code lambda}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLambda(SuperScriptParser.LambdaContext ctx);
	/**
	 * Exit a parse tree produced by the {@code lambda}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLambda(SuperScriptParser.LambdaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code varModify}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterVarModify(SuperScriptParser.VarModifyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code varModify}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitVarModify(SuperScriptParser.VarModifyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code yieldGen}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterYieldGen(SuperScriptParser.YieldGenContext ctx);
	/**
	 * Exit a parse tree produced by the {@code yieldGen}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitYieldGen(SuperScriptParser.YieldGenContext ctx);
	/**
	 * Enter a parse tree produced by the {@code continue}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterContinue(SuperScriptParser.ContinueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code continue}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitContinue(SuperScriptParser.ContinueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code yield}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterYield(SuperScriptParser.YieldContext ctx);
	/**
	 * Exit a parse tree produced by the {@code yield}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitYield(SuperScriptParser.YieldContext ctx);
	/**
	 * Enter a parse tree produced by the {@code function}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterFunction(SuperScriptParser.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code function}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitFunction(SuperScriptParser.FunctionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code await}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAwait(SuperScriptParser.AwaitContext ctx);
	/**
	 * Exit a parse tree produced by the {@code await}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAwait(SuperScriptParser.AwaitContext ctx);
	/**
	 * Enter a parse tree produced by the {@code refinement}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterRefinement(SuperScriptParser.RefinementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code refinement}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitRefinement(SuperScriptParser.RefinementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code has}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterHas(SuperScriptParser.HasContext ctx);
	/**
	 * Exit a parse tree produced by the {@code has}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitHas(SuperScriptParser.HasContext ctx);
	/**
	 * Enter a parse tree produced by the {@code postfix}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPostfix(SuperScriptParser.PostfixContext ctx);
	/**
	 * Exit a parse tree produced by the {@code postfix}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPostfix(SuperScriptParser.PostfixContext ctx);
	/**
	 * Enter a parse tree produced by the {@code if}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterIf(SuperScriptParser.IfContext ctx);
	/**
	 * Exit a parse tree produced by the {@code if}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitIf(SuperScriptParser.IfContext ctx);
	/**
	 * Enter a parse tree produced by the {@code varManagement}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterVarManagement(SuperScriptParser.VarManagementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code varManagement}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitVarManagement(SuperScriptParser.VarManagementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code chain}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterChain(SuperScriptParser.ChainContext ctx);
	/**
	 * Exit a parse tree produced by the {@code chain}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitChain(SuperScriptParser.ChainContext ctx);
	/**
	 * Enter a parse tree produced by the {@code break}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBreak(SuperScriptParser.BreakContext ctx);
	/**
	 * Exit a parse tree produced by the {@code break}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBreak(SuperScriptParser.BreakContext ctx);
	/**
	 * Enter a parse tree produced by the {@code blocks}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBlocks(SuperScriptParser.BlocksContext ctx);
	/**
	 * Exit a parse tree produced by the {@code blocks}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBlocks(SuperScriptParser.BlocksContext ctx);
	/**
	 * Enter a parse tree produced by the {@code forEach}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterForEach(SuperScriptParser.ForEachContext ctx);
	/**
	 * Exit a parse tree produced by the {@code forEach}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitForEach(SuperScriptParser.ForEachContext ctx);
	/**
	 * Enter a parse tree produced by the {@code lambdaGen}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLambdaGen(SuperScriptParser.LambdaGenContext ctx);
	/**
	 * Exit a parse tree produced by the {@code lambdaGen}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLambdaGen(SuperScriptParser.LambdaGenContext ctx);
	/**
	 * Enter a parse tree produced by the {@code match}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMatch(SuperScriptParser.MatchContext ctx);
	/**
	 * Exit a parse tree produced by the {@code match}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMatch(SuperScriptParser.MatchContext ctx);
	/**
	 * Enter a parse tree produced by the {@code infix}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterInfix(SuperScriptParser.InfixContext ctx);
	/**
	 * Exit a parse tree produced by the {@code infix}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitInfix(SuperScriptParser.InfixContext ctx);
	/**
	 * Enter a parse tree produced by the {@code setType}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterSetType(SuperScriptParser.SetTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code setType}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitSetType(SuperScriptParser.SetTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code pipelineOp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPipelineOp(SuperScriptParser.PipelineOpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code pipelineOp}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPipelineOp(SuperScriptParser.PipelineOpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code varset}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterVarset(SuperScriptParser.VarsetContext ctx);
	/**
	 * Exit a parse tree produced by the {@code varset}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitVarset(SuperScriptParser.VarsetContext ctx);
	/**
	 * Enter a parse tree produced by the {@code throw}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterThrow(SuperScriptParser.ThrowContext ctx);
	/**
	 * Exit a parse tree produced by the {@code throw}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitThrow(SuperScriptParser.ThrowContext ctx);
	/**
	 * Enter a parse tree produced by the {@code doWhile}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterDoWhile(SuperScriptParser.DoWhileContext ctx);
	/**
	 * Exit a parse tree produced by the {@code doWhile}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitDoWhile(SuperScriptParser.DoWhileContext ctx);
	/**
	 * Enter a parse tree produced by the {@code specificEvaluation}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterSpecificEvaluation(SuperScriptParser.SpecificEvaluationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code specificEvaluation}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitSpecificEvaluation(SuperScriptParser.SpecificEvaluationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code genericRefining}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterGenericRefining(SuperScriptParser.GenericRefiningContext ctx);
	/**
	 * Exit a parse tree produced by the {@code genericRefining}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitGenericRefining(SuperScriptParser.GenericRefiningContext ctx);
	/**
	 * Enter a parse tree produced by the {@code try}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterTry(SuperScriptParser.TryContext ctx);
	/**
	 * Exit a parse tree produced by the {@code try}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitTry(SuperScriptParser.TryContext ctx);
	/**
	 * Enter a parse tree produced by the {@code infixHas}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterInfixHas(SuperScriptParser.InfixHasContext ctx);
	/**
	 * Exit a parse tree produced by the {@code infixHas}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitInfixHas(SuperScriptParser.InfixHasContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ternary}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterTernary(SuperScriptParser.TernaryContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ternary}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitTernary(SuperScriptParser.TernaryContext ctx);
	/**
	 * Enter a parse tree produced by the {@code return}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterReturn(SuperScriptParser.ReturnContext ctx);
	/**
	 * Exit a parse tree produced by the {@code return}
	 * labeled alternative in {@link SuperScriptParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitReturn(SuperScriptParser.ReturnContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#matchresult}.
	 * @param ctx the parse tree
	 */
	void enterMatchresult(SuperScriptParser.MatchresultContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#matchresult}.
	 * @param ctx the parse tree
	 */
	void exitMatchresult(SuperScriptParser.MatchresultContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#urlexp}.
	 * @param ctx the parse tree
	 */
	void enterUrlexp(SuperScriptParser.UrlexpContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#urlexp}.
	 * @param ctx the parse tree
	 */
	void exitUrlexp(SuperScriptParser.UrlexpContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#urlname}.
	 * @param ctx the parse tree
	 */
	void enterUrlname(SuperScriptParser.UrlnameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#urlname}.
	 * @param ctx the parse tree
	 */
	void exitUrlname(SuperScriptParser.UrlnameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#specificfunctionarg}.
	 * @param ctx the parse tree
	 */
	void enterSpecificfunctionarg(SuperScriptParser.SpecificfunctionargContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#specificfunctionarg}.
	 * @param ctx the parse tree
	 */
	void exitSpecificfunctionarg(SuperScriptParser.SpecificfunctionargContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#functionarg}.
	 * @param ctx the parse tree
	 */
	void enterFunctionarg(SuperScriptParser.FunctionargContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#functionarg}.
	 * @param ctx the parse tree
	 */
	void exitFunctionarg(SuperScriptParser.FunctionargContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#cases}.
	 * @param ctx the parse tree
	 */
	void enterCases(SuperScriptParser.CasesContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#cases}.
	 * @param ctx the parse tree
	 */
	void exitCases(SuperScriptParser.CasesContext ctx);
	/**
	 * Enter a parse tree produced by the {@code caseExp}
	 * labeled alternative in {@link SuperScriptParser#acase}.
	 * @param ctx the parse tree
	 */
	void enterCaseExp(SuperScriptParser.CaseExpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code caseExp}
	 * labeled alternative in {@link SuperScriptParser#acase}.
	 * @param ctx the parse tree
	 */
	void exitCaseExp(SuperScriptParser.CaseExpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code caseDes}
	 * labeled alternative in {@link SuperScriptParser#acase}.
	 * @param ctx the parse tree
	 */
	void enterCaseDes(SuperScriptParser.CaseDesContext ctx);
	/**
	 * Exit a parse tree produced by the {@code caseDes}
	 * labeled alternative in {@link SuperScriptParser#acase}.
	 * @param ctx the parse tree
	 */
	void exitCaseDes(SuperScriptParser.CaseDesContext ctx);
	/**
	 * Enter a parse tree produced by the {@code caseDef}
	 * labeled alternative in {@link SuperScriptParser#acase}.
	 * @param ctx the parse tree
	 */
	void enterCaseDef(SuperScriptParser.CaseDefContext ctx);
	/**
	 * Exit a parse tree produced by the {@code caseDef}
	 * labeled alternative in {@link SuperScriptParser#acase}.
	 * @param ctx the parse tree
	 */
	void exitCaseDef(SuperScriptParser.CaseDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#args}.
	 * @param ctx the parse tree
	 */
	void enterArgs(SuperScriptParser.ArgsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#args}.
	 * @param ctx the parse tree
	 */
	void exitArgs(SuperScriptParser.ArgsContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#arg}.
	 * @param ctx the parse tree
	 */
	void enterArg(SuperScriptParser.ArgContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#arg}.
	 * @param ctx the parse tree
	 */
	void exitArg(SuperScriptParser.ArgContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#var}.
	 * @param ctx the parse tree
	 */
	void enterVar(SuperScriptParser.VarContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#var}.
	 * @param ctx the parse tree
	 */
	void exitVar(SuperScriptParser.VarContext ctx);
	/**
	 * Enter a parse tree produced by the {@code arrDestruct}
	 * labeled alternative in {@link SuperScriptParser#destruct}.
	 * @param ctx the parse tree
	 */
	void enterArrDestruct(SuperScriptParser.ArrDestructContext ctx);
	/**
	 * Exit a parse tree produced by the {@code arrDestruct}
	 * labeled alternative in {@link SuperScriptParser#destruct}.
	 * @param ctx the parse tree
	 */
	void exitArrDestruct(SuperScriptParser.ArrDestructContext ctx);
	/**
	 * Enter a parse tree produced by the {@code objDestructAndStore}
	 * labeled alternative in {@link SuperScriptParser#destruct}.
	 * @param ctx the parse tree
	 */
	void enterObjDestructAndStore(SuperScriptParser.ObjDestructAndStoreContext ctx);
	/**
	 * Exit a parse tree produced by the {@code objDestructAndStore}
	 * labeled alternative in {@link SuperScriptParser#destruct}.
	 * @param ctx the parse tree
	 */
	void exitObjDestructAndStore(SuperScriptParser.ObjDestructAndStoreContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tupDestruct}
	 * labeled alternative in {@link SuperScriptParser#destruct}.
	 * @param ctx the parse tree
	 */
	void enterTupDestruct(SuperScriptParser.TupDestructContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tupDestruct}
	 * labeled alternative in {@link SuperScriptParser#destruct}.
	 * @param ctx the parse tree
	 */
	void exitTupDestruct(SuperScriptParser.TupDestructContext ctx);
	/**
	 * Enter a parse tree produced by the {@code name}
	 * labeled alternative in {@link SuperScriptParser#destruct}.
	 * @param ctx the parse tree
	 */
	void enterName(SuperScriptParser.NameContext ctx);
	/**
	 * Exit a parse tree produced by the {@code name}
	 * labeled alternative in {@link SuperScriptParser#destruct}.
	 * @param ctx the parse tree
	 */
	void exitName(SuperScriptParser.NameContext ctx);
	/**
	 * Enter a parse tree produced by the {@code objDestruct}
	 * labeled alternative in {@link SuperScriptParser#destruct}.
	 * @param ctx the parse tree
	 */
	void enterObjDestruct(SuperScriptParser.ObjDestructContext ctx);
	/**
	 * Exit a parse tree produced by the {@code objDestruct}
	 * labeled alternative in {@link SuperScriptParser#destruct}.
	 * @param ctx the parse tree
	 */
	void exitObjDestruct(SuperScriptParser.ObjDestructContext ctx);
	/**
	 * Enter a parse tree produced by the {@code arrDestructAndStore}
	 * labeled alternative in {@link SuperScriptParser#destruct}.
	 * @param ctx the parse tree
	 */
	void enterArrDestructAndStore(SuperScriptParser.ArrDestructAndStoreContext ctx);
	/**
	 * Exit a parse tree produced by the {@code arrDestructAndStore}
	 * labeled alternative in {@link SuperScriptParser#destruct}.
	 * @param ctx the parse tree
	 */
	void exitArrDestructAndStore(SuperScriptParser.ArrDestructAndStoreContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tupDestructAndStore}
	 * labeled alternative in {@link SuperScriptParser#destruct}.
	 * @param ctx the parse tree
	 */
	void enterTupDestructAndStore(SuperScriptParser.TupDestructAndStoreContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tupDestructAndStore}
	 * labeled alternative in {@link SuperScriptParser#destruct}.
	 * @param ctx the parse tree
	 */
	void exitTupDestructAndStore(SuperScriptParser.TupDestructAndStoreContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#destructarrparam}.
	 * @param ctx the parse tree
	 */
	void enterDestructarrparam(SuperScriptParser.DestructarrparamContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#destructarrparam}.
	 * @param ctx the parse tree
	 */
	void exitDestructarrparam(SuperScriptParser.DestructarrparamContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#destructtupparam}.
	 * @param ctx the parse tree
	 */
	void enterDestructtupparam(SuperScriptParser.DestructtupparamContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#destructtupparam}.
	 * @param ctx the parse tree
	 */
	void exitDestructtupparam(SuperScriptParser.DestructtupparamContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#destructobjparam}.
	 * @param ctx the parse tree
	 */
	void enterDestructobjparam(SuperScriptParser.DestructobjparamContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#destructobjparam}.
	 * @param ctx the parse tree
	 */
	void exitDestructobjparam(SuperScriptParser.DestructobjparamContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#number}.
	 * @param ctx the parse tree
	 */
	void enterNumber(SuperScriptParser.NumberContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#number}.
	 * @param ctx the parse tree
	 */
	void exitNumber(SuperScriptParser.NumberContext ctx);
	/**
	 * Enter a parse tree produced by the {@code object}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void enterObject(SuperScriptParser.ObjectContext ctx);
	/**
	 * Exit a parse tree produced by the {@code object}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void exitObject(SuperScriptParser.ObjectContext ctx);
	/**
	 * Enter a parse tree produced by the {@code array}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void enterArray(SuperScriptParser.ArrayContext ctx);
	/**
	 * Exit a parse tree produced by the {@code array}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void exitArray(SuperScriptParser.ArrayContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tuple}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void enterTuple(SuperScriptParser.TupleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tuple}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void exitTuple(SuperScriptParser.TupleContext ctx);
	/**
	 * Enter a parse tree produced by the {@code templateString}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void enterTemplateString(SuperScriptParser.TemplateStringContext ctx);
	/**
	 * Exit a parse tree produced by the {@code templateString}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void exitTemplateString(SuperScriptParser.TemplateStringContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numb}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void enterNumb(SuperScriptParser.NumbContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numb}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void exitNumb(SuperScriptParser.NumbContext ctx);
	/**
	 * Enter a parse tree produced by the {@code string}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void enterString(SuperScriptParser.StringContext ctx);
	/**
	 * Exit a parse tree produced by the {@code string}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void exitString(SuperScriptParser.StringContext ctx);
	/**
	 * Enter a parse tree produced by the {@code char}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void enterChar(SuperScriptParser.CharContext ctx);
	/**
	 * Exit a parse tree produced by the {@code char}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void exitChar(SuperScriptParser.CharContext ctx);
	/**
	 * Enter a parse tree produced by the {@code true}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void enterTrue(SuperScriptParser.TrueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code true}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void exitTrue(SuperScriptParser.TrueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code false}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void enterFalse(SuperScriptParser.FalseContext ctx);
	/**
	 * Exit a parse tree produced by the {@code false}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void exitFalse(SuperScriptParser.FalseContext ctx);
	/**
	 * Enter a parse tree produced by the {@code NaN}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void enterNaN(SuperScriptParser.NaNContext ctx);
	/**
	 * Exit a parse tree produced by the {@code NaN}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void exitNaN(SuperScriptParser.NaNContext ctx);
	/**
	 * Enter a parse tree produced by the {@code this}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void enterThis(SuperScriptParser.ThisContext ctx);
	/**
	 * Exit a parse tree produced by the {@code this}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void exitThis(SuperScriptParser.ThisContext ctx);
	/**
	 * Enter a parse tree produced by the {@code super}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void enterSuper(SuperScriptParser.SuperContext ctx);
	/**
	 * Exit a parse tree produced by the {@code super}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void exitSuper(SuperScriptParser.SuperContext ctx);
	/**
	 * Enter a parse tree produced by the {@code self}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void enterSelf(SuperScriptParser.SelfContext ctx);
	/**
	 * Exit a parse tree produced by the {@code self}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void exitSelf(SuperScriptParser.SelfContext ctx);
	/**
	 * Enter a parse tree produced by the {@code void}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void enterVoid(SuperScriptParser.VoidContext ctx);
	/**
	 * Exit a parse tree produced by the {@code void}
	 * labeled alternative in {@link SuperScriptParser#lit}.
	 * @param ctx the parse tree
	 */
	void exitVoid(SuperScriptParser.VoidContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#templatestr}.
	 * @param ctx the parse tree
	 */
	void enterTemplatestr(SuperScriptParser.TemplatestrContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#templatestr}.
	 * @param ctx the parse tree
	 */
	void exitTemplatestr(SuperScriptParser.TemplatestrContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperScriptParser#exprsemi}.
	 * @param ctx the parse tree
	 */
	void enterExprsemi(SuperScriptParser.ExprsemiContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperScriptParser#exprsemi}.
	 * @param ctx the parse tree
	 */
	void exitExprsemi(SuperScriptParser.ExprsemiContext ctx);
}