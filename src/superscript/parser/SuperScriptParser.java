// Generated from SuperScriptParser.g4 by ANTLR 4.7.2
package superscript.parser;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SuperScriptParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		LCURLY=1, RCURLY=2, LPARAM=3, RPARAM=4, LANGLE=5, RANGLE=6, LBRACKET=7, 
		RBRACKET=8, TEMPLATESTART=9, GEQ=10, LEQ=11, SELF=12, FINAL=13, CONST=14, 
		LET=15, ASYNC=16, VOID=17, COLON=18, INC=19, DEC=20, PACKAGE=21, IMPORT=22, 
		EXPORT=23, GET=24, SET=25, TYPEOF=26, EXTENDS=27, ABSTRACT=28, PLUS=29, 
		MINUS=30, POW=31, MOD=32, MULT=33, DIV=34, INSTANCEOF=35, EQEQ=36, NQEQ=37, 
		NQEQEQ=38, EQEQEQ=39, BOTHAND=40, BOTHOR=41, BOTHXOR=42, AND=43, OR=44, 
		QUESTION=45, EQ=46, PLUSEQ=47, MINUSEQ=48, MULTEQ=49, DIVEQ=50, BOTHANDEQ=51, 
		BOTHOREQ=52, BOTHXOREQ=53, ANDEQ=54, OREQ=55, POWEQ=56, MODEQ=57, LSHIFTEQ=58, 
		RSHIFTEQ=59, UNSRSHIFTEQ=60, CHAIN=61, PIPELINE=62, PIPELINERESULT=63, 
		DECLARE=64, RETURN=65, YIELD=66, YIELDGEN=67, BREAK=68, CONTINUE=69, FUNCTION=70, 
		LAMBDA=71, ARROW=72, IF=73, ELSE=74, DO=75, WHILE=76, FOR=77, MATCH=78, 
		DEFAULT=79, REFINE=80, SEMI=81, COMMA=82, NOT=83, NOT2=84, RESTSPREAD=85, 
		AWAIT=86, HAS=87, TYPE=88, TRY=89, CATCH=90, FINALLY=91, THIS=92, SUPER=93, 
		NAN=94, THROW=95, BOOLEANTRUE=96, BOOLEANFALSE=97, STRING=98, SSDOC=99, 
		WS=100, NAME=101, CHAR=102, NUMBER=103, TEMPLATEEND=104, STRINGINTERP=105, 
		CHUNK=106, CHARACTER=107;
	public static final int
		RULE_file = 0, RULE_pkg = 1, RULE_url = 2, RULE_block = 3, RULE_arr = 4, 
		RULE_obj = 5, RULE_tup = 6, RULE_tupparam = 7, RULE_arrparam = 8, RULE_objparam = 9, 
		RULE_id = 10, RULE_valdec = 11, RULE_vardec = 12, RULE_vartype = 13, RULE_typevalue = 14, 
		RULE_typedec = 15, RULE_functionname = 16, RULE_descriptivefunctionname = 17, 
		RULE_typeobj = 18, RULE_typetup = 19, RULE_typeobjparam = 20, RULE_typetupparam = 21, 
		RULE_gentype = 22, RULE_declaredgentype = 23, RULE_generic = 24, RULE_functionoptions = 25, 
		RULE_infixop = 26, RULE_prefixop = 27, RULE_postfixop = 28, RULE_expression = 29, 
		RULE_matchresult = 30, RULE_urlexp = 31, RULE_urlname = 32, RULE_specificfunctionarg = 33, 
		RULE_functionarg = 34, RULE_cases = 35, RULE_acase = 36, RULE_args = 37, 
		RULE_arg = 38, RULE_var = 39, RULE_destruct = 40, RULE_destructarrparam = 41, 
		RULE_destructtupparam = 42, RULE_destructobjparam = 43, RULE_number = 44, 
		RULE_lit = 45, RULE_templatestr = 46, RULE_exprsemi = 47;
	private static String[] makeRuleNames() {
		return new String[] {
			"file", "pkg", "url", "block", "arr", "obj", "tup", "tupparam", "arrparam", 
			"objparam", "id", "valdec", "vardec", "vartype", "typevalue", "typedec", 
			"functionname", "descriptivefunctionname", "typeobj", "typetup", "typeobjparam", 
			"typetupparam", "gentype", "declaredgentype", "generic", "functionoptions", 
			"infixop", "prefixop", "postfixop", "expression", "matchresult", "urlexp", 
			"urlname", "specificfunctionarg", "functionarg", "cases", "acase", "args", 
			"arg", "var", "destruct", "destructarrparam", "destructtupparam", "destructobjparam", 
			"number", "lit", "templatestr", "exprsemi"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'{'", "'}'", "'('", "')'", "'<'", "'>'", "'['", "']'", null, "'>='", 
			"'<='", "'self'", "'final'", "'const'", "'let'", "'async'", "'void'", 
			"':'", "'++'", "'--'", "'package'", "'@import'", "'@export'", "'get'", 
			"'set'", "'typeof'", "'extends'", "'abstract'", "'+'", "'-'", "'**'", 
			"'%'", "'*'", "'/'", "'instanceof'", "'=='", "'!='", "'!=='", "'==='", 
			"'&'", "'|'", "'^'", "'&&'", "'||'", "'?'", "'='", "'+='", "'-='", "'*='", 
			"'/='", "'&='", "'|='", "'^='", "'&&='", "'||='", "'**='", "'%='", "'<<='", 
			"'>>='", "'>>>='", "'>>=='", "'|>'", "'#'", "'declare'", "'return'", 
			"'yield'", "'yield*'", "'break'", "'continue'", "'function'", "'=>'", 
			"'->'", "'if'", "'else'", "'do'", "'while'", "'for'", "'match'", "'default'", 
			"'.'", "';'", "','", "'!'", "'~'", "'...'", "'await'", "'has'", "'type'", 
			"'try'", "'catch'", "'finally'", "'this'", "'super'", "'NaN'", "'throw'", 
			"'true'", "'false'", null, null, null, null, null, null, null, "'${'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "LCURLY", "RCURLY", "LPARAM", "RPARAM", "LANGLE", "RANGLE", "LBRACKET", 
			"RBRACKET", "TEMPLATESTART", "GEQ", "LEQ", "SELF", "FINAL", "CONST", 
			"LET", "ASYNC", "VOID", "COLON", "INC", "DEC", "PACKAGE", "IMPORT", "EXPORT", 
			"GET", "SET", "TYPEOF", "EXTENDS", "ABSTRACT", "PLUS", "MINUS", "POW", 
			"MOD", "MULT", "DIV", "INSTANCEOF", "EQEQ", "NQEQ", "NQEQEQ", "EQEQEQ", 
			"BOTHAND", "BOTHOR", "BOTHXOR", "AND", "OR", "QUESTION", "EQ", "PLUSEQ", 
			"MINUSEQ", "MULTEQ", "DIVEQ", "BOTHANDEQ", "BOTHOREQ", "BOTHXOREQ", "ANDEQ", 
			"OREQ", "POWEQ", "MODEQ", "LSHIFTEQ", "RSHIFTEQ", "UNSRSHIFTEQ", "CHAIN", 
			"PIPELINE", "PIPELINERESULT", "DECLARE", "RETURN", "YIELD", "YIELDGEN", 
			"BREAK", "CONTINUE", "FUNCTION", "LAMBDA", "ARROW", "IF", "ELSE", "DO", 
			"WHILE", "FOR", "MATCH", "DEFAULT", "REFINE", "SEMI", "COMMA", "NOT", 
			"NOT2", "RESTSPREAD", "AWAIT", "HAS", "TYPE", "TRY", "CATCH", "FINALLY", 
			"THIS", "SUPER", "NAN", "THROW", "BOOLEANTRUE", "BOOLEANFALSE", "STRING", 
			"SSDOC", "WS", "NAME", "CHAR", "NUMBER", "TEMPLATEEND", "STRINGINTERP", 
			"CHUNK", "CHARACTER"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "SuperScriptParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public SuperScriptParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class FileContext extends ParserRuleContext {
		public PkgContext pkg() {
			return getRuleContext(PkgContext.class,0);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public TerminalNode EOF() { return getToken(SuperScriptParser.EOF, 0); }
		public FileContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_file; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterFile(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitFile(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitFile(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FileContext file() throws RecognitionException {
		FileContext _localctx = new FileContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_file);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(96);
			pkg();
			setState(97);
			block();
			setState(98);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PkgContext extends ParserRuleContext {
		public TerminalNode PACKAGE() { return getToken(SuperScriptParser.PACKAGE, 0); }
		public UrlContext url() {
			return getRuleContext(UrlContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(SuperScriptParser.SEMI, 0); }
		public PkgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pkg; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterPkg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitPkg(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitPkg(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PkgContext pkg() throws RecognitionException {
		PkgContext _localctx = new PkgContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_pkg);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(100);
			match(PACKAGE);
			setState(101);
			url();
			setState(102);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UrlContext extends ParserRuleContext {
		public List<TerminalNode> PIPELINERESULT() { return getTokens(SuperScriptParser.PIPELINERESULT); }
		public TerminalNode PIPELINERESULT(int i) {
			return getToken(SuperScriptParser.PIPELINERESULT, i);
		}
		public List<TerminalNode> NAME() { return getTokens(SuperScriptParser.NAME); }
		public TerminalNode NAME(int i) {
			return getToken(SuperScriptParser.NAME, i);
		}
		public List<TerminalNode> COLON() { return getTokens(SuperScriptParser.COLON); }
		public TerminalNode COLON(int i) {
			return getToken(SuperScriptParser.COLON, i);
		}
		public UrlContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_url; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterUrl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitUrl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitUrl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UrlContext url() throws RecognitionException {
		UrlContext _localctx = new UrlContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_url);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(104);
			match(PIPELINERESULT);
			setState(105);
			match(NAME);
			setState(110);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(106);
					match(COLON);
					setState(107);
					match(NAME);
					}
					} 
				}
				setState(112);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			}
			setState(117);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(113);
					match(PIPELINERESULT);
					setState(114);
					match(NAME);
					}
					} 
				}
				setState(119);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public List<TerminalNode> SEMI() { return getTokens(SuperScriptParser.SEMI); }
		public TerminalNode SEMI(int i) {
			return getToken(SuperScriptParser.SEMI, i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitBlock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(126);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LANGLE) | (1L << LBRACKET) | (1L << TEMPLATESTART) | (1L << SELF) | (1L << FINAL) | (1L << CONST) | (1L << LET) | (1L << ASYNC) | (1L << VOID) | (1L << INC) | (1L << DEC) | (1L << IMPORT) | (1L << EXPORT) | (1L << TYPEOF) | (1L << ABSTRACT) | (1L << PLUS) | (1L << MINUS) | (1L << QUESTION) | (1L << PIPELINERESULT))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (DECLARE - 64)) | (1L << (RETURN - 64)) | (1L << (YIELD - 64)) | (1L << (YIELDGEN - 64)) | (1L << (BREAK - 64)) | (1L << (CONTINUE - 64)) | (1L << (FUNCTION - 64)) | (1L << (IF - 64)) | (1L << (DO - 64)) | (1L << (WHILE - 64)) | (1L << (FOR - 64)) | (1L << (MATCH - 64)) | (1L << (SEMI - 64)) | (1L << (NOT - 64)) | (1L << (NOT2 - 64)) | (1L << (RESTSPREAD - 64)) | (1L << (AWAIT - 64)) | (1L << (HAS - 64)) | (1L << (TYPE - 64)) | (1L << (TRY - 64)) | (1L << (THIS - 64)) | (1L << (SUPER - 64)) | (1L << (NAN - 64)) | (1L << (THROW - 64)) | (1L << (BOOLEANTRUE - 64)) | (1L << (BOOLEANFALSE - 64)) | (1L << (STRING - 64)) | (1L << (NAME - 64)) | (1L << (CHAR - 64)) | (1L << (NUMBER - 64)))) != 0)) {
				{
				{
				setState(121);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LANGLE) | (1L << LBRACKET) | (1L << TEMPLATESTART) | (1L << SELF) | (1L << FINAL) | (1L << CONST) | (1L << LET) | (1L << ASYNC) | (1L << VOID) | (1L << INC) | (1L << DEC) | (1L << IMPORT) | (1L << EXPORT) | (1L << TYPEOF) | (1L << ABSTRACT) | (1L << PLUS) | (1L << MINUS) | (1L << QUESTION) | (1L << PIPELINERESULT))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (DECLARE - 64)) | (1L << (RETURN - 64)) | (1L << (YIELD - 64)) | (1L << (YIELDGEN - 64)) | (1L << (BREAK - 64)) | (1L << (CONTINUE - 64)) | (1L << (FUNCTION - 64)) | (1L << (IF - 64)) | (1L << (DO - 64)) | (1L << (WHILE - 64)) | (1L << (FOR - 64)) | (1L << (MATCH - 64)) | (1L << (NOT - 64)) | (1L << (NOT2 - 64)) | (1L << (RESTSPREAD - 64)) | (1L << (AWAIT - 64)) | (1L << (HAS - 64)) | (1L << (TYPE - 64)) | (1L << (TRY - 64)) | (1L << (THIS - 64)) | (1L << (SUPER - 64)) | (1L << (NAN - 64)) | (1L << (THROW - 64)) | (1L << (BOOLEANTRUE - 64)) | (1L << (BOOLEANFALSE - 64)) | (1L << (STRING - 64)) | (1L << (NAME - 64)) | (1L << (CHAR - 64)) | (1L << (NUMBER - 64)))) != 0)) {
					{
					setState(120);
					expression(0);
					}
				}

				setState(123);
				match(SEMI);
				}
				}
				setState(128);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrContext extends ParserRuleContext {
		public ArrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arr; }
	 
		public ArrContext() { }
		public void copyFrom(ArrContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class LengthSetContext extends ArrContext {
		public TypedecContext typedec() {
			return getRuleContext(TypedecContext.class,0);
		}
		public List<TerminalNode> LBRACKET() { return getTokens(SuperScriptParser.LBRACKET); }
		public TerminalNode LBRACKET(int i) {
			return getToken(SuperScriptParser.LBRACKET, i);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<TerminalNode> RBRACKET() { return getTokens(SuperScriptParser.RBRACKET); }
		public TerminalNode RBRACKET(int i) {
			return getToken(SuperScriptParser.RBRACKET, i);
		}
		public LengthSetContext(ArrContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterLengthSet(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitLengthSet(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitLengthSet(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ValuesSetContext extends ArrContext {
		public TypedecContext typedec() {
			return getRuleContext(TypedecContext.class,0);
		}
		public TerminalNode LBRACKET() { return getToken(SuperScriptParser.LBRACKET, 0); }
		public TerminalNode RBRACKET() { return getToken(SuperScriptParser.RBRACKET, 0); }
		public List<ArrparamContext> arrparam() {
			return getRuleContexts(ArrparamContext.class);
		}
		public ArrparamContext arrparam(int i) {
			return getRuleContext(ArrparamContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SuperScriptParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SuperScriptParser.COMMA, i);
		}
		public ValuesSetContext(ArrContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterValuesSet(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitValuesSet(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitValuesSet(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArrContext arr() throws RecognitionException {
		ArrContext _localctx = new ArrContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_arr);
		int _la;
		try {
			setState(152);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				_localctx = new ValuesSetContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(129);
				typedec(0);
				setState(130);
				match(LBRACKET);
				setState(132);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LANGLE) | (1L << LBRACKET) | (1L << TEMPLATESTART) | (1L << SELF) | (1L << FINAL) | (1L << CONST) | (1L << LET) | (1L << ASYNC) | (1L << VOID) | (1L << INC) | (1L << DEC) | (1L << IMPORT) | (1L << EXPORT) | (1L << TYPEOF) | (1L << ABSTRACT) | (1L << PLUS) | (1L << MINUS) | (1L << QUESTION) | (1L << PIPELINERESULT))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (DECLARE - 64)) | (1L << (RETURN - 64)) | (1L << (YIELD - 64)) | (1L << (YIELDGEN - 64)) | (1L << (BREAK - 64)) | (1L << (CONTINUE - 64)) | (1L << (FUNCTION - 64)) | (1L << (IF - 64)) | (1L << (DO - 64)) | (1L << (WHILE - 64)) | (1L << (FOR - 64)) | (1L << (MATCH - 64)) | (1L << (NOT - 64)) | (1L << (NOT2 - 64)) | (1L << (RESTSPREAD - 64)) | (1L << (AWAIT - 64)) | (1L << (HAS - 64)) | (1L << (TYPE - 64)) | (1L << (TRY - 64)) | (1L << (THIS - 64)) | (1L << (SUPER - 64)) | (1L << (NAN - 64)) | (1L << (THROW - 64)) | (1L << (BOOLEANTRUE - 64)) | (1L << (BOOLEANFALSE - 64)) | (1L << (STRING - 64)) | (1L << (NAME - 64)) | (1L << (CHAR - 64)) | (1L << (NUMBER - 64)))) != 0)) {
					{
					setState(131);
					arrparam();
					}
				}

				setState(140);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(134);
					match(COMMA);
					setState(136);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LANGLE) | (1L << LBRACKET) | (1L << TEMPLATESTART) | (1L << SELF) | (1L << FINAL) | (1L << CONST) | (1L << LET) | (1L << ASYNC) | (1L << VOID) | (1L << INC) | (1L << DEC) | (1L << IMPORT) | (1L << EXPORT) | (1L << TYPEOF) | (1L << ABSTRACT) | (1L << PLUS) | (1L << MINUS) | (1L << QUESTION) | (1L << PIPELINERESULT))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (DECLARE - 64)) | (1L << (RETURN - 64)) | (1L << (YIELD - 64)) | (1L << (YIELDGEN - 64)) | (1L << (BREAK - 64)) | (1L << (CONTINUE - 64)) | (1L << (FUNCTION - 64)) | (1L << (IF - 64)) | (1L << (DO - 64)) | (1L << (WHILE - 64)) | (1L << (FOR - 64)) | (1L << (MATCH - 64)) | (1L << (NOT - 64)) | (1L << (NOT2 - 64)) | (1L << (RESTSPREAD - 64)) | (1L << (AWAIT - 64)) | (1L << (HAS - 64)) | (1L << (TYPE - 64)) | (1L << (TRY - 64)) | (1L << (THIS - 64)) | (1L << (SUPER - 64)) | (1L << (NAN - 64)) | (1L << (THROW - 64)) | (1L << (BOOLEANTRUE - 64)) | (1L << (BOOLEANFALSE - 64)) | (1L << (STRING - 64)) | (1L << (NAME - 64)) | (1L << (CHAR - 64)) | (1L << (NUMBER - 64)))) != 0)) {
						{
						setState(135);
						arrparam();
						}
					}

					}
					}
					setState(142);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(143);
				match(RBRACKET);
				}
				break;
			case 2:
				_localctx = new LengthSetContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(145);
				typedec(0);
				setState(146);
				match(LBRACKET);
				setState(147);
				match(LBRACKET);
				setState(148);
				expression(0);
				setState(149);
				match(RBRACKET);
				setState(150);
				match(RBRACKET);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ObjContext extends ParserRuleContext {
		public TerminalNode LCURLY() { return getToken(SuperScriptParser.LCURLY, 0); }
		public TerminalNode RCURLY() { return getToken(SuperScriptParser.RCURLY, 0); }
		public TerminalNode ABSTRACT() { return getToken(SuperScriptParser.ABSTRACT, 0); }
		public List<ObjparamContext> objparam() {
			return getRuleContexts(ObjparamContext.class);
		}
		public ObjparamContext objparam(int i) {
			return getRuleContext(ObjparamContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SuperScriptParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SuperScriptParser.COMMA, i);
		}
		public ObjContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_obj; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterObj(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitObj(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitObj(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ObjContext obj() throws RecognitionException {
		ObjContext _localctx = new ObjContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_obj);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(155);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ABSTRACT) {
				{
				setState(154);
				match(ABSTRACT);
				}
			}

			setState(157);
			match(LCURLY);
			setState(159);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (((((_la - 14)) & ~0x3f) == 0 && ((1L << (_la - 14)) & ((1L << (CONST - 14)) | (1L << (LET - 14)) | (1L << (GET - 14)) | (1L << (SET - 14)) | (1L << (ABSTRACT - 14)) | (1L << (DECLARE - 14)))) != 0)) {
				{
				setState(158);
				objparam();
				}
			}

			setState(167);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(161);
				match(COMMA);
				setState(163);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (((((_la - 14)) & ~0x3f) == 0 && ((1L << (_la - 14)) & ((1L << (CONST - 14)) | (1L << (LET - 14)) | (1L << (GET - 14)) | (1L << (SET - 14)) | (1L << (ABSTRACT - 14)) | (1L << (DECLARE - 14)))) != 0)) {
					{
					setState(162);
					objparam();
					}
				}

				}
				}
				setState(169);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(170);
			match(RCURLY);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TupContext extends ParserRuleContext {
		public TerminalNode LPARAM() { return getToken(SuperScriptParser.LPARAM, 0); }
		public TerminalNode RPARAM() { return getToken(SuperScriptParser.RPARAM, 0); }
		public List<TupparamContext> tupparam() {
			return getRuleContexts(TupparamContext.class);
		}
		public TupparamContext tupparam(int i) {
			return getRuleContext(TupparamContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SuperScriptParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SuperScriptParser.COMMA, i);
		}
		public TupContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tup; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterTup(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitTup(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitTup(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TupContext tup() throws RecognitionException {
		TupContext _localctx = new TupContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_tup);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(172);
			match(LPARAM);
			setState(174);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LANGLE) | (1L << LBRACKET) | (1L << TEMPLATESTART) | (1L << SELF) | (1L << FINAL) | (1L << CONST) | (1L << LET) | (1L << ASYNC) | (1L << VOID) | (1L << INC) | (1L << DEC) | (1L << IMPORT) | (1L << EXPORT) | (1L << TYPEOF) | (1L << ABSTRACT) | (1L << PLUS) | (1L << MINUS) | (1L << QUESTION) | (1L << PIPELINERESULT))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (DECLARE - 64)) | (1L << (RETURN - 64)) | (1L << (YIELD - 64)) | (1L << (YIELDGEN - 64)) | (1L << (BREAK - 64)) | (1L << (CONTINUE - 64)) | (1L << (FUNCTION - 64)) | (1L << (IF - 64)) | (1L << (DO - 64)) | (1L << (WHILE - 64)) | (1L << (FOR - 64)) | (1L << (MATCH - 64)) | (1L << (NOT - 64)) | (1L << (NOT2 - 64)) | (1L << (RESTSPREAD - 64)) | (1L << (AWAIT - 64)) | (1L << (HAS - 64)) | (1L << (TYPE - 64)) | (1L << (TRY - 64)) | (1L << (THIS - 64)) | (1L << (SUPER - 64)) | (1L << (NAN - 64)) | (1L << (THROW - 64)) | (1L << (BOOLEANTRUE - 64)) | (1L << (BOOLEANFALSE - 64)) | (1L << (STRING - 64)) | (1L << (NAME - 64)) | (1L << (CHAR - 64)) | (1L << (NUMBER - 64)))) != 0)) {
				{
				setState(173);
				tupparam();
				}
			}

			setState(182);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(176);
				match(COMMA);
				setState(178);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LANGLE) | (1L << LBRACKET) | (1L << TEMPLATESTART) | (1L << SELF) | (1L << FINAL) | (1L << CONST) | (1L << LET) | (1L << ASYNC) | (1L << VOID) | (1L << INC) | (1L << DEC) | (1L << IMPORT) | (1L << EXPORT) | (1L << TYPEOF) | (1L << ABSTRACT) | (1L << PLUS) | (1L << MINUS) | (1L << QUESTION) | (1L << PIPELINERESULT))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (DECLARE - 64)) | (1L << (RETURN - 64)) | (1L << (YIELD - 64)) | (1L << (YIELDGEN - 64)) | (1L << (BREAK - 64)) | (1L << (CONTINUE - 64)) | (1L << (FUNCTION - 64)) | (1L << (IF - 64)) | (1L << (DO - 64)) | (1L << (WHILE - 64)) | (1L << (FOR - 64)) | (1L << (MATCH - 64)) | (1L << (NOT - 64)) | (1L << (NOT2 - 64)) | (1L << (RESTSPREAD - 64)) | (1L << (AWAIT - 64)) | (1L << (HAS - 64)) | (1L << (TYPE - 64)) | (1L << (TRY - 64)) | (1L << (THIS - 64)) | (1L << (SUPER - 64)) | (1L << (NAN - 64)) | (1L << (THROW - 64)) | (1L << (BOOLEANTRUE - 64)) | (1L << (BOOLEANFALSE - 64)) | (1L << (STRING - 64)) | (1L << (NAME - 64)) | (1L << (CHAR - 64)) | (1L << (NUMBER - 64)))) != 0)) {
					{
					setState(177);
					tupparam();
					}
				}

				}
				}
				setState(184);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(185);
			match(RPARAM);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TupparamContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TypedecContext typedec() {
			return getRuleContext(TypedecContext.class,0);
		}
		public TerminalNode DECLARE() { return getToken(SuperScriptParser.DECLARE, 0); }
		public TupparamContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tupparam; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterTupparam(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitTupparam(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitTupparam(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TupparamContext tupparam() throws RecognitionException {
		TupparamContext _localctx = new TupparamContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_tupparam);
		try {
			setState(193);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,16,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(188);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
				case 1:
					{
					setState(187);
					typedec(0);
					}
					break;
				}
				setState(190);
				expression(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(191);
				match(DECLARE);
				setState(192);
				typedec(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrparamContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode DECLARE() { return getToken(SuperScriptParser.DECLARE, 0); }
		public ArrparamContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arrparam; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterArrparam(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitArrparam(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitArrparam(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArrparamContext arrparam() throws RecognitionException {
		ArrparamContext _localctx = new ArrparamContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_arrparam);
		try {
			setState(197);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(195);
				expression(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(196);
				match(DECLARE);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ObjparamContext extends ParserRuleContext {
		public Token newvalue;
		public Token name;
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public List<TerminalNode> COLON() { return getTokens(SuperScriptParser.COLON); }
		public TerminalNode COLON(int i) {
			return getToken(SuperScriptParser.COLON, i);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode DECLARE() { return getToken(SuperScriptParser.DECLARE, 0); }
		public TerminalNode ABSTRACT() { return getToken(SuperScriptParser.ABSTRACT, 0); }
		public VartypeContext vartype() {
			return getRuleContext(VartypeContext.class,0);
		}
		public TerminalNode LBRACKET() { return getToken(SuperScriptParser.LBRACKET, 0); }
		public TerminalNode RBRACKET() { return getToken(SuperScriptParser.RBRACKET, 0); }
		public TerminalNode LPARAM() { return getToken(SuperScriptParser.LPARAM, 0); }
		public TypedecContext typedec() {
			return getRuleContext(TypedecContext.class,0);
		}
		public TerminalNode RPARAM() { return getToken(SuperScriptParser.RPARAM, 0); }
		public InfixopContext infixop() {
			return getRuleContext(InfixopContext.class,0);
		}
		public PrefixopContext prefixop() {
			return getRuleContext(PrefixopContext.class,0);
		}
		public PostfixopContext postfixop() {
			return getRuleContext(PostfixopContext.class,0);
		}
		public TerminalNode GET() { return getToken(SuperScriptParser.GET, 0); }
		public TypevalueContext typevalue() {
			return getRuleContext(TypevalueContext.class,0);
		}
		public TerminalNode SET() { return getToken(SuperScriptParser.SET, 0); }
		public List<TerminalNode> NAME() { return getTokens(SuperScriptParser.NAME); }
		public TerminalNode NAME(int i) {
			return getToken(SuperScriptParser.NAME, i);
		}
		public TerminalNode COMMA() { return getToken(SuperScriptParser.COMMA, 0); }
		public ObjparamContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_objparam; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterObjparam(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitObjparam(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitObjparam(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ObjparamContext objparam() throws RecognitionException {
		ObjparamContext _localctx = new ObjparamContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_objparam);
		try {
			setState(261);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,19,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(199);
				id();
				setState(200);
				match(COLON);
				setState(201);
				expression(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(203);
				match(DECLARE);
				setState(204);
				id();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(205);
				match(ABSTRACT);
				setState(206);
				id();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(207);
				vartype();
				setState(208);
				match(LBRACKET);
				setState(222);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
				case 1:
					{
					setState(209);
					match(LPARAM);
					setState(210);
					typedec(0);
					setState(211);
					match(RPARAM);
					}
					break;
				case 2:
					{
					setState(213);
					match(COLON);
					setState(214);
					infixop();
					setState(215);
					match(COLON);
					}
					break;
				case 3:
					{
					setState(217);
					prefixop();
					setState(218);
					match(COLON);
					}
					break;
				case 4:
					{
					setState(220);
					match(COLON);
					setState(221);
					postfixop();
					}
					break;
				}
				setState(224);
				match(RBRACKET);
				setState(225);
				match(COLON);
				setState(226);
				expression(0);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(228);
				match(GET);
				setState(229);
				typevalue();
				setState(230);
				match(LPARAM);
				setState(231);
				match(RPARAM);
				setState(232);
				match(COLON);
				setState(233);
				expression(0);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(235);
				match(SET);
				setState(236);
				typevalue();
				setState(237);
				match(LPARAM);
				setState(238);
				((ObjparamContext)_localctx).newvalue = match(NAME);
				setState(239);
				match(RPARAM);
				setState(240);
				match(COLON);
				setState(241);
				expression(0);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(243);
				match(GET);
				setState(244);
				typedec(0);
				setState(245);
				match(LPARAM);
				setState(246);
				((ObjparamContext)_localctx).name = match(NAME);
				setState(247);
				match(RPARAM);
				setState(248);
				match(COLON);
				setState(249);
				expression(0);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(251);
				match(SET);
				setState(252);
				typedec(0);
				setState(253);
				match(LPARAM);
				setState(254);
				((ObjparamContext)_localctx).name = match(NAME);
				setState(255);
				match(COMMA);
				setState(256);
				((ObjparamContext)_localctx).newvalue = match(NAME);
				setState(257);
				match(RPARAM);
				setState(258);
				match(COLON);
				setState(259);
				expression(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdContext extends ParserRuleContext {
		public VardecContext vardec() {
			return getRuleContext(VardecContext.class,0);
		}
		public TypevalueContext typevalue() {
			return getRuleContext(TypevalueContext.class,0);
		}
		public IdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_id; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterId(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitId(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitId(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IdContext id() throws RecognitionException {
		IdContext _localctx = new IdContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_id);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(263);
			vardec();
			setState(264);
			typevalue();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValdecContext extends ParserRuleContext {
		public TerminalNode FINAL() { return getToken(SuperScriptParser.FINAL, 0); }
		public ValdecContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valdec; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterValdec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitValdec(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitValdec(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValdecContext valdec() throws RecognitionException {
		ValdecContext _localctx = new ValdecContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_valdec);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(267);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==FINAL) {
				{
				setState(266);
				match(FINAL);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VardecContext extends ParserRuleContext {
		public TerminalNode CONST() { return getToken(SuperScriptParser.CONST, 0); }
		public TerminalNode LET() { return getToken(SuperScriptParser.LET, 0); }
		public VardecContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_vardec; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterVardec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitVardec(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitVardec(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VardecContext vardec() throws RecognitionException {
		VardecContext _localctx = new VardecContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_vardec);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(269);
			_la = _input.LA(1);
			if ( !(_la==CONST || _la==LET) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VartypeContext extends ParserRuleContext {
		public VardecContext vardec() {
			return getRuleContext(VardecContext.class,0);
		}
		public TypedecContext typedec() {
			return getRuleContext(TypedecContext.class,0);
		}
		public VartypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_vartype; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterVartype(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitVartype(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitVartype(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VartypeContext vartype() throws RecognitionException {
		VartypeContext _localctx = new VartypeContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_vartype);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(271);
			vardec();
			setState(272);
			typedec(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypevalueContext extends ParserRuleContext {
		public TypedecContext typedec() {
			return getRuleContext(TypedecContext.class,0);
		}
		public TerminalNode NAME() { return getToken(SuperScriptParser.NAME, 0); }
		public TypevalueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typevalue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterTypevalue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitTypevalue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitTypevalue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypevalueContext typevalue() throws RecognitionException {
		TypevalueContext _localctx = new TypevalueContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_typevalue);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(274);
			typedec(0);
			setState(275);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypedecContext extends ParserRuleContext {
		public TypedecContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typedec; }
	 
		public TypedecContext() { }
		public void copyFrom(TypedecContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class TypeFunctionContext extends TypedecContext {
		public TerminalNode LPARAM() { return getToken(SuperScriptParser.LPARAM, 0); }
		public TerminalNode RPARAM() { return getToken(SuperScriptParser.RPARAM, 0); }
		public TerminalNode LAMBDA() { return getToken(SuperScriptParser.LAMBDA, 0); }
		public TypedecContext typedec() {
			return getRuleContext(TypedecContext.class,0);
		}
		public DeclaredgentypeContext declaredgentype() {
			return getRuleContext(DeclaredgentypeContext.class,0);
		}
		public List<DescriptivefunctionnameContext> descriptivefunctionname() {
			return getRuleContexts(DescriptivefunctionnameContext.class);
		}
		public DescriptivefunctionnameContext descriptivefunctionname(int i) {
			return getRuleContext(DescriptivefunctionnameContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SuperScriptParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SuperScriptParser.COMMA, i);
		}
		public TypeFunctionContext(TypedecContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterTypeFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitTypeFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitTypeFunction(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NameTypeContext extends TypedecContext {
		public UrlContext url() {
			return getRuleContext(UrlContext.class,0);
		}
		public NameTypeContext(TypedecContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterNameType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitNameType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitNameType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TypeExpandContext extends TypedecContext {
		public TerminalNode RESTSPREAD() { return getToken(SuperScriptParser.RESTSPREAD, 0); }
		public TypedecContext typedec() {
			return getRuleContext(TypedecContext.class,0);
		}
		public TypeExpandContext(TypedecContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterTypeExpand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitTypeExpand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitTypeExpand(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TypeArrayContext extends TypedecContext {
		public TypedecContext typedec() {
			return getRuleContext(TypedecContext.class,0);
		}
		public TerminalNode LBRACKET() { return getToken(SuperScriptParser.LBRACKET, 0); }
		public TerminalNode RBRACKET() { return getToken(SuperScriptParser.RBRACKET, 0); }
		public TypeArrayContext(TypedecContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterTypeArray(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitTypeArray(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitTypeArray(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NoneTypeContext extends TypedecContext {
		public TerminalNode LANGLE() { return getToken(SuperScriptParser.LANGLE, 0); }
		public TerminalNode RANGLE() { return getToken(SuperScriptParser.RANGLE, 0); }
		public NoneTypeContext(TypedecContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterNoneType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitNoneType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitNoneType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TypeTupleContext extends TypedecContext {
		public TypetupContext typetup() {
			return getRuleContext(TypetupContext.class,0);
		}
		public TypeTupleContext(TypedecContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterTypeTuple(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitTypeTuple(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitTypeTuple(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TypeObjectContext extends TypedecContext {
		public ValdecContext valdec() {
			return getRuleContext(ValdecContext.class,0);
		}
		public TypeobjContext typeobj() {
			return getRuleContext(TypeobjContext.class,0);
		}
		public TypeObjectContext(TypedecContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterTypeObject(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitTypeObject(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitTypeObject(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VoidTypeContext extends TypedecContext {
		public TerminalNode VOID() { return getToken(SuperScriptParser.VOID, 0); }
		public VoidTypeContext(TypedecContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterVoidType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitVoidType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitVoidType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GenericTypeContext extends TypedecContext {
		public TypedecContext typedec() {
			return getRuleContext(TypedecContext.class,0);
		}
		public GentypeContext gentype() {
			return getRuleContext(GentypeContext.class,0);
		}
		public GenericTypeContext(TypedecContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterGenericType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitGenericType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitGenericType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TypeGenFunctionContext extends TypedecContext {
		public TerminalNode LPARAM() { return getToken(SuperScriptParser.LPARAM, 0); }
		public TerminalNode RPARAM() { return getToken(SuperScriptParser.RPARAM, 0); }
		public TerminalNode MULT() { return getToken(SuperScriptParser.MULT, 0); }
		public TerminalNode LAMBDA() { return getToken(SuperScriptParser.LAMBDA, 0); }
		public TypedecContext typedec() {
			return getRuleContext(TypedecContext.class,0);
		}
		public DeclaredgentypeContext declaredgentype() {
			return getRuleContext(DeclaredgentypeContext.class,0);
		}
		public List<DescriptivefunctionnameContext> descriptivefunctionname() {
			return getRuleContexts(DescriptivefunctionnameContext.class);
		}
		public DescriptivefunctionnameContext descriptivefunctionname(int i) {
			return getRuleContext(DescriptivefunctionnameContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SuperScriptParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SuperScriptParser.COMMA, i);
		}
		public TypeGenFunctionContext(TypedecContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterTypeGenFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitTypeGenFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitTypeGenFunction(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class UnknownTypeContext extends TypedecContext {
		public TerminalNode QUESTION() { return getToken(SuperScriptParser.QUESTION, 0); }
		public UnknownTypeContext(TypedecContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterUnknownType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitUnknownType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitUnknownType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParensTypeContext extends TypedecContext {
		public TerminalNode LPARAM() { return getToken(SuperScriptParser.LPARAM, 0); }
		public TypedecContext typedec() {
			return getRuleContext(TypedecContext.class,0);
		}
		public TerminalNode RPARAM() { return getToken(SuperScriptParser.RPARAM, 0); }
		public ParensTypeContext(TypedecContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterParensType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitParensType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitParensType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypedecContext typedec() throws RecognitionException {
		return typedec(0);
	}

	private TypedecContext typedec(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		TypedecContext _localctx = new TypedecContext(_ctx, _parentState);
		TypedecContext _prevctx = _localctx;
		int _startState = 30;
		enterRecursionRule(_localctx, 30, RULE_typedec, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(328);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,27,_ctx) ) {
			case 1:
				{
				_localctx = new ParensTypeContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(278);
				match(LPARAM);
				setState(279);
				typedec(0);
				setState(280);
				match(RPARAM);
				}
				break;
			case 2:
				{
				_localctx = new TypeObjectContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(282);
				valdec();
				setState(283);
				typeobj();
				}
				break;
			case 3:
				{
				_localctx = new TypeTupleContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(285);
				typetup();
				}
				break;
			case 4:
				{
				_localctx = new TypeFunctionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(287);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LANGLE) {
					{
					setState(286);
					declaredgentype();
					}
				}

				setState(289);
				match(LPARAM);
				setState(298);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LANGLE) | (1L << FINAL) | (1L << VOID) | (1L << QUESTION) | (1L << PIPELINERESULT))) != 0) || _la==RESTSPREAD) {
					{
					setState(290);
					descriptivefunctionname();
					setState(295);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(291);
						match(COMMA);
						setState(292);
						descriptivefunctionname();
						}
						}
						setState(297);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(300);
				match(RPARAM);
				setState(301);
				match(LAMBDA);
				setState(302);
				typedec(8);
				}
				break;
			case 5:
				{
				_localctx = new TypeGenFunctionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(304);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LANGLE) {
					{
					setState(303);
					declaredgentype();
					}
				}

				setState(306);
				match(LPARAM);
				setState(315);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LANGLE) | (1L << FINAL) | (1L << VOID) | (1L << QUESTION) | (1L << PIPELINERESULT))) != 0) || _la==RESTSPREAD) {
					{
					setState(307);
					descriptivefunctionname();
					setState(312);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(308);
						match(COMMA);
						setState(309);
						descriptivefunctionname();
						}
						}
						setState(314);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(317);
				match(RPARAM);
				setState(318);
				match(MULT);
				setState(319);
				match(LAMBDA);
				setState(320);
				typedec(7);
				}
				break;
			case 6:
				{
				_localctx = new VoidTypeContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(321);
				match(VOID);
				}
				break;
			case 7:
				{
				_localctx = new NoneTypeContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(322);
				match(LANGLE);
				setState(323);
				match(RANGLE);
				}
				break;
			case 8:
				{
				_localctx = new UnknownTypeContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(324);
				match(QUESTION);
				}
				break;
			case 9:
				{
				_localctx = new TypeExpandContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(325);
				match(RESTSPREAD);
				setState(326);
				typedec(2);
				}
				break;
			case 10:
				{
				_localctx = new NameTypeContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(327);
				url();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(337);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(335);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,28,_ctx) ) {
					case 1:
						{
						_localctx = new TypeArrayContext(new TypedecContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_typedec);
						setState(330);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(331);
						match(LBRACKET);
						setState(332);
						match(RBRACKET);
						}
						break;
					case 2:
						{
						_localctx = new GenericTypeContext(new TypedecContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_typedec);
						setState(333);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(334);
						gentype();
						}
						break;
					}
					} 
				}
				setState(339);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class FunctionnameContext extends ParserRuleContext {
		public TypedecContext typedec() {
			return getRuleContext(TypedecContext.class,0);
		}
		public TerminalNode COLON() { return getToken(SuperScriptParser.COLON, 0); }
		public TerminalNode NAME() { return getToken(SuperScriptParser.NAME, 0); }
		public FunctionnameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionname; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterFunctionname(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitFunctionname(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitFunctionname(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionnameContext functionname() throws RecognitionException {
		FunctionnameContext _localctx = new FunctionnameContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_functionname);
		int _la;
		try {
			setState(347);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,31,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(340);
				typedec(0);
				setState(342);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==NAME) {
					{
					setState(341);
					match(NAME);
					}
				}

				setState(344);
				match(COLON);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(346);
				typedec(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DescriptivefunctionnameContext extends ParserRuleContext {
		public TypedecContext typedec() {
			return getRuleContext(TypedecContext.class,0);
		}
		public TerminalNode NAME() { return getToken(SuperScriptParser.NAME, 0); }
		public DescriptivefunctionnameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_descriptivefunctionname; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterDescriptivefunctionname(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitDescriptivefunctionname(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitDescriptivefunctionname(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DescriptivefunctionnameContext descriptivefunctionname() throws RecognitionException {
		DescriptivefunctionnameContext _localctx = new DescriptivefunctionnameContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_descriptivefunctionname);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(349);
			typedec(0);
			setState(351);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==NAME) {
				{
				setState(350);
				match(NAME);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeobjContext extends ParserRuleContext {
		public TerminalNode LCURLY() { return getToken(SuperScriptParser.LCURLY, 0); }
		public TerminalNode RCURLY() { return getToken(SuperScriptParser.RCURLY, 0); }
		public List<TypeobjparamContext> typeobjparam() {
			return getRuleContexts(TypeobjparamContext.class);
		}
		public TypeobjparamContext typeobjparam(int i) {
			return getRuleContext(TypeobjparamContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SuperScriptParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SuperScriptParser.COMMA, i);
		}
		public TypeobjContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeobj; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterTypeobj(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitTypeobj(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitTypeobj(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeobjContext typeobj() throws RecognitionException {
		TypeobjContext _localctx = new TypeobjContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_typeobj);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(353);
			match(LCURLY);
			setState(355);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==CONST || _la==LET) {
				{
				setState(354);
				typeobjparam();
				}
			}

			setState(363);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(357);
				match(COMMA);
				setState(359);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==CONST || _la==LET) {
					{
					setState(358);
					typeobjparam();
					}
				}

				}
				}
				setState(365);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(366);
			match(RCURLY);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypetupContext extends ParserRuleContext {
		public TerminalNode LPARAM() { return getToken(SuperScriptParser.LPARAM, 0); }
		public TerminalNode RPARAM() { return getToken(SuperScriptParser.RPARAM, 0); }
		public List<TypetupparamContext> typetupparam() {
			return getRuleContexts(TypetupparamContext.class);
		}
		public TypetupparamContext typetupparam(int i) {
			return getRuleContext(TypetupparamContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SuperScriptParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SuperScriptParser.COMMA, i);
		}
		public TypetupContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typetup; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterTypetup(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitTypetup(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitTypetup(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypetupContext typetup() throws RecognitionException {
		TypetupContext _localctx = new TypetupContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_typetup);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(368);
			match(LPARAM);
			setState(370);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==CONST || _la==LET) {
				{
				setState(369);
				typetupparam();
				}
			}

			setState(378);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(372);
				match(COMMA);
				setState(374);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==CONST || _la==LET) {
					{
					setState(373);
					typetupparam();
					}
				}

				}
				}
				setState(380);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(381);
			match(RPARAM);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeobjparamContext extends ParserRuleContext {
		public VardecContext vardec() {
			return getRuleContext(VardecContext.class,0);
		}
		public TerminalNode NAME() { return getToken(SuperScriptParser.NAME, 0); }
		public TerminalNode COLON() { return getToken(SuperScriptParser.COLON, 0); }
		public TypedecContext typedec() {
			return getRuleContext(TypedecContext.class,0);
		}
		public TypeobjparamContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeobjparam; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterTypeobjparam(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitTypeobjparam(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitTypeobjparam(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeobjparamContext typeobjparam() throws RecognitionException {
		TypeobjparamContext _localctx = new TypeobjparamContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_typeobjparam);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(383);
			vardec();
			setState(384);
			match(NAME);
			setState(385);
			match(COLON);
			setState(386);
			typedec(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypetupparamContext extends ParserRuleContext {
		public VartypeContext vartype() {
			return getRuleContext(VartypeContext.class,0);
		}
		public VardecContext vardec() {
			return getRuleContext(VardecContext.class,0);
		}
		public TerminalNode TYPE() { return getToken(SuperScriptParser.TYPE, 0); }
		public TypetupparamContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typetupparam; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterTypetupparam(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitTypetupparam(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitTypetupparam(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypetupparamContext typetupparam() throws RecognitionException {
		TypetupparamContext _localctx = new TypetupparamContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_typetupparam);
		try {
			setState(392);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,39,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(388);
				vartype();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(389);
				vardec();
				setState(390);
				match(TYPE);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GentypeContext extends ParserRuleContext {
		public TerminalNode LANGLE() { return getToken(SuperScriptParser.LANGLE, 0); }
		public List<TypedecContext> typedec() {
			return getRuleContexts(TypedecContext.class);
		}
		public TypedecContext typedec(int i) {
			return getRuleContext(TypedecContext.class,i);
		}
		public TerminalNode RANGLE() { return getToken(SuperScriptParser.RANGLE, 0); }
		public List<TerminalNode> COMMA() { return getTokens(SuperScriptParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SuperScriptParser.COMMA, i);
		}
		public GentypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_gentype; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterGentype(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitGentype(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitGentype(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GentypeContext gentype() throws RecognitionException {
		GentypeContext _localctx = new GentypeContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_gentype);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(394);
			match(LANGLE);
			setState(395);
			typedec(0);
			setState(400);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(396);
				match(COMMA);
				setState(397);
				typedec(0);
				}
				}
				setState(402);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(403);
			match(RANGLE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclaredgentypeContext extends ParserRuleContext {
		public TerminalNode LANGLE() { return getToken(SuperScriptParser.LANGLE, 0); }
		public List<GenericContext> generic() {
			return getRuleContexts(GenericContext.class);
		}
		public GenericContext generic(int i) {
			return getRuleContext(GenericContext.class,i);
		}
		public TerminalNode RANGLE() { return getToken(SuperScriptParser.RANGLE, 0); }
		public List<TerminalNode> COMMA() { return getTokens(SuperScriptParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SuperScriptParser.COMMA, i);
		}
		public DeclaredgentypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declaredgentype; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterDeclaredgentype(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitDeclaredgentype(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitDeclaredgentype(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeclaredgentypeContext declaredgentype() throws RecognitionException {
		DeclaredgentypeContext _localctx = new DeclaredgentypeContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_declaredgentype);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(405);
			match(LANGLE);
			setState(406);
			generic();
			setState(411);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(407);
				match(COMMA);
				setState(408);
				generic();
				}
				}
				setState(413);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(414);
			match(RANGLE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GenericContext extends ParserRuleContext {
		public Token variance;
		public Token modify;
		public TypedecContext typedec;
		public List<TypedecContext> rules = new ArrayList<TypedecContext>();
		public List<TypedecContext> typedec() {
			return getRuleContexts(TypedecContext.class);
		}
		public TypedecContext typedec(int i) {
			return getRuleContext(TypedecContext.class,i);
		}
		public TerminalNode SUPER() { return getToken(SuperScriptParser.SUPER, 0); }
		public TerminalNode EXTENDS() { return getToken(SuperScriptParser.EXTENDS, 0); }
		public TerminalNode PLUS() { return getToken(SuperScriptParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(SuperScriptParser.MINUS, 0); }
		public List<TerminalNode> BOTHAND() { return getTokens(SuperScriptParser.BOTHAND); }
		public TerminalNode BOTHAND(int i) {
			return getToken(SuperScriptParser.BOTHAND, i);
		}
		public GenericContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_generic; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterGeneric(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitGeneric(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitGeneric(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GenericContext generic() throws RecognitionException {
		GenericContext _localctx = new GenericContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_generic);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(417);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==PLUS || _la==MINUS) {
				{
				setState(416);
				((GenericContext)_localctx).variance = _input.LT(1);
				_la = _input.LA(1);
				if ( !(_la==PLUS || _la==MINUS) ) {
					((GenericContext)_localctx).variance = (Token)_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(419);
			typedec(0);
			{
			setState(420);
			((GenericContext)_localctx).modify = _input.LT(1);
			_la = _input.LA(1);
			if ( !(_la==EXTENDS || _la==SUPER) ) {
				((GenericContext)_localctx).modify = (Token)_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(421);
			((GenericContext)_localctx).typedec = typedec(0);
			((GenericContext)_localctx).rules.add(((GenericContext)_localctx).typedec);
			setState(426);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==BOTHAND) {
				{
				{
				setState(422);
				match(BOTHAND);
				setState(423);
				((GenericContext)_localctx).typedec = typedec(0);
				((GenericContext)_localctx).rules.add(((GenericContext)_localctx).typedec);
				}
				}
				setState(428);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionoptionsContext extends ParserRuleContext {
		public TerminalNode ASYNC() { return getToken(SuperScriptParser.ASYNC, 0); }
		public FunctionoptionsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionoptions; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterFunctionoptions(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitFunctionoptions(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitFunctionoptions(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionoptionsContext functionoptions() throws RecognitionException {
		FunctionoptionsContext _localctx = new FunctionoptionsContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_functionoptions);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(430);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ASYNC) {
				{
				setState(429);
				match(ASYNC);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InfixopContext extends ParserRuleContext {
		public TerminalNode POW() { return getToken(SuperScriptParser.POW, 0); }
		public TerminalNode MOD() { return getToken(SuperScriptParser.MOD, 0); }
		public TerminalNode MULT() { return getToken(SuperScriptParser.MULT, 0); }
		public TerminalNode DIV() { return getToken(SuperScriptParser.DIV, 0); }
		public TerminalNode PLUS() { return getToken(SuperScriptParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(SuperScriptParser.MINUS, 0); }
		public List<TerminalNode> LANGLE() { return getTokens(SuperScriptParser.LANGLE); }
		public TerminalNode LANGLE(int i) {
			return getToken(SuperScriptParser.LANGLE, i);
		}
		public List<TerminalNode> RANGLE() { return getTokens(SuperScriptParser.RANGLE); }
		public TerminalNode RANGLE(int i) {
			return getToken(SuperScriptParser.RANGLE, i);
		}
		public TerminalNode LEQ() { return getToken(SuperScriptParser.LEQ, 0); }
		public TerminalNode GEQ() { return getToken(SuperScriptParser.GEQ, 0); }
		public TerminalNode EQEQ() { return getToken(SuperScriptParser.EQEQ, 0); }
		public TerminalNode NQEQ() { return getToken(SuperScriptParser.NQEQ, 0); }
		public TerminalNode BOTHAND() { return getToken(SuperScriptParser.BOTHAND, 0); }
		public TerminalNode BOTHOR() { return getToken(SuperScriptParser.BOTHOR, 0); }
		public TerminalNode BOTHXOR() { return getToken(SuperScriptParser.BOTHXOR, 0); }
		public InfixopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_infixop; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterInfixop(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitInfixop(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitInfixop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InfixopContext infixop() throws RecognitionException {
		InfixopContext _localctx = new InfixopContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_infixop);
		try {
			setState(454);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,45,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(432);
				match(POW);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(433);
				match(MOD);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(434);
				match(MULT);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(435);
				match(DIV);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(436);
				match(PLUS);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(437);
				match(MINUS);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(438);
				match(LANGLE);
				setState(439);
				match(LANGLE);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(440);
				match(RANGLE);
				setState(441);
				match(RANGLE);
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(442);
				match(RANGLE);
				setState(443);
				match(RANGLE);
				setState(444);
				match(RANGLE);
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(445);
				match(RANGLE);
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(446);
				match(LANGLE);
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(447);
				match(LEQ);
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(448);
				match(GEQ);
				}
				break;
			case 14:
				enterOuterAlt(_localctx, 14);
				{
				setState(449);
				match(EQEQ);
				}
				break;
			case 15:
				enterOuterAlt(_localctx, 15);
				{
				setState(450);
				match(NQEQ);
				}
				break;
			case 16:
				enterOuterAlt(_localctx, 16);
				{
				setState(451);
				match(BOTHAND);
				}
				break;
			case 17:
				enterOuterAlt(_localctx, 17);
				{
				setState(452);
				match(BOTHOR);
				}
				break;
			case 18:
				enterOuterAlt(_localctx, 18);
				{
				setState(453);
				match(BOTHXOR);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrefixopContext extends ParserRuleContext {
		public TerminalNode PLUS() { return getToken(SuperScriptParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(SuperScriptParser.MINUS, 0); }
		public TerminalNode INC() { return getToken(SuperScriptParser.INC, 0); }
		public TerminalNode DEC() { return getToken(SuperScriptParser.DEC, 0); }
		public TerminalNode NOT2() { return getToken(SuperScriptParser.NOT2, 0); }
		public PrefixopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prefixop; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterPrefixop(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitPrefixop(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitPrefixop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrefixopContext prefixop() throws RecognitionException {
		PrefixopContext _localctx = new PrefixopContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_prefixop);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(456);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INC) | (1L << DEC) | (1L << PLUS) | (1L << MINUS))) != 0) || _la==NOT2) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PostfixopContext extends ParserRuleContext {
		public TerminalNode INC() { return getToken(SuperScriptParser.INC, 0); }
		public TerminalNode DEC() { return getToken(SuperScriptParser.DEC, 0); }
		public PostfixopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_postfixop; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterPostfixop(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitPostfixop(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitPostfixop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PostfixopContext postfixop() throws RecognitionException {
		PostfixopContext _localctx = new PostfixopContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_postfixop);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(458);
			_la = _input.LA(1);
			if ( !(_la==INC || _la==DEC) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	 
		public ExpressionContext() { }
		public void copyFrom(ExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ParensContext extends ExpressionContext {
		public TerminalNode LPARAM() { return getToken(SuperScriptParser.LPARAM, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RPARAM() { return getToken(SuperScriptParser.RPARAM, 0); }
		public ParensContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterParens(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitParens(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitParens(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AccessContext extends ExpressionContext {
		public TerminalNode NAME() { return getToken(SuperScriptParser.NAME, 0); }
		public AccessContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterAccess(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitAccess(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitAccess(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DeclareContext extends ExpressionContext {
		public TerminalNode DECLARE() { return getToken(SuperScriptParser.DECLARE, 0); }
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(SuperScriptParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SuperScriptParser.COMMA, i);
		}
		public List<TypevalueContext> typevalue() {
			return getRuleContexts(TypevalueContext.class);
		}
		public TypevalueContext typevalue(int i) {
			return getRuleContext(TypevalueContext.class,i);
		}
		public DeclareContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterDeclare(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitDeclare(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitDeclare(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExportObjsContext extends ExpressionContext {
		public TerminalNode EXPORT() { return getToken(SuperScriptParser.EXPORT, 0); }
		public TerminalNode LCURLY() { return getToken(SuperScriptParser.LCURLY, 0); }
		public List<UrlexpContext> urlexp() {
			return getRuleContexts(UrlexpContext.class);
		}
		public UrlexpContext urlexp(int i) {
			return getRuleContext(UrlexpContext.class,i);
		}
		public TerminalNode RCURLY() { return getToken(SuperScriptParser.RCURLY, 0); }
		public List<TerminalNode> COMMA() { return getTokens(SuperScriptParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SuperScriptParser.COMMA, i);
		}
		public ExportObjsContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterExportObjs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitExportObjs(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitExportObjs(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PrefixContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode TYPEOF() { return getToken(SuperScriptParser.TYPEOF, 0); }
		public TerminalNode PLUS() { return getToken(SuperScriptParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(SuperScriptParser.MINUS, 0); }
		public TerminalNode INC() { return getToken(SuperScriptParser.INC, 0); }
		public TerminalNode DEC() { return getToken(SuperScriptParser.DEC, 0); }
		public TerminalNode NOT() { return getToken(SuperScriptParser.NOT, 0); }
		public TerminalNode NOT2() { return getToken(SuperScriptParser.NOT2, 0); }
		public PrefixContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterPrefix(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitPrefix(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitPrefix(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ForContext extends ExpressionContext {
		public ExpressionContext first;
		public ExpressionContext second;
		public ExpressionContext third;
		public ExpressionContext next;
		public TerminalNode FOR() { return getToken(SuperScriptParser.FOR, 0); }
		public TerminalNode LPARAM() { return getToken(SuperScriptParser.LPARAM, 0); }
		public List<TerminalNode> SEMI() { return getTokens(SuperScriptParser.SEMI); }
		public TerminalNode SEMI(int i) {
			return getToken(SuperScriptParser.SEMI, i);
		}
		public TerminalNode RPARAM() { return getToken(SuperScriptParser.RPARAM, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ForContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterFor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitFor(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitFor(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GeneratorContext extends ExpressionContext {
		public FunctionoptionsContext functionoptions() {
			return getRuleContext(FunctionoptionsContext.class,0);
		}
		public TerminalNode FUNCTION() { return getToken(SuperScriptParser.FUNCTION, 0); }
		public TerminalNode MULT() { return getToken(SuperScriptParser.MULT, 0); }
		public ArgsContext args() {
			return getRuleContext(ArgsContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public DeclaredgentypeContext declaredgentype() {
			return getRuleContext(DeclaredgentypeContext.class,0);
		}
		public GeneratorContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterGenerator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitGenerator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitGenerator(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ImportObjsContext extends ExpressionContext {
		public TerminalNode IMPORT() { return getToken(SuperScriptParser.IMPORT, 0); }
		public TerminalNode LCURLY() { return getToken(SuperScriptParser.LCURLY, 0); }
		public List<UrlnameContext> urlname() {
			return getRuleContexts(UrlnameContext.class);
		}
		public UrlnameContext urlname(int i) {
			return getRuleContext(UrlnameContext.class,i);
		}
		public TerminalNode RCURLY() { return getToken(SuperScriptParser.RCURLY, 0); }
		public List<TerminalNode> COMMA() { return getTokens(SuperScriptParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SuperScriptParser.COMMA, i);
		}
		public ImportObjsContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterImportObjs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitImportObjs(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitImportObjs(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class WhileContext extends ExpressionContext {
		public TerminalNode WHILE() { return getToken(SuperScriptParser.WHILE, 0); }
		public TerminalNode LPARAM() { return getToken(SuperScriptParser.LPARAM, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode RPARAM() { return getToken(SuperScriptParser.RPARAM, 0); }
		public WhileContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterWhile(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitWhile(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitWhile(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LiteralContext extends ExpressionContext {
		public LitContext lit() {
			return getRuleContext(LitContext.class,0);
		}
		public LiteralContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitLiteral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitLiteral(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ResultContext extends ExpressionContext {
		public TerminalNode PIPELINERESULT() { return getToken(SuperScriptParser.PIPELINERESULT, 0); }
		public ResultContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterResult(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitResult(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitResult(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EvaluationContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode LPARAM() { return getToken(SuperScriptParser.LPARAM, 0); }
		public TerminalNode RPARAM() { return getToken(SuperScriptParser.RPARAM, 0); }
		public List<FunctionargContext> functionarg() {
			return getRuleContexts(FunctionargContext.class);
		}
		public FunctionargContext functionarg(int i) {
			return getRuleContext(FunctionargContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SuperScriptParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SuperScriptParser.COMMA, i);
		}
		public EvaluationContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterEvaluation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitEvaluation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitEvaluation(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CastContext extends ExpressionContext {
		public TerminalNode LPARAM() { return getToken(SuperScriptParser.LPARAM, 0); }
		public TypedecContext typedec() {
			return getRuleContext(TypedecContext.class,0);
		}
		public TerminalNode RPARAM() { return getToken(SuperScriptParser.RPARAM, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public CastContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterCast(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitCast(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitCast(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LambdaContext extends ExpressionContext {
		public FunctionoptionsContext functionoptions() {
			return getRuleContext(FunctionoptionsContext.class,0);
		}
		public ArgsContext args() {
			return getRuleContext(ArgsContext.class,0);
		}
		public TerminalNode LAMBDA() { return getToken(SuperScriptParser.LAMBDA, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public DeclaredgentypeContext declaredgentype() {
			return getRuleContext(DeclaredgentypeContext.class,0);
		}
		public LambdaContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterLambda(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitLambda(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitLambda(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VarModifyContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode EQ() { return getToken(SuperScriptParser.EQ, 0); }
		public TerminalNode PLUSEQ() { return getToken(SuperScriptParser.PLUSEQ, 0); }
		public TerminalNode MINUSEQ() { return getToken(SuperScriptParser.MINUSEQ, 0); }
		public TerminalNode MULTEQ() { return getToken(SuperScriptParser.MULTEQ, 0); }
		public TerminalNode DIVEQ() { return getToken(SuperScriptParser.DIVEQ, 0); }
		public TerminalNode BOTHANDEQ() { return getToken(SuperScriptParser.BOTHANDEQ, 0); }
		public TerminalNode BOTHOREQ() { return getToken(SuperScriptParser.BOTHOREQ, 0); }
		public TerminalNode BOTHXOREQ() { return getToken(SuperScriptParser.BOTHXOREQ, 0); }
		public TerminalNode ANDEQ() { return getToken(SuperScriptParser.ANDEQ, 0); }
		public TerminalNode OREQ() { return getToken(SuperScriptParser.OREQ, 0); }
		public TerminalNode POWEQ() { return getToken(SuperScriptParser.POWEQ, 0); }
		public TerminalNode MODEQ() { return getToken(SuperScriptParser.MODEQ, 0); }
		public TerminalNode LSHIFTEQ() { return getToken(SuperScriptParser.LSHIFTEQ, 0); }
		public TerminalNode RSHIFTEQ() { return getToken(SuperScriptParser.RSHIFTEQ, 0); }
		public TerminalNode UNSRSHIFTEQ() { return getToken(SuperScriptParser.UNSRSHIFTEQ, 0); }
		public VarModifyContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterVarModify(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitVarModify(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitVarModify(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class YieldGenContext extends ExpressionContext {
		public TerminalNode YIELDGEN() { return getToken(SuperScriptParser.YIELDGEN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public YieldGenContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterYieldGen(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitYieldGen(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitYieldGen(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ContinueContext extends ExpressionContext {
		public TerminalNode CONTINUE() { return getToken(SuperScriptParser.CONTINUE, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ContinueContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterContinue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitContinue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitContinue(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class YieldContext extends ExpressionContext {
		public TerminalNode YIELD() { return getToken(SuperScriptParser.YIELD, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public YieldContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterYield(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitYield(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitYield(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FunctionContext extends ExpressionContext {
		public FunctionoptionsContext functionoptions() {
			return getRuleContext(FunctionoptionsContext.class,0);
		}
		public TerminalNode FUNCTION() { return getToken(SuperScriptParser.FUNCTION, 0); }
		public ArgsContext args() {
			return getRuleContext(ArgsContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public DeclaredgentypeContext declaredgentype() {
			return getRuleContext(DeclaredgentypeContext.class,0);
		}
		public FunctionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitFunction(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AwaitContext extends ExpressionContext {
		public TerminalNode AWAIT() { return getToken(SuperScriptParser.AWAIT, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public AwaitContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterAwait(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitAwait(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitAwait(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RefinementContext extends ExpressionContext {
		public ExpressionContext refine;
		public ExpressionContext refinement;
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode REFINE() { return getToken(SuperScriptParser.REFINE, 0); }
		public TerminalNode NAME() { return getToken(SuperScriptParser.NAME, 0); }
		public TerminalNode LBRACKET() { return getToken(SuperScriptParser.LBRACKET, 0); }
		public TerminalNode RBRACKET() { return getToken(SuperScriptParser.RBRACKET, 0); }
		public RefinementContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterRefinement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitRefinement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitRefinement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class HasContext extends ExpressionContext {
		public TerminalNode HAS() { return getToken(SuperScriptParser.HAS, 0); }
		public TerminalNode NAME() { return getToken(SuperScriptParser.NAME, 0); }
		public HasContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterHas(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitHas(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitHas(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PostfixContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode INC() { return getToken(SuperScriptParser.INC, 0); }
		public TerminalNode DEC() { return getToken(SuperScriptParser.DEC, 0); }
		public PostfixContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterPostfix(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitPostfix(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitPostfix(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IfContext extends ExpressionContext {
		public TerminalNode IF() { return getToken(SuperScriptParser.IF, 0); }
		public TerminalNode LPARAM() { return getToken(SuperScriptParser.LPARAM, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode RPARAM() { return getToken(SuperScriptParser.RPARAM, 0); }
		public TerminalNode ELSE() { return getToken(SuperScriptParser.ELSE, 0); }
		public IfContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterIf(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitIf(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitIf(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VarManagementContext extends ExpressionContext {
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public TerminalNode EQ() { return getToken(SuperScriptParser.EQ, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public VarManagementContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterVarManagement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitVarManagement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitVarManagement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ChainContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode CHAIN() { return getToken(SuperScriptParser.CHAIN, 0); }
		public ChainContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterChain(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitChain(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitChain(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BreakContext extends ExpressionContext {
		public TerminalNode BREAK() { return getToken(SuperScriptParser.BREAK, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public BreakContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterBreak(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitBreak(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitBreak(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BlocksContext extends ExpressionContext {
		public TerminalNode LCURLY() { return getToken(SuperScriptParser.LCURLY, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public TerminalNode RCURLY() { return getToken(SuperScriptParser.RCURLY, 0); }
		public BlocksContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterBlocks(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitBlocks(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitBlocks(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ForEachContext extends ExpressionContext {
		public TerminalNode FOR() { return getToken(SuperScriptParser.FOR, 0); }
		public TerminalNode LPARAM() { return getToken(SuperScriptParser.LPARAM, 0); }
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public TerminalNode COLON() { return getToken(SuperScriptParser.COLON, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode RPARAM() { return getToken(SuperScriptParser.RPARAM, 0); }
		public ForEachContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterForEach(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitForEach(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitForEach(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LambdaGenContext extends ExpressionContext {
		public FunctionoptionsContext functionoptions() {
			return getRuleContext(FunctionoptionsContext.class,0);
		}
		public ArgsContext args() {
			return getRuleContext(ArgsContext.class,0);
		}
		public TerminalNode MULT() { return getToken(SuperScriptParser.MULT, 0); }
		public TerminalNode LAMBDA() { return getToken(SuperScriptParser.LAMBDA, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public DeclaredgentypeContext declaredgentype() {
			return getRuleContext(DeclaredgentypeContext.class,0);
		}
		public LambdaGenContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterLambdaGen(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitLambdaGen(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitLambdaGen(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MatchContext extends ExpressionContext {
		public TerminalNode MATCH() { return getToken(SuperScriptParser.MATCH, 0); }
		public TerminalNode LPARAM() { return getToken(SuperScriptParser.LPARAM, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RPARAM() { return getToken(SuperScriptParser.RPARAM, 0); }
		public TerminalNode LCURLY() { return getToken(SuperScriptParser.LCURLY, 0); }
		public TerminalNode RCURLY() { return getToken(SuperScriptParser.RCURLY, 0); }
		public List<MatchresultContext> matchresult() {
			return getRuleContexts(MatchresultContext.class);
		}
		public MatchresultContext matchresult(int i) {
			return getRuleContext(MatchresultContext.class,i);
		}
		public List<TerminalNode> SEMI() { return getTokens(SuperScriptParser.SEMI); }
		public TerminalNode SEMI(int i) {
			return getToken(SuperScriptParser.SEMI, i);
		}
		public MatchContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterMatch(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitMatch(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitMatch(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class InfixContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode POW() { return getToken(SuperScriptParser.POW, 0); }
		public TerminalNode MOD() { return getToken(SuperScriptParser.MOD, 0); }
		public TerminalNode MULT() { return getToken(SuperScriptParser.MULT, 0); }
		public TerminalNode DIV() { return getToken(SuperScriptParser.DIV, 0); }
		public TerminalNode PLUS() { return getToken(SuperScriptParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(SuperScriptParser.MINUS, 0); }
		public List<TerminalNode> LANGLE() { return getTokens(SuperScriptParser.LANGLE); }
		public TerminalNode LANGLE(int i) {
			return getToken(SuperScriptParser.LANGLE, i);
		}
		public List<TerminalNode> RANGLE() { return getTokens(SuperScriptParser.RANGLE); }
		public TerminalNode RANGLE(int i) {
			return getToken(SuperScriptParser.RANGLE, i);
		}
		public TerminalNode LEQ() { return getToken(SuperScriptParser.LEQ, 0); }
		public TerminalNode GEQ() { return getToken(SuperScriptParser.GEQ, 0); }
		public TerminalNode INSTANCEOF() { return getToken(SuperScriptParser.INSTANCEOF, 0); }
		public TerminalNode EQEQ() { return getToken(SuperScriptParser.EQEQ, 0); }
		public TerminalNode NQEQ() { return getToken(SuperScriptParser.NQEQ, 0); }
		public TerminalNode EQEQEQ() { return getToken(SuperScriptParser.EQEQEQ, 0); }
		public TerminalNode NQEQEQ() { return getToken(SuperScriptParser.NQEQEQ, 0); }
		public TerminalNode BOTHAND() { return getToken(SuperScriptParser.BOTHAND, 0); }
		public TerminalNode BOTHOR() { return getToken(SuperScriptParser.BOTHOR, 0); }
		public TerminalNode BOTHXOR() { return getToken(SuperScriptParser.BOTHXOR, 0); }
		public TerminalNode AND() { return getToken(SuperScriptParser.AND, 0); }
		public TerminalNode OR() { return getToken(SuperScriptParser.OR, 0); }
		public InfixContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterInfix(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitInfix(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitInfix(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SetTypeContext extends ExpressionContext {
		public TerminalNode TYPE() { return getToken(SuperScriptParser.TYPE, 0); }
		public TerminalNode NAME() { return getToken(SuperScriptParser.NAME, 0); }
		public TerminalNode EQ() { return getToken(SuperScriptParser.EQ, 0); }
		public TypedecContext typedec() {
			return getRuleContext(TypedecContext.class,0);
		}
		public TerminalNode ABSTRACT() { return getToken(SuperScriptParser.ABSTRACT, 0); }
		public SetTypeContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterSetType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitSetType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitSetType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PipelineOpContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode PIPELINE() { return getToken(SuperScriptParser.PIPELINE, 0); }
		public PipelineOpContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterPipelineOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitPipelineOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitPipelineOp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VarsetContext extends ExpressionContext {
		public ExpressionContext first;
		public TypevalueContext typevalue;
		public List<TypevalueContext> values = new ArrayList<TypevalueContext>();
		public ExpressionContext expression;
		public List<ExpressionContext> further = new ArrayList<ExpressionContext>();
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public List<TerminalNode> EQ() { return getTokens(SuperScriptParser.EQ); }
		public TerminalNode EQ(int i) {
			return getToken(SuperScriptParser.EQ, i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SuperScriptParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SuperScriptParser.COMMA, i);
		}
		public List<TypevalueContext> typevalue() {
			return getRuleContexts(TypevalueContext.class);
		}
		public TypevalueContext typevalue(int i) {
			return getRuleContext(TypevalueContext.class,i);
		}
		public VarsetContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterVarset(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitVarset(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitVarset(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ThrowContext extends ExpressionContext {
		public TerminalNode THROW() { return getToken(SuperScriptParser.THROW, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ThrowContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterThrow(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitThrow(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitThrow(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DoWhileContext extends ExpressionContext {
		public TerminalNode DO() { return getToken(SuperScriptParser.DO, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode WHILE() { return getToken(SuperScriptParser.WHILE, 0); }
		public TerminalNode LPARAM() { return getToken(SuperScriptParser.LPARAM, 0); }
		public TerminalNode RPARAM() { return getToken(SuperScriptParser.RPARAM, 0); }
		public DoWhileContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterDoWhile(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitDoWhile(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitDoWhile(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SpecificEvaluationContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode LCURLY() { return getToken(SuperScriptParser.LCURLY, 0); }
		public TerminalNode RCURLY() { return getToken(SuperScriptParser.RCURLY, 0); }
		public List<SpecificfunctionargContext> specificfunctionarg() {
			return getRuleContexts(SpecificfunctionargContext.class);
		}
		public SpecificfunctionargContext specificfunctionarg(int i) {
			return getRuleContext(SpecificfunctionargContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SuperScriptParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SuperScriptParser.COMMA, i);
		}
		public SpecificEvaluationContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterSpecificEvaluation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitSpecificEvaluation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitSpecificEvaluation(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class GenericRefiningContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public GentypeContext gentype() {
			return getRuleContext(GentypeContext.class,0);
		}
		public GenericRefiningContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterGenericRefining(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitGenericRefining(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitGenericRefining(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TryContext extends ExpressionContext {
		public ExpressionContext attempt;
		public TypevalueContext typevalue;
		public List<TypevalueContext> errortype = new ArrayList<TypevalueContext>();
		public ExpressionContext expression;
		public List<ExpressionContext> errorexpression = new ArrayList<ExpressionContext>();
		public ExpressionContext end;
		public TerminalNode TRY() { return getToken(SuperScriptParser.TRY, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> LPARAM() { return getTokens(SuperScriptParser.LPARAM); }
		public TerminalNode LPARAM(int i) {
			return getToken(SuperScriptParser.LPARAM, i);
		}
		public List<TerminalNode> RPARAM() { return getTokens(SuperScriptParser.RPARAM); }
		public TerminalNode RPARAM(int i) {
			return getToken(SuperScriptParser.RPARAM, i);
		}
		public List<TerminalNode> CATCH() { return getTokens(SuperScriptParser.CATCH); }
		public TerminalNode CATCH(int i) {
			return getToken(SuperScriptParser.CATCH, i);
		}
		public TerminalNode FINALLY() { return getToken(SuperScriptParser.FINALLY, 0); }
		public List<TypevalueContext> typevalue() {
			return getRuleContexts(TypevalueContext.class);
		}
		public TypevalueContext typevalue(int i) {
			return getRuleContext(TypevalueContext.class,i);
		}
		public TryContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterTry(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitTry(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitTry(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class InfixHasContext extends ExpressionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode NAME() { return getToken(SuperScriptParser.NAME, 0); }
		public TerminalNode HAS() { return getToken(SuperScriptParser.HAS, 0); }
		public InfixHasContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterInfixHas(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitInfixHas(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitInfixHas(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TernaryContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode QUESTION() { return getToken(SuperScriptParser.QUESTION, 0); }
		public TerminalNode COLON() { return getToken(SuperScriptParser.COLON, 0); }
		public TernaryContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterTernary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitTernary(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitTernary(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ReturnContext extends ExpressionContext {
		public TerminalNode RETURN() { return getToken(SuperScriptParser.RETURN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ReturnContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterReturn(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitReturn(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitReturn(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 58;
		enterRecursionRule(_localctx, 58, RULE_expression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(681);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,68,_ctx) ) {
			case 1:
				{
				_localctx = new AccessContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(461);
				match(NAME);
				}
				break;
			case 2:
				{
				_localctx = new ResultContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(462);
				match(PIPELINERESULT);
				}
				break;
			case 3:
				{
				_localctx = new ParensContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(463);
				match(LPARAM);
				setState(464);
				expression(0);
				setState(465);
				match(RPARAM);
				}
				break;
			case 4:
				{
				_localctx = new LiteralContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(467);
				lit();
				}
				break;
			case 5:
				{
				_localctx = new PrefixContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(468);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INC) | (1L << DEC) | (1L << TYPEOF) | (1L << PLUS) | (1L << MINUS))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(469);
				expression(46);
				}
				break;
			case 6:
				{
				_localctx = new PrefixContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(470);
				_la = _input.LA(1);
				if ( !(_la==NOT || _la==NOT2) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(471);
				expression(45);
				}
				break;
			case 7:
				{
				_localctx = new HasContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(472);
				match(HAS);
				setState(473);
				match(NAME);
				}
				break;
			case 8:
				{
				_localctx = new CastContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(474);
				match(LPARAM);
				setState(475);
				typedec(0);
				setState(476);
				match(RPARAM);
				setState(477);
				expression(27);
				}
				break;
			case 9:
				{
				_localctx = new VarManagementContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(479);
				var();
				setState(480);
				match(EQ);
				setState(481);
				expression(26);
				}
				break;
			case 10:
				{
				_localctx = new VarsetContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(483);
				id();
				setState(484);
				match(EQ);
				setState(485);
				((VarsetContext)_localctx).first = expression(0);
				setState(493);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,46,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(486);
						match(COMMA);
						setState(487);
						((VarsetContext)_localctx).typevalue = typevalue();
						((VarsetContext)_localctx).values.add(((VarsetContext)_localctx).typevalue);
						setState(488);
						match(EQ);
						setState(489);
						((VarsetContext)_localctx).expression = expression(0);
						((VarsetContext)_localctx).further.add(((VarsetContext)_localctx).expression);
						}
						} 
					}
					setState(495);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,46,_ctx);
				}
				}
				break;
			case 11:
				{
				_localctx = new DeclareContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(496);
				match(DECLARE);
				setState(497);
				id();
				setState(502);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,47,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(498);
						match(COMMA);
						setState(499);
						typevalue();
						}
						} 
					}
					setState(504);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,47,_ctx);
				}
				}
				break;
			case 12:
				{
				_localctx = new SetTypeContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(505);
				match(TYPE);
				setState(506);
				match(NAME);
				setState(507);
				match(EQ);
				setState(509);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==ABSTRACT) {
					{
					setState(508);
					match(ABSTRACT);
					}
				}

				setState(511);
				typedec(0);
				}
				break;
			case 13:
				{
				_localctx = new ImportObjsContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(512);
				match(IMPORT);
				setState(513);
				match(LCURLY);
				setState(514);
				urlname();
				setState(519);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(515);
					match(COMMA);
					setState(516);
					urlname();
					}
					}
					setState(521);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(522);
				match(RCURLY);
				}
				break;
			case 14:
				{
				_localctx = new ExportObjsContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(524);
				match(EXPORT);
				setState(525);
				match(LCURLY);
				setState(526);
				urlexp();
				setState(531);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(527);
					match(COMMA);
					setState(528);
					urlexp();
					}
					}
					setState(533);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(534);
				match(RCURLY);
				}
				break;
			case 15:
				{
				_localctx = new ReturnContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(536);
				match(RETURN);
				setState(538);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,51,_ctx) ) {
				case 1:
					{
					setState(537);
					expression(0);
					}
					break;
				}
				}
				break;
			case 16:
				{
				_localctx = new YieldContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(540);
				match(YIELD);
				setState(542);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,52,_ctx) ) {
				case 1:
					{
					setState(541);
					expression(0);
					}
					break;
				}
				}
				break;
			case 17:
				{
				_localctx = new YieldGenContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(544);
				match(YIELDGEN);
				setState(545);
				expression(17);
				}
				break;
			case 18:
				{
				_localctx = new BreakContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(546);
				match(BREAK);
				setState(548);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,53,_ctx) ) {
				case 1:
					{
					setState(547);
					expression(0);
					}
					break;
				}
				}
				break;
			case 19:
				{
				_localctx = new ContinueContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(550);
				match(CONTINUE);
				setState(552);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,54,_ctx) ) {
				case 1:
					{
					setState(551);
					expression(0);
					}
					break;
				}
				}
				break;
			case 20:
				{
				_localctx = new ThrowContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(554);
				match(THROW);
				setState(555);
				expression(14);
				}
				break;
			case 21:
				{
				_localctx = new AwaitContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(556);
				match(AWAIT);
				setState(557);
				expression(13);
				}
				break;
			case 22:
				{
				_localctx = new FunctionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(558);
				functionoptions();
				setState(559);
				match(FUNCTION);
				setState(561);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LANGLE) {
					{
					setState(560);
					declaredgentype();
					}
				}

				setState(563);
				args();
				setState(564);
				expression(12);
				}
				break;
			case 23:
				{
				_localctx = new GeneratorContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(566);
				functionoptions();
				setState(567);
				match(FUNCTION);
				setState(568);
				match(MULT);
				setState(570);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LANGLE) {
					{
					setState(569);
					declaredgentype();
					}
				}

				setState(572);
				args();
				setState(573);
				expression(11);
				}
				break;
			case 24:
				{
				_localctx = new LambdaContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(575);
				functionoptions();
				setState(577);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LANGLE) {
					{
					setState(576);
					declaredgentype();
					}
				}

				setState(579);
				args();
				setState(580);
				match(LAMBDA);
				setState(581);
				expression(10);
				}
				break;
			case 25:
				{
				_localctx = new LambdaGenContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(583);
				functionoptions();
				setState(585);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LANGLE) {
					{
					setState(584);
					declaredgentype();
					}
				}

				setState(587);
				args();
				setState(588);
				match(MULT);
				setState(589);
				match(LAMBDA);
				setState(590);
				expression(9);
				}
				break;
			case 26:
				{
				_localctx = new BlocksContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(592);
				match(LCURLY);
				setState(593);
				block();
				setState(594);
				match(RCURLY);
				}
				break;
			case 27:
				{
				_localctx = new IfContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(596);
				match(IF);
				setState(597);
				match(LPARAM);
				setState(598);
				expression(0);
				setState(599);
				match(RPARAM);
				setState(600);
				expression(0);
				setState(603);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,59,_ctx) ) {
				case 1:
					{
					setState(601);
					match(ELSE);
					setState(602);
					expression(0);
					}
					break;
				}
				}
				break;
			case 28:
				{
				_localctx = new DoWhileContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(605);
				match(DO);
				setState(606);
				expression(0);
				setState(607);
				match(WHILE);
				setState(608);
				match(LPARAM);
				setState(609);
				expression(0);
				setState(610);
				match(RPARAM);
				}
				break;
			case 29:
				{
				_localctx = new WhileContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(612);
				match(WHILE);
				setState(613);
				match(LPARAM);
				setState(614);
				expression(0);
				setState(615);
				match(RPARAM);
				setState(616);
				expression(5);
				}
				break;
			case 30:
				{
				_localctx = new ForContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(618);
				match(FOR);
				setState(619);
				match(LPARAM);
				setState(621);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LANGLE) | (1L << LBRACKET) | (1L << TEMPLATESTART) | (1L << SELF) | (1L << FINAL) | (1L << CONST) | (1L << LET) | (1L << ASYNC) | (1L << VOID) | (1L << INC) | (1L << DEC) | (1L << IMPORT) | (1L << EXPORT) | (1L << TYPEOF) | (1L << ABSTRACT) | (1L << PLUS) | (1L << MINUS) | (1L << QUESTION) | (1L << PIPELINERESULT))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (DECLARE - 64)) | (1L << (RETURN - 64)) | (1L << (YIELD - 64)) | (1L << (YIELDGEN - 64)) | (1L << (BREAK - 64)) | (1L << (CONTINUE - 64)) | (1L << (FUNCTION - 64)) | (1L << (IF - 64)) | (1L << (DO - 64)) | (1L << (WHILE - 64)) | (1L << (FOR - 64)) | (1L << (MATCH - 64)) | (1L << (NOT - 64)) | (1L << (NOT2 - 64)) | (1L << (RESTSPREAD - 64)) | (1L << (AWAIT - 64)) | (1L << (HAS - 64)) | (1L << (TYPE - 64)) | (1L << (TRY - 64)) | (1L << (THIS - 64)) | (1L << (SUPER - 64)) | (1L << (NAN - 64)) | (1L << (THROW - 64)) | (1L << (BOOLEANTRUE - 64)) | (1L << (BOOLEANFALSE - 64)) | (1L << (STRING - 64)) | (1L << (NAME - 64)) | (1L << (CHAR - 64)) | (1L << (NUMBER - 64)))) != 0)) {
					{
					setState(620);
					((ForContext)_localctx).first = expression(0);
					}
				}

				setState(623);
				match(SEMI);
				setState(625);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LANGLE) | (1L << LBRACKET) | (1L << TEMPLATESTART) | (1L << SELF) | (1L << FINAL) | (1L << CONST) | (1L << LET) | (1L << ASYNC) | (1L << VOID) | (1L << INC) | (1L << DEC) | (1L << IMPORT) | (1L << EXPORT) | (1L << TYPEOF) | (1L << ABSTRACT) | (1L << PLUS) | (1L << MINUS) | (1L << QUESTION) | (1L << PIPELINERESULT))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (DECLARE - 64)) | (1L << (RETURN - 64)) | (1L << (YIELD - 64)) | (1L << (YIELDGEN - 64)) | (1L << (BREAK - 64)) | (1L << (CONTINUE - 64)) | (1L << (FUNCTION - 64)) | (1L << (IF - 64)) | (1L << (DO - 64)) | (1L << (WHILE - 64)) | (1L << (FOR - 64)) | (1L << (MATCH - 64)) | (1L << (NOT - 64)) | (1L << (NOT2 - 64)) | (1L << (RESTSPREAD - 64)) | (1L << (AWAIT - 64)) | (1L << (HAS - 64)) | (1L << (TYPE - 64)) | (1L << (TRY - 64)) | (1L << (THIS - 64)) | (1L << (SUPER - 64)) | (1L << (NAN - 64)) | (1L << (THROW - 64)) | (1L << (BOOLEANTRUE - 64)) | (1L << (BOOLEANFALSE - 64)) | (1L << (STRING - 64)) | (1L << (NAME - 64)) | (1L << (CHAR - 64)) | (1L << (NUMBER - 64)))) != 0)) {
					{
					setState(624);
					((ForContext)_localctx).second = expression(0);
					}
				}

				setState(627);
				match(SEMI);
				setState(629);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LANGLE) | (1L << LBRACKET) | (1L << TEMPLATESTART) | (1L << SELF) | (1L << FINAL) | (1L << CONST) | (1L << LET) | (1L << ASYNC) | (1L << VOID) | (1L << INC) | (1L << DEC) | (1L << IMPORT) | (1L << EXPORT) | (1L << TYPEOF) | (1L << ABSTRACT) | (1L << PLUS) | (1L << MINUS) | (1L << QUESTION) | (1L << PIPELINERESULT))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (DECLARE - 64)) | (1L << (RETURN - 64)) | (1L << (YIELD - 64)) | (1L << (YIELDGEN - 64)) | (1L << (BREAK - 64)) | (1L << (CONTINUE - 64)) | (1L << (FUNCTION - 64)) | (1L << (IF - 64)) | (1L << (DO - 64)) | (1L << (WHILE - 64)) | (1L << (FOR - 64)) | (1L << (MATCH - 64)) | (1L << (NOT - 64)) | (1L << (NOT2 - 64)) | (1L << (RESTSPREAD - 64)) | (1L << (AWAIT - 64)) | (1L << (HAS - 64)) | (1L << (TYPE - 64)) | (1L << (TRY - 64)) | (1L << (THIS - 64)) | (1L << (SUPER - 64)) | (1L << (NAN - 64)) | (1L << (THROW - 64)) | (1L << (BOOLEANTRUE - 64)) | (1L << (BOOLEANFALSE - 64)) | (1L << (STRING - 64)) | (1L << (NAME - 64)) | (1L << (CHAR - 64)) | (1L << (NUMBER - 64)))) != 0)) {
					{
					setState(628);
					((ForContext)_localctx).third = expression(0);
					}
				}

				setState(631);
				match(RPARAM);
				setState(632);
				((ForContext)_localctx).next = expression(4);
				}
				break;
			case 31:
				{
				_localctx = new ForEachContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(633);
				match(FOR);
				setState(634);
				match(LPARAM);
				setState(635);
				id();
				setState(636);
				match(COLON);
				setState(637);
				expression(0);
				setState(638);
				match(RPARAM);
				setState(639);
				expression(3);
				}
				break;
			case 32:
				{
				_localctx = new MatchContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(641);
				match(MATCH);
				setState(642);
				match(LPARAM);
				setState(643);
				expression(0);
				setState(644);
				match(RPARAM);
				setState(645);
				match(LCURLY);
				setState(654);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LANGLE) | (1L << LBRACKET) | (1L << TEMPLATESTART) | (1L << SELF) | (1L << FINAL) | (1L << CONST) | (1L << LET) | (1L << ASYNC) | (1L << VOID) | (1L << INC) | (1L << DEC) | (1L << IMPORT) | (1L << EXPORT) | (1L << TYPEOF) | (1L << ABSTRACT) | (1L << PLUS) | (1L << MINUS) | (1L << QUESTION) | (1L << PIPELINERESULT))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (DECLARE - 64)) | (1L << (RETURN - 64)) | (1L << (YIELD - 64)) | (1L << (YIELDGEN - 64)) | (1L << (BREAK - 64)) | (1L << (CONTINUE - 64)) | (1L << (FUNCTION - 64)) | (1L << (IF - 64)) | (1L << (DO - 64)) | (1L << (WHILE - 64)) | (1L << (FOR - 64)) | (1L << (MATCH - 64)) | (1L << (DEFAULT - 64)) | (1L << (NOT - 64)) | (1L << (NOT2 - 64)) | (1L << (RESTSPREAD - 64)) | (1L << (AWAIT - 64)) | (1L << (HAS - 64)) | (1L << (TYPE - 64)) | (1L << (TRY - 64)) | (1L << (THIS - 64)) | (1L << (SUPER - 64)) | (1L << (NAN - 64)) | (1L << (THROW - 64)) | (1L << (BOOLEANTRUE - 64)) | (1L << (BOOLEANFALSE - 64)) | (1L << (STRING - 64)) | (1L << (NAME - 64)) | (1L << (CHAR - 64)) | (1L << (NUMBER - 64)))) != 0)) {
					{
					setState(646);
					matchresult();
					setState(651);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==SEMI) {
						{
						{
						setState(647);
						match(SEMI);
						setState(648);
						matchresult();
						}
						}
						setState(653);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(656);
				match(RCURLY);
				}
				break;
			case 33:
				{
				_localctx = new TryContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(658);
				match(TRY);
				setState(663);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,65,_ctx) ) {
				case 1:
					{
					setState(659);
					match(LPARAM);
					setState(660);
					expression(0);
					setState(661);
					match(RPARAM);
					}
					break;
				}
				setState(665);
				((TryContext)_localctx).attempt = expression(0);
				setState(674);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,66,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(666);
						match(CATCH);
						setState(667);
						match(LPARAM);
						setState(668);
						((TryContext)_localctx).typevalue = typevalue();
						((TryContext)_localctx).errortype.add(((TryContext)_localctx).typevalue);
						setState(669);
						match(RPARAM);
						setState(670);
						((TryContext)_localctx).expression = expression(0);
						((TryContext)_localctx).errorexpression.add(((TryContext)_localctx).expression);
						}
						} 
					}
					setState(676);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,66,_ctx);
				}
				setState(679);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,67,_ctx) ) {
				case 1:
					{
					setState(677);
					match(FINALLY);
					setState(678);
					((TryContext)_localctx).end = expression(0);
					}
					break;
				}
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(788);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,77,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(786);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,76,_ctx) ) {
					case 1:
						{
						_localctx = new InfixContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(683);
						if (!(precpred(_ctx, 44))) throw new FailedPredicateException(this, "precpred(_ctx, 44)");
						{
						setState(684);
						match(POW);
						}
						setState(685);
						expression(44);
						}
						break;
					case 2:
						{
						_localctx = new InfixContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(686);
						if (!(precpred(_ctx, 43))) throw new FailedPredicateException(this, "precpred(_ctx, 43)");
						setState(687);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << MOD) | (1L << MULT) | (1L << DIV))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(688);
						expression(44);
						}
						break;
					case 3:
						{
						_localctx = new InfixContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(689);
						if (!(precpred(_ctx, 42))) throw new FailedPredicateException(this, "precpred(_ctx, 42)");
						setState(690);
						_la = _input.LA(1);
						if ( !(_la==PLUS || _la==MINUS) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(691);
						expression(43);
						}
						break;
					case 4:
						{
						_localctx = new InfixContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(692);
						if (!(precpred(_ctx, 41))) throw new FailedPredicateException(this, "precpred(_ctx, 41)");
						setState(700);
						_errHandler.sync(this);
						switch ( getInterpreter().adaptivePredict(_input,69,_ctx) ) {
						case 1:
							{
							setState(693);
							match(LANGLE);
							setState(694);
							match(LANGLE);
							}
							break;
						case 2:
							{
							setState(695);
							match(RANGLE);
							setState(696);
							match(RANGLE);
							}
							break;
						case 3:
							{
							setState(697);
							match(RANGLE);
							setState(698);
							match(RANGLE);
							setState(699);
							match(RANGLE);
							}
							break;
						}
						setState(702);
						expression(42);
						}
						break;
					case 5:
						{
						_localctx = new InfixContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(703);
						if (!(precpred(_ctx, 40))) throw new FailedPredicateException(this, "precpred(_ctx, 40)");
						setState(704);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LANGLE) | (1L << RANGLE) | (1L << GEQ) | (1L << LEQ))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(705);
						expression(41);
						}
						break;
					case 6:
						{
						_localctx = new InfixContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(706);
						if (!(precpred(_ctx, 39))) throw new FailedPredicateException(this, "precpred(_ctx, 39)");
						{
						setState(707);
						match(INSTANCEOF);
						}
						setState(708);
						expression(40);
						}
						break;
					case 7:
						{
						_localctx = new InfixContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(709);
						if (!(precpred(_ctx, 38))) throw new FailedPredicateException(this, "precpred(_ctx, 38)");
						setState(710);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQEQ) | (1L << NQEQ) | (1L << NQEQEQ) | (1L << EQEQEQ))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(711);
						expression(39);
						}
						break;
					case 8:
						{
						_localctx = new InfixContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(712);
						if (!(precpred(_ctx, 37))) throw new FailedPredicateException(this, "precpred(_ctx, 37)");
						{
						setState(713);
						match(BOTHAND);
						}
						setState(714);
						expression(38);
						}
						break;
					case 9:
						{
						_localctx = new InfixContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(715);
						if (!(precpred(_ctx, 36))) throw new FailedPredicateException(this, "precpred(_ctx, 36)");
						{
						setState(716);
						match(BOTHOR);
						}
						setState(717);
						expression(37);
						}
						break;
					case 10:
						{
						_localctx = new InfixContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(718);
						if (!(precpred(_ctx, 35))) throw new FailedPredicateException(this, "precpred(_ctx, 35)");
						{
						setState(719);
						match(BOTHXOR);
						}
						setState(720);
						expression(36);
						}
						break;
					case 11:
						{
						_localctx = new InfixContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(721);
						if (!(precpred(_ctx, 34))) throw new FailedPredicateException(this, "precpred(_ctx, 34)");
						{
						setState(722);
						match(AND);
						}
						setState(723);
						expression(35);
						}
						break;
					case 12:
						{
						_localctx = new InfixContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(724);
						if (!(precpred(_ctx, 33))) throw new FailedPredicateException(this, "precpred(_ctx, 33)");
						{
						setState(725);
						match(OR);
						}
						setState(726);
						expression(34);
						}
						break;
					case 13:
						{
						_localctx = new TernaryContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(727);
						if (!(precpred(_ctx, 31))) throw new FailedPredicateException(this, "precpred(_ctx, 31)");
						setState(728);
						match(QUESTION);
						setState(729);
						expression(0);
						setState(730);
						match(COLON);
						setState(731);
						expression(32);
						}
						break;
					case 14:
						{
						_localctx = new ChainContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(733);
						if (!(precpred(_ctx, 30))) throw new FailedPredicateException(this, "precpred(_ctx, 30)");
						setState(734);
						match(CHAIN);
						setState(735);
						expression(31);
						}
						break;
					case 15:
						{
						_localctx = new PipelineOpContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(736);
						if (!(precpred(_ctx, 29))) throw new FailedPredicateException(this, "precpred(_ctx, 29)");
						setState(737);
						match(PIPELINE);
						setState(738);
						expression(30);
						}
						break;
					case 16:
						{
						_localctx = new VarModifyContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(739);
						if (!(precpred(_ctx, 25))) throw new FailedPredicateException(this, "precpred(_ctx, 25)");
						setState(740);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQ) | (1L << PLUSEQ) | (1L << MINUSEQ) | (1L << MULTEQ) | (1L << DIVEQ) | (1L << BOTHANDEQ) | (1L << BOTHOREQ) | (1L << BOTHXOREQ) | (1L << ANDEQ) | (1L << OREQ) | (1L << POWEQ) | (1L << MODEQ) | (1L << LSHIFTEQ) | (1L << RSHIFTEQ) | (1L << UNSRSHIFTEQ))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(741);
						expression(25);
						}
						break;
					case 17:
						{
						_localctx = new GenericRefiningContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(742);
						if (!(precpred(_ctx, 53))) throw new FailedPredicateException(this, "precpred(_ctx, 53)");
						setState(743);
						gentype();
						}
						break;
					case 18:
						{
						_localctx = new EvaluationContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(744);
						if (!(precpred(_ctx, 50))) throw new FailedPredicateException(this, "precpred(_ctx, 50)");
						setState(745);
						match(LPARAM);
						setState(754);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LANGLE) | (1L << LBRACKET) | (1L << TEMPLATESTART) | (1L << SELF) | (1L << FINAL) | (1L << CONST) | (1L << LET) | (1L << ASYNC) | (1L << VOID) | (1L << INC) | (1L << DEC) | (1L << IMPORT) | (1L << EXPORT) | (1L << TYPEOF) | (1L << ABSTRACT) | (1L << PLUS) | (1L << MINUS) | (1L << QUESTION) | (1L << PIPELINERESULT))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (DECLARE - 64)) | (1L << (RETURN - 64)) | (1L << (YIELD - 64)) | (1L << (YIELDGEN - 64)) | (1L << (BREAK - 64)) | (1L << (CONTINUE - 64)) | (1L << (FUNCTION - 64)) | (1L << (IF - 64)) | (1L << (DO - 64)) | (1L << (WHILE - 64)) | (1L << (FOR - 64)) | (1L << (MATCH - 64)) | (1L << (NOT - 64)) | (1L << (NOT2 - 64)) | (1L << (RESTSPREAD - 64)) | (1L << (AWAIT - 64)) | (1L << (HAS - 64)) | (1L << (TYPE - 64)) | (1L << (TRY - 64)) | (1L << (THIS - 64)) | (1L << (SUPER - 64)) | (1L << (NAN - 64)) | (1L << (THROW - 64)) | (1L << (BOOLEANTRUE - 64)) | (1L << (BOOLEANFALSE - 64)) | (1L << (STRING - 64)) | (1L << (NAME - 64)) | (1L << (CHAR - 64)) | (1L << (NUMBER - 64)))) != 0)) {
							{
							setState(746);
							functionarg();
							setState(751);
							_errHandler.sync(this);
							_la = _input.LA(1);
							while (_la==COMMA) {
								{
								{
								setState(747);
								match(COMMA);
								setState(748);
								functionarg();
								}
								}
								setState(753);
								_errHandler.sync(this);
								_la = _input.LA(1);
							}
							}
						}

						setState(756);
						match(RPARAM);
						}
						break;
					case 19:
						{
						_localctx = new SpecificEvaluationContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(757);
						if (!(precpred(_ctx, 49))) throw new FailedPredicateException(this, "precpred(_ctx, 49)");
						setState(758);
						match(LCURLY);
						setState(760);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==NAME) {
							{
							setState(759);
							specificfunctionarg();
							}
						}

						setState(768);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==COMMA) {
							{
							{
							setState(762);
							match(COMMA);
							setState(764);
							_errHandler.sync(this);
							_la = _input.LA(1);
							if (_la==NAME) {
								{
								setState(763);
								specificfunctionarg();
								}
							}

							}
							}
							setState(770);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						setState(771);
						match(RCURLY);
						}
						break;
					case 20:
						{
						_localctx = new RefinementContext(new ExpressionContext(_parentctx, _parentState));
						((RefinementContext)_localctx).refine = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(772);
						if (!(precpred(_ctx, 48))) throw new FailedPredicateException(this, "precpred(_ctx, 48)");
						setState(779);
						_errHandler.sync(this);
						switch (_input.LA(1)) {
						case REFINE:
							{
							setState(773);
							match(REFINE);
							setState(774);
							match(NAME);
							}
							break;
						case LBRACKET:
							{
							setState(775);
							match(LBRACKET);
							setState(776);
							((RefinementContext)_localctx).refinement = expression(0);
							setState(777);
							match(RBRACKET);
							}
							break;
						default:
							throw new NoViableAltException(this);
						}
						}
						break;
					case 21:
						{
						_localctx = new PostfixContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(781);
						if (!(precpred(_ctx, 47))) throw new FailedPredicateException(this, "precpred(_ctx, 47)");
						setState(782);
						_la = _input.LA(1);
						if ( !(_la==INC || _la==DEC) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						}
						break;
					case 22:
						{
						_localctx = new InfixHasContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(783);
						if (!(precpred(_ctx, 32))) throw new FailedPredicateException(this, "precpred(_ctx, 32)");
						{
						setState(784);
						match(HAS);
						}
						setState(785);
						match(NAME);
						}
						break;
					}
					} 
				}
				setState(790);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,77,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class MatchresultContext extends ParserRuleContext {
		public TerminalNode ARROW() { return getToken(SuperScriptParser.ARROW, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public CasesContext cases() {
			return getRuleContext(CasesContext.class,0);
		}
		public TerminalNode LPARAM() { return getToken(SuperScriptParser.LPARAM, 0); }
		public TerminalNode RPARAM() { return getToken(SuperScriptParser.RPARAM, 0); }
		public MatchresultContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_matchresult; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterMatchresult(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitMatchresult(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitMatchresult(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MatchresultContext matchresult() throws RecognitionException {
		MatchresultContext _localctx = new MatchresultContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_matchresult);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(796);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,78,_ctx) ) {
			case 1:
				{
				setState(791);
				cases();
				}
				break;
			case 2:
				{
				setState(792);
				match(LPARAM);
				setState(793);
				cases();
				setState(794);
				match(RPARAM);
				}
				break;
			}
			setState(798);
			match(ARROW);
			setState(799);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UrlexpContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(SuperScriptParser.NAME, 0); }
		public TerminalNode COLON() { return getToken(SuperScriptParser.COLON, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode TYPE() { return getToken(SuperScriptParser.TYPE, 0); }
		public TypedecContext typedec() {
			return getRuleContext(TypedecContext.class,0);
		}
		public UrlexpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_urlexp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterUrlexp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitUrlexp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitUrlexp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UrlexpContext urlexp() throws RecognitionException {
		UrlexpContext _localctx = new UrlexpContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_urlexp);
		try {
			setState(808);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NAME:
				enterOuterAlt(_localctx, 1);
				{
				setState(801);
				match(NAME);
				setState(802);
				match(COLON);
				setState(803);
				expression(0);
				}
				break;
			case TYPE:
				enterOuterAlt(_localctx, 2);
				{
				setState(804);
				match(TYPE);
				setState(805);
				match(NAME);
				setState(806);
				match(COLON);
				setState(807);
				typedec(0);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UrlnameContext extends ParserRuleContext {
		public List<TerminalNode> NAME() { return getTokens(SuperScriptParser.NAME); }
		public TerminalNode NAME(int i) {
			return getToken(SuperScriptParser.NAME, i);
		}
		public TerminalNode COLON() { return getToken(SuperScriptParser.COLON, 0); }
		public UrlnameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_urlname; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterUrlname(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitUrlname(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitUrlname(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UrlnameContext urlname() throws RecognitionException {
		UrlnameContext _localctx = new UrlnameContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_urlname);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(810);
			match(NAME);
			setState(811);
			match(COLON);
			setState(812);
			match(NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SpecificfunctionargContext extends ParserRuleContext {
		public TerminalNode NAME() { return getToken(SuperScriptParser.NAME, 0); }
		public TerminalNode COLON() { return getToken(SuperScriptParser.COLON, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public SpecificfunctionargContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_specificfunctionarg; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterSpecificfunctionarg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitSpecificfunctionarg(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitSpecificfunctionarg(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SpecificfunctionargContext specificfunctionarg() throws RecognitionException {
		SpecificfunctionargContext _localctx = new SpecificfunctionargContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_specificfunctionarg);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(814);
			match(NAME);
			setState(815);
			match(COLON);
			setState(816);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionargContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RESTSPREAD() { return getToken(SuperScriptParser.RESTSPREAD, 0); }
		public FunctionargContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionarg; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterFunctionarg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitFunctionarg(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitFunctionarg(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionargContext functionarg() throws RecognitionException {
		FunctionargContext _localctx = new FunctionargContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_functionarg);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(819);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,80,_ctx) ) {
			case 1:
				{
				setState(818);
				match(RESTSPREAD);
				}
				break;
			}
			setState(821);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CasesContext extends ParserRuleContext {
		public List<AcaseContext> acase() {
			return getRuleContexts(AcaseContext.class);
		}
		public AcaseContext acase(int i) {
			return getRuleContext(AcaseContext.class,i);
		}
		public TerminalNode COMMA() { return getToken(SuperScriptParser.COMMA, 0); }
		public CasesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cases; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterCases(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitCases(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitCases(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CasesContext cases() throws RecognitionException {
		CasesContext _localctx = new CasesContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_cases);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(823);
			acase();
			setState(826);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==COMMA) {
				{
				setState(824);
				match(COMMA);
				setState(825);
				acase();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AcaseContext extends ParserRuleContext {
		public AcaseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_acase; }
	 
		public AcaseContext() { }
		public void copyFrom(AcaseContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class CaseDesContext extends AcaseContext {
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public CaseDesContext(AcaseContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterCaseDes(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitCaseDes(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitCaseDes(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CaseDefContext extends AcaseContext {
		public TerminalNode DEFAULT() { return getToken(SuperScriptParser.DEFAULT, 0); }
		public CaseDefContext(AcaseContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterCaseDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitCaseDef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitCaseDef(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CaseExpContext extends AcaseContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public CaseExpContext(AcaseContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterCaseExp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitCaseExp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitCaseExp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AcaseContext acase() throws RecognitionException {
		AcaseContext _localctx = new AcaseContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_acase);
		try {
			setState(831);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,82,_ctx) ) {
			case 1:
				_localctx = new CaseExpContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(828);
				expression(0);
				}
				break;
			case 2:
				_localctx = new CaseDesContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(829);
				var();
				}
				break;
			case 3:
				_localctx = new CaseDefContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(830);
				match(DEFAULT);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgsContext extends ParserRuleContext {
		public TerminalNode LPARAM() { return getToken(SuperScriptParser.LPARAM, 0); }
		public TerminalNode RPARAM() { return getToken(SuperScriptParser.RPARAM, 0); }
		public List<ArgContext> arg() {
			return getRuleContexts(ArgContext.class);
		}
		public ArgContext arg(int i) {
			return getRuleContext(ArgContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SuperScriptParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SuperScriptParser.COMMA, i);
		}
		public ArgsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_args; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterArgs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitArgs(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitArgs(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgsContext args() throws RecognitionException {
		ArgsContext _localctx = new ArgsContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_args);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(833);
			match(LPARAM);
			setState(835);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LANGLE) | (1L << FINAL) | (1L << VOID) | (1L << QUESTION) | (1L << PIPELINERESULT))) != 0) || _la==RESTSPREAD) {
				{
				setState(834);
				arg();
				}
			}

			setState(843);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(837);
				match(COMMA);
				setState(839);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LANGLE) | (1L << FINAL) | (1L << VOID) | (1L << QUESTION) | (1L << PIPELINERESULT))) != 0) || _la==RESTSPREAD) {
					{
					setState(838);
					arg();
					}
				}

				}
				}
				setState(845);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(846);
			match(RPARAM);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgContext extends ParserRuleContext {
		public FunctionnameContext functionname() {
			return getRuleContext(FunctionnameContext.class,0);
		}
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public TerminalNode RESTSPREAD() { return getToken(SuperScriptParser.RESTSPREAD, 0); }
		public ArgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arg; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterArg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitArg(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitArg(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArgContext arg() throws RecognitionException {
		ArgContext _localctx = new ArgContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_arg);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(848);
			functionname();
			setState(850);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==RESTSPREAD) {
				{
				setState(849);
				match(RESTSPREAD);
				}
			}

			setState(852);
			var();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarContext extends ParserRuleContext {
		public DestructContext destruct() {
			return getRuleContext(DestructContext.class,0);
		}
		public VarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterVar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitVar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitVar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VarContext var() throws RecognitionException {
		VarContext _localctx = new VarContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_var);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(854);
			destruct(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DestructContext extends ParserRuleContext {
		public DestructContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_destruct; }
	 
		public DestructContext() { }
		public void copyFrom(DestructContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ArrDestructContext extends DestructContext {
		public TerminalNode LBRACKET() { return getToken(SuperScriptParser.LBRACKET, 0); }
		public TerminalNode RBRACKET() { return getToken(SuperScriptParser.RBRACKET, 0); }
		public List<DestructarrparamContext> destructarrparam() {
			return getRuleContexts(DestructarrparamContext.class);
		}
		public DestructarrparamContext destructarrparam(int i) {
			return getRuleContext(DestructarrparamContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SuperScriptParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SuperScriptParser.COMMA, i);
		}
		public ArrDestructContext(DestructContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterArrDestruct(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitArrDestruct(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitArrDestruct(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ObjDestructAndStoreContext extends DestructContext {
		public DestructContext destruct() {
			return getRuleContext(DestructContext.class,0);
		}
		public TerminalNode LCURLY() { return getToken(SuperScriptParser.LCURLY, 0); }
		public TerminalNode RCURLY() { return getToken(SuperScriptParser.RCURLY, 0); }
		public List<DestructobjparamContext> destructobjparam() {
			return getRuleContexts(DestructobjparamContext.class);
		}
		public DestructobjparamContext destructobjparam(int i) {
			return getRuleContext(DestructobjparamContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SuperScriptParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SuperScriptParser.COMMA, i);
		}
		public ObjDestructAndStoreContext(DestructContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterObjDestructAndStore(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitObjDestructAndStore(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitObjDestructAndStore(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TupDestructContext extends DestructContext {
		public TerminalNode LPARAM() { return getToken(SuperScriptParser.LPARAM, 0); }
		public TerminalNode RPARAM() { return getToken(SuperScriptParser.RPARAM, 0); }
		public List<DestructtupparamContext> destructtupparam() {
			return getRuleContexts(DestructtupparamContext.class);
		}
		public DestructtupparamContext destructtupparam(int i) {
			return getRuleContext(DestructtupparamContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SuperScriptParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SuperScriptParser.COMMA, i);
		}
		public TupDestructContext(DestructContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterTupDestruct(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitTupDestruct(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitTupDestruct(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NameContext extends DestructContext {
		public TerminalNode NAME() { return getToken(SuperScriptParser.NAME, 0); }
		public TerminalNode EQ() { return getToken(SuperScriptParser.EQ, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public NameContext(DestructContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitName(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ObjDestructContext extends DestructContext {
		public TerminalNode LCURLY() { return getToken(SuperScriptParser.LCURLY, 0); }
		public TerminalNode RCURLY() { return getToken(SuperScriptParser.RCURLY, 0); }
		public List<DestructobjparamContext> destructobjparam() {
			return getRuleContexts(DestructobjparamContext.class);
		}
		public DestructobjparamContext destructobjparam(int i) {
			return getRuleContext(DestructobjparamContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SuperScriptParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SuperScriptParser.COMMA, i);
		}
		public ObjDestructContext(DestructContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterObjDestruct(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitObjDestruct(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitObjDestruct(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArrDestructAndStoreContext extends DestructContext {
		public DestructContext destruct() {
			return getRuleContext(DestructContext.class,0);
		}
		public TerminalNode LBRACKET() { return getToken(SuperScriptParser.LBRACKET, 0); }
		public TerminalNode RBRACKET() { return getToken(SuperScriptParser.RBRACKET, 0); }
		public List<DestructarrparamContext> destructarrparam() {
			return getRuleContexts(DestructarrparamContext.class);
		}
		public DestructarrparamContext destructarrparam(int i) {
			return getRuleContext(DestructarrparamContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SuperScriptParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SuperScriptParser.COMMA, i);
		}
		public ArrDestructAndStoreContext(DestructContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterArrDestructAndStore(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitArrDestructAndStore(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitArrDestructAndStore(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TupDestructAndStoreContext extends DestructContext {
		public DestructContext destruct() {
			return getRuleContext(DestructContext.class,0);
		}
		public TerminalNode LPARAM() { return getToken(SuperScriptParser.LPARAM, 0); }
		public TerminalNode RPARAM() { return getToken(SuperScriptParser.RPARAM, 0); }
		public List<DestructtupparamContext> destructtupparam() {
			return getRuleContexts(DestructtupparamContext.class);
		}
		public DestructtupparamContext destructtupparam(int i) {
			return getRuleContext(DestructtupparamContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SuperScriptParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SuperScriptParser.COMMA, i);
		}
		public TupDestructAndStoreContext(DestructContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterTupDestructAndStore(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitTupDestructAndStore(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitTupDestructAndStore(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DestructContext destruct() throws RecognitionException {
		return destruct(0);
	}

	private DestructContext destruct(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		DestructContext _localctx = new DestructContext(_ctx, _parentState);
		DestructContext _prevctx = _localctx;
		int _startState = 80;
		enterRecursionRule(_localctx, 80, RULE_destruct, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(902);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LBRACKET:
				{
				_localctx = new ArrDestructContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(857);
				match(LBRACKET);
				setState(859);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LBRACKET))) != 0) || _la==RESTSPREAD || _la==NAME) {
					{
					setState(858);
					destructarrparam();
					}
				}

				setState(867);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(861);
					match(COMMA);
					setState(863);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LBRACKET))) != 0) || _la==RESTSPREAD || _la==NAME) {
						{
						setState(862);
						destructarrparam();
						}
					}

					}
					}
					setState(869);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(870);
				match(RBRACKET);
				}
				break;
			case LPARAM:
				{
				_localctx = new TupDestructContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(871);
				match(LPARAM);
				setState(873);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LBRACKET))) != 0) || _la==RESTSPREAD || _la==NAME) {
					{
					setState(872);
					destructtupparam();
					}
				}

				setState(881);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(875);
					match(COMMA);
					setState(877);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LBRACKET))) != 0) || _la==RESTSPREAD || _la==NAME) {
						{
						setState(876);
						destructtupparam();
						}
					}

					}
					}
					setState(883);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(884);
				match(RPARAM);
				}
				break;
			case LCURLY:
				{
				_localctx = new ObjDestructContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(885);
				match(LCURLY);
				setState(894);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LBRACKET))) != 0) || _la==NAME) {
					{
					setState(886);
					destructobjparam();
					setState(891);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(887);
						match(COMMA);
						setState(888);
						destructobjparam();
						}
						}
						setState(893);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(896);
				match(RCURLY);
				}
				break;
			case NAME:
				{
				_localctx = new NameContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(897);
				match(NAME);
				setState(900);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,95,_ctx) ) {
				case 1:
					{
					setState(898);
					match(EQ);
					setState(899);
					expression(0);
					}
					break;
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(949);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,106,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(947);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,105,_ctx) ) {
					case 1:
						{
						_localctx = new ArrDestructAndStoreContext(new DestructContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_destruct);
						setState(904);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(905);
						match(LBRACKET);
						setState(907);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LBRACKET))) != 0) || _la==RESTSPREAD || _la==NAME) {
							{
							setState(906);
							destructarrparam();
							}
						}

						setState(915);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==COMMA) {
							{
							{
							setState(909);
							match(COMMA);
							setState(911);
							_errHandler.sync(this);
							_la = _input.LA(1);
							if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LBRACKET))) != 0) || _la==RESTSPREAD || _la==NAME) {
								{
								setState(910);
								destructarrparam();
								}
							}

							}
							}
							setState(917);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						setState(918);
						match(RBRACKET);
						}
						break;
					case 2:
						{
						_localctx = new TupDestructAndStoreContext(new DestructContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_destruct);
						setState(919);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(920);
						match(LPARAM);
						setState(922);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LBRACKET))) != 0) || _la==RESTSPREAD || _la==NAME) {
							{
							setState(921);
							destructtupparam();
							}
						}

						setState(930);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==COMMA) {
							{
							{
							setState(924);
							match(COMMA);
							setState(926);
							_errHandler.sync(this);
							_la = _input.LA(1);
							if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LBRACKET))) != 0) || _la==RESTSPREAD || _la==NAME) {
								{
								setState(925);
								destructtupparam();
								}
							}

							}
							}
							setState(932);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						setState(933);
						match(RPARAM);
						}
						break;
					case 3:
						{
						_localctx = new ObjDestructAndStoreContext(new DestructContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_destruct);
						setState(934);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(935);
						match(LCURLY);
						setState(944);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LCURLY) | (1L << LPARAM) | (1L << LBRACKET))) != 0) || _la==NAME) {
							{
							setState(936);
							destructobjparam();
							setState(941);
							_errHandler.sync(this);
							_la = _input.LA(1);
							while (_la==COMMA) {
								{
								{
								setState(937);
								match(COMMA);
								setState(938);
								destructobjparam();
								}
								}
								setState(943);
								_errHandler.sync(this);
								_la = _input.LA(1);
							}
							}
						}

						setState(946);
						match(RCURLY);
						}
						break;
					}
					} 
				}
				setState(951);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,106,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class DestructarrparamContext extends ParserRuleContext {
		public DestructContext destruct() {
			return getRuleContext(DestructContext.class,0);
		}
		public TerminalNode RESTSPREAD() { return getToken(SuperScriptParser.RESTSPREAD, 0); }
		public DestructarrparamContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_destructarrparam; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterDestructarrparam(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitDestructarrparam(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitDestructarrparam(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DestructarrparamContext destructarrparam() throws RecognitionException {
		DestructarrparamContext _localctx = new DestructarrparamContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_destructarrparam);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(953);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==RESTSPREAD) {
				{
				setState(952);
				match(RESTSPREAD);
				}
			}

			setState(955);
			destruct(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DestructtupparamContext extends ParserRuleContext {
		public DestructContext destruct() {
			return getRuleContext(DestructContext.class,0);
		}
		public TerminalNode RESTSPREAD() { return getToken(SuperScriptParser.RESTSPREAD, 0); }
		public DestructtupparamContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_destructtupparam; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterDestructtupparam(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitDestructtupparam(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitDestructtupparam(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DestructtupparamContext destructtupparam() throws RecognitionException {
		DestructtupparamContext _localctx = new DestructtupparamContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_destructtupparam);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(958);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==RESTSPREAD) {
				{
				setState(957);
				match(RESTSPREAD);
				}
			}

			setState(960);
			destruct(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DestructobjparamContext extends ParserRuleContext {
		public List<DestructContext> destruct() {
			return getRuleContexts(DestructContext.class);
		}
		public DestructContext destruct(int i) {
			return getRuleContext(DestructContext.class,i);
		}
		public TerminalNode COLON() { return getToken(SuperScriptParser.COLON, 0); }
		public DestructobjparamContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_destructobjparam; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterDestructobjparam(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitDestructobjparam(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitDestructobjparam(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DestructobjparamContext destructobjparam() throws RecognitionException {
		DestructobjparamContext _localctx = new DestructobjparamContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_destructobjparam);
		try {
			setState(967);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,109,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(962);
				destruct(0);
				setState(963);
				match(COLON);
				setState(964);
				destruct(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(966);
				destruct(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumberContext extends ParserRuleContext {
		public TerminalNode NUMBER() { return getToken(SuperScriptParser.NUMBER, 0); }
		public NumberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_number; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterNumber(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitNumber(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitNumber(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NumberContext number() throws RecognitionException {
		NumberContext _localctx = new NumberContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_number);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(969);
			match(NUMBER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LitContext extends ParserRuleContext {
		public LitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lit; }
	 
		public LitContext() { }
		public void copyFrom(LitContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class VoidContext extends LitContext {
		public TerminalNode VOID() { return getToken(SuperScriptParser.VOID, 0); }
		public VoidContext(LitContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterVoid(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitVoid(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitVoid(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StringContext extends LitContext {
		public TerminalNode STRING() { return getToken(SuperScriptParser.STRING, 0); }
		public StringContext(LitContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterString(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitString(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitString(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FalseContext extends LitContext {
		public TerminalNode BOOLEANFALSE() { return getToken(SuperScriptParser.BOOLEANFALSE, 0); }
		public FalseContext(LitContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterFalse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitFalse(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitFalse(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ThisContext extends LitContext {
		public TerminalNode THIS() { return getToken(SuperScriptParser.THIS, 0); }
		public ThisContext(LitContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterThis(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitThis(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitThis(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TemplateStringContext extends LitContext {
		public TemplatestrContext templatestr() {
			return getRuleContext(TemplatestrContext.class,0);
		}
		public TemplateStringContext(LitContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterTemplateString(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitTemplateString(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitTemplateString(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumbContext extends LitContext {
		public NumberContext number() {
			return getRuleContext(NumberContext.class,0);
		}
		public NumbContext(LitContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterNumb(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitNumb(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitNumb(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SuperContext extends LitContext {
		public TerminalNode SUPER() { return getToken(SuperScriptParser.SUPER, 0); }
		public SuperContext(LitContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterSuper(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitSuper(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitSuper(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TupleContext extends LitContext {
		public TupContext tup() {
			return getRuleContext(TupContext.class,0);
		}
		public TupleContext(LitContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterTuple(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitTuple(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitTuple(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArrayContext extends LitContext {
		public ArrContext arr() {
			return getRuleContext(ArrContext.class,0);
		}
		public ArrayContext(LitContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterArray(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitArray(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitArray(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CharContext extends LitContext {
		public TerminalNode CHAR() { return getToken(SuperScriptParser.CHAR, 0); }
		public CharContext(LitContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterChar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitChar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitChar(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TrueContext extends LitContext {
		public TerminalNode BOOLEANTRUE() { return getToken(SuperScriptParser.BOOLEANTRUE, 0); }
		public TrueContext(LitContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterTrue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitTrue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitTrue(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SelfContext extends LitContext {
		public TerminalNode SELF() { return getToken(SuperScriptParser.SELF, 0); }
		public SelfContext(LitContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterSelf(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitSelf(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitSelf(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NaNContext extends LitContext {
		public TerminalNode NAN() { return getToken(SuperScriptParser.NAN, 0); }
		public NaNContext(LitContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterNaN(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitNaN(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitNaN(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ObjectContext extends LitContext {
		public ValdecContext valdec() {
			return getRuleContext(ValdecContext.class,0);
		}
		public ObjContext obj() {
			return getRuleContext(ObjContext.class,0);
		}
		public ObjectContext(LitContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterObject(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitObject(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitObject(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LitContext lit() throws RecognitionException {
		LitContext _localctx = new LitContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_lit);
		try {
			setState(987);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,110,_ctx) ) {
			case 1:
				_localctx = new ObjectContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(971);
				valdec();
				setState(972);
				obj();
				}
				break;
			case 2:
				_localctx = new ArrayContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(974);
				arr();
				}
				break;
			case 3:
				_localctx = new TupleContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(975);
				tup();
				}
				break;
			case 4:
				_localctx = new TemplateStringContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(976);
				templatestr();
				}
				break;
			case 5:
				_localctx = new NumbContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(977);
				number();
				}
				break;
			case 6:
				_localctx = new StringContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(978);
				match(STRING);
				}
				break;
			case 7:
				_localctx = new CharContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(979);
				match(CHAR);
				}
				break;
			case 8:
				_localctx = new TrueContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(980);
				match(BOOLEANTRUE);
				}
				break;
			case 9:
				_localctx = new FalseContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(981);
				match(BOOLEANFALSE);
				}
				break;
			case 10:
				_localctx = new NaNContext(_localctx);
				enterOuterAlt(_localctx, 10);
				{
				setState(982);
				match(NAN);
				}
				break;
			case 11:
				_localctx = new ThisContext(_localctx);
				enterOuterAlt(_localctx, 11);
				{
				setState(983);
				match(THIS);
				}
				break;
			case 12:
				_localctx = new SuperContext(_localctx);
				enterOuterAlt(_localctx, 12);
				{
				setState(984);
				match(SUPER);
				}
				break;
			case 13:
				_localctx = new SelfContext(_localctx);
				enterOuterAlt(_localctx, 13);
				{
				setState(985);
				match(SELF);
				}
				break;
			case 14:
				_localctx = new VoidContext(_localctx);
				enterOuterAlt(_localctx, 14);
				{
				setState(986);
				match(VOID);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TemplatestrContext extends ParserRuleContext {
		public TerminalNode TEMPLATESTART() { return getToken(SuperScriptParser.TEMPLATESTART, 0); }
		public TerminalNode TEMPLATEEND() { return getToken(SuperScriptParser.TEMPLATEEND, 0); }
		public List<TerminalNode> CHUNK() { return getTokens(SuperScriptParser.CHUNK); }
		public TerminalNode CHUNK(int i) {
			return getToken(SuperScriptParser.CHUNK, i);
		}
		public List<TerminalNode> STRINGINTERP() { return getTokens(SuperScriptParser.STRINGINTERP); }
		public TerminalNode STRINGINTERP(int i) {
			return getToken(SuperScriptParser.STRINGINTERP, i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> RCURLY() { return getTokens(SuperScriptParser.RCURLY); }
		public TerminalNode RCURLY(int i) {
			return getToken(SuperScriptParser.RCURLY, i);
		}
		public List<TerminalNode> CHARACTER() { return getTokens(SuperScriptParser.CHARACTER); }
		public TerminalNode CHARACTER(int i) {
			return getToken(SuperScriptParser.CHARACTER, i);
		}
		public TemplatestrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_templatestr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterTemplatestr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitTemplatestr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitTemplatestr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TemplatestrContext templatestr() throws RecognitionException {
		TemplatestrContext _localctx = new TemplatestrContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_templatestr);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(989);
			match(TEMPLATESTART);
			setState(998);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,112,_ctx);
			while ( _alt!=1 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1+1 ) {
					{
					setState(996);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case CHUNK:
						{
						setState(990);
						match(CHUNK);
						}
						break;
					case STRINGINTERP:
						{
						setState(991);
						match(STRINGINTERP);
						setState(992);
						expression(0);
						setState(993);
						match(RCURLY);
						}
						break;
					case CHARACTER:
						{
						setState(995);
						match(CHARACTER);
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					} 
				}
				setState(1000);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,112,_ctx);
			}
			setState(1001);
			match(TEMPLATEEND);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprsemiContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(SuperScriptParser.SEMI, 0); }
		public ExprsemiContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exprsemi; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).enterExprsemi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperScriptParserListener ) ((SuperScriptParserListener)listener).exitExprsemi(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SuperScriptParserVisitor ) return ((SuperScriptParserVisitor<? extends T>)visitor).visitExprsemi(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprsemiContext exprsemi() throws RecognitionException {
		ExprsemiContext _localctx = new ExprsemiContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_exprsemi);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1004);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SEMI) {
				{
				setState(1003);
				match(SEMI);
				}
			}

			setState(1006);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 15:
			return typedec_sempred((TypedecContext)_localctx, predIndex);
		case 29:
			return expression_sempred((ExpressionContext)_localctx, predIndex);
		case 40:
			return destruct_sempred((DestructContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean typedec_sempred(TypedecContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 10);
		case 1:
			return precpred(_ctx, 6);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2:
			return precpred(_ctx, 44);
		case 3:
			return precpred(_ctx, 43);
		case 4:
			return precpred(_ctx, 42);
		case 5:
			return precpred(_ctx, 41);
		case 6:
			return precpred(_ctx, 40);
		case 7:
			return precpred(_ctx, 39);
		case 8:
			return precpred(_ctx, 38);
		case 9:
			return precpred(_ctx, 37);
		case 10:
			return precpred(_ctx, 36);
		case 11:
			return precpred(_ctx, 35);
		case 12:
			return precpred(_ctx, 34);
		case 13:
			return precpred(_ctx, 33);
		case 14:
			return precpred(_ctx, 31);
		case 15:
			return precpred(_ctx, 30);
		case 16:
			return precpred(_ctx, 29);
		case 17:
			return precpred(_ctx, 25);
		case 18:
			return precpred(_ctx, 53);
		case 19:
			return precpred(_ctx, 50);
		case 20:
			return precpred(_ctx, 49);
		case 21:
			return precpred(_ctx, 48);
		case 22:
			return precpred(_ctx, 47);
		case 23:
			return precpred(_ctx, 32);
		}
		return true;
	}
	private boolean destruct_sempred(DestructContext _localctx, int predIndex) {
		switch (predIndex) {
		case 24:
			return precpred(_ctx, 7);
		case 25:
			return precpred(_ctx, 6);
		case 26:
			return precpred(_ctx, 5);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3m\u03f3\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\3\2\3\2\3\2\3\2\3\3\3\3\3\3"+
		"\3\3\3\4\3\4\3\4\3\4\7\4o\n\4\f\4\16\4r\13\4\3\4\3\4\7\4v\n\4\f\4\16\4"+
		"y\13\4\3\5\5\5|\n\5\3\5\7\5\177\n\5\f\5\16\5\u0082\13\5\3\6\3\6\3\6\5"+
		"\6\u0087\n\6\3\6\3\6\5\6\u008b\n\6\7\6\u008d\n\6\f\6\16\6\u0090\13\6\3"+
		"\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6\u009b\n\6\3\7\5\7\u009e\n\7\3\7"+
		"\3\7\5\7\u00a2\n\7\3\7\3\7\5\7\u00a6\n\7\7\7\u00a8\n\7\f\7\16\7\u00ab"+
		"\13\7\3\7\3\7\3\b\3\b\5\b\u00b1\n\b\3\b\3\b\5\b\u00b5\n\b\7\b\u00b7\n"+
		"\b\f\b\16\b\u00ba\13\b\3\b\3\b\3\t\5\t\u00bf\n\t\3\t\3\t\3\t\5\t\u00c4"+
		"\n\t\3\n\3\n\5\n\u00c8\n\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\5\13\u00e1\n\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\5\13\u0108"+
		"\n\13\3\f\3\f\3\f\3\r\5\r\u010e\n\r\3\16\3\16\3\17\3\17\3\17\3\20\3\20"+
		"\3\20\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\5\21\u0122\n\21"+
		"\3\21\3\21\3\21\3\21\7\21\u0128\n\21\f\21\16\21\u012b\13\21\5\21\u012d"+
		"\n\21\3\21\3\21\3\21\3\21\5\21\u0133\n\21\3\21\3\21\3\21\3\21\7\21\u0139"+
		"\n\21\f\21\16\21\u013c\13\21\5\21\u013e\n\21\3\21\3\21\3\21\3\21\3\21"+
		"\3\21\3\21\3\21\3\21\3\21\3\21\5\21\u014b\n\21\3\21\3\21\3\21\3\21\3\21"+
		"\7\21\u0152\n\21\f\21\16\21\u0155\13\21\3\22\3\22\5\22\u0159\n\22\3\22"+
		"\3\22\3\22\5\22\u015e\n\22\3\23\3\23\5\23\u0162\n\23\3\24\3\24\5\24\u0166"+
		"\n\24\3\24\3\24\5\24\u016a\n\24\7\24\u016c\n\24\f\24\16\24\u016f\13\24"+
		"\3\24\3\24\3\25\3\25\5\25\u0175\n\25\3\25\3\25\5\25\u0179\n\25\7\25\u017b"+
		"\n\25\f\25\16\25\u017e\13\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\27\3"+
		"\27\3\27\3\27\5\27\u018b\n\27\3\30\3\30\3\30\3\30\7\30\u0191\n\30\f\30"+
		"\16\30\u0194\13\30\3\30\3\30\3\31\3\31\3\31\3\31\7\31\u019c\n\31\f\31"+
		"\16\31\u019f\13\31\3\31\3\31\3\32\5\32\u01a4\n\32\3\32\3\32\3\32\3\32"+
		"\3\32\7\32\u01ab\n\32\f\32\16\32\u01ae\13\32\3\33\5\33\u01b1\n\33\3\34"+
		"\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34"+
		"\3\34\3\34\3\34\3\34\3\34\3\34\3\34\5\34\u01c9\n\34\3\35\3\35\3\36\3\36"+
		"\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37"+
		"\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37"+
		"\3\37\3\37\3\37\7\37\u01ee\n\37\f\37\16\37\u01f1\13\37\3\37\3\37\3\37"+
		"\3\37\7\37\u01f7\n\37\f\37\16\37\u01fa\13\37\3\37\3\37\3\37\3\37\5\37"+
		"\u0200\n\37\3\37\3\37\3\37\3\37\3\37\3\37\7\37\u0208\n\37\f\37\16\37\u020b"+
		"\13\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\7\37\u0214\n\37\f\37\16\37\u0217"+
		"\13\37\3\37\3\37\3\37\3\37\5\37\u021d\n\37\3\37\3\37\5\37\u0221\n\37\3"+
		"\37\3\37\3\37\3\37\5\37\u0227\n\37\3\37\3\37\5\37\u022b\n\37\3\37\3\37"+
		"\3\37\3\37\3\37\3\37\3\37\5\37\u0234\n\37\3\37\3\37\3\37\3\37\3\37\3\37"+
		"\3\37\5\37\u023d\n\37\3\37\3\37\3\37\3\37\3\37\5\37\u0244\n\37\3\37\3"+
		"\37\3\37\3\37\3\37\3\37\5\37\u024c\n\37\3\37\3\37\3\37\3\37\3\37\3\37"+
		"\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\5\37\u025e\n\37\3\37"+
		"\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37"+
		"\3\37\5\37\u0270\n\37\3\37\3\37\5\37\u0274\n\37\3\37\3\37\5\37\u0278\n"+
		"\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3"+
		"\37\3\37\3\37\3\37\3\37\7\37\u028c\n\37\f\37\16\37\u028f\13\37\5\37\u0291"+
		"\n\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\5\37\u029a\n\37\3\37\3\37\3\37"+
		"\3\37\3\37\3\37\3\37\7\37\u02a3\n\37\f\37\16\37\u02a6\13\37\3\37\3\37"+
		"\5\37\u02aa\n\37\5\37\u02ac\n\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3"+
		"\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\5\37\u02bf\n\37\3\37"+
		"\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37"+
		"\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37"+
		"\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37"+
		"\3\37\3\37\3\37\3\37\7\37\u02f0\n\37\f\37\16\37\u02f3\13\37\5\37\u02f5"+
		"\n\37\3\37\3\37\3\37\3\37\5\37\u02fb\n\37\3\37\3\37\5\37\u02ff\n\37\7"+
		"\37\u0301\n\37\f\37\16\37\u0304\13\37\3\37\3\37\3\37\3\37\3\37\3\37\3"+
		"\37\3\37\5\37\u030e\n\37\3\37\3\37\3\37\3\37\3\37\7\37\u0315\n\37\f\37"+
		"\16\37\u0318\13\37\3 \3 \3 \3 \3 \5 \u031f\n \3 \3 \3 \3!\3!\3!\3!\3!"+
		"\3!\3!\5!\u032b\n!\3\"\3\"\3\"\3\"\3#\3#\3#\3#\3$\5$\u0336\n$\3$\3$\3"+
		"%\3%\3%\5%\u033d\n%\3&\3&\3&\5&\u0342\n&\3\'\3\'\5\'\u0346\n\'\3\'\3\'"+
		"\5\'\u034a\n\'\7\'\u034c\n\'\f\'\16\'\u034f\13\'\3\'\3\'\3(\3(\5(\u0355"+
		"\n(\3(\3(\3)\3)\3*\3*\3*\5*\u035e\n*\3*\3*\5*\u0362\n*\7*\u0364\n*\f*"+
		"\16*\u0367\13*\3*\3*\3*\5*\u036c\n*\3*\3*\5*\u0370\n*\7*\u0372\n*\f*\16"+
		"*\u0375\13*\3*\3*\3*\3*\3*\7*\u037c\n*\f*\16*\u037f\13*\5*\u0381\n*\3"+
		"*\3*\3*\3*\5*\u0387\n*\5*\u0389\n*\3*\3*\3*\5*\u038e\n*\3*\3*\5*\u0392"+
		"\n*\7*\u0394\n*\f*\16*\u0397\13*\3*\3*\3*\3*\5*\u039d\n*\3*\3*\5*\u03a1"+
		"\n*\7*\u03a3\n*\f*\16*\u03a6\13*\3*\3*\3*\3*\3*\3*\7*\u03ae\n*\f*\16*"+
		"\u03b1\13*\5*\u03b3\n*\3*\7*\u03b6\n*\f*\16*\u03b9\13*\3+\5+\u03bc\n+"+
		"\3+\3+\3,\5,\u03c1\n,\3,\3,\3-\3-\3-\3-\3-\5-\u03ca\n-\3.\3.\3/\3/\3/"+
		"\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\5/\u03de\n/\3\60\3\60\3\60\3\60"+
		"\3\60\3\60\3\60\7\60\u03e7\n\60\f\60\16\60\u03ea\13\60\3\60\3\60\3\61"+
		"\5\61\u03ef\n\61\3\61\3\61\3\61\3\u03e8\5 <R\62\2\4\6\b\n\f\16\20\22\24"+
		"\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJLNPRTVXZ\\^`\2\r\3\2\20"+
		"\21\3\2\37 \4\2\35\35__\5\2\25\26\37 VV\3\2\25\26\5\2\25\26\34\34\37 "+
		"\3\2UV\3\2\"$\4\2\7\b\f\r\3\2&)\3\2\60>\2\u0499\2b\3\2\2\2\4f\3\2\2\2"+
		"\6j\3\2\2\2\b\u0080\3\2\2\2\n\u009a\3\2\2\2\f\u009d\3\2\2\2\16\u00ae\3"+
		"\2\2\2\20\u00c3\3\2\2\2\22\u00c7\3\2\2\2\24\u0107\3\2\2\2\26\u0109\3\2"+
		"\2\2\30\u010d\3\2\2\2\32\u010f\3\2\2\2\34\u0111\3\2\2\2\36\u0114\3\2\2"+
		"\2 \u014a\3\2\2\2\"\u015d\3\2\2\2$\u015f\3\2\2\2&\u0163\3\2\2\2(\u0172"+
		"\3\2\2\2*\u0181\3\2\2\2,\u018a\3\2\2\2.\u018c\3\2\2\2\60\u0197\3\2\2\2"+
		"\62\u01a3\3\2\2\2\64\u01b0\3\2\2\2\66\u01c8\3\2\2\28\u01ca\3\2\2\2:\u01cc"+
		"\3\2\2\2<\u02ab\3\2\2\2>\u031e\3\2\2\2@\u032a\3\2\2\2B\u032c\3\2\2\2D"+
		"\u0330\3\2\2\2F\u0335\3\2\2\2H\u0339\3\2\2\2J\u0341\3\2\2\2L\u0343\3\2"+
		"\2\2N\u0352\3\2\2\2P\u0358\3\2\2\2R\u0388\3\2\2\2T\u03bb\3\2\2\2V\u03c0"+
		"\3\2\2\2X\u03c9\3\2\2\2Z\u03cb\3\2\2\2\\\u03dd\3\2\2\2^\u03df\3\2\2\2"+
		"`\u03ee\3\2\2\2bc\5\4\3\2cd\5\b\5\2de\7\2\2\3e\3\3\2\2\2fg\7\27\2\2gh"+
		"\5\6\4\2hi\7S\2\2i\5\3\2\2\2jk\7A\2\2kp\7g\2\2lm\7\24\2\2mo\7g\2\2nl\3"+
		"\2\2\2or\3\2\2\2pn\3\2\2\2pq\3\2\2\2qw\3\2\2\2rp\3\2\2\2st\7A\2\2tv\7"+
		"g\2\2us\3\2\2\2vy\3\2\2\2wu\3\2\2\2wx\3\2\2\2x\7\3\2\2\2yw\3\2\2\2z|\5"+
		"<\37\2{z\3\2\2\2{|\3\2\2\2|}\3\2\2\2}\177\7S\2\2~{\3\2\2\2\177\u0082\3"+
		"\2\2\2\u0080~\3\2\2\2\u0080\u0081\3\2\2\2\u0081\t\3\2\2\2\u0082\u0080"+
		"\3\2\2\2\u0083\u0084\5 \21\2\u0084\u0086\7\t\2\2\u0085\u0087\5\22\n\2"+
		"\u0086\u0085\3\2\2\2\u0086\u0087\3\2\2\2\u0087\u008e\3\2\2\2\u0088\u008a"+
		"\7T\2\2\u0089\u008b\5\22\n\2\u008a\u0089\3\2\2\2\u008a\u008b\3\2\2\2\u008b"+
		"\u008d\3\2\2\2\u008c\u0088\3\2\2\2\u008d\u0090\3\2\2\2\u008e\u008c\3\2"+
		"\2\2\u008e\u008f\3\2\2\2\u008f\u0091\3\2\2\2\u0090\u008e\3\2\2\2\u0091"+
		"\u0092\7\n\2\2\u0092\u009b\3\2\2\2\u0093\u0094\5 \21\2\u0094\u0095\7\t"+
		"\2\2\u0095\u0096\7\t\2\2\u0096\u0097\5<\37\2\u0097\u0098\7\n\2\2\u0098"+
		"\u0099\7\n\2\2\u0099\u009b\3\2\2\2\u009a\u0083\3\2\2\2\u009a\u0093\3\2"+
		"\2\2\u009b\13\3\2\2\2\u009c\u009e\7\36\2\2\u009d\u009c\3\2\2\2\u009d\u009e"+
		"\3\2\2\2\u009e\u009f\3\2\2\2\u009f\u00a1\7\3\2\2\u00a0\u00a2\5\24\13\2"+
		"\u00a1\u00a0\3\2\2\2\u00a1\u00a2\3\2\2\2\u00a2\u00a9\3\2\2\2\u00a3\u00a5"+
		"\7T\2\2\u00a4\u00a6\5\24\13\2\u00a5\u00a4\3\2\2\2\u00a5\u00a6\3\2\2\2"+
		"\u00a6\u00a8\3\2\2\2\u00a7\u00a3\3\2\2\2\u00a8\u00ab\3\2\2\2\u00a9\u00a7"+
		"\3\2\2\2\u00a9\u00aa\3\2\2\2\u00aa\u00ac\3\2\2\2\u00ab\u00a9\3\2\2\2\u00ac"+
		"\u00ad\7\4\2\2\u00ad\r\3\2\2\2\u00ae\u00b0\7\5\2\2\u00af\u00b1\5\20\t"+
		"\2\u00b0\u00af\3\2\2\2\u00b0\u00b1\3\2\2\2\u00b1\u00b8\3\2\2\2\u00b2\u00b4"+
		"\7T\2\2\u00b3\u00b5\5\20\t\2\u00b4\u00b3\3\2\2\2\u00b4\u00b5\3\2\2\2\u00b5"+
		"\u00b7\3\2\2\2\u00b6\u00b2\3\2\2\2\u00b7\u00ba\3\2\2\2\u00b8\u00b6\3\2"+
		"\2\2\u00b8\u00b9\3\2\2\2\u00b9\u00bb\3\2\2\2\u00ba\u00b8\3\2\2\2\u00bb"+
		"\u00bc\7\6\2\2\u00bc\17\3\2\2\2\u00bd\u00bf\5 \21\2\u00be\u00bd\3\2\2"+
		"\2\u00be\u00bf\3\2\2\2\u00bf\u00c0\3\2\2\2\u00c0\u00c4\5<\37\2\u00c1\u00c2"+
		"\7B\2\2\u00c2\u00c4\5 \21\2\u00c3\u00be\3\2\2\2\u00c3\u00c1\3\2\2\2\u00c4"+
		"\21\3\2\2\2\u00c5\u00c8\5<\37\2\u00c6\u00c8\7B\2\2\u00c7\u00c5\3\2\2\2"+
		"\u00c7\u00c6\3\2\2\2\u00c8\23\3\2\2\2\u00c9\u00ca\5\26\f\2\u00ca\u00cb"+
		"\7\24\2\2\u00cb\u00cc\5<\37\2\u00cc\u0108\3\2\2\2\u00cd\u00ce\7B\2\2\u00ce"+
		"\u0108\5\26\f\2\u00cf\u00d0\7\36\2\2\u00d0\u0108\5\26\f\2\u00d1\u00d2"+
		"\5\34\17\2\u00d2\u00e0\7\t\2\2\u00d3\u00d4\7\5\2\2\u00d4\u00d5\5 \21\2"+
		"\u00d5\u00d6\7\6\2\2\u00d6\u00e1\3\2\2\2\u00d7\u00d8\7\24\2\2\u00d8\u00d9"+
		"\5\66\34\2\u00d9\u00da\7\24\2\2\u00da\u00e1\3\2\2\2\u00db\u00dc\58\35"+
		"\2\u00dc\u00dd\7\24\2\2\u00dd\u00e1\3\2\2\2\u00de\u00df\7\24\2\2\u00df"+
		"\u00e1\5:\36\2\u00e0\u00d3\3\2\2\2\u00e0\u00d7\3\2\2\2\u00e0\u00db\3\2"+
		"\2\2\u00e0\u00de\3\2\2\2\u00e1\u00e2\3\2\2\2\u00e2\u00e3\7\n\2\2\u00e3"+
		"\u00e4\7\24\2\2\u00e4\u00e5\5<\37\2\u00e5\u0108\3\2\2\2\u00e6\u00e7\7"+
		"\32\2\2\u00e7\u00e8\5\36\20\2\u00e8\u00e9\7\5\2\2\u00e9\u00ea\7\6\2\2"+
		"\u00ea\u00eb\7\24\2\2\u00eb\u00ec\5<\37\2\u00ec\u0108\3\2\2\2\u00ed\u00ee"+
		"\7\33\2\2\u00ee\u00ef\5\36\20\2\u00ef\u00f0\7\5\2\2\u00f0\u00f1\7g\2\2"+
		"\u00f1\u00f2\7\6\2\2\u00f2\u00f3\7\24\2\2\u00f3\u00f4\5<\37\2\u00f4\u0108"+
		"\3\2\2\2\u00f5\u00f6\7\32\2\2\u00f6\u00f7\5 \21\2\u00f7\u00f8\7\5\2\2"+
		"\u00f8\u00f9\7g\2\2\u00f9\u00fa\7\6\2\2\u00fa\u00fb\7\24\2\2\u00fb\u00fc"+
		"\5<\37\2\u00fc\u0108\3\2\2\2\u00fd\u00fe\7\33\2\2\u00fe\u00ff\5 \21\2"+
		"\u00ff\u0100\7\5\2\2\u0100\u0101\7g\2\2\u0101\u0102\7T\2\2\u0102\u0103"+
		"\7g\2\2\u0103\u0104\7\6\2\2\u0104\u0105\7\24\2\2\u0105\u0106\5<\37\2\u0106"+
		"\u0108\3\2\2\2\u0107\u00c9\3\2\2\2\u0107\u00cd\3\2\2\2\u0107\u00cf\3\2"+
		"\2\2\u0107\u00d1\3\2\2\2\u0107\u00e6\3\2\2\2\u0107\u00ed\3\2\2\2\u0107"+
		"\u00f5\3\2\2\2\u0107\u00fd\3\2\2\2\u0108\25\3\2\2\2\u0109\u010a\5\32\16"+
		"\2\u010a\u010b\5\36\20\2\u010b\27\3\2\2\2\u010c\u010e\7\17\2\2\u010d\u010c"+
		"\3\2\2\2\u010d\u010e\3\2\2\2\u010e\31\3\2\2\2\u010f\u0110\t\2\2\2\u0110"+
		"\33\3\2\2\2\u0111\u0112\5\32\16\2\u0112\u0113\5 \21\2\u0113\35\3\2\2\2"+
		"\u0114\u0115\5 \21\2\u0115\u0116\7g\2\2\u0116\37\3\2\2\2\u0117\u0118\b"+
		"\21\1\2\u0118\u0119\7\5\2\2\u0119\u011a\5 \21\2\u011a\u011b\7\6\2\2\u011b"+
		"\u014b\3\2\2\2\u011c\u011d\5\30\r\2\u011d\u011e\5&\24\2\u011e\u014b\3"+
		"\2\2\2\u011f\u014b\5(\25\2\u0120\u0122\5\60\31\2\u0121\u0120\3\2\2\2\u0121"+
		"\u0122\3\2\2\2\u0122\u0123\3\2\2\2\u0123\u012c\7\5\2\2\u0124\u0129\5$"+
		"\23\2\u0125\u0126\7T\2\2\u0126\u0128\5$\23\2\u0127\u0125\3\2\2\2\u0128"+
		"\u012b\3\2\2\2\u0129\u0127\3\2\2\2\u0129\u012a\3\2\2\2\u012a\u012d\3\2"+
		"\2\2\u012b\u0129\3\2\2\2\u012c\u0124\3\2\2\2\u012c\u012d\3\2\2\2\u012d"+
		"\u012e\3\2\2\2\u012e\u012f\7\6\2\2\u012f\u0130\7I\2\2\u0130\u014b\5 \21"+
		"\n\u0131\u0133\5\60\31\2\u0132\u0131\3\2\2\2\u0132\u0133\3\2\2\2\u0133"+
		"\u0134\3\2\2\2\u0134\u013d\7\5\2\2\u0135\u013a\5$\23\2\u0136\u0137\7T"+
		"\2\2\u0137\u0139\5$\23\2\u0138\u0136\3\2\2\2\u0139\u013c\3\2\2\2\u013a"+
		"\u0138\3\2\2\2\u013a\u013b\3\2\2\2\u013b\u013e\3\2\2\2\u013c\u013a\3\2"+
		"\2\2\u013d\u0135\3\2\2\2\u013d\u013e\3\2\2\2\u013e\u013f\3\2\2\2\u013f"+
		"\u0140\7\6\2\2\u0140\u0141\7#\2\2\u0141\u0142\7I\2\2\u0142\u014b\5 \21"+
		"\t\u0143\u014b\7\23\2\2\u0144\u0145\7\7\2\2\u0145\u014b\7\b\2\2\u0146"+
		"\u014b\7/\2\2\u0147\u0148\7W\2\2\u0148\u014b\5 \21\4\u0149\u014b\5\6\4"+
		"\2\u014a\u0117\3\2\2\2\u014a\u011c\3\2\2\2\u014a\u011f\3\2\2\2\u014a\u0121"+
		"\3\2\2\2\u014a\u0132\3\2\2\2\u014a\u0143\3\2\2\2\u014a\u0144\3\2\2\2\u014a"+
		"\u0146\3\2\2\2\u014a\u0147\3\2\2\2\u014a\u0149\3\2\2\2\u014b\u0153\3\2"+
		"\2\2\u014c\u014d\f\f\2\2\u014d\u014e\7\t\2\2\u014e\u0152\7\n\2\2\u014f"+
		"\u0150\f\b\2\2\u0150\u0152\5.\30\2\u0151\u014c\3\2\2\2\u0151\u014f\3\2"+
		"\2\2\u0152\u0155\3\2\2\2\u0153\u0151\3\2\2\2\u0153\u0154\3\2\2\2\u0154"+
		"!\3\2\2\2\u0155\u0153\3\2\2\2\u0156\u0158\5 \21\2\u0157\u0159\7g\2\2\u0158"+
		"\u0157\3\2\2\2\u0158\u0159\3\2\2\2\u0159\u015a\3\2\2\2\u015a\u015b\7\24"+
		"\2\2\u015b\u015e\3\2\2\2\u015c\u015e\5 \21\2\u015d\u0156\3\2\2\2\u015d"+
		"\u015c\3\2\2\2\u015e#\3\2\2\2\u015f\u0161\5 \21\2\u0160\u0162\7g\2\2\u0161"+
		"\u0160\3\2\2\2\u0161\u0162\3\2\2\2\u0162%\3\2\2\2\u0163\u0165\7\3\2\2"+
		"\u0164\u0166\5*\26\2\u0165\u0164\3\2\2\2\u0165\u0166\3\2\2\2\u0166\u016d"+
		"\3\2\2\2\u0167\u0169\7T\2\2\u0168\u016a\5*\26\2\u0169\u0168\3\2\2\2\u0169"+
		"\u016a\3\2\2\2\u016a\u016c\3\2\2\2\u016b\u0167\3\2\2\2\u016c\u016f\3\2"+
		"\2\2\u016d\u016b\3\2\2\2\u016d\u016e\3\2\2\2\u016e\u0170\3\2\2\2\u016f"+
		"\u016d\3\2\2\2\u0170\u0171\7\4\2\2\u0171\'\3\2\2\2\u0172\u0174\7\5\2\2"+
		"\u0173\u0175\5,\27\2\u0174\u0173\3\2\2\2\u0174\u0175\3\2\2\2\u0175\u017c"+
		"\3\2\2\2\u0176\u0178\7T\2\2\u0177\u0179\5,\27\2\u0178\u0177\3\2\2\2\u0178"+
		"\u0179\3\2\2\2\u0179\u017b\3\2\2\2\u017a\u0176\3\2\2\2\u017b\u017e\3\2"+
		"\2\2\u017c\u017a\3\2\2\2\u017c\u017d\3\2\2\2\u017d\u017f\3\2\2\2\u017e"+
		"\u017c\3\2\2\2\u017f\u0180\7\6\2\2\u0180)\3\2\2\2\u0181\u0182\5\32\16"+
		"\2\u0182\u0183\7g\2\2\u0183\u0184\7\24\2\2\u0184\u0185\5 \21\2\u0185+"+
		"\3\2\2\2\u0186\u018b\5\34\17\2\u0187\u0188\5\32\16\2\u0188\u0189\7Z\2"+
		"\2\u0189\u018b\3\2\2\2\u018a\u0186\3\2\2\2\u018a\u0187\3\2\2\2\u018b-"+
		"\3\2\2\2\u018c\u018d\7\7\2\2\u018d\u0192\5 \21\2\u018e\u018f\7T\2\2\u018f"+
		"\u0191\5 \21\2\u0190\u018e\3\2\2\2\u0191\u0194\3\2\2\2\u0192\u0190\3\2"+
		"\2\2\u0192\u0193\3\2\2\2\u0193\u0195\3\2\2\2\u0194\u0192\3\2\2\2\u0195"+
		"\u0196\7\b\2\2\u0196/\3\2\2\2\u0197\u0198\7\7\2\2\u0198\u019d\5\62\32"+
		"\2\u0199\u019a\7T\2\2\u019a\u019c\5\62\32\2\u019b\u0199\3\2\2\2\u019c"+
		"\u019f\3\2\2\2\u019d\u019b\3\2\2\2\u019d\u019e\3\2\2\2\u019e\u01a0\3\2"+
		"\2\2\u019f\u019d\3\2\2\2\u01a0\u01a1\7\b\2\2\u01a1\61\3\2\2\2\u01a2\u01a4"+
		"\t\3\2\2\u01a3\u01a2\3\2\2\2\u01a3\u01a4\3\2\2\2\u01a4\u01a5\3\2\2\2\u01a5"+
		"\u01a6\5 \21\2\u01a6\u01a7\t\4\2\2\u01a7\u01ac\5 \21\2\u01a8\u01a9\7*"+
		"\2\2\u01a9\u01ab\5 \21\2\u01aa\u01a8\3\2\2\2\u01ab\u01ae\3\2\2\2\u01ac"+
		"\u01aa\3\2\2\2\u01ac\u01ad\3\2\2\2\u01ad\63\3\2\2\2\u01ae\u01ac\3\2\2"+
		"\2\u01af\u01b1\7\22\2\2\u01b0\u01af\3\2\2\2\u01b0\u01b1\3\2\2\2\u01b1"+
		"\65\3\2\2\2\u01b2\u01c9\7!\2\2\u01b3\u01c9\7\"\2\2\u01b4\u01c9\7#\2\2"+
		"\u01b5\u01c9\7$\2\2\u01b6\u01c9\7\37\2\2\u01b7\u01c9\7 \2\2\u01b8\u01b9"+
		"\7\7\2\2\u01b9\u01c9\7\7\2\2\u01ba\u01bb\7\b\2\2\u01bb\u01c9\7\b\2\2\u01bc"+
		"\u01bd\7\b\2\2\u01bd\u01be\7\b\2\2\u01be\u01c9\7\b\2\2\u01bf\u01c9\7\b"+
		"\2\2\u01c0\u01c9\7\7\2\2\u01c1\u01c9\7\r\2\2\u01c2\u01c9\7\f\2\2\u01c3"+
		"\u01c9\7&\2\2\u01c4\u01c9\7\'\2\2\u01c5\u01c9\7*\2\2\u01c6\u01c9\7+\2"+
		"\2\u01c7\u01c9\7,\2\2\u01c8\u01b2\3\2\2\2\u01c8\u01b3\3\2\2\2\u01c8\u01b4"+
		"\3\2\2\2\u01c8\u01b5\3\2\2\2\u01c8\u01b6\3\2\2\2\u01c8\u01b7\3\2\2\2\u01c8"+
		"\u01b8\3\2\2\2\u01c8\u01ba\3\2\2\2\u01c8\u01bc\3\2\2\2\u01c8\u01bf\3\2"+
		"\2\2\u01c8\u01c0\3\2\2\2\u01c8\u01c1\3\2\2\2\u01c8\u01c2\3\2\2\2\u01c8"+
		"\u01c3\3\2\2\2\u01c8\u01c4\3\2\2\2\u01c8\u01c5\3\2\2\2\u01c8\u01c6\3\2"+
		"\2\2\u01c8\u01c7\3\2\2\2\u01c9\67\3\2\2\2\u01ca\u01cb\t\5\2\2\u01cb9\3"+
		"\2\2\2\u01cc\u01cd\t\6\2\2\u01cd;\3\2\2\2\u01ce\u01cf\b\37\1\2\u01cf\u02ac"+
		"\7g\2\2\u01d0\u02ac\7A\2\2\u01d1\u01d2\7\5\2\2\u01d2\u01d3\5<\37\2\u01d3"+
		"\u01d4\7\6\2\2\u01d4\u02ac\3\2\2\2\u01d5\u02ac\5\\/\2\u01d6\u01d7\t\7"+
		"\2\2\u01d7\u02ac\5<\37\60\u01d8\u01d9\t\b\2\2\u01d9\u02ac\5<\37/\u01da"+
		"\u01db\7Y\2\2\u01db\u02ac\7g\2\2\u01dc\u01dd\7\5\2\2\u01dd\u01de\5 \21"+
		"\2\u01de\u01df\7\6\2\2\u01df\u01e0\5<\37\35\u01e0\u02ac\3\2\2\2\u01e1"+
		"\u01e2\5P)\2\u01e2\u01e3\7\60\2\2\u01e3\u01e4\5<\37\34\u01e4\u02ac\3\2"+
		"\2\2\u01e5\u01e6\5\26\f\2\u01e6\u01e7\7\60\2\2\u01e7\u01ef\5<\37\2\u01e8"+
		"\u01e9\7T\2\2\u01e9\u01ea\5\36\20\2\u01ea\u01eb\7\60\2\2\u01eb\u01ec\5"+
		"<\37\2\u01ec\u01ee\3\2\2\2\u01ed\u01e8\3\2\2\2\u01ee\u01f1\3\2\2\2\u01ef"+
		"\u01ed\3\2\2\2\u01ef\u01f0\3\2\2\2\u01f0\u02ac\3\2\2\2\u01f1\u01ef\3\2"+
		"\2\2\u01f2\u01f3\7B\2\2\u01f3\u01f8\5\26\f\2\u01f4\u01f5\7T\2\2\u01f5"+
		"\u01f7\5\36\20\2\u01f6\u01f4\3\2\2\2\u01f7\u01fa\3\2\2\2\u01f8\u01f6\3"+
		"\2\2\2\u01f8\u01f9\3\2\2\2\u01f9\u02ac\3\2\2\2\u01fa\u01f8\3\2\2\2\u01fb"+
		"\u01fc\7Z\2\2\u01fc\u01fd\7g\2\2\u01fd\u01ff\7\60\2\2\u01fe\u0200\7\36"+
		"\2\2\u01ff\u01fe\3\2\2\2\u01ff\u0200\3\2\2\2\u0200\u0201\3\2\2\2\u0201"+
		"\u02ac\5 \21\2\u0202\u0203\7\30\2\2\u0203\u0204\7\3\2\2\u0204\u0209\5"+
		"B\"\2\u0205\u0206\7T\2\2\u0206\u0208\5B\"\2\u0207\u0205\3\2\2\2\u0208"+
		"\u020b\3\2\2\2\u0209\u0207\3\2\2\2\u0209\u020a\3\2\2\2\u020a\u020c\3\2"+
		"\2\2\u020b\u0209\3\2\2\2\u020c\u020d\7\4\2\2\u020d\u02ac\3\2\2\2\u020e"+
		"\u020f\7\31\2\2\u020f\u0210\7\3\2\2\u0210\u0215\5@!\2\u0211\u0212\7T\2"+
		"\2\u0212\u0214\5@!\2\u0213\u0211\3\2\2\2\u0214\u0217\3\2\2\2\u0215\u0213"+
		"\3\2\2\2\u0215\u0216\3\2\2\2\u0216\u0218\3\2\2\2\u0217\u0215\3\2\2\2\u0218"+
		"\u0219\7\4\2\2\u0219\u02ac\3\2\2\2\u021a\u021c\7C\2\2\u021b\u021d\5<\37"+
		"\2\u021c\u021b\3\2\2\2\u021c\u021d\3\2\2\2\u021d\u02ac\3\2\2\2\u021e\u0220"+
		"\7D\2\2\u021f\u0221\5<\37\2\u0220\u021f\3\2\2\2\u0220\u0221\3\2\2\2\u0221"+
		"\u02ac\3\2\2\2\u0222\u0223\7E\2\2\u0223\u02ac\5<\37\23\u0224\u0226\7F"+
		"\2\2\u0225\u0227\5<\37\2\u0226\u0225\3\2\2\2\u0226\u0227\3\2\2\2\u0227"+
		"\u02ac\3\2\2\2\u0228\u022a\7G\2\2\u0229\u022b\5<\37\2\u022a\u0229\3\2"+
		"\2\2\u022a\u022b\3\2\2\2\u022b\u02ac\3\2\2\2\u022c\u022d\7a\2\2\u022d"+
		"\u02ac\5<\37\20\u022e\u022f\7X\2\2\u022f\u02ac\5<\37\17\u0230\u0231\5"+
		"\64\33\2\u0231\u0233\7H\2\2\u0232\u0234\5\60\31\2\u0233\u0232\3\2\2\2"+
		"\u0233\u0234\3\2\2\2\u0234\u0235\3\2\2\2\u0235\u0236\5L\'\2\u0236\u0237"+
		"\5<\37\16\u0237\u02ac\3\2\2\2\u0238\u0239\5\64\33\2\u0239\u023a\7H\2\2"+
		"\u023a\u023c\7#\2\2\u023b\u023d\5\60\31\2\u023c\u023b\3\2\2\2\u023c\u023d"+
		"\3\2\2\2\u023d\u023e\3\2\2\2\u023e\u023f\5L\'\2\u023f\u0240\5<\37\r\u0240"+
		"\u02ac\3\2\2\2\u0241\u0243\5\64\33\2\u0242\u0244\5\60\31\2\u0243\u0242"+
		"\3\2\2\2\u0243\u0244\3\2\2\2\u0244\u0245\3\2\2\2\u0245\u0246\5L\'\2\u0246"+
		"\u0247\7I\2\2\u0247\u0248\5<\37\f\u0248\u02ac\3\2\2\2\u0249\u024b\5\64"+
		"\33\2\u024a\u024c\5\60\31\2\u024b\u024a\3\2\2\2\u024b\u024c\3\2\2\2\u024c"+
		"\u024d\3\2\2\2\u024d\u024e\5L\'\2\u024e\u024f\7#\2\2\u024f\u0250\7I\2"+
		"\2\u0250\u0251\5<\37\13\u0251\u02ac\3\2\2\2\u0252\u0253\7\3\2\2\u0253"+
		"\u0254\5\b\5\2\u0254\u0255\7\4\2\2\u0255\u02ac\3\2\2\2\u0256\u0257\7K"+
		"\2\2\u0257\u0258\7\5\2\2\u0258\u0259\5<\37\2\u0259\u025a\7\6\2\2\u025a"+
		"\u025d\5<\37\2\u025b\u025c\7L\2\2\u025c\u025e\5<\37\2\u025d\u025b\3\2"+
		"\2\2\u025d\u025e\3\2\2\2\u025e\u02ac\3\2\2\2\u025f\u0260\7M\2\2\u0260"+
		"\u0261\5<\37\2\u0261\u0262\7N\2\2\u0262\u0263\7\5\2\2\u0263\u0264\5<\37"+
		"\2\u0264\u0265\7\6\2\2\u0265\u02ac\3\2\2\2\u0266\u0267\7N\2\2\u0267\u0268"+
		"\7\5\2\2\u0268\u0269\5<\37\2\u0269\u026a\7\6\2\2\u026a\u026b\5<\37\7\u026b"+
		"\u02ac\3\2\2\2\u026c\u026d\7O\2\2\u026d\u026f\7\5\2\2\u026e\u0270\5<\37"+
		"\2\u026f\u026e\3\2\2\2\u026f\u0270\3\2\2\2\u0270\u0271\3\2\2\2\u0271\u0273"+
		"\7S\2\2\u0272\u0274\5<\37\2\u0273\u0272\3\2\2\2\u0273\u0274\3\2\2\2\u0274"+
		"\u0275\3\2\2\2\u0275\u0277\7S\2\2\u0276\u0278\5<\37\2\u0277\u0276\3\2"+
		"\2\2\u0277\u0278\3\2\2\2\u0278\u0279\3\2\2\2\u0279\u027a\7\6\2\2\u027a"+
		"\u02ac\5<\37\6\u027b\u027c\7O\2\2\u027c\u027d\7\5\2\2\u027d\u027e\5\26"+
		"\f\2\u027e\u027f\7\24\2\2\u027f\u0280\5<\37\2\u0280\u0281\7\6\2\2\u0281"+
		"\u0282\5<\37\5\u0282\u02ac\3\2\2\2\u0283\u0284\7P\2\2\u0284\u0285\7\5"+
		"\2\2\u0285\u0286\5<\37\2\u0286\u0287\7\6\2\2\u0287\u0290\7\3\2\2\u0288"+
		"\u028d\5> \2\u0289\u028a\7S\2\2\u028a\u028c\5> \2\u028b\u0289\3\2\2\2"+
		"\u028c\u028f\3\2\2\2\u028d\u028b\3\2\2\2\u028d\u028e\3\2\2\2\u028e\u0291"+
		"\3\2\2\2\u028f\u028d\3\2\2\2\u0290\u0288\3\2\2\2\u0290\u0291\3\2\2\2\u0291"+
		"\u0292\3\2\2\2\u0292\u0293\7\4\2\2\u0293\u02ac\3\2\2\2\u0294\u0299\7["+
		"\2\2\u0295\u0296\7\5\2\2\u0296\u0297\5<\37\2\u0297\u0298\7\6\2\2\u0298"+
		"\u029a\3\2\2\2\u0299\u0295\3\2\2\2\u0299\u029a\3\2\2\2\u029a\u029b\3\2"+
		"\2\2\u029b\u02a4\5<\37\2\u029c\u029d\7\\\2\2\u029d\u029e\7\5\2\2\u029e"+
		"\u029f\5\36\20\2\u029f\u02a0\7\6\2\2\u02a0\u02a1\5<\37\2\u02a1\u02a3\3"+
		"\2\2\2\u02a2\u029c\3\2\2\2\u02a3\u02a6\3\2\2\2\u02a4\u02a2\3\2\2\2\u02a4"+
		"\u02a5\3\2\2\2\u02a5\u02a9\3\2\2\2\u02a6\u02a4\3\2\2\2\u02a7\u02a8\7]"+
		"\2\2\u02a8\u02aa\5<\37\2\u02a9\u02a7\3\2\2\2\u02a9\u02aa\3\2\2\2\u02aa"+
		"\u02ac\3\2\2\2\u02ab\u01ce\3\2\2\2\u02ab\u01d0\3\2\2\2\u02ab\u01d1\3\2"+
		"\2\2\u02ab\u01d5\3\2\2\2\u02ab\u01d6\3\2\2\2\u02ab\u01d8\3\2\2\2\u02ab"+
		"\u01da\3\2\2\2\u02ab\u01dc\3\2\2\2\u02ab\u01e1\3\2\2\2\u02ab\u01e5\3\2"+
		"\2\2\u02ab\u01f2\3\2\2\2\u02ab\u01fb\3\2\2\2\u02ab\u0202\3\2\2\2\u02ab"+
		"\u020e\3\2\2\2\u02ab\u021a\3\2\2\2\u02ab\u021e\3\2\2\2\u02ab\u0222\3\2"+
		"\2\2\u02ab\u0224\3\2\2\2\u02ab\u0228\3\2\2\2\u02ab\u022c\3\2\2\2\u02ab"+
		"\u022e\3\2\2\2\u02ab\u0230\3\2\2\2\u02ab\u0238\3\2\2\2\u02ab\u0241\3\2"+
		"\2\2\u02ab\u0249\3\2\2\2\u02ab\u0252\3\2\2\2\u02ab\u0256\3\2\2\2\u02ab"+
		"\u025f\3\2\2\2\u02ab\u0266\3\2\2\2\u02ab\u026c\3\2\2\2\u02ab\u027b\3\2"+
		"\2\2\u02ab\u0283\3\2\2\2\u02ab\u0294\3\2\2\2\u02ac\u0316\3\2\2\2\u02ad"+
		"\u02ae\f.\2\2\u02ae\u02af\7!\2\2\u02af\u0315\5<\37.\u02b0\u02b1\f-\2\2"+
		"\u02b1\u02b2\t\t\2\2\u02b2\u0315\5<\37.\u02b3\u02b4\f,\2\2\u02b4\u02b5"+
		"\t\3\2\2\u02b5\u0315\5<\37-\u02b6\u02be\f+\2\2\u02b7\u02b8\7\7\2\2\u02b8"+
		"\u02bf\7\7\2\2\u02b9\u02ba\7\b\2\2\u02ba\u02bf\7\b\2\2\u02bb\u02bc\7\b"+
		"\2\2\u02bc\u02bd\7\b\2\2\u02bd\u02bf\7\b\2\2\u02be\u02b7\3\2\2\2\u02be"+
		"\u02b9\3\2\2\2\u02be\u02bb\3\2\2\2\u02bf\u02c0\3\2\2\2\u02c0\u0315\5<"+
		"\37,\u02c1\u02c2\f*\2\2\u02c2\u02c3\t\n\2\2\u02c3\u0315\5<\37+\u02c4\u02c5"+
		"\f)\2\2\u02c5\u02c6\7%\2\2\u02c6\u0315\5<\37*\u02c7\u02c8\f(\2\2\u02c8"+
		"\u02c9\t\13\2\2\u02c9\u0315\5<\37)\u02ca\u02cb\f\'\2\2\u02cb\u02cc\7*"+
		"\2\2\u02cc\u0315\5<\37(\u02cd\u02ce\f&\2\2\u02ce\u02cf\7+\2\2\u02cf\u0315"+
		"\5<\37\'\u02d0\u02d1\f%\2\2\u02d1\u02d2\7,\2\2\u02d2\u0315\5<\37&\u02d3"+
		"\u02d4\f$\2\2\u02d4\u02d5\7-\2\2\u02d5\u0315\5<\37%\u02d6\u02d7\f#\2\2"+
		"\u02d7\u02d8\7.\2\2\u02d8\u0315\5<\37$\u02d9\u02da\f!\2\2\u02da\u02db"+
		"\7/\2\2\u02db\u02dc\5<\37\2\u02dc\u02dd\7\24\2\2\u02dd\u02de\5<\37\"\u02de"+
		"\u0315\3\2\2\2\u02df\u02e0\f \2\2\u02e0\u02e1\7?\2\2\u02e1\u0315\5<\37"+
		"!\u02e2\u02e3\f\37\2\2\u02e3\u02e4\7@\2\2\u02e4\u0315\5<\37 \u02e5\u02e6"+
		"\f\33\2\2\u02e6\u02e7\t\f\2\2\u02e7\u0315\5<\37\33\u02e8\u02e9\f\67\2"+
		"\2\u02e9\u0315\5.\30\2\u02ea\u02eb\f\64\2\2\u02eb\u02f4\7\5\2\2\u02ec"+
		"\u02f1\5F$\2\u02ed\u02ee\7T\2\2\u02ee\u02f0\5F$\2\u02ef\u02ed\3\2\2\2"+
		"\u02f0\u02f3\3\2\2\2\u02f1\u02ef\3\2\2\2\u02f1\u02f2\3\2\2\2\u02f2\u02f5"+
		"\3\2\2\2\u02f3\u02f1\3\2\2\2\u02f4\u02ec\3\2\2\2\u02f4\u02f5\3\2\2\2\u02f5"+
		"\u02f6\3\2\2\2\u02f6\u0315\7\6\2\2\u02f7\u02f8\f\63\2\2\u02f8\u02fa\7"+
		"\3\2\2\u02f9\u02fb\5D#\2\u02fa\u02f9\3\2\2\2\u02fa\u02fb\3\2\2\2\u02fb"+
		"\u0302\3\2\2\2\u02fc\u02fe\7T\2\2\u02fd\u02ff\5D#\2\u02fe\u02fd\3\2\2"+
		"\2\u02fe\u02ff\3\2\2\2\u02ff\u0301\3\2\2\2\u0300\u02fc\3\2\2\2\u0301\u0304"+
		"\3\2\2\2\u0302\u0300\3\2\2\2\u0302\u0303\3\2\2\2\u0303\u0305\3\2\2\2\u0304"+
		"\u0302\3\2\2\2\u0305\u0315\7\4\2\2\u0306\u030d\f\62\2\2\u0307\u0308\7"+
		"R\2\2\u0308\u030e\7g\2\2\u0309\u030a\7\t\2\2\u030a\u030b\5<\37\2\u030b"+
		"\u030c\7\n\2\2\u030c\u030e\3\2\2\2\u030d\u0307\3\2\2\2\u030d\u0309\3\2"+
		"\2\2\u030e\u0315\3\2\2\2\u030f\u0310\f\61\2\2\u0310\u0315\t\6\2\2\u0311"+
		"\u0312\f\"\2\2\u0312\u0313\7Y\2\2\u0313\u0315\7g\2\2\u0314\u02ad\3\2\2"+
		"\2\u0314\u02b0\3\2\2\2\u0314\u02b3\3\2\2\2\u0314\u02b6\3\2\2\2\u0314\u02c1"+
		"\3\2\2\2\u0314\u02c4\3\2\2\2\u0314\u02c7\3\2\2\2\u0314\u02ca\3\2\2\2\u0314"+
		"\u02cd\3\2\2\2\u0314\u02d0\3\2\2\2\u0314\u02d3\3\2\2\2\u0314\u02d6\3\2"+
		"\2\2\u0314\u02d9\3\2\2\2\u0314\u02df\3\2\2\2\u0314\u02e2\3\2\2\2\u0314"+
		"\u02e5\3\2\2\2\u0314\u02e8\3\2\2\2\u0314\u02ea\3\2\2\2\u0314\u02f7\3\2"+
		"\2\2\u0314\u0306\3\2\2\2\u0314\u030f\3\2\2\2\u0314\u0311\3\2\2\2\u0315"+
		"\u0318\3\2\2\2\u0316\u0314\3\2\2\2\u0316\u0317\3\2\2\2\u0317=\3\2\2\2"+
		"\u0318\u0316\3\2\2\2\u0319\u031f\5H%\2\u031a\u031b\7\5\2\2\u031b\u031c"+
		"\5H%\2\u031c\u031d\7\6\2\2\u031d\u031f\3\2\2\2\u031e\u0319\3\2\2\2\u031e"+
		"\u031a\3\2\2\2\u031f\u0320\3\2\2\2\u0320\u0321\7J\2\2\u0321\u0322\5<\37"+
		"\2\u0322?\3\2\2\2\u0323\u0324\7g\2\2\u0324\u0325\7\24\2\2\u0325\u032b"+
		"\5<\37\2\u0326\u0327\7Z\2\2\u0327\u0328\7g\2\2\u0328\u0329\7\24\2\2\u0329"+
		"\u032b\5 \21\2\u032a\u0323\3\2\2\2\u032a\u0326\3\2\2\2\u032bA\3\2\2\2"+
		"\u032c\u032d\7g\2\2\u032d\u032e\7\24\2\2\u032e\u032f\7g\2\2\u032fC\3\2"+
		"\2\2\u0330\u0331\7g\2\2\u0331\u0332\7\24\2\2\u0332\u0333\5<\37\2\u0333"+
		"E\3\2\2\2\u0334\u0336\7W\2\2\u0335\u0334\3\2\2\2\u0335\u0336\3\2\2\2\u0336"+
		"\u0337\3\2\2\2\u0337\u0338\5<\37\2\u0338G\3\2\2\2\u0339\u033c\5J&\2\u033a"+
		"\u033b\7T\2\2\u033b\u033d\5J&\2\u033c\u033a\3\2\2\2\u033c\u033d\3\2\2"+
		"\2\u033dI\3\2\2\2\u033e\u0342\5<\37\2\u033f\u0342\5P)\2\u0340\u0342\7"+
		"Q\2\2\u0341\u033e\3\2\2\2\u0341\u033f\3\2\2\2\u0341\u0340\3\2\2\2\u0342"+
		"K\3\2\2\2\u0343\u0345\7\5\2\2\u0344\u0346\5N(\2\u0345\u0344\3\2\2\2\u0345"+
		"\u0346\3\2\2\2\u0346\u034d\3\2\2\2\u0347\u0349\7T\2\2\u0348\u034a\5N("+
		"\2\u0349\u0348\3\2\2\2\u0349\u034a\3\2\2\2\u034a\u034c\3\2\2\2\u034b\u0347"+
		"\3\2\2\2\u034c\u034f\3\2\2\2\u034d\u034b\3\2\2\2\u034d\u034e\3\2\2\2\u034e"+
		"\u0350\3\2\2\2\u034f\u034d\3\2\2\2\u0350\u0351\7\6\2\2\u0351M\3\2\2\2"+
		"\u0352\u0354\5\"\22\2\u0353\u0355\7W\2\2\u0354\u0353\3\2\2\2\u0354\u0355"+
		"\3\2\2\2\u0355\u0356\3\2\2\2\u0356\u0357\5P)\2\u0357O\3\2\2\2\u0358\u0359"+
		"\5R*\2\u0359Q\3\2\2\2\u035a\u035b\b*\1\2\u035b\u035d\7\t\2\2\u035c\u035e"+
		"\5T+\2\u035d\u035c\3\2\2\2\u035d\u035e\3\2\2\2\u035e\u0365\3\2\2\2\u035f"+
		"\u0361\7T\2\2\u0360\u0362\5T+\2\u0361\u0360\3\2\2\2\u0361\u0362\3\2\2"+
		"\2\u0362\u0364\3\2\2\2\u0363\u035f\3\2\2\2\u0364\u0367\3\2\2\2\u0365\u0363"+
		"\3\2\2\2\u0365\u0366\3\2\2\2\u0366\u0368\3\2\2\2\u0367\u0365\3\2\2\2\u0368"+
		"\u0389\7\n\2\2\u0369\u036b\7\5\2\2\u036a\u036c\5V,\2\u036b\u036a\3\2\2"+
		"\2\u036b\u036c\3\2\2\2\u036c\u0373\3\2\2\2\u036d\u036f\7T\2\2\u036e\u0370"+
		"\5V,\2\u036f\u036e\3\2\2\2\u036f\u0370\3\2\2\2\u0370\u0372\3\2\2\2\u0371"+
		"\u036d\3\2\2\2\u0372\u0375\3\2\2\2\u0373\u0371\3\2\2\2\u0373\u0374\3\2"+
		"\2\2\u0374\u0376\3\2\2\2\u0375\u0373\3\2\2\2\u0376\u0389\7\6\2\2\u0377"+
		"\u0380\7\3\2\2\u0378\u037d\5X-\2\u0379\u037a\7T\2\2\u037a\u037c\5X-\2"+
		"\u037b\u0379\3\2\2\2\u037c\u037f\3\2\2\2\u037d\u037b\3\2\2\2\u037d\u037e"+
		"\3\2\2\2\u037e\u0381\3\2\2\2\u037f\u037d\3\2\2\2\u0380\u0378\3\2\2\2\u0380"+
		"\u0381\3\2\2\2\u0381\u0382\3\2\2\2\u0382\u0389\7\4\2\2\u0383\u0386\7g"+
		"\2\2\u0384\u0385\7\60\2\2\u0385\u0387\5<\37\2\u0386\u0384\3\2\2\2\u0386"+
		"\u0387\3\2\2\2\u0387\u0389\3\2\2\2\u0388\u035a\3\2\2\2\u0388\u0369\3\2"+
		"\2\2\u0388\u0377\3\2\2\2\u0388\u0383\3\2\2\2\u0389\u03b7\3\2\2\2\u038a"+
		"\u038b\f\t\2\2\u038b\u038d\7\t\2\2\u038c\u038e\5T+\2\u038d\u038c\3\2\2"+
		"\2\u038d\u038e\3\2\2\2\u038e\u0395\3\2\2\2\u038f\u0391\7T\2\2\u0390\u0392"+
		"\5T+\2\u0391\u0390\3\2\2\2\u0391\u0392\3\2\2\2\u0392\u0394\3\2\2\2\u0393"+
		"\u038f\3\2\2\2\u0394\u0397\3\2\2\2\u0395\u0393\3\2\2\2\u0395\u0396\3\2"+
		"\2\2\u0396\u0398\3\2\2\2\u0397\u0395\3\2\2\2\u0398\u03b6\7\n\2\2\u0399"+
		"\u039a\f\b\2\2\u039a\u039c\7\5\2\2\u039b\u039d\5V,\2\u039c\u039b\3\2\2"+
		"\2\u039c\u039d\3\2\2\2\u039d\u03a4\3\2\2\2\u039e\u03a0\7T\2\2\u039f\u03a1"+
		"\5V,\2\u03a0\u039f\3\2\2\2\u03a0\u03a1\3\2\2\2\u03a1\u03a3\3\2\2\2\u03a2"+
		"\u039e\3\2\2\2\u03a3\u03a6\3\2\2\2\u03a4\u03a2\3\2\2\2\u03a4\u03a5\3\2"+
		"\2\2\u03a5\u03a7\3\2\2\2\u03a6\u03a4\3\2\2\2\u03a7\u03b6\7\6\2\2\u03a8"+
		"\u03a9\f\7\2\2\u03a9\u03b2\7\3\2\2\u03aa\u03af\5X-\2\u03ab\u03ac\7T\2"+
		"\2\u03ac\u03ae\5X-\2\u03ad\u03ab\3\2\2\2\u03ae\u03b1\3\2\2\2\u03af\u03ad"+
		"\3\2\2\2\u03af\u03b0\3\2\2\2\u03b0\u03b3\3\2\2\2\u03b1\u03af\3\2\2\2\u03b2"+
		"\u03aa\3\2\2\2\u03b2\u03b3\3\2\2\2\u03b3\u03b4\3\2\2\2\u03b4\u03b6\7\4"+
		"\2\2\u03b5\u038a\3\2\2\2\u03b5\u0399\3\2\2\2\u03b5\u03a8\3\2\2\2\u03b6"+
		"\u03b9\3\2\2\2\u03b7\u03b5\3\2\2\2\u03b7\u03b8\3\2\2\2\u03b8S\3\2\2\2"+
		"\u03b9\u03b7\3\2\2\2\u03ba\u03bc\7W\2\2\u03bb\u03ba\3\2\2\2\u03bb\u03bc"+
		"\3\2\2\2\u03bc\u03bd\3\2\2\2\u03bd\u03be\5R*\2\u03beU\3\2\2\2\u03bf\u03c1"+
		"\7W\2\2\u03c0\u03bf\3\2\2\2\u03c0\u03c1\3\2\2\2\u03c1\u03c2\3\2\2\2\u03c2"+
		"\u03c3\5R*\2\u03c3W\3\2\2\2\u03c4\u03c5\5R*\2\u03c5\u03c6\7\24\2\2\u03c6"+
		"\u03c7\5R*\2\u03c7\u03ca\3\2\2\2\u03c8\u03ca\5R*\2\u03c9\u03c4\3\2\2\2"+
		"\u03c9\u03c8\3\2\2\2\u03caY\3\2\2\2\u03cb\u03cc\7i\2\2\u03cc[\3\2\2\2"+
		"\u03cd\u03ce\5\30\r\2\u03ce\u03cf\5\f\7\2\u03cf\u03de\3\2\2\2\u03d0\u03de"+
		"\5\n\6\2\u03d1\u03de\5\16\b\2\u03d2\u03de\5^\60\2\u03d3\u03de\5Z.\2\u03d4"+
		"\u03de\7d\2\2\u03d5\u03de\7h\2\2\u03d6\u03de\7b\2\2\u03d7\u03de\7c\2\2"+
		"\u03d8\u03de\7`\2\2\u03d9\u03de\7^\2\2\u03da\u03de\7_\2\2\u03db\u03de"+
		"\7\16\2\2\u03dc\u03de\7\23\2\2\u03dd\u03cd\3\2\2\2\u03dd\u03d0\3\2\2\2"+
		"\u03dd\u03d1\3\2\2\2\u03dd\u03d2\3\2\2\2\u03dd\u03d3\3\2\2\2\u03dd\u03d4"+
		"\3\2\2\2\u03dd\u03d5\3\2\2\2\u03dd\u03d6\3\2\2\2\u03dd\u03d7\3\2\2\2\u03dd"+
		"\u03d8\3\2\2\2\u03dd\u03d9\3\2\2\2\u03dd\u03da\3\2\2\2\u03dd\u03db\3\2"+
		"\2\2\u03dd\u03dc\3\2\2\2\u03de]\3\2\2\2\u03df\u03e8\7\13\2\2\u03e0\u03e7"+
		"\7l\2\2\u03e1\u03e2\7k\2\2\u03e2\u03e3\5<\37\2\u03e3\u03e4\7\4\2\2\u03e4"+
		"\u03e7\3\2\2\2\u03e5\u03e7\7m\2\2\u03e6\u03e0\3\2\2\2\u03e6\u03e1\3\2"+
		"\2\2\u03e6\u03e5\3\2\2\2\u03e7\u03ea\3\2\2\2\u03e8\u03e9\3\2\2\2\u03e8"+
		"\u03e6\3\2\2\2\u03e9\u03eb\3\2\2\2\u03ea\u03e8\3\2\2\2\u03eb\u03ec\7j"+
		"\2\2\u03ec_\3\2\2\2\u03ed\u03ef\7S\2\2\u03ee\u03ed\3\2\2\2\u03ee\u03ef"+
		"\3\2\2\2\u03ef\u03f0\3\2\2\2\u03f0\u03f1\5<\37\2\u03f1a\3\2\2\2tpw{\u0080"+
		"\u0086\u008a\u008e\u009a\u009d\u00a1\u00a5\u00a9\u00b0\u00b4\u00b8\u00be"+
		"\u00c3\u00c7\u00e0\u0107\u010d\u0121\u0129\u012c\u0132\u013a\u013d\u014a"+
		"\u0151\u0153\u0158\u015d\u0161\u0165\u0169\u016d\u0174\u0178\u017c\u018a"+
		"\u0192\u019d\u01a3\u01ac\u01b0\u01c8\u01ef\u01f8\u01ff\u0209\u0215\u021c"+
		"\u0220\u0226\u022a\u0233\u023c\u0243\u024b\u025d\u026f\u0273\u0277\u028d"+
		"\u0290\u0299\u02a4\u02a9\u02ab\u02be\u02f1\u02f4\u02fa\u02fe\u0302\u030d"+
		"\u0314\u0316\u031e\u032a\u0335\u033c\u0341\u0345\u0349\u034d\u0354\u035d"+
		"\u0361\u0365\u036b\u036f\u0373\u037d\u0380\u0386\u0388\u038d\u0391\u0395"+
		"\u039c\u03a0\u03a4\u03af\u03b2\u03b5\u03b7\u03bb\u03c0\u03c9\u03dd\u03e6"+
		"\u03e8\u03ee";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}