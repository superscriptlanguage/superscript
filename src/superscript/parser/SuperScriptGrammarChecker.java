package superscript.parser;

import java.util.List;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.TerminalNode;

public class SuperScriptGrammarChecker extends SuperScriptParserBaseListener {
    TokenStream tokens;
    SuperScriptParser parser;

    public SuperScriptGrammarChecker(SuperScriptParser parser) {
        this.parser = parser;
        tokens = parser.getTokenStream();
    }

    @Override
    public void enterInfix(SuperScriptParser.InfixContext ctx) {
        List<TerminalNode> langles = ctx.LANGLE();
        List<TerminalNode> rangles = ctx.RANGLE();
        checkNoWs(langles, "extraneous input between '<' in infix operator", ctx);
        checkNoWs(rangles, "extraneous input between '>' in infix operator", ctx);
    }

    @Override
    public void enterVar(SuperScriptParser.VarContext ctx) {
        if (ctx.destruct() instanceof SuperScriptParser.NameContext) {
            Token name = ((SuperScriptParser.NameContext) ctx.destruct()).NAME().getSymbol();
            parser.notifyErrorListeners(name, "Invalid destructuring including a name in primary scope.",
                    new RecognitionException(parser, name.getInputStream(), ctx));
        }
    }

    @Override
    public void enterInfixop(SuperScriptParser.InfixopContext ctx) {
        List<TerminalNode> langles = ctx.LANGLE();
        List<TerminalNode> rangles = ctx.RANGLE();
        checkNoWs(langles, "extraneous input between '<' in infix operator", ctx);
        checkNoWs(rangles, "extraneous input between '>' in infix operator", ctx);
    }

    private void checkNoWs(List<TerminalNode> nodes, String msg, ParserRuleContext ctx) {
        if (nodes.size() == 0)
            return;
        for (int i = 0; i < nodes.size() - 1; i++) {
            Token symbol = nodes.get(i).getSymbol();
            if (tokens.get(symbol.getTokenIndex() + 1).getType() == SuperScriptLexer.WS) {
                parser.notifyErrorListeners(symbol, msg,
                        new RecognitionException(parser, symbol.getInputStream(), ctx));
            }
        }
    }

}