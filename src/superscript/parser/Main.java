package superscript.parser;

import superscript.utils.*;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.CharStream;
import java.io.IOException;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

public class Main {
    public static String printf() {
        CharStream s = null;
        try {
            s = CharStreams.fromFileName("..//input.txt");
        } catch (IOException e) {
            try {
                s = CharStreams.fromFileName("input.txt");
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        return printfbase(s);
    }

    public static String printf(String arg) {
        CharStream s = CharStreams.fromString(arg);
        return printfbase(s);
    }

    private static String printfbase(CharStream s) {
        SuperScriptLexer lexer = new SuperScriptLexer(s);
        SuperScriptParser parser = new SuperScriptParser(new CommonTokenStream(lexer));
        SuperScriptPrinterErrorListener listener = new SuperScriptPrinterErrorListener("src/input.txt", "\t", "\t",
                new ErrorHandler()).uninstall();
        setErrors(parser, listener);
        ParseTreeWalker walker = new ParseTreeWalker();
        SuperScriptPrinter printer = new SuperScriptPrinter(parser);
        SuperScriptParser.FileContext file = parser.file();
        walker.walk(new SuperScriptGrammarChecker(parser), file);
        walker.walk(printer, file);
        if (parser.getNumberOfSyntaxErrors() > 0) {
            throw new RuntimeException("Syntax errors:\n" + listener.errors);
        }
        return printer.toString();
    }

    public static String print() {
        CharStream s = null;
        try {
            s = CharStreams.fromFileName("..//input.txt");
        } catch (IOException e) {
            try {
                s = CharStreams.fromFileName("input.txt");
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        return printbase(s);
    }

    public static String print(String arg) {
        CharStream s = CharStreams.fromString(arg);
        return printbase(s);
    }

    private static String printbase(CharStream s) {
        SuperScriptLexer lexer = new SuperScriptLexer(s);
        SuperScriptParser parser = new SuperScriptParser(new CommonTokenStream(lexer));
        SuperScriptPrinterErrorListener listener = new SuperScriptPrinterErrorListener("src/input.txt", "\t", "\t",
                new ErrorHandler()).uninstall();
        setErrors(parser, listener);
        ParseTree file = parser.file();
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(new SuperScriptGrammarChecker(parser), file);
        if (parser.getNumberOfSyntaxErrors() > 0) {
            throw new RuntimeException("Syntax errors at " + file.toStringTree(parser) + ":\n" + listener.errors);
        }
        return file.toStringTree(parser);
    }

    public static boolean hasErrored(String arg) {
        CharStream s = CharStreams.fromString(arg);
        SuperScriptLexer lexer = new SuperScriptLexer(s);
        SuperScriptParser parser = new SuperScriptParser(new CommonTokenStream(lexer));
        setErrors(parser, new BaseErrorListener());
        ParseTree file = parser.file();
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(new SuperScriptGrammarChecker(parser), file);
        return parser.getNumberOfSyntaxErrors() > 0;
    }

    private static void setErrors(Parser parser, ANTLRErrorListener errorListener) {
        parser.removeErrorListeners();
        parser.addErrorListener(errorListener);
    }

    public static boolean hasErrored() {
        CharStream s = null;
        try {
            s = CharStreams.fromFileName("..//input.txt");
        } catch (IOException e) {
            try {
                s = CharStreams.fromFileName("input.txt");
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        SuperScriptLexer lexer = new SuperScriptLexer(s);
        SuperScriptParser parser = new SuperScriptParser(new CommonTokenStream(lexer));
        setErrors(parser, new BaseErrorListener());
        ParseTree file = parser.file();
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(new SuperScriptGrammarChecker(parser), file);
        return parser.getNumberOfSyntaxErrors() > 0;
    }

    public static void main(String[] args) {
        try {
            System.out.println(print());
            System.out.println(printf());
            System.out.println(hasErrored());
        } catch (RuntimeException e) {
            System.out.print(e.getMessage());
        }
    }
}