package superscript.parser;

import java.util.ArrayList;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.runtime.tree.Trees;

public class SuperScriptPrinter extends SuperScriptParserBaseListener {
    private int indent;
    private String str;
    private SuperScriptParser parser;
    private ArrayList<String> errs;

    SuperScriptPrinter(SuperScriptParser parser) {
        this.parser = parser;
        str = "Parse Tree:\n";
        indent = 0;
        errs = new ArrayList<>();
    }

    @Override
    public void enterEveryRule(ParserRuleContext ctx) {
        format(Trees.getNodeText(ctx, parser) + " (");
        indent++;
    }

    @Override
    public void exitEveryRule(ParserRuleContext ctx) {
        indent--;
        format(")");
    }

    @Override
    public void visitTerminal(TerminalNode node) {
        format('"' + node.getSymbol().getText() + '"');
    }

    public String getName(int val) {
        return parser.getVocabulary().getLiteralName(val);
    }

    public void format(String string) {
        for (int i = 0; i < indent; i++) {
            str += " ";// A space.
        }
        str += string + "\n";
    }

    @Override
    public String toString() {
        return str;
    }

    public String[] getErrors() {
        return errs.toArray(new String[0]);
    }
}