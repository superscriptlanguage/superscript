scalaVersion := "0.19.0"
scalaSource in Compile := baseDirectory.value / "src"
mainClass in (Compile, run) := Some("org.superscript.compiler.Compile")
assemblyJarName in assembly := "compile.jar"
mainClass in assembly := Some("org.superscript.compiler.Compile")
ensimeIgnoreScalaMismatch in ThisBuild := true