# Event Loops

This:

```SuperScript
let stream = getInputStream();

stream.on("data", (chunk) => {
    print << chunk;
});
```

Would go to:

```Java
public class Main {
    public static void main(String[] args) {
        EventLoop loop = new EventLoop();
        InputStream stream = getInputStream.call();
        int id = loop.addInputStream(stream);
        loop.addListener(id, "data", (chunk) => {
            System.out.print(chunk);
        });
        loop.start();
    }
}
```

or:

```Java
public class Main {
    public static void main(String[] args) {
        EventLoop loop = new EventLoop();
        InputStream stream = getInputStream.call();
        loop.addInputStream(stream);
        loop.addListener(stream, "data", (chunk) => {
            System.out.print(chunk);
        });
        loop.start();
    }
}
```
