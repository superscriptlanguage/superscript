addSbtPlugin("ch.epfl.lamp" % "sbt-dotty" % "0.3.1")
addSbtPlugin("org.ensime" % "sbt-ensime" % "2.5.1")
addSbtPlugin("ch.epfl.scala" % "sbt-scalafix" % "0.9.5")
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.10")