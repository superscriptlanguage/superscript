# My Guide to Krakatau

[TOC]

## `.signature` (Class)

Syntax: `.signature '<generics>superclass interfaces*'`

For example, `.signature '<Q:Ljava/lang/Object;>Ljava/lang/Object'` would be valid.

To have the generic extend something, use `'<Q::Lextend>'`.

## `.signature` (Method)

Syntax: `.signature (args)return`

The return value is a type with generics.

## `.code`

Syntax: `.code stack * locals *`

Both ints, end with `.end code`, for methods.

## Types

There are many types:

* `Z` is a `boolean`.
* `J` is a `long`.
* `B` is a `byte`.
* `I` is a `int`.
* `F` is a `float`.
* `D` is a `double`.
* `C` is a `char`.
* `T...;` means a generic type of name `...`.
* `L...;` means a object of type `...`.