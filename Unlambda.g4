/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 by Bart Kiers
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Project : python3-parser; an ANTLR4 grammar for Python 3 https://github.com/bkiers/python3-parser
 * Developed by : Bart Kiers, bart@big-o.nl
 */
grammar Unlambda;

// All comments that start with "///" are copy-pasted from The Python Language Reference

tokens {
	INDENT,
	DEDENT
}

@lexer::header {
import java.util.Stack;
import java.util.LinkedList;
}

@lexer::members {
    private int opening = 0;
    private Stack<Integer> indents = new Stack<Integer>();
  
  	private Token lastToken = null;
	private LinkedList<Token> tokens = new LinkedList<>();
    @Override
    public void emit(Token t) {
        super.setToken(t);
        tokens.offer(t);
    }
  
    @Override
    public Token nextToken() {
        if (_input.LA(1) == EOF && !this.indents.isEmpty()) {
            // Remove any trailing EOF tokens from our buffer.
            for (int i = tokens.size() - 1; i >= 0; i--) {
                if (tokens.get(i).getType() == EOF) {
                    tokens.remove(i);
                }
            }
    
            this.emit(commonToken(UnlambdaParser.NEWLINE, "\n"));
    
            while (!indents.isEmpty()) {
                dedent();
                indents.pop();
            }
    
            this.emit(commonToken(UnlambdaParser.EOF, "<EOF>"));
        }
    
        Token next = super.nextToken();
    
        if (next.getChannel() == Token.DEFAULT_CHANNEL) {
            this.lastToken = next;
        }
    
        return tokens.isEmpty() ? next : tokens.poll();
    }
  
    private void dedent() {
        CommonToken dedent = commonToken(UnlambdaParser.DEDENT, "\n");
        dedent.setLine(this.lastToken.getLine());
        emit(dedent);
        emit(commonToken(NEWLINE, "\n"));
    }
  
    private CommonToken commonToken(int type, String text) {
        int stop = this.getCharIndex() - 1;
        int start = text.isEmpty() ? stop : stop - text.length() + 1;
        return new CommonToken(this._tokenFactorySourcePair, type, DEFAULT_TOKEN_CHANNEL, start, stop);
    }
 
    static int getIndentationCount(String spaces) {
        int count = 0;
        for (char ch : spaces.toCharArray()) {
            switch (ch) {
                case '\t':
                    count += 4;
                    break;
                default:
                    count++;
            }
        }
    
        return count;
    }
}

/*
 * parser rules
 */
file: NEWLINE* statement* EOF;

statement:
	expression NEWLINE+	# exprstmt
	| nullexp NEWLINE+	# nullstmt;

expression:
	NAME # access
	| first = expression '(' (
		call += expression (',' call += expression)*
	)? ')' # call
	| first = expression call += expression (
		',' call += expression
	)* # call
	| (
		'(' (lazyname (',' lazyname)*)? ')'
		| (lazyname (',' lazyname)*)?
	) '->' expression									# define
	| '\\' (lazyname (',' lazyname)*)? '.' expression	# define
	| NEWLINE INDENT NEWLINE* statement+ DEDENT			# block
	| '(' expression ')'								# parens
	| 'return' expression								# return;
nullexp: lazyname ':=' expression # create;

lazyname: ('=>' | '|') NAME # lazyy | NAME # lazyn;
LPAREN: '(' {opening++;};
RPAREN: ')' {opening--;};

NAME: [a-zA-Z_$] [a-zA-Z_$0-9]* | '`' (~'`' | '\\' .)+ '`';

WS: ([ \t]+ | '\\' WS NL | '#' ~[\r\n\f]* | {opening > 0}? NL) -> skip;

NEWLINE:
	NL [ \t]* {
	String spaces = getText().replaceAll("[\r\n\f]+", "");
	int next = _input.LA(1);
	if(opening > 0 || next == '\r' || next == '\n' || next == '\f' || next == '#') {
        skip();
	} else {
        emit(commonToken(NEWLINE, "\n"));
		int indent = getIndentationCount(spaces);
		int previous = indents.isEmpty() ? 0 : indents.peek();
		if(indent != previous) {
			if(indent > previous) {
				indents.push(indent);
				emit(commonToken(UnlambdaParser.INDENT, spaces));
			} else if(indent < previous) {
				while(!indents.isEmpty() && indents.peek() > indent) {
                    dedent();
					indents.pop();
				}
			}
		}
	}
};

fragment NL: ('\r'? '\n' | '\r' | '\f');