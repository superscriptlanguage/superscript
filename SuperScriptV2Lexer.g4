lexer grammar SuperScriptV2Lexer;
channels {
	DOCUMENTATION
}

LCURLY: '{' -> pushMode(DEFAULT_MODE);
RCURLY: '}' -> popMode;
LPAREN: '(';
RPAREN: ')';
LBRACK: '[';
RBRACK: ']';
LANGLE: '<';
RANGLE: '>';

BIGSTRINGSTART: LETTER+ '"""' -> pushMode(BIGSTRING);
STRINGSTART: LETTER+ '"' -> pushMode(STRING);

TYPEOF: 'typeof';

SEMI: ';';
COMMA: ',';
PERIOD: '.';
EQ: '=';
COLON: ':'; //Colon cancer: Too many colons.

OTHER: '_';

BIGSTRINGY:
	'"""' (~'"' | '"' ~'"' | '""' ~'"' | '\\' .)*? '"""';
STRINGY: '"' (~["\r\n] | '\\' .)*? '"';

SSDOC: '/**' .*? '*/' -> channel(DOCUMENTATION);
WS: (SPACING | BLOCKCOMMENT | INLINECOMMENT) -> channel(HIDDEN);
fragment SPACING: [ \r\t\n];
fragment BLOCKCOMMENT: '/*' .*? '*/';
fragment INLINECOMMENT: '//' ~[\r\n]*;

// LNAME0 to LNAME9 are infix operator names in decreasing order of precedence.
LNAME1: ('*' | '/' | '%') LLETIFY;
LNAME2: ('+' | '-') LLETIFY;
LNAME3: (':') LLETIFY;
LNAME4: ('=' | '!') LLETIFY;
LNAME5: ('<' | '>') LLETIFY;
LNAME6: ('&') LLETIFY;
LNAME7: ('^') LLETIFY;
LNAME8: ('|') LLETIFY;
LNAME9: ALPHA LLETIFY;
// The parser takes the first alternative avaialable, so we need LNAME0 to be after LNAME9 even
// though it binds tighter.
LNAME0: GLLETIFY;

// RNAME0 to RNAME9 are infix operator names in decreasing order of precedence.
RNAME1: ('*' | '/' | '%') RLETIFY;
RNAME2: ('+' | '-') RLETIFY;
RNAME3: (':') RLETIFY;
RNAME4: ('=' | '!') RLETIFY;
RNAME5: ('<' | '>') RLETIFY;
RNAME6: ('&') RLETIFY;
RNAME7: ('^') RLETIFY;
RNAME8: ('|') RLETIFY;
RNAME9: ALPHA RLETIFY;
// The parser takes the first alternative avaialable, so we need RNAME0 to be after RNAME9 even
// though it binds tighter.
RNAME0: GRLETIFY;

CHAR: '\'' (~['\r\n] | '\\' .) '\'';
fragment INTNONEG: [1-9] DIGIT*;
fragment INT: '0' | '-'? INTNONEG;
NUMBER:
	(
		INTNONEG ('.' DIGIT*)? ([eE] INT)?
		| '0'
		| '0x' HEXDIGIT*
		| '0b' BINDIGIT*
		| '0o' OCTDIGIT*
	) [a-z]?;
fragment DIGIT: [0-9];
fragment HEXDIGIT: [0-9a-fA-F];
fragment BINDIGIT: [01];
fragment OCTDIGIT: [0-7];

// LLETIFY is a left associative infix operator.
fragment LLETIFY: GLLETIFY?;
fragment GLLETIFY: LETUN* LLETTER;
// RLETIFY is right associative.
fragment RLETIFY: GRLETIFY?;
fragment GRLETIFY: LETUN* RLETTER;

fragment LLETTER: [!#-&*+\-/-9<-Z^a-z|~];
fragment RLETTER: ':';
fragment LETTER: [!#-&*+\-/-:<-Z^a-z|~];
fragment LETUN: LETTER | '_';
fragment ALPHA: [a-zA-Z];

mode BIGSTRING;
BIGSTRINGEND: '"""' -> popMode;
BIGINTERPEXC: '$$';
BIGSTRINGINTERP: '${' -> pushMode(DEFAULT_MODE);
BIGSTRINGINTERP2: '$' LETUN* LETTER;
BIGCHUNK: (~'"' | '"' ~'"' | '""' ~'"' | '\\' .)+;
BIGCHARACTER: ~'"';

mode STRING;

STRINGEND: '"' -> popMode;
INTERPEXC: '$$';
STRINGINTERP: '${' -> pushMode(DEFAULT_MODE);
STRINGINTERP2: '$' LETUN* LETTER;
CHUNK: (~'"' | '\\' .)+;
CHARACTER: ~'"';