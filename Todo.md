# Todo

- [ ] Videogame: "Boink.io"
  - [ ] One giant arena
  - [ ] Custom tanks
  - [ ] Levelling up
  - [ ] Stats
- [ ] Minecraft tower of DOOM build
  - [ ] Has a giant pit around it
  - [ ] A "pipeline" leading to it, which feeds it supplies and people
  - [ ] Odd amount of blocks, 3x3 doors
