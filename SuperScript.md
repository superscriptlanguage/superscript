# SuperScript

- [ ] Prototypal Inheritance
- [x] Static Type Checking
- [x] Event Loop
- [ ] Yield

Sample class:

```Scala
class Foo {
    def bar: Int
}

def createFoo(num: Int): Foo = new Foo {
    val bar = num
}
```

Sample trait:

```Scala
trait Foo {
    def bar: Int
}

class Bar extends Foo {
    def baz: Int
}
```

Sample mixin:

```Scala
trait Foo {
    def bar: Int = 0
}

val baz = 1 :: Nil

val qux = baz with Foo
qux.bar //> 0
```

Sample inheritance:

```Scala
abstract class IntSeq {
    abstract def apply(pos: Int): Int
    abstract def size: Int

    def foreach(f: Int => Unit): Unit
}

def seq: IntSeq = new IntSeq {
    def foreach(f: Int => Unit): Unit = for(val i = 0; i < size; i++) f(apply(i))
}

implicit val defaultSeq = seq

// This:

class IntList inherits IntSeq

def (num: Int) :: (list: IntList)(implicit seq: IntSeq): IntList = new IntList from seq {
    def apply(pos: Int): Int = if(pos == 0) num else list(pos - 1)
    def size: Int = list.size + 1
}

def Nil(implicit seq: IntSeq) = new IntList from seq {
    def apply(pos: Int): Int = throw new IndexOutOfBoundsException("List Index out of bounds for list 'Nil'")
    def size: Int = 0
}

// Or:

abstract class IntList inherits IntSeq {
    def ::(num: Int)(implicit seq: IntSeq): IntList
}

def list(implicit seq: IntSeq): IntList = new IntList from seq { self =>
    def ::(num: Int)(implicit seq: IntSeq, implicit list: IntList): IntList = new IntListImpl from list(seq) {
        def apply(pos: Int): Int = if(pos == 0) num else self(pos - 1)
        def size: Int = self.size + 1

        override def foreach(f: Int => Unit): Unit = {
            f(num)
            self.foreach(f)
        }
    }
}

implicit val defaultList = list

class IntListImpl inherits IntList {
    def apply(pos: Int): Int
    def size: Int
}

def Nil(implicit list: IntList, seq: IntSeq) = new IntListImpl from list(seq) {
    def apply(pos: Int): Int = throw new IndexOutOfBoundsException("List Index out of bounds for list 'Nil'")
    def size: Int = 0

    override def foreach(f: Int => Unit): Unit = ()
}
```

Or:

```Scala
trait IntSeq {
    def apply(pos: Int): Int
    def size: Int
}

class IntSeqOps {
    def foreach(f: Int => Unit): Unit
}

implicit def getSequenceOps(seq: IntSeq): IntSeqOps = new IntSeqOps {
    def foreach(f: Int => Unit): Unit = for(i <- 0 until seq.size) f(seq(i))
}
```

And:

```Scala
abstract class A {
    abstract def foo: Int
    def bar: Double
}

def a: A = new A {
    def bar: Double = 1d
}

abstract class C inherits A {
    def baz: Boolean
}

def c(a: A) = new C from a {
    def baz: Boolean = true
}

b(c)
```

```Ascii
          A
         / \
Abstract | | Concrete
         / \
            Some implementation of the concrete members
```

```Scala
abstract class IntSeq(delegator: IntSeq, prototype: SSObject) extends SSObject(prototype, prototype) {
    def apply(pos: Int): Int
    def size: Int

    def foreach(f: Int => Unit): Unit = delegator.foreach(f)
}

abstract class IntList(delegator: IntList, prototype: IntSeq) extends IntSeq(prototype, prototype) {
    def ::(num: Int): IntList = delegator.::(num)
}
```

```Scala
val bar = 10

f[|foo ${bar} baz|]
f(quasi"foo ${bar} baz")

f"foo ${bar} baz"

def (sc: StringContext) quasi[A >: Any] (args: A*): Nothing = ???
```

Example of Prototypal Inheritance:

```Scala
class InputStream {
    def read: Int

    def available: Int
}

def doSomethingWithInputStream(stream: InputStream): Unit = ???

def getDebugInputStream(stream: InputStream) = stream {
    def read: Int = {
        val value = stream.read
        println(s"Reading: $value")
        value
    }
}
doSomethingWithInputStream(getDebugInputStream(stream))
```

```Scala
abstract class InputStream {
    def read: Int

    def available: Int
}

def doSomethingWithInputStream(stream: InputStream): Unit = ???

def getDebugInputStream(stream: InputStream) = new InputStream {
    def read: Int = {
        val value = stream.read
        println(s"Reading: $value")
        value
    }

    def available: Int = stream.available
}
```

```Scala
trait Functor[F[_]] {
    def apply[A, B](f: A => B): F[A] => F[B]
}

object SeqFunctor extends Functor[Seq] {
    def apply[A, B](f: A => B): Seq[A] => Seq[B] = seq => seq.map(f)
}

implicit class FunctorOps[F[_], T](func: F[T]) extends AnyVal {
    def map[S](f: T => S)(implicit functor: Functor[F]) = functor(f)(func)
}
```

```Scala (SuperScript)
class A
    def x: Int
    val y: Double


class B
    def z: String


val a = new A
    def x: Int = 0
    val y: Double = 1


val b = new B from a
    def z: String = " "
```

```Scala
abstract class A {
    def x: Int
    val y: Double
}

class ADelegator(__0_del: A) extends A {
    def x: Int = __0_del.x
    val y: Double = __0_del.y
}

abstract class B(__0_proto: A) extends ADelegator(__0_proto) {
    def z: String
}

class BDelegator(__0_del: B) extends B(__0_del) {
    def z: String = __0_del.z
}

val a = new A {
    def x: Int = 0
    val y: Double = 1
}

val b = new B(a) {
    def z: String = " "
}
```
