# How To Compile SuperScript

[TOC]

This:

```SuperScript
// -> Main.ss
let ((int) => Integer) create = function(int x) (Integer) {
    return final {
        const int firstValue: x,
        let int value: x
    };
};
```

Would become:

```Java
// -> Main.java
import org.superscript.*;
import org.superscript.annotation.*;
import org.superscript.function.*;
@SuperScript
public class Main { // No SSObject as this is host class.
    private interface Integer /*extends SSObject*/{ //Also extends get<T>...
        @get(name = "firstValue", type = "I")
        public int getFirstValue();
        @get(name = "value", type = "I")
        public int getValue();
        @set(name = "value", type = "I")
        public void setValue(int value);
    }
    private class IntegerImpl implements Integer extends SSObject/*Base*/ {
        private final int firstValue;
        private int value;

        public int getFirstValue() {
            return firstValue;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public IntegerImpl(int firstValue, int value) {
            this.firstValue = firstValue;
            this.value = value;
        }
    }
    public static void main(String[] args) {
        IFunction<Integer> create = (x) -> {
            return new IntegerImpl(x, x);
        }
    }
}
```

V 2.0:

```Java
public class Main { // No SSObject as this is host class.
    @Superscript
    private abstract class Integer extends SSObject { //Also extends get<T>...
        @get(name = "firstValue", type = "I")
        public abstract int getFirstValue();
        @get(name = "value", type = "I")
        public abstract int getValue();
        @set(name = "value", type = "I")
        public abstract void setValue(int value);

        private Integer() {}
    }
    @Superscript
    private class IntegerImpl extends Integer {
        private final int firstValue;
        private int value;

        @Override
        public int getFirstValue() {
            return firstValue;
        }

        @Override
        public int getValue() {
            return value;
        }

        @Override
        public void setValue(int value) {
            this.value = value;
        }

        @Override
        public IntegerImpl(int firstValue, int value) {
            this.firstValue = firstValue;
            this.value = value;
        }
    }
    public static void main(String[] args) {
        IFunction<Integer> create = (x) -> {
            return new IntegerImpl(x, x);
        }
    }
}
```

V 3.0:

```Java
// -> Main.java
import org.superscript.*;
import org.superscript.annotation.*;
import org.superscript.function.*;
@SuperScript
public class Main { // No SSObject as this is host class.
    public interface Integer /*extends SSObject*/{ //Also extends get<T>...
        @get(name = "firstValue", type = "I")
        public int getFirstValue();
        @get(name = "value", type = "I")
        public int getValue();
        @set(name = "value", type = "I")
        public void setValue(int value);
    }
    private class IntegerImpl implements Integer extends SSObject/*Base*/ {
        private final int firstValue;
        private int value;

        public int getFirstValue() {
            return firstValue;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public IntegerImpl(int firstValue, int value) {
            this.firstValue = firstValue;
            this.value = value;
        }
    }
    public static void main(String[] args) {
        IFunction<Integer> create = (x) -> {
            return new IntegerImpl(x, x);
        }
    }
}
```

The code may be a little harder to read, due to distingushing from `SSObject` and other objects, such as dollar signs or underscores prefixing classes.

## Functions

Functions are simulated by having using currying. For example, the function

```SuperScript
function(int x, int y, int z) (int) ...;
```

becomes in Java:

```Java
(@Function ITFunction<ITFunction<IIFunction>>) (x) -> (y) -> (z) -> ...;
```

There is no function overloading, as that would be impossible in this circumstance.

Varargs are used using a `@Varargs` annotation at the right part, which takes in an `T[]` type.

There will also be an annotation to allow calling using key-value pairs instead: Update, this is `@CallByKey(String value)`.

For example, this:

```SuperScript
function(int :x, int :y, int :z) (int) ...;
```

would become this:

```Java
(@Function @CallByKey("x") ITFunction<@CallByKey("y") ITFunction<@CallByKey("z") IIFunction>>>) (x) -> (y) -> (z) -> ...;
```

There only needs to be 100 functions, one for each input type, one for each return type(Including `void` (Although void is a seperate type in my language)), as generics can be used for objects, and a function that takes in nothing, and returns something. [^1]

Additionally, there will be an `@Function` annotation, as actually curried functions would be indistingushable from non-curried functions. Functions will be prefixed with `@Function` for every return type that is a function.

For example,

```SuperScript
function(int x) (() => int) () => (int) x;
```

would turn into:

```Java
(@Function ITFunction<@Function VIFunction>) (x) -> () -> x;
```

## Properties

Properties are simulated having a `get` method, and a `set` method if it is not final.

These will be annotated with `@get(String name, String type)`, and `@set(String name, String type)`. The types will be encoded in standard JVM notation. This will probably make it easier for the compiler to figure out the getters and setters.

This is because having getters and setters means that variables do not have to be real, and can be replaced with a get method.

## Headers

Each class will have an header, using the annotation `@SuperScript`. All SuperScript objects will also inherit from `SuperScriptObject`, which would have default functionality for add, multiply, etc.

## Macros

Macros will be preprocessed by another processor, which will throw away everything and just return it un-modified except for macros, which would be put into a database, and then inserted into the grammar, and then the parser will be re-compiled, with an extra step that inserts the macros.

Macros must be defined at the header, and imported using macro imports, at the top of the file. They are global.

## Packages

There will be package declarations. [^2]

## Imports

There is no import static. If anybody wants import static, they should just set a local variable to that imported thing.

For example,

```SuperScript
@import{superscript:lang#class : Class}
const <> method = Class.method;
```

## Urls

Work in progress:

Ideas:

* `superscript.lang.class`
* `superscript:lang#class`
* `:superscript:lang#class`
* `superscript:lang:class`
* `superscript.lang#class`
* `#superscript:lang#class`
* `superscript#lang#class`
* `#superscript#lang:class`
* `\superscript:lang#class\`

This must currently not be valid syntax, as they need to be referenced, for example: `superscript:lang#create(x, y);`

While `superscript.lang` would not work as that would be treated as a sub-property.

Additionally, imports and objects use `:` as well.

Alternatively, `@import`s may be required.

The latest method, `:superscript:lang#class` means that colons will be detected, as an import would show a double colon, which can appear nowhere else in code. (Same for `#superscript.lang#class`)

```SuperScript
// -> Main.ss
type A = {
    let int number,
    const B prototype
};
(int) -> A getAObject = function(int a) (A) {
    B b = getBObject();
    b.doBFunction();
    b.number = 10;
    return final {
        let int number: a,
        const B prototype: b
    };
};
```

Goes to:

```Java
// -> Main.java
import org.superscript.function.*;
import org.superscript.*;

public class Main {

    public interface B extends SSObject {
        @get(name = "number", type = "I")
        public int getNumber();
        @get(name = "number", type = "I")
        public void setNumber(int value);
        @get(name = "doBFunction", type = "@Method Lorg/superscript/function/VVFunction")
        public VVFunction getDoBFunction();
    }

    public interface A extends B {
        public B getPrototype();
    }

    public class BImpl implements B extends SSObject {
        private int number;
        private VVFunction doBFunction;

        public int getNumber() {
            return number;
        }
        public void setNumber(int value) {
            this.number = value;
        }
        public VVFunction getDoBFunction() {
            return doBFunction;
        }
        public BImpl(int number, VVFunction doBFunction) {
            this.number = number;
            this.doBFunction = doBFunction;
        }
    }

    public class AImpl extends BImpl implements A extends SSObject {
        private final B prototype;

        public B getPrototype() {
            return prototype;
        }

        public AImpl(B prototype, int number) {
            super(b.getNumber(), b.getDoBFunction());
            setNumber(number);
            this.prototype = b;
        }
    }

    public static void main(String[] args) {
        ITFunction<A> getAObject = (ITFunction<A>) (a) -> {
            B b = getBObject.call();
            b.getDoBFunction().call();
            b.setNumber(10);
            return new AImpl(b, a);
        };
    }
}
```

* Prototypal Inheritance
* Generics
* Write constructors instead of types
* Operator overloading by turning into methods
* No method overloading except for operators because methods are lambdas
* Functions are objects that have a call method
* `int`s are not objects
* Functions are curried
* Bitmask enums

```SuperScript
let x = final {
    let foo: final {
        let int bar: 10,
        prototype: () => { print << "Hello World!"; void; }
    }
};
x.foo(); //Prints hello world!
x.foo.bar; //10
```

[^1]: Unless I replace all the return types with generics, and use empty functions, `()I`, etc.
[^2]: Unless somebody thinks packages should be categorized by folders only.
