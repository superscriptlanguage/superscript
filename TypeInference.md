---
title: "Type Inferencing"
author: Rouli Freeman
output:
    pdf_document:
        path: /output/TypeInference.pdf
        toc: true
        toc_depth: 3
        includes:
            in_header: header.tex
documentclass: report
export_on_save:
    pandoc: true
---

# Notation used

## Rules of inference

Rules of inference are presented in the following form:

$$\infer{
    Premise\ 1\\
    Premise\ 2\\
    \ldots
}{
    Conclusion\ 1\\
    Conclusion\ 2\\
    \ldots
}$$

and reads as: "If Premises 1 and 2 and so on are both true, then Conclusions 1 and 2 and so on are true".

## Standard symbols

$T, S, U, T_0, T_1, \ldots$ stand for types. $x, y, x_0, x_1, \ldots$ stand for variables, $e, e_0, e_1, \ldots$ stand for expressions, strings are represented by $s, s_0, s_1, \ldots$, and unique identifiers are represented by $u, u_0, u_1, \ldots$. Types always are an uppercase letter. Types belong to the universal set $U_T$. Variables are always a lowercase letter towards the end of the alphabet, belonging to $U_V$. Expressions belong to $U_E$, and are lowercase letters, towards the beginning of the alphabet. A string belongs to $U_S$. An identifier belongs to $U_U$, and is a lowercase letter at the middle of the alphabet. Each time one is made, $u \in U_U$, it means to generate an unique one. Generally,

$$T, S, U, T_0, T_1, \ldots \in U_T$$

$$x, y, x_0, x_1, \ldots \in U_V$$

$$e, e_0, e_1, \ldots \in U_E$$

$$s, s_0, s_1, \ldots \in U_S$$

$$u, u_0, u_1, \ldots \in U_U$$

$\Gamma$ is the enviroment. $\Gamma\p{x}$ means to get the type associated with the variable $x$, $\Gamma\b{s}$ means to get the type associated with the name $s$, $\Gamma\a{s}$ means to get the variable associated with the name $s$:

$$\infer{x \in U_V}{\Gamma\p{x} \in U_T}$$

$$\infer{s \in U_S}{\Gamma\b{s} \in U_T}$$

$$\infer{s \in U_S}{\Gamma\a{s} \in U_V}$$

{>> Version 2.0 <<}

$\Gamma$ is the variable environment. This correlates variables to types and whether it's constant or not. $\Gamma\p{x}$ returns an ordered pair, the first is whether it's constant, and the second is it's type.

$\Xi$ is the name type environment. This corrolates the names of types to the types. $\Xi\p{s}$ means to get the type associated with the name $s$.

$\Omega$ is the type environment. This is a ordered n-tuple of the set of all super-types.

The return environment is a set of all possible return types. It is represented by $\Delta$.

$self$, $this$, and $super$, are also environment variables. {>>Added #<<}

Only $\Gamma$, $\Xi$, $\Omega$, and $\Delta$ get changed.

The input context is a ordered pair of $\Gamma$, $\Xi$, $\Omega$, $\Delta$, $self$, $this$, $super$, and the output context is a ordered pair of $\Gamma$, $\Xi$, $\Omega$, $\Delta$:

$$\Phi = (\Gamma, \Xi, \Omega, \Delta, self, this, super)$$

$$\Psi = (\Gamma, \Xi, \Omega, \Delta)$$

## Operations on the enviorment

$\Gamma_1 = \Gamma\b{x_0:T_0, x_1:T_1, \ldots}$ means to create a new enviorment from $\Gamma$, with the added properties that $\Gamma_1\p{x_0}=T_0, \Gamma_1\p{x_1}=T_1, \ldots$. $\Xi_1 = \Xi\b{s_0:T_0, s_1:T_1, \ldots}$ means the same thing.

## Mathmatical operations

$\Delta\b{a, b, c, \ldots}$, where $\Delta$ is a set, means to create a new set with all the same things, and $a, b, c, \ldots$. $\Delta_0\a{\Delta_1}$ means to return a new set with both $\Delta_0$'s members and $\Delta_1$'s members. $\Omega\b{n:a, k:b, i:c, \ldots}$, where $\Omega$ is a ordered n-tuple means to create a new n-tuple, and set the $n$th number to be $a$, the $k$th number to be $b$, and so on. $\Omega_0\a{\Omega_1}$ means to override $\Omega_0$ to $\Omega_1$'s numbers.

## Operators

There are a couple of operators:

* $$(T \in U_T \geq S \in U_T) \in \mathbb{B}$$
  
  The $\geq$ operator compaires two types, and returns true if the type on the left, $T$ is a supertype of $S$, the type on the right. It is reflexive, transitive, and antisymmetric:

  $$\infer{}{T \geq T}$$

  $$\infer{
    T \geq S\\
    S \geq T
  }{
    T = S
  }$$

  $$\infer{
    T \geq S\\
    S \geq U
  }{
    T \geq U
  }$$

* $$(T \in U_T \leq S \in U_T) \in \mathbb{B}$$
  
  The $\leq$ operator is the opposite of the $\geq$ operator:
  $$\infer{T \geq S}{S \leq T}$$

* $$(T \in U_T > S \in U_T) \in \mathbb{B}$$
  
  The $>$ operator is true if and only if the left is not equal to the right, and the left is greater than the right, that is:

  $$\infer{
    T \geq S\\
    T \neq S
  }{
    T > S
  }$$
  {>>Probably don't need this.<<}

* $$(T \in U_T < S \in U_T) \in \mathbb{B}$$
  
  The $<$ operator is the opposite of the $>$ operator: {>>Probably don't need this.}

  $$\infer{T > S}{S < T}$$

* $$(T \in U_T \cup S \in U_T) \in U_T$$
  
  The union operator on two types gets the minimum type that is greater than or equal to both:

  $$\infer{
    T \geq T_0\\
    T \geq T_1\\
    S \geq T_0 \wedge S \geq T_1 \to S \geq T
  }{
    T = T_0 \cup T_1
  }$$

  More generally:

  $$\infer{
    \bigwedge_{i=0}^n T \geq T_i\\
    \bigwedge_{i=0}^n S \geq T_i \to S \geq T
  }{
    T=\bigcup_{i=0}^n T_i
  }$$

* $$(T \in U_T \cap S \in U_T) \in U_T$$
  
  The intersection operator on two types gets the maximum type that is less than or equal to both:

  $$\infer{
    T \leq T_0\\
    T \leq T_1\\
    S \leq T_0 \wedge S \leq T_1 \to S \leq T
  }{
    T = T_0 \cap T_1
  }$$

  More generally:

  $$\infer{
    \bigwedge_{i=0}^n T_i \geq T\\
    \bigwedge_{i=0}^n T_i \geq S \to S \leq T
  }{
    T=\bigcap_{i=0}^n T_i
  }$$

## Types

So far, types have not been defined. Types are defined as an ordered tuple of a unique identifier, whether it is final, and the supertypes of it:

$$U_T = \c{a \in U_U, b \in \mathbb{B}, c \subseteq U_T| \forall x \in c: x[1] = 0: (a, b, c)}$$

$$T \geq S \equiv T = S \wedge \exists U \in S[3] : T \geq U$$

$$\forall S \in T[3] : T \not \geq S$$

## Variables

Variables are stored as an ordered pair of a boolean whether it is final, and an unique identifier.

$$U_V = \c{a \in \c{``const", ``let"}, b \in U_U: (a, b)}$$

## Evaluation syntax

The syntax to say that an expression $e$ evaluates to type $T$ in context $\Phi$, and that the new modified context is $\Psi$ is:

$$\Phi \vdash e : T, \Psi$$

## Expressions

### The block expression

The block expression evaluates each sub-expression in order, passing the contexts from each expression to the next:

$$\infer{
    \Phi \vdash e_0 : T_0, \Psi_0\\
    \Phi\a{\Psi_1} \vdash e_1 : T_1, \Psi_1\\
    \Phi\a{\Psi_1} \vdash e_2 : T_2, \Psi_2\\
    \ldots\\
    \Phi\a{\Psi_{n-1}} \vdash e_n : T_n, \Psi_n
}{
    \Phi \vdash \c{e_0; e_1; e_2; \ldots e_n; } : T_n , \Phi
}$$

### The let expression

The let expression returns the evaluated expression, and adds that variable to the environment:

$$\infer{
    \Phi \vdash e : T , \Psi_0\\
    s \in \c{``const", ``let"}\\
    x \in U_V\\
    \Gamma = \Psi_0\p{``\Gamma"}\b{x : (s, T)}\\
    \Psi_1 = \Psi_0\b{``\Gamma": \Gamma}
}{
    \Phi \vdash s\ T\ x = e : T , \Psi_1
}$$

### The access expression

The access expression accesses a variable:

$$\infer{}{
    \Phi \vdash x : \Phi\p{``\Gamma"}\p{x}\p{1} , \Phi\b{:3}
}$$

### Other expressions

$$\infer{
    \Phi \vdash e : T , \Psi
}{
    \Phi \vdash (e) : T , \Psi
}$$

$$\infer{
    I \in \c{\text{Infix Operators}}\\
    \Phi \vdash e_0[``[:"+I+``:]"](e_1) : T, \Psi
}{
    \Phi \vdash e_0\ I\ e_1 : T, \Psi
}$$

$$\infer{
    P \in \c{\text{Prefix Operators}}\\
    \Phi \vdash e[``["+P+``:]"]() : T, \Psi
}{
    \Phi \vdash P\ e : T, \Psi
}$$

$$\infer{
    P \in \c{\text{Postfix Operators}}\\
    \Phi \vdash e[``[:"+P+``]"]() : T, \Psi
}{
    \Phi \vdash e\ P : T, \Psi
}$$

$$\infer{
    \Phi \vdash e : T , \Psi\\
}{
    \Phi \vdash typeof\ e : Class\a{T} , \Psi
}$$

$$\infer{
    \Phi \vdash e_0 : T , \Psi_0\\
    \Phi\a{\Psi_0} \vdash e_1(e_0) : S , \Psi_1
}{
    \Gamma \vdash e_0 |> e_1 : S , \Psi_1
}$$

$$\infer{
    \Phi \vdash e_0 : T , \Psi_0\\
    \Phi\a{\Psi_0}\b{``self" : T} \vdash e_1 : S , \Psi_1\\
}{
    \Phi \vdash e_0 >>== e_1 : T , \Psi_1
}$$

$$\infer{
    \Phi \vdash e : S , \Psi
}{
    \Phi \vdash (T)\ e : T , \Psi
}$$

$$\infer{
    \Phi \vdash e : T , \Psi_0\\
    \Delta = \Psi_0\p{4}\b{T}\\
    \Psi_1 = \Psi_0\b{4 : \Delta}\\
    s \in \c{``return", ``yield"}
}{
    \Phi \vdash s\ e : T, \Psi_1
}$$

$$\infer{
    I \in \c{\text{Infix Operators}}\\
    \Phi \vdash e_0 = e_0\ I\ e_1 : T, \Psi
}{
    \Phi \vdash e_0\ I= e_1 : T, \Psi
}$$

```SuperScript
declare const int b, d;
{
    a:b,
    c:d
} = final {
    let int a : 15,
    let int c : 3,
    let int q : 4
};
//b gets 15
//d gets 3

b = 1; //Error

let Foo z = final {
    let int q : 12,
    let int a : 1
};
z.a += 9; //10

3 + 4 = 19; // Type error. Wish it were a parse error.

(a, b, c) = getABC(); // Ambiguous, but the parser will treat as destructuring.

(a, b, c) + (4, "x"); //(a, b, c, 4, "x")

z.a = 12;
z.a + z.a;

(z.a, b, c) = (4, "x", w);

(z.a, b, c=(u, v)) = (4, "x", (r, s));
z.a == 4; //True
b == "x"; //True
c == (r, s); //True
u == r; //True
v == s; //True
```
