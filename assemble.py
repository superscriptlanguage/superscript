import Krakatau.Krakatau
from Krakatau.Krakatau.assembler import parse
from Krakatau.Krakatau import script_util

import sys
import time


def assembleSource(source, basename, single, fatal=False):
    source = source.replace('\t', '  ') + '\n'
    if single:
        source = source.replace('"', "'")
    return list(parse.assemble(source, basename, fatal=fatal))


if __name__ == "__main__":
    l = sys.argv
    if l[1] == '-h':
        print("Usage: 'assemble out log filename quotes input'")
        print("\tout: The output directory")
        print("\tlog: '1' -> only errors and warnings")
        print("\tfilename: the file this was generated from")
        print("\tquotes: '1' -> using single quotes, replace double with single quotes")
        print("\tinput: the inputed thing")
        sys.exit()
    out = l[1]  # output dir
    q = l[2] == "1"  # errors
    f = l[3]  # filename
    b = l[4] == "1"  # using single or double quotes (1 for single)
    input = l[5]  # source
    print(out, q, input)
    log = script_util.Logger('warning' if q else 'info')
    log.info(script_util.copyright)

    out = script_util.makeWriter(out, '.class')

    start_time = time.time()
    with out:

        pairs = assembleSource(input, f, b)
        for name, data in pairs:
            filename = out.write(name, data)
            log.info('Class written to', filename)
    print('Total time', time.time() - start_time)
