parser grammar SuperScriptV2Parser;
options {
	tokenVocab = SuperScriptV2Lexer;
}
file: block EOF;

block: (statement? ';') statement?;

statement: expression;

expression:
	NAME												# access
	| lit												# literal
	| '(' expression ')'								# parens
	| 'typeof' expression								# typeof
	| expression NAME expression						# infix
	| expression NAME									# postfix
	| NAME expression									# prefix
	| expression '(' (applyarg (',' applyarg)*)? ')'	# invocation
	| value = expression (
		'.' NAME
		| '[' refinement = expression ']'
	)							# refinement
	| '(' type ')' expression	# cast
	| var '=' expression		# varset
	| expression '=' expression	# set
	| 'declare' id (',' value)*	# declare
	| id '=' first = expression (
		',' values += value '=' further += expression
	)*																		# valueset
	| 'match' '(' expression ')' '{' match (';' match)* '}'					# match
	| '{' block '}'															# blocks
	| 'declare'? 'final'? 'new' NAME '<' url (',' url)* '>' '{' block '}'	# object
	| 'with' '(' expression ')' expression									# with
	| 'try' ('(' expression ')')? attempt = expression (
		'catch' '{' match (';' match)* '}'
	)* ('finally' end = expression)?									# try
	| 'for' '(' expression ';' expression ';' expression ')' expression	# for
	| 'for' '(' expression '<-' expression ')' 'collect'? expression	# foreach;

id: 'var' valued | 'val' valued | 'val' '=>' valued;

valued: value | value typed;

typed: ':' type;

lit:
	arr				# array
	| tup			# tuple
	| templatestr	# templateString
	| numb			# number
	| STRING		# string
	| CHAR			# char
	| 'true'		# true
	| 'false'		# false
	| 'NaN'			# nan
	| 'this'		# this
	| 'super'		# super
	| 'self'		# self
	| 'void'		# void;

arr:
	type '[' arrparam (',' arrparam)* ']'	# arrvalues
	| type '[' '[' expression ']' ']'		# arrlength;
arrparam: expression | 'declare';

tup: '(' tupparam? (',' tupparam?)* ')';
tupparam: expression | 'declare' type;

templatestr:
	TEMPLATESTART (
		CHUNK
		| STRINGINTERP expression RCURLY
		| CHARACTER
	)*? TEMPLATEEND;

numb: NUMBER;

match: (cases | '(' cases ')') '->' expression;

cases: expression | var | '_';

applyarg: (NAME '=')? '...'? expression;

type:
	type '|' type		# ortype
	| type '&' type		# andtype
	| type NAME type	# infix
	| type gentype		# generic;

name:
	LNAME0
	| LNAME1
	| LNAME2
	| LNAME3
	| LNAME4
	| LNAME5
	| LNAME6
	| LNAME7
	| LNAME8
	| LNAME9
	| RNAME0
	| RNAME1
	| RNAME2
	| RNAME3
	| RNAME4
	| RNAME5
	| RNAME6
	| RNAME7
	| RNAME8
	| RNAME9;