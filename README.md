# SuperScript

[TOC]

## Goals

* Types
* Prototypal Inheritance
* First order functions
* Easy threads
* Everything is an expression
* No nulls

## Syntax

The syntax is mostly like JavaScript, but there are a few deviations.

### Declare

A variable may be kept undefined using the `declare` keyword.

There are some restrictions:

* A object with a `declare`d property may not be `return`ed.
* A declared property or variable may not be accessed without it being known to be valid.
* It may not be set to be a value of an incorrect type.
* An object with a declared property may not be passed into a function.

### Types

Variables have types.

To declare a variable, use

```SuperScript
let Type variable = value;
```

To prevent the variable from changing, use `const`:

```SuperScript
const Type variable = value;
```

Then, trying to set the variable, there will be a compile-time error.

You may also override variables:

For example:

```SuperScript
let Type variable = value;

const OtherType variable = otherValue;
```

This changes the variable, and makes it's type be `OtherType`. Things that were accessing variable before will still be treating it as if it was `value` instead of `otherValue`.

#### Casting

Types may be cast to other types if the author of the code knows that a value is a certain type but the compiler does not, or if the author knows that if it is not that type his code will fail.

The syntax is `(Type) value`.

Additionally, casting may be overloaded using the `[(Type)]` property of the object, which is a function that returns a thing of type `Type`.

### Block scope

Variables have block scope.

This means that a variable in a block can not be accessed outside of a block.

For example, this throws an error:

```SuperScript
{
    let int x = 1;
};
console.log(x); //Error
```

Additionally, variables may be shadowed:

```SuperScript
let Integer x = 1;
let any y = {
    let Integer value : x
}; //y contains a refrence to x.
let any x = {}; //x has been shadowed.
let any z = {
    let any value : x
}; //z accesses the new x, not the old x.
```

### Operators

The operators of SuperScript are similar to Java, with one extra operator `===` which does pointer comparison, while `==` can be overriden.

#### Chaining

The `>>==` operator takes in two arguments, `x` and `y`, in this pattern: `x >>== y`. This returns `x`, and evaluates `x.y`. As such, this can be used to chain functions:

```SuperScript
x >>== y() >>== z();
```

means the same as

```SuperScript
{ x.y(); x.z(); x; };
```

but is more convienent to write.

#### Operator overloading

You may overload any operator except:

* `>>==`
* `===`
* `!`, `&&`, `||`
* `?:`
* `+=`, etc
* `typeof`
* `instanceof`
* `has`

The operators `>=`, `<=`, `>`, and `<` are allowed.

To overload an infix operator `infixop` for a variable `x`, set the property `[:infixop:]` of the variable. This will be a single value function which takes in the value `y` when `x infixop y` is ran.

To overload a prefix operator `prefixop` for a variable `x`, set the property `[prefixop:]` of the variable. This is a no-value function that gets called when `prefixop x` is ran.

To overload a postfix operator `postfixop` for a variable `x`, set the property `[:postfixop]` of the variable. This is a no-value function that gets called when `x postfixop` is ran.

For example: `[:+:]` in `x` would be called with the single argument `y` when `x + y` is evaluated.

### Match expressions

Instead of a switch expression, as most languages have, there is a `match` expression.

This improves a couple of things, including there is no fallthrough.

The syntax is:

```SuperScript
match(variable) {
    x -> {
        //Expressions
    }
    (y, z) -> {
        //Expressions
    }
    w, default -> {
        //Expressions
    }
};
```

If `variable` is equal(`==`) to `x`, then it would execute the block expression following after.

You may use destructuring to match the variable:

```SuperScript
match(variable) {
    [x, ...y] -> {
        //Expressions
    }
};
```

In this case, if variable was an array of length 1 or more, then it would match, and the block would have the two variables `x` and `y`, `x` would be `variable[0]` and `y` would be the rest of it.

### Everything is an expression

Everything is an expression.

For examples, it is possible to set a variable to be the result of a block, or an if else statement:

```SuperScript
let int x = {
    let int y = 1;
    let int z = 2;
    y + z;
}; //3
```

If there is a difficulty, the expression may be cast to another type.

```SuperScript
let int x = (int) {
    let double y = 1.0;
    let double z = 2.0;
    y + z; //3.0
}; //3
```

### Mandatory semicolons

In JavaScript, semicolons get automatically inserted.

This can have some problems, such as:

```JavaScript
return //;
{
    value: 1
};
```

Which actually returns nothing, as a semicolon has been inserted, and then creates an object with a paramater called value that is 1.

So, semicolons are manditory.

This is even true for if expressions. For example, this would fail:

```SuperScript
if(someBoolean) {
    //Do stuff
}
```

as it needs a semicolon at the end.

### Constant object fields

Objects must specify if the property is constant.

For example, this code is correct:

```SuperScript
let Type variable = {
    let int x = 1
};
```

while this code is not:

```SuperScript
let Type variable = {
    int x = 1
};
```

it is missing a `let`/`const`.

Objects must also specify the types of their properties.

Additionally, objects may be `final`, which prevents extra properties to be added.

For example, this would throw an error:

```SuperScript
let Type variable = final {
    let int x = 1
};

variable.y = 2; //Error
```

## Built in libraries

### Object

#### Description

All objects inherit from `Object.prototype`. `Object` also has many utilities relating to Objects.

#### Global object

##### ToString

`Object.toString(any value)`

Parameters:

* `value` is the value to be stringified.

Returns a string which is the default `toString` method of that value.

#### Object properties

##### ToString

`object.toString()`

Returns a string which looks like this: `"[object Type]"`.

### Function

#### Description

All `function` objects inherit from `Function.prototype`. `Function` also has many utilities relating to Functions.

#### Global object

##### Nullary

Spec:

`Function.Nullary = type autonomous () => void`

A function that follows `Function.Nullary` takes in no values, and returns `void`.

##### Unary\<T>

Spec:

`Function.Unary = type autonomous (T) => void <T>`

A function that follows `Function.Unary` takes in one value of type `T`, and returns `void`.

##### Binary\<T, S>

Spec:

`Function.Binary = type autonomous (T, S) => void <T, S>`

A function that follows `Function.Binary` takes in two values, one of type `T` and one of type `S`, and returns `void`.

#### Object properties

### Lock

#### Description

Usually used with [Threads](#threads), for thread saftey.

#### Global object

##### Create

`Lock.create<T>(T [value])`

Parameters:

* `value` is the value to be stored in the `locked` object.

##### Lock

`Lock.lock<T>(T [value])`

Parameters:

* `value` is the value to be stored in the [`lock`](#lock-object-properties) object.

Returns a [`lock`](#lock-object-properties) object storing value `value`.

#### Object properties

##### Lock

`locked.lock()`

Returns a promise that will be resolved with the return value the [`lock`](#lock-object-properties) object that has the value `T`.

##### Require

`locked.require()`

Immediately gets the `lock` object, and throws an error if it can not be gotten.

#### Lock object properties

##### Release

`lock.release()`

Releases the lock, and allows other things to attempt to access it.

##### Set

`lock.set(T [value])`

Parameters:

* `value` is the value to set to.
Sets the value contained inside of the `lock` to `value`.

##### Get

`lock.get()`

Returns the value contained inside of the `lock`.

### Threads

#### Description

Tools for managing and creating `Thread`s.

#### Global object

##### Create

`Threads.create(Function.Nullary [executor])`

Parameters:

* `executor` is the thing to be executed in a seperated thread.

Returns a thread that represents this `executor` running in a seperate thread. Does not execute `executor`, but waits for `thread.run()` to be called.

##### Execute

`Threads.execute(Function.Nullary [executor])`

Parameters:

* `executor` is the thing to be executed in a seperated thread.

Returns a thread that represents this `executor` running in a seperate thread. It is different than [Create](#create) as it executes the thread instead of waiting for `thread.run()` to be called.

#### Object properties

##### Halt

`thread.halt()`

Makes the thread stop running. Used with [Continue](#continue).

##### Terminate

`thread.terminate()`

Stops the thread completely.

##### Run

`thread.run()`

Causes the thread to run from the beginning. Sometimes used with [Terminate](#terminate).

##### Continue

`thread.continue()`

Makes the thread continue running. Used with [Halt](#halt).

##### Sleep

`thread.sleep(long [mills])`

Parameters:

* `millis` tells how long the thread should sleep for in milliseconds.

Makes the thread sleep for `millis` long in milliseconds.

### Thread

#### Description

The current thread object.

#### Global object

See [Threads](#threads)

### Promise

#### Description

Utilities to create promises, and create other promises from those promises.

#### Global object

##### All

`Promise.all<T>(...Promise<...T> [promises])`

Parameters:

* `promises` is a list of `Promise`s.

Creates a promise that return value is a tuple of all the return values of the promises inside of `promises`, and gets settled when the last promise gets settled.

##### Race

`Promise.race<T>(...Promise<...T> [promises])`

Parameters:

* `promises` is a list of `Promise`s.

Creates a promise that return value is a tuple. The first element of the tuple is the return value of the first promise to be settled, and the second element is the index of that promise. The promise gets settled when any one of the promises are settled.

##### Create

`Promise.create<T>(Function.Binary<Function.Unary<T>, Function.Unary<Throwable>> [executor])`

Parameters:

* `executor` is a function that takes in two arguments: `resolve` and `reject`. `resolve` takes in the output value. Reject takes in a `Throwable` as an error value.

Creates a `Promise<T>`.

##### Resolve

`Promise.resolve<T>(T [value])`

Parameters:

* `value` is the value to be resolved to.

Returns a `Promise<T>` that has been resolved with return value `value`.

#### Object properties

##### Then

`promise.then(Function.Unary<T> [executor])`

Parameters:

* `executor` is a function that is called when the `promise` is `resolve`d, with the output value passed in as the first parameter.

Adds `executor` as a listener to the `promise` being `resolve`d.

##### Catch

`promise.catch(Function.Unary<Throwable> [executor])`

Parameters:

* `executor` is a function that is called when the `promise` is `reject`ed, with the error value passed in as the first parameter.

Adds `executor` as a listener to the `promise` being `reject`ed.

##### Finally

`promise.finally(Function.Unary<Throwable | T> [executor])`

Parameters:

* `executor` is a function that is called when the `promise` is settled, whether by getting `resolve`d or `reject`ed, with the output/error value passed in as the first parameter.

Adds `executor` to listen to both it being `resolve`d and `reject`ed.

##### State

`promise.state`

This may either be `"pending"`, `"fulfilled"`, or `"rejected"`. It is pending if it has not been `resolve`d or `reject`ed yet. It is fulfilled if it has been `resolve`d and it is rejected if it has been `reject`ed.

#### Functionality

* The keyword `await` waits for a promise to finish, and then outputs the output value, or if there is an error value, `throw`s that error.
* The keyword `async` on a function makes that function to return a promise. When the function returns a value, it may be attached to using `promise.then`, or if it throws an error, use `promise.catch`.

### Event

#### Description

`Event<T>`

An `autonomous` type that represents an event that can be thrown by an [`EventTarget`](#eventtarget).

#### Event properties

##### Value

`event.value`

This is a value of type `T`.

##### Type

`event.type`

The type of the event. Is a string.

### EventTarget

#### Description

An object for manipulating and creating `EventTarget<T>`s.

#### Global object

##### Create

`EventTarget.create<T>()`

Returns a tuple, the first element is the `EventTarget` which should be put into the prototype chain, and the second element is a object with one property: `dispatchEvent(String [type], T [value])`, which is a function that is called with a value of type T and a String telling which type it is. This alerts all event listeners listening to that type.

#### Object properties

##### AddEventListener

`target.addEventListener(String [type], Function.Unary<Event<T>> [listener])`

Parameters:

* `type` is a String which tells which type to listen to.
* `listener` is a function that gets called with an event when the `target` dispatches an event.

Adds listener to listen on events of type `type`.

##### RemoveEventListener

`target.removeEventListener(String [type], Function.Unary<Event<T>> [listener])`

Parameters:

* `type` is the type of the event handler to remove.
* `listener` is the handler to remove.

Removes `listener` from listening on type `type`.

##### RemoveEventListeners

`target.removeEventListeners(String [type])`

Parameters:

* `type` is the type to remove all event listeners from.

Removes all event listeners of type `type`.

##### RemoveEventListeners

`target.removeEventListeners()`

Removes all event listeners.

## Notes

* The extension is `.srsc`.
* To have a block without any expressions, use `{ ; }` as `{}` will be treated as an object, although it is rercommended to use `void` instead.
* To overload a function, create a `final` empty object, and make the functions in its prototype chain:

        let any x = final {
            const <> prototypes = (
                function() (int) {
                    return 0;
                },
                function<T>(T t) (T) {
                    return t;
                }
            )
        };
        x(); //0
        x(10); //10
* It is recommended to use an `async` function for threads, instead of using `locked.require()`, use `await locked.lock()`