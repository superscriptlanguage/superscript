grammar SuperScript;
tokens {
	INDENT,
	DEDENT
}

@lexer::header {
import java.util.Stack;
import java.util.LinkedList;
}

@lexer::members {
    private int opening = 0;
    private Stack<Integer> indents = new Stack<Integer>();

  	private Token lastToken = null;
	private LinkedList<Token> tokens = new LinkedList<>();
    @Override
    public void emit(Token t) {
        super.setToken(t);
        tokens.offer(t);
    }

    @Override
    public Token nextToken() {
        if (_input.LA(1) == EOF && !this.indents.isEmpty()) {
            // Remove any trailing EOF tokens from our buffer.
            for (int i = tokens.size() - 1; i >= 0; i--) {
                if (tokens.get(i).getType() == EOF) {
                    tokens.remove(i);
                }
            }

            this.emit(commonToken(SuperScriptParser.NEWLINE, "\n"));

            while (!indents.isEmpty()) {
                dedent();
                indents.pop();
            }

            this.emit(commonToken(SuperScriptParser.EOF, "<EOF>"));
        }

        Token next = super.nextToken();

        if (next.getChannel() == Token.DEFAULT_CHANNEL) {
            this.lastToken = next;
        }

        return tokens.isEmpty() ? next : tokens.poll();
    }

    private void dedent() {
        CommonToken dedent = commonToken(SuperScriptParser.DEDENT, "\n");
        dedent.setLine(this.lastToken.getLine());
        emit(dedent);
        emit(commonToken(NEWLINE, "\n"));
    }

    private CommonToken commonToken(int type, String text) {
        int stop = this.getCharIndex() - 1;
        int start = text.isEmpty() ? stop : stop - text.length() + 1;
        return new CommonToken(this._tokenFactorySourcePair, type, DEFAULT_TOKEN_CHANNEL, start, stop);
    }

    static int getIndentationCount(String spaces) {
        int count = 0;
        for (char ch : spaces.toCharArray()) {
            switch (ch) {
                case '\t':
                    count += 4;
                    break;
                default:
                    count++;
            }
        }

        return count;
    }
}

file: NEWLINE* statement* EOF;

statement: stmt NEWLINE+;

stmt:
	expression		# exprstmt
	| definition	# defstmt
	| imp			# impstmt
	| pkg			# pkgstmt;

imp:
	'import' imppath name								# nameimp
	| 'import' imppath '_'								# anyimp
	| 'import' imppath '{' imptype (',' imptype)* '}'	# typeimp;

imppath: name ('.' name)* '.';

imptype:
	'_'					# allimptype
	| 'given'           # givenimptype
	| name				# nameimptype
	| name '=>' '_'		# ignoreimptype
	| name '=>' name	# renameimptype;

pkg: 'package' name ('.' name)*;

appliable:
	name ':' expression	# nameappliable
	| expression		# exprappliable;

expressioneof: expression EOF;

expression:
	block														# blkexp
	| '(' expression ')'										# parensexp
	| name														# identexp
	| expression '.' infixname									# selectexp
	| 'this'													# thisexp
	| 'super'													# superexp
	| expression ':' t											# typedexp
	| 'with' '(' expression ')' expression						# w
	| 'with' expression 'do' expression							# w
	| 'if' '(' expression ')' expression ('else' expression)?	# if
	| 'if' expression 'then' expression ('else' expression)?	# if
	| expression '(' (expression (',' expression)*)? ')'		# apply
	| expression '(' 'given' expression (',' expression)* ')'	# gapply
	| 'new' t ('from' expression)? block?						# createexp
	| mat														# match
	| 'try' expression 'catch' mat ('finally' expression)?		# try
	| expression calltypeargs									# tapply
	| goptionalargs '=>' expression								# functionexp
	| expression infixname expression							# infixexp
	| STRING													# strexp
	| NUM														# numexp;

block: NEWLINE INDENT NEWLINE* statement+ DEDENT NEWLINE*;

mat:
	NEWLINE INDENT 'case' pattern '=>' expression (
		NEWLINE 'case' pattern '=>' expression
	)* NEWLINE DEDENT NEWLINE*;

pattern:
	name											# valuepat
	| pattern '@' pattern							# bindpat
	| expression '(' (pattern (',' pattern)*)? ')'	# unapplypat
	| pattern '|' pattern							# altpat
	| pattern ':' t									# typepat
	| '_'											# wildpat;

definition:
	'val' infixname (':' t)? ('=' expression)?						# valdef
	| 'var' infixname (':' t)? ('=' expression)?					# vardef
	| 'type' name typebounds ('=' t)?								# typedef
	| 'def' infixname typeargs? gargs+ (':' t)? ('=' expression)?	# methoddef
	| 'def' name typeargs? (':' t)? ('=' expression)?				# methoddef
	| 'given' infixname? 'as' t '=' expression						# implicitdef
	| 'class' name varianttypeargs? (
		'extends' urlwitharg ('with' urlwitharg)*
	)? block? # classdef
	| 'trait' name varianttypeargs? (
		'extends' urlwitharg ('with' urlwitharg)*
	)? block? # traitdef
	| 'object' name ('extends' urlwitharg ('with' urlwitharg)*)? (
		'from' expression
	)? block?					# objectdef
	| 'override' definition		# overdef
	| 'private' definition		# privatedef
	| 'protected' definition	# protecteddef;

urlwitharg: name ('.' name)* calltypeargs?;

t:
	name ('.' name)*			# namedt
	| expression '/' name		# selectt
	| t '|' t					# ort
	| t '&' t					# andt
	| t calltypeargs			# appliedt
	| varianttypeargs '=>>' t	# lambdat
	| expression '.' 'type'		# singlet
	| '=>' t					# bynamet
	| t 'match' tm				# matcht;

tm:
	NEWLINE INDENT 'case' tp '=>' t (NEWLINE 'case' tp '=>' t)* NEWLINE DEDENT NEWLINE;

tp:
	t '[' tp (',' tp)* ']'	# extracttp
	| '_'					# tpwildcard
	| t						# valuetp;

typebounds: boundtype*;

boundtype:
	'<:' t		# subclassboundtype
	| '>:' t	# superclassboundtype
	| ':' t		# existsimplicitboundtype;

calltypeargs: '[' t (',' t)* ']';

typeargs: '[' (t typebounds) (',' (t typebounds))* ']';

varianttypeargs:
	'[' (variance t typebounds) (',' (variance t typebounds))* ']';

variance:
	'+'		# covariance
	|		# invariance
	| '-'	# contravariance;

goptionalargs:
	'given' optionalargs	# givenoptionalargs
	| optionalargs			# normaloptionalargs;

optionalargs: '(' (oarg (',' oarg)*)? ')';

oarg: name (':' t)?;

gargs: 'given' args # givenargs | args # normalargs;

args: '(' (arg (',' arg)*)? ')';

arg: name ':' t;

infixname:
	NAME
	| '+'
	| '-'
	| '/'
	| '|'
	| '&'
	| 'match'
	| STARTNAMEINFIX;

name: NAME;

STRING: '"' .*? '"';

LCURLY: '{' -> pushMode(DEFAULT_MODE);
RCURLY: '}' -> popMode;
LPAREN: '('; //{opening++;};
RPAREN: ')'; //{opening--;};
LBRACK: '[';
RBRACK: ']';

NAME: ALPHA ALPHANUM* | '`' (~'`' | '\\' .)+ '`';

STARTNAMEINFIX: SPECIALCHARACTER (SPECIALCHARACTER | ALPHANUM)*;

WS: ([ \t]+ | '\\' WS NL | '#' ~[\r\n\f]* | {opening > 0}? NL) -> skip;

NEWLINE:
	NL [ \t]* {
	String spaces = getText().replaceAll("[\r\n\f]+", "");
	int next = _input.LA(1);
	if(/*opening > 0 ||*/ next == '\r' || next == '\n' || next == '\f' || next == '#') {
        skip();
	} else {
        emit(commonToken(NEWLINE, "\n"));
		int indent = getIndentationCount(spaces);
		int previous = indents.isEmpty() ? 0 : indents.peek();
		if(indent != previous) {
			if(indent > previous) {
				indents.push(indent);
				emit(commonToken(SuperScriptParser.INDENT, spaces));
			} else if(indent < previous) {
				while(!indents.isEmpty() && indents.peek() > indent) {
                    dedent();
					indents.pop();
				}
			}
		}
	}
};

NUM: [0-9]+ ('.' [0-9]*)?;

fragment NL: ('\r'? '\n' | '\r' | '\f');

fragment ALPHA: [a-zA-Z_$];

fragment ALPHANUM: [a-zA-Z_$0-9];

fragment SPECIALCHARACTER: [*/%+\-:=!<>&^|];